﻿using Nop.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService
{
    public partial class UploadFileWatcher : ServiceBase
    {
        public UploadFileWatcher()
        {
            InitializeComponent();

            this.ServiceName = "Oldham Chemical Upload File Watcher";

            Helpers.ElmahWrapper.LogToElmah(new Exception(this.ServiceName + " Started"));
        }

        protected override void OnStart(string[] args)
        {
            FileWatchers.CustomerFileWatcher CustomerFileWatcher = new FileWatchers.CustomerFileWatcher();

            //FileWatchers.ManufacturerFileWatcher ManufacturerFileWatcher = new FileWatchers.ManufacturerFileWatcher();

            FileWatchers.OrderFileWatcher OrderFileWatcher = new FileWatchers.OrderFileWatcher();

            FileWatchers.ProductFileWatcher ProductFileWatcher = new FileWatchers.ProductFileWatcher();
        }

        protected override void OnStop()
        {
            Helpers.ElmahWrapper.LogToElmah(new Exception(this.ServiceName + " Stopped"));
        }        
    }
}
