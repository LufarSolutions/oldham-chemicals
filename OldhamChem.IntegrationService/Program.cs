﻿using Nop.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {

            ServiceBase[] ServicesToRun;
                       

            ServicesToRun = new ServiceBase[]
            {
                new UploadFileWatcher()
            };


            ServiceBase.Run(ServicesToRun);
        }        
    }
}
