﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.Models
{
    public class OrderItem
    {
        public string IsTaxable { get; set; }

        public string InvoiceNumber { get; set; }
        public string ItemNumber { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Total { get; set; }
    }
}
