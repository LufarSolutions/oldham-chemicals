﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.Models
{
    public class Customer
    {
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public Address Address { get; set; }
        public string PhoneNumber { get; set; }
        public int PriceLevel { get; set; }
        //public decimal CurrentBalance { get; set; }
        //public decimal BalanceIn30 { get; set; }
        //public decimal BalanceIn60 { get; set; }
        //public decimal BalanceIn90 { get; set; }
        //public decimal BalanceIn120 { get; set; }
    }
}
