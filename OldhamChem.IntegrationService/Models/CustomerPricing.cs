﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.Models
{
    public class CustomerPricing
    {
        public string CustomerNumber { get; set; }
        public string ItemNumber { get; set; }
        public decimal ContractPrice { get; set; }
    }
}
