﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.Models
{
    public class Order
    {
        public string CustomerNumber { get; set; }

        public decimal FreightCharge { get; set; }

        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string OrderNumber { get; set; }
        public string Terms { get; set; }
        public decimal InvoiceBalance { get; set; }

        public decimal SalesTax { get; set; }

        public string WarehouseCode { get; set; }
    }
}
