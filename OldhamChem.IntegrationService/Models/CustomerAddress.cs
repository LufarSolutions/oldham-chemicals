﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.Models
{
    public class CustomerAddress
    {
        public string CustomerNumber { get; set; }
        public string CustomerEmail { get; set; }
        public string Company { get; set; }
        public Address Address { get; set; }
    }
}
