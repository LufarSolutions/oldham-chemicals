﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.Models
{
    public class Product
    {
        public string Category { get; set; }

        public string Discontinued { get; set; }

        public string ItemDescription { get; set; }

        public string ItemNumber { get; set; }

        public string ManufacturerNumber { get; set; }

        public string ManufacturerPartNumber { get; set; }

        public int NumberPerUnitOfMeasure { get; set; }

        public decimal PriceLevel1 { get; set; }

        public decimal PriceLevel2 { get; set; }

        public decimal PriceLevel3 { get; set; }

        public decimal PriceLevel4 { get; set; }

        public string SubCategory { get; set; }

        public string UnitOfMeasure { get; set; }
    }
}
