﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.FileWatchers
{
    public class CustomerFileWatcher : BaseFileWatcher<Models.Customer>
    {
        // NopCommerce Services
        private IEngine NopEngineContext;
        private ICustomerRegistrationService CustomerRegistrationService;
        private ICustomerService CustomerService;
        private CustomerSettings CustomerSettings;
        private IAddressService AddressService;
        private IGenericAttributeService GenericAttributeService;
        private IProductService ProductService;
        private IStateProvinceService StateService;
        private ISettingService SettingService;

        // Get roles for tiered pricing
        Nop.Core.Domain.Customers.CustomerRole PriceLevel1Role;
        Nop.Core.Domain.Customers.CustomerRole PriceLevel2Role;
        Nop.Core.Domain.Customers.CustomerRole PriceLevel3Role;
        Nop.Core.Domain.Customers.CustomerRole PriceLevel4Role;
        Nop.Core.Domain.Customers.CustomerRole RegisteredRole;

        public CustomerFileWatcher()
        {
            this.Filter = System.Configuration.ConfigurationManager.AppSettings["CustomerFileName"];

            this.Created += CustomerFileWatcher_Created;
        }

        public void ArchiveCustomerFiles(List<string> filenamesAndPaths)
        {
            string ArchiveDirectory = System.Configuration.ConfigurationManager.AppSettings["BaseArchiveDirectory"] + "\\" + System.Configuration.ConfigurationManager.AppSettings["CustomerDirectory"];

            foreach (var filenameAndPath in filenamesAndPaths)
            {
                ArchiveFile(filenameAndPath, ArchiveDirectory);
            }
        }

        private void CustomerFileWatcher_Created(object sender, FileSystemEventArgs e)
        {
            InitializeServices();

            // Read in the customer file
            List<Models.Customer> Customers = ReadCsvFile(e.FullPath);
            List<Models.CustomerPricing> CustomerPricing;
            List<Models.CustomerAddress> CustomerAddresses;

            List<string> FilenamesAndPaths = new List<string>();
            FilenamesAndPaths.Add(e.FullPath);

            string CustomerPricingFilename = System.Configuration.ConfigurationManager.AppSettings["CustomerPricingFileName"];
            string CustomerPricingFilenameAndPath = this.Path + "\\" + CustomerPricingFilename;

            if (System.IO.File.Exists(CustomerPricingFilenameAndPath))
                FilenamesAndPaths.Add(CustomerPricingFilenameAndPath);
            
            string CustomerAddressFilename = System.Configuration.ConfigurationManager.AppSettings["CustomerAddressFileName"];
            string CustomerAddressFilenameAndPath = this.Path + "\\" + CustomerAddressFilename;

            if (System.IO.File.Exists(CustomerAddressFilenameAndPath))
                FilenamesAndPaths.Add(CustomerAddressFilenameAndPath);

            // If customers exist in the file, process the customers
            if (Customers != null && Customers.Any())
            {
                // 10/26/2016 (BLEWIS) - Commented out due to this overwritting updates made on the website.
                //ProcessCustomers(Customers);                
            }

            // Read in the customer pricing file
            // If customer pricings exist, process the file
            //if (System.IO.File.Exists(CustomerPricingFilenameAndPath))
            //{
            //    CustomerPricing = ReadCsvFile<Models.CustomerPricing>(CustomerPricingFilenameAndPath);

            //    if (CustomerPricing != null && CustomerPricing.Any())
            //        ProcessCustomerPricings(CustomerPricing);
            //}
            
            // Read in the Customer Address File
            // If addresses exist, process the addresses
            // 07/24/2017 - Commenting out due to processing in real-time with a job.
            //if (System.IO.File.Exists(CustomerAddressFilenameAndPath))
            //{
            //    CustomerAddresses = ReadCsvFile<Models.CustomerAddress>(CustomerAddressFilenameAndPath);

            //    if (CustomerAddresses != null && CustomerAddresses.Any())
            //        ProcessCustomerAddresses(CustomerAddresses);
            //}
            ArchiveCustomerFiles(FilenamesAndPaths);
        }

        private CustomerRole GetPriceLevelRole(int priceLevel)
        {
            CustomerRole PriceLevelRole;

            switch (priceLevel)
            {
                case 1:
                    PriceLevelRole = PriceLevel1Role;
                    break;
                case 2:
                    PriceLevelRole = PriceLevel2Role;
                    break;
                case 3:
                    PriceLevelRole = PriceLevel3Role;
                    break;
                case 4:
                    PriceLevelRole = PriceLevel4Role;
                    break;
                default:
                    PriceLevelRole = PriceLevel1Role;
                    break;
            }

            return PriceLevelRole;
        }

        private void InitializeServices()
        {
            // Initialize the NopEngine.
            NopEngineContext = EngineContext.Initialize(true);

            // Initialize the Customer Service and any additional services
            AddressService = EngineContext.Current.Resolve<IAddressService>();
            CustomerRegistrationService = EngineContext.Current.Resolve<ICustomerRegistrationService>();
            CustomerService = EngineContext.Current.Resolve<ICustomerService>();
            GenericAttributeService = EngineContext.Current.Resolve<IGenericAttributeService>();
            ProductService = EngineContext.Current.Resolve<IProductService>();
            SettingService = EngineContext.Current.Resolve<ISettingService>();
            StateService = EngineContext.Current.Resolve<IStateProvinceService>();

            // Load specific settings
            CustomerSettings = SettingService.LoadSetting<CustomerSettings>();
            
            // Get roles for tiered pricing
            PriceLevel1Role = CustomerService.GetCustomerRoleBySystemName("PriceLevel1");
            PriceLevel2Role = CustomerService.GetCustomerRoleBySystemName("PriceLevel2");
            PriceLevel3Role = CustomerService.GetCustomerRoleBySystemName("PriceLevel3");
            PriceLevel4Role = CustomerService.GetCustomerRoleBySystemName("PriceLevel4");
            RegisteredRole = CustomerService.GetCustomerRoleBySystemName("Registered");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customers"></param>
        /// <returns></returns>
        public List<Models.Customer> ProcessCustomers(List<Models.Customer> customers)
        {            
            // Loop through the Customers from the upload
            foreach (var Customer in customers)
            {
                try
                {
                    // If the email is not provided in the file, default it to the customerNumber@oldhamchem.com
                    if (String.IsNullOrWhiteSpace(Customer.Email))
                        Customer.Email = String.Join("@", Customer.CustomerNumber, "oldhamchem.com");

                    Nop.Core.Domain.Customers.Customer StoreCustomer = CustomerService.GetCustomerByUsername(Customer.Email);
                        //CustomerService.GetCustomerByCustomerNumber(Customer.CustomerNumber);

                    // If customer exists, update the customer
                    // Else add the customer
                    if (StoreCustomer != null)
                    {
                        // Update the customer
                        StoreCustomer.Email = Customer.Email.Trim().ToLower();

                        CustomerService.UpdateCustomer(StoreCustomer);
                        
                        GenericAttributeService.SaveAttribute(StoreCustomer, SystemCustomerAttributeNames.Company, System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Customer.CustomerName.Trim().ToLower()));
                        GenericAttributeService.SaveAttribute(StoreCustomer, SystemCustomerAttributeNames.Phone, Customer.PhoneNumber.Trim());
                    }
                    else
                    {
                        // 1. Create the customer (Password defaults to customer number)
                        // Copied from Admin/Controllers/CustomerController
                        StoreCustomer = new Nop.Core.Domain.Customers.Customer
                        {
                            CustomerGuid = Guid.NewGuid(),
                            CustomerNumber = Customer.CustomerNumber.Trim(),
                            Email = Customer.Email.Trim().ToLower(),
                            Username = Customer.Email.Trim().ToLower(),
                            //VendorId = model.VendorId,
                            //AdminComment = model.AdminComment,
                            IsTaxExempt = false,
                            Active = true,
                            CreatedOnUtc = DateTime.UtcNow,
                            LastActivityDateUtc = DateTime.UtcNow,
                        };

                        CustomerService.InsertCustomer(StoreCustomer);

                        GenericAttributeService.SaveAttribute(StoreCustomer, SystemCustomerAttributeNames.Company, System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Customer.CustomerName.ToLower()));
                        GenericAttributeService.SaveAttribute(StoreCustomer, SystemCustomerAttributeNames.Phone, Customer.PhoneNumber.Trim());

                        // Set password
                        // Copied from Admin/Controllers/CustomerController
                        var changePassRequest = new ChangePasswordRequest(Customer.Email, false, CustomerSettings.DefaultPasswordFormat, Customer.CustomerNumber);
                        var changePassResult = CustomerRegistrationService.ChangePassword(changePassRequest);
                    }

                    UpdateCustomerRoles(StoreCustomer, Customer.PriceLevel);

                    // Pull out an addresses in the file and process
                    List<Models.CustomerAddress> CustomerAddresses = customers
                        .Where(w => w.CustomerNumber.Trim() == StoreCustomer.CustomerNumber &&  w.Address != null)
                        .Select(s => new Models.CustomerAddress()
                        {
                            Company = s.CustomerName,
                            CustomerNumber = s.CustomerNumber,
                            Address = s.Address
                        })
                        .ToList();

                    // Customer file contains an address. Process this as well
                    ProcessCustomerAddresses(CustomerAddresses);
                }
                catch (Exception ex)
                {
                    Helpers.ElmahWrapper.LogToElmah(ex);
                }                
            }            

            return customers;
        }

        public void ProcessCustomerAddresses(List<Models.CustomerAddress> customerAddresses)
        {            
            foreach (var CustomerAddress in customerAddresses)
            {
                try
                {
                    List<Nop.Core.Domain.Customers.Customer> StoreCustomers = CustomerService.GetCustomerByCustomerNumber(CustomerAddress.CustomerNumber).ToList();

                    if (StoreCustomers != null && StoreCustomers.Any())
                    {
                        // Check for an existing address
                        string TrimmedAddress1 = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CustomerAddress.Address.Address1.Trim().ToLower());

                        foreach(var StoreCustomer in StoreCustomers)
                        {
                            Nop.Core.Domain.Common.Address ExistingAddress = StoreCustomer.Addresses.Where(w => w.Address1 == TrimmedAddress1).FirstOrDefault();

                            // If an address does not already exist, add the address
                            if (ExistingAddress == null)
                            {
                                Nop.Core.Domain.Directory.StateProvince State = StateService.GetStateProvinceByAbbreviation(CustomerAddress.Address.State);

                                Nop.Core.Domain.Common.Address NewAddress = new Nop.Core.Domain.Common.Address()
                                {
                                    Address1 = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CustomerAddress.Address.Address1.Trim().ToLower()),
                                    Address2 = (!String.IsNullOrWhiteSpace(CustomerAddress.Address.Address2) ? System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CustomerAddress.Address.Address2.Trim().ToLower()) : null),
                                    City = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CustomerAddress.Address.City.Trim().ToLower()),
                                    Company = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CustomerAddress.Company.Trim().ToLower()),
                                    CountryId = 1,
                                    CreatedOnUtc = DateTime.UtcNow,
                                    Email = StoreCustomer.Email,
                                    StateProvinceId = State.Id,
                                    //StateProvince = State,
                                    ZipPostalCode = CustomerAddress.Address.PostalCode
                                };

                                StoreCustomer.Addresses.Add(NewAddress);

                                // If the billing address is not set, set it to the new address
                                if (StoreCustomer.BillingAddress == null || StoreCustomer.BillingAddress.Id == 0)
                                    StoreCustomer.BillingAddress = NewAddress;

                                // If the shipping address is not set, set it to the new address
                                if (StoreCustomer.ShippingAddress == null || StoreCustomer.ShippingAddress.Id == 0)
                                    StoreCustomer.ShippingAddress = NewAddress;

                                CustomerService.UpdateCustomer(StoreCustomer);
                            }
                            // If the address does exist, make sure to update the e-mail address associated with it in case the user has changed it.
                            else
                            {
                                ExistingAddress.Email = StoreCustomer.Email;
                                AddressService.UpdateAddress(ExistingAddress);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helpers.ElmahWrapper.LogToElmah(ex);
                }
            }

        }

        public void ProcessCustomerPricings(List<Models.CustomerPricing> customerPricings)
        {
            // Get the distinct customer numbers to process 1 customer at a time instead of 1 record at a time.
            // This will increase the efficiency.
            List<string> DistinctCustomerNumbers = customerPricings
                .Select(s => s.CustomerNumber)
                .Distinct()
                .ToList();

            foreach (var CustomerNumber in DistinctCustomerNumbers)
            {
                try
                {
                    // Get the customer from the store
                    IList<Nop.Core.Domain.Customers.Customer> StoreCustomers = CustomerService.GetCustomerByCustomerNumber(CustomerNumber);

                    if (StoreCustomers != null && StoreCustomers.Any())
                    {
                        // Get the contract pricing for this specific customer
                        List<Models.CustomerPricing> ContractPricings = customerPricings
                            .Where(w => w.CustomerNumber == CustomerNumber)
                            .ToList();

                        // Get a list of item numbers to find products with a corresponding sku
                        List<String> ItemNumbers = ContractPricings
                            .Select(s => s.ItemNumber.Trim())
                            .Distinct()
                            .ToList();

                        // Search products for matching sku/item numbers
                        IList<Nop.Core.Domain.Catalog.Product> MatchingProducts = ProductService.GetProductsBySku(ItemNumbers);

                        foreach (var StoreCustomer in StoreCustomers)
                        {
                            // Loop through each product found
                            foreach (var Product in MatchingProducts)
                            {
                                // Get the contract price for that product from the file
                                decimal ProductContractPrice = ContractPricings
                                    .Where(w => w.ItemNumber.Trim() == Product.Sku)
                                    .Select(s => s.ContractPrice)
                                    .FirstOrDefault();

                                // Check for an existing contract price
                                ContractPrice ExistingContractPrice = CustomerService.GetContractPricesByCustomerIdAndProductId(StoreCustomer.Id, Product.Id);

                                // If a contract price does not exist, add one
                                // Else check if it is different and update if so
                                if (ExistingContractPrice == null)
                                {
                                    ContractPrice NewContractPrice = new ContractPrice()
                                    {
                                        CustomerId = StoreCustomer.Id,
                                        DateCreated = DateTime.UtcNow,
                                        Price = ProductContractPrice,
                                        ProductId = Product.Id,
                                        Quantity = 1, // This must be at least 1,
                                        StoreId = StoreId // This is the oldham store id
                                    };

                                    CustomerService.InsertContractPrice(NewContractPrice);
                                }
                                else
                                {
                                    if (ExistingContractPrice.Price != ProductContractPrice)
                                    {
                                        ExistingContractPrice.DateUpdated = DateTime.UtcNow;
                                        ExistingContractPrice.Price = ProductContractPrice;
                                        CustomerService.UpdateContractPrice(ExistingContractPrice);
                                    }
                                }
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    Helpers.ElmahWrapper.LogToElmah(ex);
                }

                // Remove the processes records from the list. This will speed up each additional iteration.
                // customerPricings.RemoveAll(ra => ra.CustomerNumber == CustomerNumber);
            }
        }
        
        private void UpdateCustomerRoles(Customer customer, int priceLevel)
        {
            try
            {
                ICollection<CustomerRole> CustomerRoles = customer.CustomerRoles;

                CustomerRole PriceLevelRole = GetPriceLevelRole(priceLevel);

                if (CustomerRoles.Any())
                {
                    List<CustomerRole> PriceLevelRoles = CustomerRoles
                        .Where(w => w.Name.StartsWith("Price Level"))
                        .ToList();

                    if (PriceLevelRoles.Any())
                    {
                        // If more than 1 price level role exists, remove them all and add just the one from the file
                        // Else, check the current price level role
                        if (PriceLevelRoles.Count > 1)
                        {
                            foreach (var Role in PriceLevelRoles)
                            {
                                customer.CustomerRoles.Remove(Role);
                            }

                            customer.CustomerRoles.Add(PriceLevelRole);
                        }
                        else
                        {
                            // If different, remove it and add the one from the file
                            // Else do nothing
                            CustomerRole CurrentPriceLevelRole = PriceLevelRoles.First();

                            if (PriceLevelRole != CurrentPriceLevelRole)
                            {
                                customer.CustomerRoles.Remove(CurrentPriceLevelRole);
                                customer.CustomerRoles.Add(PriceLevelRole);
                            }
                        }
                    }
                    else
                    {
                        customer.CustomerRoles.Add(PriceLevelRole);
                    }
                }
                else
                {
                    customer.CustomerRoles.Add(RegisteredRole);

                    customer.CustomerRoles.Add(PriceLevelRole);
                }

                CustomerService.UpdateCustomer(customer);

            }
            catch (Exception ex)
            {
                Helpers.ElmahWrapper.LogToElmah(ex);
            }
        }
    }
}
