﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Orders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.FileWatchers
{
    public class OrderFileWatcher : BaseFileWatcher<Models.Order>
    {
        private IEngine NopEngineContext;
        private ICustomerService CustomerService;
        private IOrderService OrderService;

        public OrderFileWatcher()
        {
            this.Filter = System.Configuration.ConfigurationManager.AppSettings["OrderFileName"];

            this.Created += OrderFileWatcher_Created;
        }

        public void ArchiveOrderFiles(List<string> filenamesAndPaths)
        {
            string ArchiveDirectory = System.Configuration.ConfigurationManager.AppSettings["BaseArchiveDirectory"] + "\\" + System.Configuration.ConfigurationManager.AppSettings["OrderDirectory"];

            foreach (var filenameAndPath in filenamesAndPaths)
            {
                ArchiveFile(filenameAndPath, ArchiveDirectory);
            }
        }

        private void InitializeServices()
        {
            // Initialize the NopEngine.
            NopEngineContext = EngineContext.Initialize(true);

            // Initialize the Order Service
            CustomerService = EngineContext.Current.Resolve<ICustomerService>();
            OrderService = EngineContext.Current.Resolve<IOrderService>();
        }

        private void OrderFileWatcher_Created(object sender, FileSystemEventArgs e)
        {

            Helpers.ElmahWrapper.LogToElmah(new Exception("Order File Detected."));

            InitializeServices();

            List<Models.Order> Orders = ReadCsvFile(e.FullPath);
            List<Models.OrderItem> OrderItems = new List<Models.OrderItem>();

            List<string> FilenamesAndPaths = new List<string>();
            FilenamesAndPaths.Add(e.FullPath);

            string OrderItemFilename = System.Configuration.ConfigurationManager.AppSettings["OrderItemFileName"];
            string OrderItemFilenameAndPath = this.Path + "\\" + OrderItemFilename;

            if (System.IO.File.Exists(OrderItemFilenameAndPath))
            {
                OrderItems = ReadCsvFile<Models.OrderItem>(OrderItemFilenameAndPath);

                FilenamesAndPaths.Add(OrderItemFilenameAndPath);
            }

            if (Orders != null && Orders.Any())
            {
                //ProcessOrders(Orders, OrderItems);
            }

            ArchiveOrderFiles(FilenamesAndPaths);
        }

        //public List<Models.Order> ProcessOrders(List<Models.Order> orders, List<Models.OrderItem> orderItems)
        //{

        //    try
        //    {
        //        OrderHistorySortParameter SortParameter = new OrderHistorySortParameter(OrderHistorySortParameterNames.InvoiceDate, SortDirection.DESC);

        //        int OrderHistoryCount = 0;

        //        OrderHistory MostRecentOrderHistory = OrderService
        //            .SearchOrderHistory(0, 1, null, null, SortParameter, out OrderHistoryCount)
        //            .ToList()
        //            .FirstOrDefault();

        //        DateTime MostRecentInvoiceDate = DateTime.MinValue;
        //        if (MostRecentOrderHistory != null)
        //            MostRecentInvoiceDate = MostRecentOrderHistory.DateInvoiceCreated;

        //        List<Models.Order> OrderHistoryToProcess = orders
        //            .Where(w => w.InvoiceDate > MostRecentInvoiceDate)
        //            .OrderBy(ob => ob.InvoiceDate)
        //            .ToList();

        //        // Loop through the Orders from the upload
        //        foreach (var Order in OrderHistoryToProcess)
        //        {
        //            OrderHistory OrderHistoryToInsert = new OrderHistory()
        //            {
        //                CustomerNumber = Order.CustomerNumber.Trim(),
        //                DateCreated = DateTime.UtcNow,
        //                DateInvoiceCreated = Order.InvoiceDate,
        //                FreightCharge = Order.FreightCharge,
        //                InvoiceAmount = Order.InvoiceAmount,
        //                InvoiceBalance = Order.InvoiceBalance,
        //                InvoiceNumber = Order.InvoiceNumber.Trim(),
        //                OrderNumber = Order.OrderNumber.Trim(),
        //                SalesTax = Order.SalesTax,
        //                Terms = Order.Terms.Trim(),
        //                WarehouseCode = Order.WarehouseCode.Trim()
        //            };

        //            if (orderItems != null && orderItems.Any())
        //            {
        //                List<Models.OrderItem> ItemsForThisOrder = orderItems
        //                    .Where(w => w.InvoiceNumber == Order.InvoiceNumber)
        //                    .ToList();

        //                if (ItemsForThisOrder != null && ItemsForThisOrder.Any())
        //                {
        //                    OrderHistoryToInsert.OrderHistoryItems = new List<OrderHistoryItem>();

        //                    foreach (var OrderItem in ItemsForThisOrder)
        //                    {
        //                        OrderHistoryItem OrderHistoryItemToInsert = new OrderHistoryItem()
        //                        {
        //                            DateCreated = DateTime.UtcNow,
        //                            IsTaxable = (!String.IsNullOrWhiteSpace(OrderItem.IsTaxable) && OrderItem.IsTaxable.Trim().ToUpper() == "Y" ? true : false),
        //                            ProductNumber = OrderItem.ItemNumber.Trim(),
        //                            Quantity = OrderItem.Quantity,
        //                            Total = OrderItem.Total,
        //                            UnitPrice = OrderItem.UnitPrice
        //                        };

        //                        OrderHistoryToInsert.OrderHistoryItems.Add(OrderHistoryItemToInsert);
        //                    }

        //                    orderItems.RemoveAll(ra => ra.InvoiceNumber == Order.InvoiceNumber);
        //                }
        //            }

        //            OrderService.InsertOrderHistory(OrderHistoryToInsert);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Helpers.ElmahWrapper.LogToElmah(ex);
        //    }

        //    return orders;
        //}
    }
}
