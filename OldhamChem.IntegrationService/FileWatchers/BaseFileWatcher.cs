﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.FileWatchers
{
    public class BaseFileWatcher<T> : FileSystemWatcher
    {
        public CsvHelper.Configuration.CsvConfiguration CsvConfiguration { get; private set; }
        public int StoreId { get; private set; }

        /// <summary>
        /// Initializes a file watcher using the working directory from the AppSettings
        /// Also initializes teh CsvConfiguration
        /// </summary>
        public BaseFileWatcher()
        {
            // The path must be set first otherwise throws an error.
            this.Path = System.Configuration.ConfigurationManager.AppSettings["BaseWorkingDirectory"]; //+ System.Configuration.ConfigurationManager.AppSettings["ManufacturerDirectory"];
            
            this.EnableRaisingEvents = true;

            this.NotifyFilter = NotifyFilters.LastAccess |
                         NotifyFilters.LastWrite |
                         NotifyFilters.FileName |
                         NotifyFilters.DirectoryName;

            this.CsvConfiguration = GetCsvHelperConfiguration();

            this.StoreId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["StoreId"]);
        }
                
        /// <summary>
        /// Moves a file from the working directory to the archive directory.
        /// If the archive directory does not exist, it creates it.
        /// </summary>
        /// <param name="filenameAndPath"></param>
        /// <param name="archiveDirectory"></param>
        protected void ArchiveFile(string filenameAndPath, string archiveDirectory)
        {
            if (!System.IO.Directory.Exists(archiveDirectory))
            {
                System.IO.Directory.CreateDirectory(archiveDirectory);
            }

            string ArchiveFileExtension = System.IO.Path.GetExtension(filenameAndPath);
            string ArchiveFilenameAndPath = String.Format("{0}\\{1}_{2}{3}", archiveDirectory, System.IO.Path.GetFileNameWithoutExtension(filenameAndPath), DateTime.UtcNow.Ticks.ToString(), ArchiveFileExtension);

            WaitForFile(filenameAndPath);

            File.Move(filenameAndPath, ArchiveFilenameAndPath);
        }
        
        public string GenerateUrlRecord(string value)
        {
            value = value.Trim();
            value = value.ToLower();
            value = value.Replace("&", "and");
            value = value.Replace("%", "pct");
            value = value.Replace("(", "");
            value = value.Replace(")", "");
            value = value.Replace("#", "");
            value = value.Replace(".", "point");
            value = value.Replace("/", "-");
            value = value.Replace(" ", "-");
            value = value.Replace("\"", "in");
            value = value.Replace("'", "ft");

            return value;
        }

        /// <summary>
        /// Returns the default configuration for CSV Helper.
        /// Initializes mapping for the CSV files to working models
        /// </summary>
        /// <returns></returns>
        protected CsvHelper.Configuration.CsvConfiguration GetCsvHelperConfiguration()
        {
            DataMaps.AddressMap AddressMap = new DataMaps.AddressMap();
            DataMaps.CustomerMap CustomerMap = new DataMaps.CustomerMap();
            DataMaps.CustomerAddressMap CustomerAddressMap = new DataMaps.CustomerAddressMap();
            DataMaps.CustomerPricingMap CustomerPricingMap = new DataMaps.CustomerPricingMap();
            DataMaps.ManufacturerMap ManufacturerMap = new DataMaps.ManufacturerMap();
            DataMaps.OrderMap OrderMap = new DataMaps.OrderMap();
            DataMaps.OrderItemMap OrderItemMap = new DataMaps.OrderItemMap();
            DataMaps.ProductMap ProductMap = new DataMaps.ProductMap();


            CsvHelper.Configuration.CsvConfiguration CsvConfiguration = new CsvHelper.Configuration.CsvConfiguration();
            CsvConfiguration.Delimiter = "|";

            CsvConfiguration.RegisterClassMap(AddressMap);
            CsvConfiguration.RegisterClassMap(CustomerMap);
            CsvConfiguration.RegisterClassMap(CustomerAddressMap);
            CsvConfiguration.RegisterClassMap(CustomerPricingMap);
            CsvConfiguration.RegisterClassMap(ManufacturerMap);
            CsvConfiguration.RegisterClassMap(OrderMap);
            CsvConfiguration.RegisterClassMap(OrderItemMap);
            CsvConfiguration.RegisterClassMap(ProductMap);

            return CsvConfiguration;
        }

        /// <summary>
        /// Reads the CSV file and returns a list of the model from the implementing class
        /// </summary>
        /// <param name="filenameAndPath"></param>
        /// <returns></returns>
        protected List<T> ReadCsvFile(string filenameAndPath)
        {
            return ReadCsvFile<T>(filenameAndPath);
        }

        protected List<TOutput> ReadCsvFile<TOutput>(string filenameAndPath)
        {
            WaitForFile(filenameAndPath);

            try
            {
                StreamReader StreamReader = File.OpenText(filenameAndPath);

                CsvReader CsvReader = new CsvReader(StreamReader, this.CsvConfiguration);

                List<TOutput> Results = CsvReader
                    .GetRecords<TOutput>()
                    .ToList();

                StreamReader.Close();
                StreamReader.Dispose();

                return Results;

            }
            catch (Exception ex)
            {
                Helpers.ElmahWrapper.LogToElmah(ex);
                return null;
            }
        }

        private bool WaitForFile(string filenameAndPath)
        {
            FileStream stream = null;

            bool FileIsReady = false;

            while (!FileIsReady)
            {
                try
                {
                    using (stream = System.IO.File.Open(filenameAndPath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                    {
                        FileIsReady = true;
                    }
                }
                catch (IOException)
                {
                    //File isn't ready yet, so we need to keep on waiting until it is.
                }

                //We'll want to wait a bit between polls, if the file isn't ready.
                if (!FileIsReady)
                    Thread.Sleep(2000);
            }

            return FileIsReady;
        }
    }
}
