﻿using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Seo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.FileWatchers
{
    public class ProductFileWatcher : BaseFileWatcher<Models.Product>
    {
        private IEngine NopEngineContext;
        private ICategoryService CategoryService;
        private ICustomerService CustomerService;
        private IManufacturerService ManufacturerService;
        private IProductService ProductService;
        private IUrlRecordService UrlRecordService;
        private List<Nop.Core.Domain.Catalog.Manufacturer> AllManufacturers;
        private List<Nop.Core.Domain.Catalog.Category> AllCategories;
        private List<Nop.Core.Domain.Catalog.Category> ParentCategories;
        private List<Nop.Core.Domain.Catalog.Category> ChildCategories;
        private Nop.Core.Domain.Customers.CustomerRole PriceLevel1Role;
        private Nop.Core.Domain.Customers.CustomerRole PriceLevel2Role;
        private Nop.Core.Domain.Customers.CustomerRole PriceLevel3Role;
        private Nop.Core.Domain.Customers.CustomerRole PriceLevel4Role;

        public ProductFileWatcher()
        {
            this.Filter = System.Configuration.ConfigurationManager.AppSettings["ProductFileName"];

            this.Created += ProductFileWatcher_Created;
        }

        private void InitializeServices()
        {
            // Initialize the NopEngine.
            NopEngineContext = EngineContext.Initialize(true);

            // Initialize the Product Service
            CategoryService = EngineContext.Current.Resolve<ICategoryService>();
            CustomerService = EngineContext.Current.Resolve<ICustomerService>();
            ManufacturerService = EngineContext.Current.Resolve<IManufacturerService>();
            ProductService = EngineContext.Current.Resolve<IProductService>();
            UrlRecordService = EngineContext.Current.Resolve<IUrlRecordService>();
        }

        private void ProductFileWatcher_Created(object sender, FileSystemEventArgs e)
        {
            InitializeServices();
            
            List<Models.Product> Products = ReadCsvFile(e.FullPath);

            if (Products != null && Products.Any())
            {
                // Get all manufacturers
                AllManufacturers = ManufacturerService.GetAllManufacturers().ToList();

                // Get all the categories and split into parent and child categories
                AllCategories = CategoryService.GetAllCategories().ToList();
                ParentCategories = AllCategories.Where(w => w.ParentCategoryId == 0).ToList();
                ChildCategories = AllCategories.Where(w => w.ParentCategoryId > 0).ToList();

                // Get roles for tiered pricing
                PriceLevel1Role = CustomerService.GetCustomerRoleBySystemName("PriceLevel1");
                PriceLevel2Role = CustomerService.GetCustomerRoleBySystemName("PriceLevel2");
                PriceLevel3Role = CustomerService.GetCustomerRoleBySystemName("PriceLevel3");
                PriceLevel4Role = CustomerService.GetCustomerRoleBySystemName("PriceLevel4");

                ProcessProducts(Products);
            }

            ArchiveProductFile(e.FullPath);
        }

        public void ArchiveProductFile(string filenameAndPath)
        {
            string ArchiveDirectory = System.Configuration.ConfigurationManager.AppSettings["BaseArchiveDirectory"] + "\\" + System.Configuration.ConfigurationManager.AppSettings["ProductDirectory"];
            ArchiveFile(filenameAndPath, ArchiveDirectory);
        }

        public List<Models.Product> ProcessProducts(List<Models.Product> Products)
        {               
            // Loop through the Products from the upload
            foreach (var Product in Products)
            {
                    //Get Existing Product if any
                Nop.Core.Domain.Catalog.Product WorkingProduct = ProductService.GetProductBySku(Product.ItemNumber.Trim());

                // If the product exists, update it's prices
                // Else add a new product, setup in categories and set pricing
                if (WorkingProduct != null)
                {
                    UpdateProduct(Product, WorkingProduct);
                }
                else
                {
                    WorkingProduct = AddProduct(Product);
                }
            }

            return Products;
        }

        private Nop.Core.Domain.Catalog.Product AddProduct(Models.Product productToAdd)
        {
            Nop.Core.Domain.Catalog.Product NewProduct = null;

            try
            {
                if (productToAdd.Discontinued.Trim() == "N")
                {

                    string TrimmedAndCasedProductName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(productToAdd.ItemDescription.Trim().ToLower());
                    string TrimmedProductNumber = productToAdd.ItemNumber.Trim();

                    NewProduct = CreateProduct(productToAdd, TrimmedAndCasedProductName, TrimmedProductNumber);

                    UpdateProductManufacturer(productToAdd.ManufacturerNumber, NewProduct);

                    UpdateProductCategories(productToAdd.SubCategory, productToAdd.Category, NewProduct);

                    UpdateFriendlyUrl(TrimmedAndCasedProductName, NewProduct);

                    UpdateTierPricing(productToAdd, NewProduct);
                }
                
            }
            catch (Exception ex)
            {
                Helpers.ElmahWrapper.LogToElmah(ex);
            }

            return NewProduct;
        }

        private void UpdateProduct(Models.Product productForUpdate, Nop.Core.Domain.Catalog.Product WorkingProduct)
        {
            try
            {
                if (productForUpdate.Discontinued.Trim() == "Y")
                {
                    WorkingProduct.Published = false;
                }
                else
                {
                    WorkingProduct.Price = productForUpdate.PriceLevel1;

                    if (String.IsNullOrWhiteSpace(WorkingProduct.ManufacturerPartNumber))
                    {
                        WorkingProduct.ManufacturerPartNumber = productForUpdate.ManufacturerPartNumber;
                    }
                }

                WorkingProduct.UpdatedOnUtc = DateTime.UtcNow;

                ProductService.UpdateProduct(WorkingProduct);
            }
            catch (Exception ex)
            {
                Helpers.ElmahWrapper.LogToElmah(ex);
            }
        }

        private void UpdateFriendlyUrl(string trimmedAndCasedProductName, Nop.Core.Domain.Catalog.Product workingProduct)
        {
            // Create Friendly Url
            string ActiveSlug = UrlRecordService.GetActiveSlug(workingProduct.Id, "Product", 0);
            string NewSlug = GenerateUrlRecord(trimmedAndCasedProductName);

            if (String.IsNullOrWhiteSpace(ActiveSlug) || ActiveSlug != NewSlug)
            {
                Nop.Core.Domain.Seo.UrlRecord ProductUrlRecord = null;

                if (!String.IsNullOrWhiteSpace(ActiveSlug))
                {
                    ProductUrlRecord = UrlRecordService.GetBySlug(ActiveSlug);

                    ProductUrlRecord.IsActive = false;

                    UrlRecordService.UpdateUrlRecord(ProductUrlRecord);
                }

                ProductUrlRecord = new Nop.Core.Domain.Seo.UrlRecord()
                {
                    EntityId = workingProduct.Id,
                    EntityName = "Product",
                    IsActive = true,
                    LanguageId = 0,
                    Slug = NewSlug
                };

                UrlRecordService.InsertUrlRecord(ProductUrlRecord);
            }
        }

        private void UpdateTierPricing(Models.Product product, Nop.Core.Domain.Catalog.Product workingProduct)
        {
            if (workingProduct != null)
            {
                Nop.Core.Domain.Catalog.TierPrice PriceLevel1 = workingProduct
                    .TierPrices
                    .Where(w => w.CustomerRoleId == PriceLevel1Role.Id)
                    .FirstOrDefault();

                if (PriceLevel1 != null)
                {
                    PriceLevel1.Price = product.PriceLevel1;
                    ProductService.UpdateTierPrice(PriceLevel1);
                }
                else
                {
                    PriceLevel1 = new Nop.Core.Domain.Catalog.TierPrice()
                    {
                        ProductId = workingProduct.Id,
                        Price = product.PriceLevel1,
                        CustomerRoleId = PriceLevel1Role.Id,
                        Quantity = 1
                    };

                    ProductService.InsertTierPrice(PriceLevel1);
                }

                Nop.Core.Domain.Catalog.TierPrice PriceLevel2 = workingProduct.TierPrices.Where(w => w.CustomerRoleId == PriceLevel2Role.Id).FirstOrDefault();
                if (PriceLevel2 != null)
                {
                    PriceLevel2.Price = product.PriceLevel2;
                    ProductService.UpdateTierPrice(PriceLevel2);
                }
                else
                {
                    PriceLevel2 = new Nop.Core.Domain.Catalog.TierPrice()
                    {
                        ProductId = workingProduct.Id,
                        Price = product.PriceLevel2,
                        CustomerRoleId = PriceLevel2Role.Id,
                        Quantity = 1
                    };

                    ProductService.InsertTierPrice(PriceLevel2);
                }

                Nop.Core.Domain.Catalog.TierPrice PriceLevel3 = workingProduct.TierPrices.Where(w => w.CustomerRoleId == PriceLevel3Role.Id).FirstOrDefault();
                if (PriceLevel3 != null)
                {
                    PriceLevel3.Price = product.PriceLevel3;
                    ProductService.UpdateTierPrice(PriceLevel3);
                }
                else
                {
                    PriceLevel3 = new Nop.Core.Domain.Catalog.TierPrice()
                    {
                        ProductId = workingProduct.Id,
                        Price = product.PriceLevel3,
                        CustomerRoleId = PriceLevel3Role.Id,
                        Quantity = 1
                    };

                    ProductService.InsertTierPrice(PriceLevel3);
                }

                Nop.Core.Domain.Catalog.TierPrice PriceLevel4 = workingProduct.TierPrices.Where(w => w.CustomerRoleId == PriceLevel4Role.Id).FirstOrDefault();
                if (PriceLevel4 != null)
                {
                    PriceLevel4.Price = product.PriceLevel4;
                    ProductService.UpdateTierPrice(PriceLevel4);
                }
                else
                {
                    PriceLevel4 = new Nop.Core.Domain.Catalog.TierPrice()
                    {
                        ProductId = workingProduct.Id,
                        Price = product.PriceLevel4,
                        CustomerRoleId = PriceLevel4Role.Id,
                        Quantity = 1
                    };

                    ProductService.InsertTierPrice(PriceLevel4);
                }                
            }
        }

        private void UpdateProductCategories(string childCategory, string parentCategory, Nop.Core.Domain.Catalog.Product workingProduct)
        {
            // Get the parent category
            string ParentCategoryName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(parentCategory.Trim().ToLower());
            string ChildCategoryName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(childCategory.Trim().ToLower());

            Nop.Core.Domain.Catalog.Category ParentCategory = ParentCategories.Where(w => w.Name == ParentCategoryName).FirstOrDefault();
            Nop.Core.Domain.Catalog.Category ChildCategory = null;

            // If parent category doesn't exist, put in "Other" so that it can be found and moved
            // Else locate the child category
            if (ParentCategory == null)
                ParentCategory = ParentCategories.Where(w => w.Name == "Other").FirstOrDefault();
            else
                ChildCategory = ChildCategories.Where(w => w.ParentCategoryId == ParentCategory.Id && w.Name == ChildCategoryName).FirstOrDefault();

            // if the product already has categories but they are different, clear the categories
            List<Nop.Core.Domain.Catalog.ProductCategory> ProductCategories = CategoryService.GetProductCategoriesByProductId(workingProduct.Id, true).ToList();
            if (ProductCategories != null && ProductCategories.Any()) // && !product.ProductCategories.Any(a => a.Category.Name == ParentCategoryName || a.Category.Name == ChildCategoryName))
            {
                foreach (var productCategory in ProductCategories)
                    CategoryService.DeleteProductCategory(productCategory);
            }

            // Create the parent category relationship and persist
            Nop.Core.Domain.Catalog.ProductCategory ParentProductCategory = new Nop.Core.Domain.Catalog.ProductCategory()
            {
                ProductId = workingProduct.Id,
                CategoryId = ParentCategory.Id
            };

            CategoryService.InsertProductCategory(ParentProductCategory);

            // If the child category exists, Create the child category relationship and persist
            if (ChildCategory != null)
            {
                Nop.Core.Domain.Catalog.ProductCategory ChildProductCategory = new Nop.Core.Domain.Catalog.ProductCategory()
                {
                    ProductId = workingProduct.Id,
                    CategoryId = ChildCategory.Id
                };

                CategoryService.InsertProductCategory(ChildProductCategory);
            }
        }

        private void UpdateProductManufacturer(string manufacturerNumber, Nop.Core.Domain.Catalog.Product workingProduct)
        {
            if (!String.IsNullOrWhiteSpace(manufacturerNumber))
            {
                string TrimmedManufacturerNumber = manufacturerNumber.Trim();
                
                // Get the manufacturer
                Nop.Core.Domain.Catalog.Manufacturer Mfg = AllManufacturers
                    .Where(w => w.MetaKeywords == TrimmedManufacturerNumber)
                    .FirstOrDefault();

                // If the manufacturer doesn't exist, get default mfg
                if (Mfg == null)
                {
                    Mfg = AllManufacturers
                        .Where(w => w.Name == "Other")
                        .FirstOrDefault();
                }

                // Get current manufacturer if any
                Nop.Core.Domain.Catalog.ProductManufacturer CurrentMfg = ManufacturerService
                    .GetProductManufacturersByProductId(workingProduct.Id)
                    .FirstOrDefault();

                // If the current MFG exists and is not the same, update it otherwise insert it
                if (CurrentMfg != null)
                {
                    if (CurrentMfg.ManufacturerId != Mfg.Id)
                    {
                        CurrentMfg.ManufacturerId = Mfg.Id;

                        ManufacturerService.UpdateProductManufacturer(CurrentMfg);
                    }                    
                }
                else
                {
                    Nop.Core.Domain.Catalog.ProductManufacturer ProductMfg = new Nop.Core.Domain.Catalog.ProductManufacturer()
                    {
                        ManufacturerId = Mfg.Id,
                        ProductId = workingProduct.Id
                    };

                    ManufacturerService.InsertProductManufacturer(ProductMfg);
                }
            }
        }
        
        private Nop.Core.Domain.Catalog.Product CreateProduct(Models.Product product, string trimmedAndCasedProductName, string trimmedProductNumber)
        {
            // Create the new product
            Nop.Core.Domain.Catalog.Product NewProduct = new Nop.Core.Domain.Catalog.Product()
            {
                AdditionalShippingCharge = 0,
                AdminComment = null,
                AllowAddingOnlyExistingAttributeCombinations = false,
                AllowBackInStockSubscriptions = false,
                AllowCustomerReviews = false,
                AllowedQuantities = null,
                ApprovedRatingSum = 0,
                ApprovedTotalReviews = 0,
                AutomaticallyAddRequiredProducts = false,
                AvailableEndDateTimeUtc = null,
                AvailableForPreOrder = false,
                AvailableStartDateTimeUtc = null,
                BackorderMode = Nop.Core.Domain.Catalog.BackorderMode.NoBackorders,
                BasepriceAmount = 0,
                BasepriceBaseAmount = 0,
                BasepriceEnabled = false,
                BasepriceBaseUnitId = 0,
                CallForPrice = false,
                CustomerEntersPrice = false,
                CreatedOnUtc = DateTime.UtcNow,
                Deleted = false,
                DeliveryDateId = 0,
                DisableBuyButton = false,
                DisableWishlistButton = true,
                DisplayOrder = 0,
                DisplayStockAvailability = false,
                DisplayStockQuantity = false,
                DownloadActivationTypeId = 0,
                DownloadExpirationDays = 0,
                DownloadId = 0,
                FullDescription = null,
                GiftCardTypeId = 0,
                Gtin = null,
                HasDiscountsApplied = false,
                HasSampleDownload = false,
                HasTierPrices = true,
                HasUserAgreement = false,
                Height = 0,
                IsDownload = false,
                IsFreeShipping = false,
                IsGiftCard = false,
                IsRecurring = false,
                IsRental = false,
                IsShipEnabled = true,
                IsTaxExempt = false,
                IsTelecommunicationsOrBroadcastingOrElectronicServices = false,
                Length = 0,
                LimitedToStores = false,
                LowStockActivityId = 0,
                ManufacturerPartNumber = !String.IsNullOrWhiteSpace(product.ManufacturerPartNumber) ? product.ManufacturerPartNumber.Trim() : null,
                ManageInventoryMethod = Nop.Core.Domain.Catalog.ManageInventoryMethod.DontManageStock,
                MarkAsNew = false,
                MarkAsNewEndDateTimeUtc = null,
                MarkAsNewStartDateTimeUtc = null,
                MaximumCustomerEnteredPrice = 0,
                MaxNumberOfDownloads = 0,
                MetaDescription = trimmedAndCasedProductName,
                MetaKeywords = trimmedAndCasedProductName,
                MetaTitle = trimmedAndCasedProductName,
                MinimumCustomerEnteredPrice = 0,
                MinStockQuantity = 0,
                Name = trimmedAndCasedProductName,
                NotApprovedRatingSum = 0,
                NotApprovedTotalReviews = 0,
                NotifyAdminForQuantityBelow = 0,
                OldPrice = 0,
                OrderMaximumQuantity = 10000,
                OrderMinimumQuantity = 1,
                OverriddenGiftCardAmount = 0,
                ParentGroupedProductId = 0,
                PreOrderAvailabilityStartDateTimeUtc = null,
                Price = product.PriceLevel1,
                ProductCost = product.PriceLevel1,
                ProductTemplateId = 1,
                ProductTypeId = 5,
                Published = true,
                ProductType = Nop.Core.Domain.Catalog.ProductType.SimpleProduct,
                RequireOtherProducts = false,
                ShipSeparately = false,
                ShortDescription = trimmedAndCasedProductName,
                ShowOnHomePage = false,
                Sku = trimmedProductNumber,
                SubjectToAcl = false,
                StockQuantity = 0,
                UnlimitedDownloads = false,
                UpdatedOnUtc = DateTime.UtcNow,
                UseMultipleWarehouses = false,
                VisibleIndividually = true,
                Weight = 0,
                Width = 0
            };

            // Persist to product to the database
            ProductService.InsertProduct(NewProduct);
            return NewProduct;
        }
    }
}
