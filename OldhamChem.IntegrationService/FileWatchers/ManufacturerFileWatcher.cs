﻿using CsvHelper;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.FileWatchers
{
    public class ManufacturerFileWatcher : BaseFileWatcher<Models.Manufacturer>
    {
        private IEngine NopEngineContext;
        private IManufacturerService ManufacturerService;

        public ManufacturerFileWatcher()
        {
            this.Filter = System.Configuration.ConfigurationManager.AppSettings["ManufacturerFileName"];

            // Initialize the NopEngine.
            NopEngineContext = EngineContext.Initialize(true);

            // Initialize the Manufacturer Service
            ManufacturerService = EngineContext.Current.Resolve<IManufacturerService>();

            this.Created += ManufacturerFileWatcher_Created;
        }

        private void ManufacturerFileWatcher_Created(object sender, FileSystemEventArgs e)
        {
            List<Models.Manufacturer> Manufacturers = ReadCsvFile(e.FullPath);

            if (Manufacturers != null && Manufacturers.Any())
            {
                ProcessManufacturers(Manufacturers);
            }

            ArchiveManufacturerFile(e.FullPath);
        }

        public void ArchiveManufacturerFile(string filenameAndPath)
        {
            string ArchiveDirectory = System.Configuration.ConfigurationManager.AppSettings["BaseArchiveDirectory"] + "\\" + System.Configuration.ConfigurationManager.AppSettings["ManufacturerDirectory"];
            ArchiveFile(filenameAndPath, ArchiveDirectory);
        }

        public List<Models.Manufacturer> ProcessManufacturers(List<Models.Manufacturer> Manufacturers)
        {
            // Get All the Manufacturers
            List<Nop.Core.Domain.Catalog.Manufacturer> AllManufacturers = ManufacturerService.GetAllManufacturers("", 0, 2147483647, true).ToList();
            int MaxDisplayOrder = AllManufacturers.Max(m => m.DisplayOrder);

            // Loop through the Manufacturers from the upload
            foreach (var Manufacturer in Manufacturers)
            {
                string TrimmedManufacturerNumber = Manufacturer.ManufacturerNumber.Trim();
                string ProperCasedManufacturerName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Manufacturer.ManufacturerName.Trim().ToLower());

                // If a Manufacturer already exists, update the record
                // Else add the new Manufacturer
                Nop.Core.Domain.Catalog.Manufacturer ExistingManufacturer = AllManufacturers
                    .Where(w => w.ManufacturerNumber == TrimmedManufacturerNumber)
                    .FirstOrDefault();
                
                if (ExistingManufacturer != null)
                {
                    ExistingManufacturer.Published = true;
                    ExistingManufacturer.Name = ProperCasedManufacturerName;
                    ExistingManufacturer.UpdatedOnUtc = DateTime.UtcNow;
                    
                    // Remove the processed Manufacturer from the list
                    AllManufacturers.RemoveAll((Predicate<Nop.Core.Domain.Catalog.Manufacturer>)((Nop.Core.Domain.Catalog.Manufacturer ra) => ra.ManufacturerNumber == TrimmedManufacturerNumber));
                }
                else
                {
                    MaxDisplayOrder = MaxDisplayOrder + 1;

                    Nop.Core.Domain.Catalog.Manufacturer NewManufacturer = new Nop.Core.Domain.Catalog.Manufacturer()
                    {
                        AllowCustomersToSelectPageSize = false,
                        CreatedOnUtc = DateTime.UtcNow,
                        DisplayOrder = MaxDisplayOrder,
                        LimitedToStores = false,
                        ManufacturerTemplateId = 1,
                        ManufacturerNumber = TrimmedManufacturerNumber,
                        MetaTitle = ProperCasedManufacturerName,
                        MetaDescription = ProperCasedManufacturerName,
                        MetaKeywords = ProperCasedManufacturerName,
                        Name = ProperCasedManufacturerName,
                        PageSize = 6,
                        PageSizeOptions = "6, 3, 9",
                        PictureId = 0,
                        Published = true,
                        SubjectToAcl = false,
                        UpdatedOnUtc = DateTime.UtcNow

                    };

                    ManufacturerService.InsertManufacturer(NewManufacturer);
                }
            }

            // Mark each remaining Manufacturer in the list as InActive
            foreach (var ManufacturerToDeactivate in AllManufacturers)
            {
                ManufacturerToDeactivate.Published = false;
                ManufacturerService.UpdateManufacturer(ManufacturerToDeactivate);
            }

            return Manufacturers;
        }
    }
}
