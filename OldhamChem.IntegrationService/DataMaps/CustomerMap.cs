﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.DataMaps
{
    public class CustomerMap : CsvClassMap<Models.Customer>
    {
        public CustomerMap()
        {
            Map(m => m.CustomerNumber).Name("CUSTOMER NUMBER");
            Map(m => m.CustomerName).Name("CUSTOMER NAME");
            References<AddressMap>(m => m.Address);
            Map(m => m.PhoneNumber).Name("PHONE NUMBER");
            Map(m => m.PriceLevel).Name("PRICE LEVEL");
            Map(m => m.Email).Name("EMAIL");
        }
    }
}
