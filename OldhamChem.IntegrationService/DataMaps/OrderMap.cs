﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.DataMaps
{
    public class OrderMap : CsvClassMap<Models.Order>
    {
        public OrderMap()
        {
            Map(m => m.CustomerNumber).Name("CUSTOMER NUMBER");
            Map(m => m.FreightCharge).Name("FREIGHT");
            Map(m => m.InvoiceNumber).Name("INVOICE NUMBER");
            Map(m => m.InvoiceDate).Name("INVOICE DATE");
            Map(m => m.InvoiceAmount).Name("INVOICE AMT");
            Map(m => m.OrderNumber).Name("ORDER NUMBER");
            Map(m => m.SalesTax).Name("SALES TAX");
            Map(m => m.Terms).Name("TERMS");
            Map(m => m.WarehouseCode).Name("WH CODE");
            Map(m => m.InvoiceBalance).Ignore(); //("INV BALANCE");
        }
    }
}
