﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.DataMaps
{
    public class CustomerAddressMap : CsvClassMap<Models.CustomerAddress>
    {
        public CustomerAddressMap()
        {
            Map(m => m.CustomerNumber).Name("CUSTOMER NUMBER");
            Map(m => m.Company).Name("CUSTOMER NAME");
            References<AddressMap>(m => m.Address);
        }
    }
}
