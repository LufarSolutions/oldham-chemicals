﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.DataMaps
{
    public sealed class ManufacturerMap : CsvClassMap<Models.Manufacturer>
    {
        public ManufacturerMap()
        {
            Map(m => m.ManufacturerNumber).Name("VENDOR NUMBER");
            Map(m => m.ManufacturerName).Name("VENDOR NAME");
        }
    }
}
