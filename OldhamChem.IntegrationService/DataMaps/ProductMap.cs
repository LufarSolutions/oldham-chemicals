﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.DataMaps
{
    public class ProductMap : CsvClassMap<Models.Product>
    {
        public ProductMap()
        {
            Map(m => m.ItemNumber).Name("ITEM NUMBER");
            Map(m => m.ManufacturerNumber).Name("VENDOR NUMBER");
            Map(m => m.ManufacturerPartNumber).Name("MFGNUMBER");
            Map(m => m.ItemDescription).Name("ITEM DESCRIPTION");
            Map(m => m.UnitOfMeasure).Name("UNIT OF MEASURE");
            Map(m => m.NumberPerUnitOfMeasure).Name("NUMBER U/M");
            Map(m => m.PriceLevel1).Name("PRICE LEVEL 1");
            Map(m => m.PriceLevel2).Name("PRICE LEVEL 2");
            Map(m => m.PriceLevel3).Name("PRICE LEVEL 3");
            Map(m => m.PriceLevel4).Name("PRICE LEVEL 4");
            Map(m => m.Category).Name("ITEM CATEGORY");
            Map(m => m.SubCategory).Name("ITEM SUBCATEGORY");
            Map(m => m.Discontinued).Name("DISCONTINUED");
        }
    }
}
