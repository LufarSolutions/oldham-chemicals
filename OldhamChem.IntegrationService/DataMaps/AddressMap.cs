﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.DataMaps
{
    public class AddressMap : CsvClassMap<Models.Address>
    {
        public AddressMap()
        {
            Map(m => m.Address1).Name("ADDRESS 1");
            Map(m => m.Address2).Name("ADDRESS 2");
            Map(m => m.City).Name("CITY");
            Map(m => m.State).Name("STATE");
            Map(m => m.PostalCode).Name("ZIP");
        }
    }
}
