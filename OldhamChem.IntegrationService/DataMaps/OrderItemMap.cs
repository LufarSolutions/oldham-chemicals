﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.DataMaps
{
    public class OrderItemMap : CsvClassMap<Models.OrderItem>
    {
        public OrderItemMap()
        {
            Map(m => m.IsTaxable).Name("TAXABLE");
            Map(m => m.InvoiceNumber).Name("INVOICE NUMBER");
            Map(m => m.ItemNumber).Name("ITEM NUMBER");
            Map(m => m.Quantity).Name("QTY");
            Map(m => m.UnitPrice).Name("UNIT PRICE");
            Map(m => m.Total).Name("TOTAL");
        }
    }
}
