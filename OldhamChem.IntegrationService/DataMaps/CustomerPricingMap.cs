﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.IntegrationService.DataMaps
{
    public class CustomerPricingMap : CsvClassMap<Models.CustomerPricing>
    {
        public CustomerPricingMap()
        {
            Map(m => m.CustomerNumber).Name("CUSTOMER NUMBER");
            Map(m => m.ItemNumber).Name("ITEM NUMBER");
            Map(m => m.ContractPrice).Name("CONTRACT PRICE");
        }
    }
}
