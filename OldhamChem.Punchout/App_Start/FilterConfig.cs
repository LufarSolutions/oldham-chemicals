﻿using System.Web;
using System.Web.Mvc;

namespace OldhamChem.Punchout
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
