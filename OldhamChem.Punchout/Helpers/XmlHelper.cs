﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace OldhamChem.Punchout.Helpers
{
    public class XmlHelper
    {
        public static string ConvertXmlToString(XmlDocument xmlDocumentToConvert)
        {
            string XmlAsString = null;

            StringWriter Swriter = new StringWriter();

            using (var xmlTextWriter = XmlWriter.Create(Swriter))
            {
                xmlDocumentToConvert.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                XmlAsString = Swriter.GetStringBuilder().ToString();
            }

            return XmlAsString;
        }
    }
}