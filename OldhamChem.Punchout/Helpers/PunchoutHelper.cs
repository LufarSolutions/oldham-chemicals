﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Orders;
using Nop.Services.Payments;
using OldhamChem.Punchout.Common.Helpers;
using OldhamChem.Punchout.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Xml;

namespace OldhamChem.Punchout.Helpers
{
    public class PunchoutHelper
    {

        // NopCommerce Services
        private IEngine NopEngineContext;
        private IAddressService AddressService;
        private ICustomerService CustomerService;
        private ICustomerAttributeService CustomerAttributeService;
        private ICustomerAttributeParser CustomerAttributeParser;
        private IGenericAttributeService GenericAttributeService;
        private IOrderService OrderService;
        private IOrderProcessingService OrderProcessingService;
        private IProductService ProductService;
        private IShoppingCartService ShoppingCartService;
        private IStateProvinceService StateService;
        private NopHelper nopHelper;

        public PunchoutHelper()
        {
            InitializeServices();
        }

        public HttpStatusCode AuthenticatePunchoutUserFromCxmlSender(cXML cXML)
        {
            HttpStatusCode StatusCode = HttpStatusCode.Forbidden;

            try
            {
                Customer MatchingCustomer = GetCustomerFromCxml(cXML);

                if (MatchingCustomer != null)
                {
                    string SharedSecret = nopHelper.GetCustomersSharedSecret(MatchingCustomer);

                    if (!String.IsNullOrWhiteSpace(SharedSecret) && SharedSecret == cXML.Header.Sender.Credential.SharedSecret)
                    {
                        StatusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        StatusCode = HttpStatusCode.Unauthorized;
                    }
                }
                else
                {
                    StatusCode = HttpStatusCode.Forbidden;
                }
            }
            catch (Exception ex)
            {
                StatusCode = HttpStatusCode.InternalServerError;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return StatusCode;
        }

        public void ArchivePunchoutRequest(string cXMLDocumentType, XmlDocument xmlDoc)
        {
            try
            {
                string ArchiveDirectory = GetCxmlArchiveDirectory();
                string Filename = GetCxmlArchieFilename(cXMLDocumentType);

                string FilenameAndPath = ArchiveDirectory + "\\" + Filename;

                xmlDoc.Save(FilenameAndPath);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }        

        public XmlDocument CreateXmlDocumentFromPunchoutRequest(HttpRequestMessage cXML)
        {
            XmlDocument xmlDoc = null;

            try
            {
                xmlDoc = new XmlDocument();
                xmlDoc.Load(cXML.Content.ReadAsStreamAsync().Result);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return xmlDoc;
        }        

        public PlaceOrderResult PlaceOrderFromCxml(cXML cxml)
        {
            var result = new PlaceOrderResult();

            Customer customerForOrder = null;

            try
            {                
                var storeId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["StoreId"]);

                var purchaseOrderNumber = cxml.Request.OrderRequest.OrderRequestHeader.orderID;

                customerForOrder = GetCustomerFromCxml(cxml);

                // Be sure the shopping cart for the customer is empty, otherwise another punchout order is in progress.
                if (customerForOrder.ShoppingCartItems.IsNullOrEmpty())
                {
                    // Validate the product before attempting to process the order.
                    // This should prevent hanging and need to empty the cart on error due to pre-checking the items all at once vs. sequentially
                    var validatedProducts = ValidateProducts(cxml.Request.OrderRequest.ItemOut);

                    if (!validatedProducts.IsNullOrEmpty())
                    {
                        SetCustomerBillingAddress(customerForOrder, cxml.Request.OrderRequest.OrderRequestHeader.BillTo.Address);

                        SetCustomerShippingAddress(customerForOrder, cxml.Request.OrderRequest.OrderRequestHeader.ShipTo.Address);

                        foreach (var orderItem in cxml.Request.OrderRequest.ItemOut)
                        {
                            var productQuantity = int.Parse(orderItem.quantity);
                            var productSku = orderItem.ItemID.SupplierPartID.Trim().ToLower();
                            var productPrice = Decimal.Parse(orderItem.ItemDetail.UnitPrice.Money.Value);

                            var productForCart = validatedProducts.FirstOrDefault(f => f.Sku.ToLower() == productSku);

                            // If the product exists and is published
                            if (productForCart != null && productForCart.Published)
                            {
                                ShoppingCartService.AddToCart(customerForOrder, productForCart, ShoppingCartType.ShoppingCart, storeId, null, productPrice, null, null, productQuantity, false, false);                               
                            }
                            // If an item from the order does not exist or isn't published empty the shopping cart and throw an error.
                            else
                            {
                                EmptyShoppingCart(customerForOrder);

                                throw new Exception($"{purchaseOrderNumber}: Product Not Found matching SKU {productSku}.");
                            }                        
                        }

                        // If the shopping cart has been populated successfully, finish processing the order
                        if (!customerForOrder.ShoppingCartItems.IsNullOrEmpty())
                        {
                            Dictionary<string, object> CustomValues = new Dictionary<string, object>();

                            if (!String.IsNullOrWhiteSpace(purchaseOrderNumber))
                            {
                                CustomValues.Add("PO Number", purchaseOrderNumber.ToLower().Replace("po", ""));
                            }

                            ProcessPaymentRequest processPaymentRequest = new ProcessPaymentRequest()
                            {
                                StoreId = storeId,
                                CustomerId = customerForOrder.Id,
                                PaymentMethodSystemName = NopHelper.SYSTEM_NAME_FOR_PAYMENT_METHOD_PURCHASE_ORDER,
                                CustomValues = CustomValues
                            };

                            result = OrderProcessingService.PlaceOrder(processPaymentRequest);

                            // Result could have error messages but the order was successfull
                            if (!result.Success && result.PlacedOrder == null)
                            {
                                throw new Exception($"{purchaseOrderNumber}: Failed to place order.");
                            }
                        }
                        else
                        {
                            throw new Exception($"{purchaseOrderNumber}: Shopping cart was empty so order could not be placed.");
                        }
                    }
                    else
                    {
                        throw new Exception($"{purchaseOrderNumber}: One or more products could not be validated.");
                    }                    
                }
                else
                {
                    throw new Exception($"{purchaseOrderNumber}: Shopping Cart is not empty. Another punchout order is being processed.");
                }                
            }
            catch (Exception ex)
            {
                result.AddError(ex.Message);

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return result;
        }

        #region " Private Methods "        

        private Address GetCustomerBillingAddress(Customer customerForOrder, cXMLRequestOrderRequestOrderRequestHeaderBillToAddress address)
        {
            Address BillingAddress = null;
            
            try
            {
                string StreetAddress1 = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.PostalAddress.Street.ToLower());
                string StreetAddress2 = null;
                string PostalCode = address.PostalAddress.PostalCode.Length > 5 ? address.PostalAddress.PostalCode.Substring(0, 5) : address.PostalAddress.PostalCode;
                int IndexOfComma = StreetAddress1.IndexOf(',');

                if (IndexOfComma >= 0)
                {
                    StreetAddress2 = StreetAddress1.Substring(IndexOfComma + 1).Trim();

                    StreetAddress1 = StreetAddress1.Substring(0, IndexOfComma);
                }

                BillingAddress = customerForOrder
                    .Addresses
                    .Where(w => w.Address1 == StreetAddress1
                                && w.Address2 == StreetAddress2
                                && w.City == System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.PostalAddress.City.ToLower())
                                && w.StateProvince.Abbreviation == address.PostalAddress.State
                                && w.ZipPostalCode == PostalCode)
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return BillingAddress;
        }

        private Address GetCustomerShippingAddress(Customer customerForOrder, cXMLRequestOrderRequestOrderRequestHeaderShipToAddress address)
        {
            Address ShippingAddress = null;

            try
            {
                string StreetAddress1 = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.PostalAddress.Street.ToLower());
                string StreetAddress2 = null;
                string PostalCode = address.PostalAddress.PostalCode.Length > 5 ? address.PostalAddress.PostalCode.Substring(0, 5) : address.PostalAddress.PostalCode;
                int IndexOfComma = StreetAddress1.IndexOf(',');

                if (IndexOfComma >= 0)
                {
                    StreetAddress2 = StreetAddress1.Substring(IndexOfComma + 1).Trim();

                    StreetAddress1 = StreetAddress1.Substring(0, IndexOfComma);
                }

                ShippingAddress = customerForOrder
                    .Addresses
                    .Where(w => w.Address1 == StreetAddress1
                                && w.Address2 == StreetAddress2
                                && w.City == System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.PostalAddress.City.ToLower())
                                && w.StateProvince.Abbreviation == address.PostalAddress.State
                                && w.ZipPostalCode == PostalCode)
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return ShippingAddress;
        }

        private Customer GetCustomerFromCxml(cXML cXML)
        {
            Customer Customer = null;

            try
            {
                string Email = cXML.Header.To.Credential.Identity + "@punchout.oldhamchem.com"; //cXML.Header.Sender.Credential.Identity;

                Customer = CustomerService.GetCustomerByEmail(Email);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Customer;
        }

        private static string GetCxmlArchiveDirectory()
        {
            return System.Configuration.ConfigurationManager.AppSettings["cXMLArchiveDirectory"];
        }

        private static string GetCxmlArchieFilename(string cXMLDocumentType)
        {
            return "cXML" + cXMLDocumentType + "_" + DateTime.Now.Ticks.ToString() + ".xml";
        }

        private void EmptyShoppingCart(Customer customerForOrder)
        {
            if (customerForOrder != null && !customerForOrder.ShoppingCartItems.IsNullOrEmpty())
            {
                var shoppingCartItems = customerForOrder.ShoppingCartItems.ToList();

                foreach (var item in shoppingCartItems)
                {
                    ShoppingCartService.DeleteShoppingCartItem(item);
                }
            }
        }

        private void InitializeServices()
        {
            try
            {
                // Initialize the NopEngine.
                NopEngineContext = EngineContext.Initialize(true);

                // Initialize the Customer Service and any additional services
                AddressService = EngineContext.Current.Resolve<IAddressService>();
                CustomerService = EngineContext.Current.Resolve<ICustomerService>();
                CustomerAttributeService = EngineContext.Current.Resolve<ICustomerAttributeService>();
                CustomerAttributeParser = EngineContext.Current.Resolve<ICustomerAttributeParser>();
                GenericAttributeService = EngineContext.Current.Resolve<IGenericAttributeService>();
                OrderService = EngineContext.Current.Resolve<IOrderService>();
                OrderProcessingService = EngineContext.Current.Resolve<IOrderProcessingService>();
                ProductService = EngineContext.Current.Resolve<IProductService>();
                ShoppingCartService = EngineContext.Current.Resolve<IShoppingCartService>();
                StateService = EngineContext.Current.Resolve<IStateProvinceService>();

                nopHelper = new NopHelper();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        private bool SetCustomerBillingAddress(Customer customerForOrder, cXMLRequestOrderRequestOrderRequestHeaderBillToAddress address)
        {
            bool Success = false;

            try
            {
                Address SavedBillingAddress = GetCustomerBillingAddress(customerForOrder, address);

                if (SavedBillingAddress == null)
                {
                    StateProvince StateForAddress = StateService.GetStateProvinceByAbbreviation(address.PostalAddress.State);

                    string StreetAddress1 = address.PostalAddress.Street;
                    string StreetAddress2 = null;
                    string PostalCode = address.PostalAddress.PostalCode.Length > 5 ? address.PostalAddress.PostalCode.Substring(0, 5) : address.PostalAddress.PostalCode;
                    int IndexOfComma = StreetAddress1.IndexOf(',');

                    if (IndexOfComma >= 0)
                    {
                        StreetAddress2 = StreetAddress1.Substring(IndexOfComma + 1).Trim();

                        StreetAddress1 = StreetAddress1.Substring(0, IndexOfComma);

                    }

                    SavedBillingAddress = new Address()
                    {
                        Address1 = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(StreetAddress1.ToLower()),
                        Address2 = StreetAddress2 != null ? System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(StreetAddress2.ToLower()) : null,
                        City = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.PostalAddress.City.ToLower()),
                        Company = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.Name.Value.ToLower()),
                        CountryId = 1,
                        CreatedOnUtc = DateTime.UtcNow,
                        Email = customerForOrder.Email,
                        FirstName = "Billing",
                        LastName = "Address",
                        PhoneNumber = "1234567890",
                        ZipPostalCode = PostalCode,
                        StateProvince = StateForAddress,
                    };

                    customerForOrder.Addresses.Add(SavedBillingAddress);
                }

                customerForOrder.BillingAddress = SavedBillingAddress;

                CustomerService.UpdateCustomer(customerForOrder);

                Success = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Success;
        }

        private bool SetCustomerShippingAddress(Customer customerForOrder, cXMLRequestOrderRequestOrderRequestHeaderShipToAddress address)
        {
            bool Success = false;

            try
            {
                Address SavedShippingAddress = GetCustomerShippingAddress(customerForOrder, address);

                if (SavedShippingAddress == null)
                {
                    StateProvince StateForAddress = StateService.GetStateProvinceByAbbreviation(address.PostalAddress.State);

                    string StreetAddress1 = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.PostalAddress.Street.ToLower());
                    string StreetAddress2 = null;
                    string City = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.PostalAddress.City.ToLower());
                    string Company = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.Name.Value.ToLower());
                    string PostalCode = address.PostalAddress.PostalCode.Length > 5 ? address.PostalAddress.PostalCode.Substring(0, 5) : address.PostalAddress.PostalCode;
                    string FaxNumber = address.Fax != null && address.Fax.TelephoneNumber != null ? address.Fax.TelephoneNumber.AreaOrCityCode + address.Fax.TelephoneNumber.Number : null;
                    string PhoneNumber = address.Phone != null && address.Phone.TelephoneNumber != null ? address.Phone.TelephoneNumber.AreaOrCityCode + address.Phone.TelephoneNumber.Number : null;
                    string Email = address.Email != null ? address.Email.Value : null;

                    int IndexOfComma = StreetAddress1.IndexOf(',');

                    if (IndexOfComma >= 0)
                    {
                        StreetAddress2 = StreetAddress1.Substring(IndexOfComma + 1).Trim();

                        StreetAddress1 = StreetAddress1.Substring(0, IndexOfComma);

                    }

                    SavedShippingAddress = new Address()
                    {
                        Address1 = StreetAddress1,
                        Address2 = StreetAddress2,
                        City = City,
                        Company = Company,
                        CountryId = 1,
                        CreatedOnUtc = DateTime.UtcNow,
                        Email = Email,
                        FaxNumber = FaxNumber,
                        PhoneNumber = PhoneNumber,
                        StateProvince = StateForAddress,
                        ZipPostalCode = PostalCode
                    };

                    if (address.PostalAddress.DeliverTo != null && !String.IsNullOrWhiteSpace(address.PostalAddress.DeliverTo))
                    {
                        int SpaceSeparatorIndex = address.PostalAddress.DeliverTo.IndexOf(" ");

                        if (SpaceSeparatorIndex >= 0)
                        {
                            SavedShippingAddress.FirstName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.PostalAddress.DeliverTo.Substring(0, SpaceSeparatorIndex).ToLower());
                            SavedShippingAddress.LastName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.PostalAddress.DeliverTo.Substring(SpaceSeparatorIndex + 1).ToLower());
                        }
                        else
                        {
                            SavedShippingAddress.FirstName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.PostalAddress.DeliverTo.ToLower());
                        }
                    }

                    customerForOrder.Addresses.Add(SavedShippingAddress);
                }

                customerForOrder.ShippingAddress = SavedShippingAddress;

                CustomerService.UpdateCustomer(customerForOrder);

                Success = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Success;
        }

        private IList<Product> ValidateProducts(cXMLRequestOrderRequestItemOut[] orderItems)
        {
            IList<Product> products = new List<Product>();

            try
            {
                var skus = orderItems
                    .Select(s => s.ItemID.SupplierPartID.Trim())
                    .Distinct()
                    .ToList();

                if (!skus.IsNullOrEmpty())
                {
                    products = ProductService.GetProductsBySku(skus);

                    if (products.IsNullOrEmpty() || products.Count() != skus.Count() || products.Any(a => !a.Published))
                    {
                        if (!products.IsNullOrEmpty())
                        {
                            products.Clear();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (!products.IsNullOrEmpty())
                {
                    products.Clear();
                }

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return products;
        }

        #endregion

    }
}