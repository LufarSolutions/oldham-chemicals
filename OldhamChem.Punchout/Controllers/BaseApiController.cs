﻿using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OldhamChem.Punchout.Controllers
{
    public class BaseApiController : ApiController
    {
                
        public BaseApiController() : base()
        {
            PunchoutHelper = new Helpers.PunchoutHelper();
        }
    }
}
