﻿using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Services.Orders;
using OldhamChem.Punchout.Common.Helpers;
using OldhamChem.Punchout.Common.Models;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Xml;

namespace OldhamChem.Punchout.Controllers
{
    public class PunchOutController : ApiController
    {
        protected Helpers.PunchoutHelper PunchoutHelper;
        protected cXMLHelper cXMLHelper;
        protected NopHelper NopHelper;

        public PunchOutController() : base()
        {
            cXMLHelper = new cXMLHelper();
            NopHelper = new NopHelper();
            PunchoutHelper = new Helpers.PunchoutHelper();
        }

        /// <summary>
        /// Accepts a cXML Order Request document and creates an order from the information inside the document
        /// </summary>
        /// <param name="cXmlMessage"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("OrderRequest")]
        public HttpResponseMessage OrderRequest(HttpRequestMessage cXmlMessage)
        {
            HttpResponseMessage ResponseMessage = new HttpResponseMessage();

            try
            {
                if (cXmlMessage == null)
                    throw new ArgumentNullException("cXML");

                XmlDocument CxmlDoc = PunchoutHelper.CreateXmlDocumentFromPunchoutRequest(cXmlMessage);

                if (CxmlDoc != null)
                {
                    cXML Cxml = cXMLHelper.DeserializeXmlDocumentToCxml(CxmlDoc);

                    if (Cxml != null)
                    {
                        ResponseMessage.StatusCode = PunchoutHelper.AuthenticatePunchoutUserFromCxmlSender(Cxml);

                        if (ResponseMessage.StatusCode == HttpStatusCode.OK)
                        {
                            PlaceOrderResult Result = PunchoutHelper.PlaceOrderFromCxml(Cxml);

                            if (!Result.Success)
                            {
                                ResponseMessage.StatusCode = HttpStatusCode.InternalServerError;
                            }
                            else
                            {
                                NopHelper.SavecXMLRequestAsOrderNote(Result.PlacedOrder, CxmlDoc, "Punchout Order Request");
                            }
                        }
                    }
                    else
                    {
                        ResponseMessage.StatusCode = HttpStatusCode.InternalServerError;
                    }
                }
                else
                {
                    ResponseMessage.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                ResponseMessage.StatusCode = HttpStatusCode.InternalServerError;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            cXML PunchoutResponse = cXMLHelper.CreatePunchoutResponse(ResponseMessage.StatusCode);

            XmlDocument PunchoutResponseXML = cXMLHelper.SerializeCxmlToXmlDocument(PunchoutResponse, cXMLDocumentType.OrderRequest);

            string PunchoutResponseAsString = PunchoutResponseXML.OuterXml; //XmlHelper.ConvertXmlToString(PunchoutResponseXML);

            ResponseMessage.Content = new StringContent(PunchoutResponseAsString, Encoding.UTF8, "application/xml");

            return ResponseMessage;
        }

        [HttpGet]
        [Route("GetOrderConfirmation")]
        public HttpResponseMessage GetOrderConfirmation(int orderId)
        {
            HttpResponseMessage ResponseMessage = new HttpResponseMessage();

            Order OrderForConfirmation = NopHelper.OrderService.GetOrderById(orderId);

            cXMLHelper cXMLHelper = new cXMLHelper();
            cXML cXMLOrderConfirmation = cXMLHelper.BuildConfirmationRequestForOrder(OrderForConfirmation);

            XmlDocument ResponseXML = cXMLHelper.SerializeCxmlToXmlDocument(cXMLOrderConfirmation, cXMLDocumentType.OrderConfirmation);

            string ResponseAsString = ResponseXML.OuterXml;

            ResponseMessage.Content = new ByteArrayContent(Encoding.Default.GetBytes(ResponseAsString));
            ResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "OrderConfirmation.xml"
            };
            ResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");

            return ResponseMessage;
        }

        [HttpGet]
        [Route("GetInvoice")]
        public HttpResponseMessage GetInvoice(int orderId)
        {
            HttpResponseMessage ResponseMessage = new HttpResponseMessage();

            Order OrderForConfirmation = NopHelper.OrderService.GetOrderById(orderId);

            cXMLHelper cXMLHelper = new cXMLHelper();
            var cxmlInvoices = cXMLHelper.BuildInvoiceRequestForOrder(OrderForConfirmation);
            string ResponseAsString = null;

            if (!cxmlInvoices.IsNullOrEmpty()) {

                cXML cXMLOrderConfirmation = cxmlInvoices.First();

                XmlDocument ResponseXML = cXMLHelper.SerializeCxmlToXmlDocument(cXMLOrderConfirmation, cXMLDocumentType.Invoice);

                ResponseAsString = ResponseXML.OuterXml;
            }

            ResponseMessage.Content = new ByteArrayContent(Encoding.Default.GetBytes(ResponseAsString));
            ResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "Invoice.xml"
            };
            ResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");

            return ResponseMessage;
        }
    }
}
