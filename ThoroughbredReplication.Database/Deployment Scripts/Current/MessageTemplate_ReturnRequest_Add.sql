﻿DECLARE @TemplateId int = 0

SELECT @TemplateId = Id FROM MessageTemplate WHERE Name = 'StoreOwnerNotification.NewRepairRequest'

IF (@TemplateId IS NULL)
BEGIN

	INSERT INTO MessageTemplate(
		Name,
		Subject,
		Body,
		IsActive,
		AttachedDownloadId,
		EmailAccountId,
		LimitedToStores)
	VALUES(
		'StoreOwnerNotification.NewRepairRequest',
		'%Store.Name%. New Repair Request',
		'<p><a href="%Store.URL%">%Store.Name%</a> <br /><br />%Customer.FullName% has just submitted a new repair request. Details are below:<br /><br />Request ID: %ReturnRequest.ID%<br /><br />Product: %ReturnRequest.Product.Quantity% x Product: %ReturnRequest.Product.Name%<br /><br />Customer comments:<br />%ReturnRequest.CustomerComment%</p>',
		1,
		0,
		1,
		0)
END

SELECT @TemplateId = Id FROM MessageTemplate WHERE Name = 'CustomerNotification.RepairRequestSubmitted'
IF (@TemplateId IS NULL)
BEGIN

	INSERT INTO MessageTemplate(
		Name,
		Subject,
		Body,
		IsActive,
		AttachedDownloadId,
		EmailAccountId,
		LimitedToStores)
	VALUES(
		'CustomerNotification.RepairRequestSubmitted',
		'%Store.Name%. Repair Request Submitted',
		'<p><a href="%Store.URL%">%Store.Name%</a> <br /><br />Your repair request was successfully submitted. Details are below:<br /><br />Request ID: %ReturnRequest.ID%<br /><br />Product: %ReturnRequest.Product.Quantity% x Product: %ReturnRequest.Product.Name%<br /><br />Customer comments:<br />%ReturnRequest.CustomerComment%</p>',
		1,
		0,
		1,
		0)
END

GO