﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

:r ./Update_ITEM_CODE_in_Item.sql
GO


:r ./Update_ITEM_CODE_in_ItemInventory.sql
GO

:r ./Populate_AGENCY_in_Item.sql
GO

:r ./PopulateMFG_IC_CODEInManufacturer.sql
GO

:r ./PopulateSTOCK_LOC_IC_CODEInWarehouse.sql
GO