﻿CREATE PROCEDURE [dbo].[AddStoreProducts]
AS

DECLARE @CurrentDateTime datetime = GETUTCDATE()
DECLARE @StoreId int = 0

DECLARE @Category1Name nvarchar(50)
DECLARE @Category2Name nvarchar(50)
DECLARE @ItemCode nvarchar(50)
DECLARE @ItemName nvarchar(50)
DECLARE @ManufacturerCode nvarchar(50)
DECLARE @ManufacturerNumber nvarchar(50)
DECLARE @Price decimal(9,2)
DECLARE @PriceLevel1 decimal(9,2)
DECLARE @PriceLevel2 decimal(9,2)
DECLARE @PriceLevel3 decimal(9,2)
DECLARE @PriceLevel4 decimal(9,2)
DECLARE @ProductId int
DECLARE @UOM nvarchar(25)


DECLARE @PriceLevelRoleNamePrefix nvarchar(25) = 'Price Level '
DECLARE @PriceLevel1CustomerRoleId int
DECLARE @PriceLevel2CustomerRoleId int
DECLARE @PriceLevel3CustomerRoleId int
DECLARE @PriceLevel4CustomerRoleId int

SET @PriceLevel1CustomerRoleId = (SELECT Id FROM [oldhamchem_com].dbo.CustomerRole WHERE [Name] = @PriceLevelRoleNamePrefix + '1')
SET @PriceLevel2CustomerRoleId = (SELECT Id FROM [oldhamchem_com].dbo.CustomerRole WHERE [Name] = @PriceLevelRoleNamePrefix + '2')
SET @PriceLevel3CustomerRoleId = (SELECT Id FROM [oldhamchem_com].dbo.CustomerRole WHERE [Name] = @PriceLevelRoleNamePrefix + '3')
SET @PriceLevel4CustomerRoleId = (SELECT Id FROM [oldhamchem_com].dbo.CustomerRole WHERE [Name] = @PriceLevelRoleNamePrefix + '4')

 --New Products
 DECLARE @NewProducts TABLE(
	RowId int IDENTITY(1,1),
	[ITEM_CODE] [varchar](24) NOT NULL,
	[ITEM_DESC] [varchar](30) NULL,
	[MFGNUM] [varchar](15) NULL,
	[PRICE] [decimal](9, 2) NULL,
	[UOM] [varchar](4) NULL,
	[PROD_TYPE] [varchar](15) NULL,
	[PROD_DESC] [varchar](30) NULL,
	[UNIT_PRICE_1] [decimal](9, 2) NULL,
	[UNIT_PRICE_2] [decimal](8, 2) NULL,
	[UNIT_PRICE_3] [decimal](8, 2) NULL,
	[UNIT_PRICE_4] [decimal](9, 2) NULL,
	[VENDOR] [varchar](6) NULL)

INSERT INTO @NewProducts(
		ITEM_CODE,
		ITEM_DESC,
		MFGNUM,
		PRICE,
		UOM,
		PROD_TYPE,
		PROD_DESC,
		UNIT_PRICE_1,
		UNIT_PRICE_2,
		UNIT_PRICE_3,
		UNIT_PRICE_4,
		VENDOR)
(SELECT
		Item.ITEM_CODE,
		Item.ITEM_DESC,
		Item.MFGNUM,
		Item.UNIT_PRICE_1,
		Item.UOM,
		Item.PROD_TYPE,
		Item.PROD_DESC,
		Item.UNIT_PRICE_1,
		Item.UNIT_PRICE_2,
		Item.UNIT_PRICE_3,
		Item.UNIT_PRICE_4,
		Item.VENDOR
	FROM Item
		LEFT JOIN oldhamchem_com.dbo.Product AS P ON P.Sku = RTRIM(LTRIM(Item.ITEM_CODE))
	WHERE
		P.Sku IS NULL
		AND Item.DISCONTINUED = 'N')

DECLARE @NumberRecords int
DECLARE @RowCount int

SET @NumberRecords = (SELECT MAX(RowId) FROM @NewProducts)
SET @RowCount = 1

WHILE @RowCount <= @NumberRecords
BEGIN

	SELECT
		@ItemCode = RTRIM(LTRIM(ITEM_CODE)),
		@ItemName = RTRIM(LTRIM(ITEM_DESC)),
		@ManufacturerNumber = RTRIM(LTRIM(MFGNUM)),
		@Price = Price,		
		@UOM = RTRIM(LTRIM(UOM)),
		@Category1Name = RTRIM(LTRIM(PROD_TYPE)),
		@Category2Name = RTRIM(LTRIM(PROD_DESC)),
		@PriceLevel1 = UNIT_PRICE_1,
		@PriceLevel2 = UNIT_PRICE_2,
		@PriceLevel3 = UNIT_PRICE_3,
		@PriceLevel4 = UNIT_PRICE_4,
		@ManufacturerCode = RTRIM(LTRIM(VENDOR))
	FROM @NewProducts
	WHERE
		RowId = @RowCount

	-- 1. Insert Product
	BEGIN
		INSERT INTO [oldhamchem_com].[dbo].[Product]
			   ([ProductTypeId]
			   ,[ParentGroupedProductId]
			   ,[VisibleIndividually]
			   ,[Name]
			   ,[ShortDescription]
			   ,[FullDescription]
			   ,[AdminComment]
			   ,[ProductTemplateId]
			   ,[VendorId]
			   ,[ShowOnHomePage]
			   ,[MetaKeywords]
			   ,[MetaDescription]
			   ,[MetaTitle]
			   ,[AllowCustomerReviews]
			   ,[ApprovedRatingSum]
			   ,[NotApprovedRatingSum]
			   ,[ApprovedTotalReviews]
			   ,[NotApprovedTotalReviews]
			   ,[SubjectToAcl]
			   ,[LimitedToStores]
			   ,[Sku]
			   ,[ManufacturerPartNumber]
			   ,[Gtin]
			   ,[IsGiftCard]
			   ,[GiftCardTypeId]
			   ,[OverriddenGiftCardAmount]
			   ,[RequireOtherProducts]
			   ,[RequiredProductIds]
			   ,[AutomaticallyAddRequiredProducts]
			   ,[IsDownload]
			   ,[DownloadId]
			   ,[UnlimitedDownloads]
			   ,[MaxNumberOfDownloads]
			   ,[DownloadExpirationDays]
			   ,[DownloadActivationTypeId]
			   ,[HasSampleDownload]
			   ,[SampleDownloadId]
			   ,[HasUserAgreement]
			   ,[UserAgreementText]
			   ,[IsRecurring]
			   ,[RecurringCycleLength]
			   ,[RecurringCyclePeriodId]
			   ,[RecurringTotalCycles]
			   ,[IsRental]
			   ,[RentalPriceLength]
			   ,[RentalPricePeriodId]
			   ,[IsShipEnabled]
			   ,[IsFreeShipping]
			   ,[ShipSeparately]
			   ,[AdditionalShippingCharge]
			   ,[DeliveryDateId]
			   ,[IsTaxExempt]
			   ,[TaxCategoryId]
			   ,[IsTelecommunicationsOrBroadcastingOrElectronicServices]
			   ,[ManageInventoryMethodId]
			   ,[UseMultipleWarehouses]
			   ,[WarehouseId]
			   ,[StockQuantity]
			   ,[DisplayStockAvailability]
			   ,[DisplayStockQuantity]
			   ,[MinStockQuantity]
			   ,[LowStockActivityId]
			   ,[NotifyAdminForQuantityBelow]
			   ,[BackorderModeId]
			   ,[AllowBackInStockSubscriptions]
			   ,[OrderMinimumQuantity]
			   ,[OrderMaximumQuantity]
			   ,[AllowedQuantities]
			   ,[AllowAddingOnlyExistingAttributeCombinations]
			   ,[DisableBuyButton]
			   ,[DisableWishlistButton]
			   ,[AvailableForPreOrder]
			   ,[PreOrderAvailabilityStartDateTimeUtc]
			   ,[CallForPrice]
			   ,[Price]
			   ,[OldPrice]
			   ,[ProductCost]
			   ,[SpecialCustom]
			   ,[SpecialPrice]
			   ,[SpecialPriceStartDateTimeUtc]
			   ,[SpecialPriceEndDateTimeUtc]
			   ,[CustomerEntersPrice]
			   ,[MinimumCustomerEnteredPrice]
			   ,[MaximumCustomerEnteredPrice]
			   ,[BasepriceEnabled]
			   ,[BasepriceAmount]
			   ,[BasepriceUnitId]
			   ,[BasepriceBaseAmount]
			   ,[BasepriceBaseUnitId]
			   ,[MarkAsNew]
			   ,[MarkAsNewStartDateTimeUtc]
			   ,[MarkAsNewEndDateTimeUtc]
			   ,[HasTierPrices]
			   ,[HasDiscountsApplied]
			   ,[Weight]
			   ,[Length]
			   ,[Width]
			   ,[Height]
			   ,[AvailableStartDateTimeUtc]
			   ,[AvailableEndDateTimeUtc]
			   ,[DisplayOrder]
			   ,[Published]
			   ,[IsPrivate]
			   ,[Deleted]
			   ,[CreatedOnUtc]
			   ,[UpdatedOnUtc])
		 VALUES
			   (5,
			   0,
			   1,
			   dbo.ToProperCase(@ItemName),
			   NULL,
			   NULL,
			   NULL,
			   1,
			   0,
			   0,
			   NULL,
			   NULL,
			   NULL,
			   0,
			   0,
			   0,
			   0,
			   0,
			   0,
			   0,
			   RTRIM(LTRIM(@ItemCode)), --<Sku, nvarchar(400),>
			   RTRIM(LTRIM(@ManufacturerCode)), --,<ManufacturerPartNumber, nvarchar(400),>
			   NULL, --,<Gtin, nvarchar(400),>
			   0, --,<IsGiftCard, bit,>
			   0, --,<GiftCardTypeId, int,>
			   0, --,<OverriddenGiftCardAmount, decimal(18,2),>
			   0, --,<RequireOtherProducts, bit,>
			   NULL, --,<RequiredProductIds, nvarchar(1000),>
			   0, --,<AutomaticallyAddRequiredProducts, bit,>
			   0, --,<IsDownload, bit,>
			   0, --,<DownloadId, int,>
			   0, --,<UnlimitedDownloads, bit,>
			   0, --,<MaxNumberOfDownloads, int,>
			   0, --,<DownloadExpirationDays, int,>
			   0, --,<DownloadActivationTypeId, int,>
			   0, --,<HasSampleDownload, bit,>
			   0, --,<SampleDownloadId, int,>
			   0, --,<HasUserAgreement, bit,>
			   NULL, --,<UserAgreementText, nvarchar(max),>
			   0, --,<IsRecurring, bit,>
			   0, --,<RecurringCycleLength, int,>
			   0, --,<RecurringCyclePeriodId, int,>
			   0, --,<RecurringTotalCycles, int,>
			   0, --,<IsRental, bit,>
			   0, --,<RentalPriceLength, int,>
			   0, --,<RentalPricePeriodId, int,>
			   1, --,<IsShipEnabled, bit,>
			   0, --,<IsFreeShipping, bit,>
			   0, --,<ShipSeparately, bit,>
			   0, --,<AdditionalShippingCharge, decimal(18,4),>
			   0, --,<DeliveryDateId, int,>
			   0, --,<IsTaxExempt, bit,>
			   0, --,<TaxCategoryId, int,>
			   0, --,<IsTelecommunicationsOrBroadcastingOrElectronicServices, bit,>
			   0, --,<ManageInventoryMethodId, int,>
			   0, --,<UseMultipleWarehouses, bit,>
			   0, --,<WarehouseId, int,>
			   0, --,<StockQuantity, int,>
			   0, --,<DisplayStockAvailability, bit,>
			   0, --,<DisplayStockQuantity, bit,>
			   0, --,<MinStockQuantity, int,>
			   0, --,<LowStockActivityId, int,>
			   0, --,<NotifyAdminForQuantityBelow, int,>
			   0, --,<BackorderModeId, int,>
			   0, --,<AllowBackInStockSubscriptions, bit,>
			   1, --,<OrderMinimumQuantity, int,>
			   1000, --,<OrderMaximumQuantity, int,>
			   NULL, --,<AllowedQuantities, nvarchar(1000),>
			   0, --,<AllowAddingOnlyExistingAttributeCombinations, bit,>
			   0, --,<DisableBuyButton, bit,>
			   1, --,<DisableWishlistButton, bit,>
			   0, --,<AvailableForPreOrder, bit,>
			   0, --,<PreOrderAvailabilityStartDateTimeUtc, datetime,>
			   0, --,<CallForPrice, bit,>
			   @Price, --,<Price, decimal(18,4),>
			   0, --,<OldPrice, decimal(18,4),>
			   0, --,<ProductCost, decimal(18,4),>
			   NULL, --,<SpecialCustom, nvarchar(max),>
			   NULL, --,<SpecialPrice, decimal(18,4),>
			   NULL, --,<SpecialPriceStartDateTimeUtc, datetime,>
			   NULL, --,<SpecialPriceEndDateTimeUtc, datetime,>
			   0, --,<CustomerEntersPrice, bit,>
			   0, --,<MinimumCustomerEnteredPrice, decimal(18,4),>
			   0, --,<MaximumCustomerEnteredPrice, decimal(18,4),>
			   0, --,<BasepriceEnabled, bit,>
			   0, --,<BasepriceAmount, decimal(18,4),>
			   0, --,<BasepriceUnitId, int,>
			   0, --,<BasepriceBaseAmount, decimal(18,4),>
			   0, --,<BasepriceBaseUnitId, int,>
			   0, --,<MarkAsNew, bit,>
			   NULL, --,<MarkAsNewStartDateTimeUtc, datetime,>
			   NULL, --,<MarkAsNewEndDateTimeUtc, datetime,>
			   1, --,<HasTierPrices, bit,>
			   0, --,<HasDiscountsApplied, bit,>
			   0, --,<Weight, decimal(18,4),>
			   0, --,<Length, decimal(18,4),>
			   0, --,<Width, decimal(18,4),>
			   0, --,<Height, decimal(18,4),>
			   NULL, --,<AvailableStartDateTimeUtc, datetime,>
			   NULL, --,<AvailableEndDateTimeUtc, datetime,>
			   0, --,<DisplayOrder, int,>
			   1, --,<Published, bit,>
			   0, --,<IsPrivate, bit,>
			   0, --,<Deleted, bit,>
			   @CurrentDateTime, --,<CreatedOnUtc, datetime,>
			   @CurrentDateTime) --,<UpdatedOnUtc, datetime,>)
	END

	SET @ProductId = (SELECT TOP 1 Id FROM oldhamchem_com.dbo.Product WHERE Sku = RTRIM(LTRIM(@ItemCode)))

	IF @ProductId IS NOT NULL
	BEGIN

		-- 2. Add UOM
		BEGIN

			DECLARE @UOMSpecificationAttributeId int = 7
			DECLARE @UOMSpecificationAttributeOptionId int
		
			SET @UOMSpecificationAttributeOptionId = (SELECT TOP 1 Id FROM [oldhamchem_com].dbo.SpecificationAttributeOption WHERE SpecificationAttributeId = @UOMSpecificationAttributeId AND [Name] = @UOM)
		
			INSERT INTO [oldhamchem_com].dbo.Product_SpecificationAttribute_Mapping(
				AttributeTypeId,
				AllowFiltering,
				CustomValue,
				DisplayOrder,			
				ProductId,
				ShowOnProductPage,
				SpecificationAttributeOptionId)
			VALUES(
				0,
				0,
				'',
				0,
				@ProductId,
				0,
				@UOMSpecificationAttributeOptionId)

		END
		
		-- 3. Associate MFG
		BEGIN

			DECLARE @ManufacturerId int

			SET @ManufacturerId = (SELECT TOP 1 Id FROM [oldhamchem_com].dbo.Manufacturer WHERE ManufacturerNumber = @ManufacturerCode)

			IF @ManufacturerId IS NOT NULL
			BEGIN
		
				INSERT INTO [oldhamchem_com].dbo.Product_Manufacturer_Mapping(
					DisplayOrder,
					IsFeaturedProduct,
					ManufacturerId,
					ProductId)
				VALUES (
					0,
					0,
					@ManufacturerId,
					@ProductId)		
							
			END

		END

		-- 4. Associate Category
		BEGIN

			DECLARE @CategoryId int
			SET @CategoryId = (SELECT TOP 1 Id FROM [oldhamchem_com].dbo.Category WHERE [Name] = @Category2Name)

			IF @CategoryId IS NULL
			BEGIN
				SET @CategoryId = (SELECT TOP 1 Id FROM [oldhamchem_com].dbo.Category WHERE [Name] = @Category1Name)
			END

			IF @CategoryId IS NOT NULL
			BEGIN
				
				INSERT INTO [oldhamchem_com].dbo.Product_Category_Mapping(
					CategoryId,
					DisplayOrder,
					IsFeaturedProduct,
					ProductId)
				VALUES(
					@CategoryId,
					0,
					0,
					@ProductId)
							
			END	
				
		END
		
		-- 5. Set Tier Prices
		BEGIN

			INSERT INTO [oldhamchem_com].dbo.TierPrice (
				CustomerRoleId,
				Price,
				ProductId,
				Quantity,
				StoreId)
			VALUES (
				@PriceLevel1CustomerRoleId,
				@PriceLevel1,
				@ProductId,
				1,
				@StoreId)

			INSERT INTO [oldhamchem_com].dbo.TierPrice (
				CustomerRoleId,
				Price,
				ProductId,
				Quantity,
				StoreId)
			VALUES (
				@PriceLevel2CustomerRoleId,
				@PriceLevel2,
				@ProductId,
				1,
				@StoreId)

			INSERT INTO [oldhamchem_com].dbo.TierPrice (
				CustomerRoleId,
				Price,
				ProductId,
				Quantity,
				StoreId)
			VALUES (
				@PriceLevel3CustomerRoleId,
				@PriceLevel3,
				@ProductId,
				1,
				@StoreId)

			INSERT INTO [oldhamchem_com].dbo.TierPrice (
				CustomerRoleId,
				Price,
				ProductId,
				Quantity,
				StoreId)
			VALUES (
				@PriceLevel4CustomerRoleId,
				@PriceLevel4,
				@ProductId,
				1,
				@StoreId)

		END

		-- 6. Set SEO page name
		BEGIN

			DECLARE @Slug nvarchar(50) = REPLACE(@ItemCode, ' ', '-')

			INSERT INTO [oldhamchem_com].dbo.UrlRecord(
				EntityId,
				EntityName,
				IsActive,
				LanguageId,
				Slug)
			VALUES(
				@ProductId,
				'Product',
				1,
				0,
				@Slug)

		END

	END

	SET @RowCount = @RowCount + 1	

END
