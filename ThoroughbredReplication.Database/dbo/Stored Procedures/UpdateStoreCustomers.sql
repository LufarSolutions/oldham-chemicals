﻿CREATE PROCEDURE [dbo].[UpdateStoreCustomers]
AS

DECLARE @PriceLevelRoleNamePrefix nvarchar(25) = 'Price Level '

DECLARE @PriceLevelUpdates TABLE (
	RowId int IDENTITY(1,1),
	CustomerId int,
	CustomerNumber nvarchar(25),
	PriceLevel nvarchar(25)
)

INSERT INTO @PriceLevelUpdates(
	CustomerId,
	CustomerNumber,
	PriceLevel)
(SELECT 
	C.Id,
	RTRIM(LTRIM(CUSTOMER_NO)),
	@PriceLevelRoleNamePrefix + RTRIM(LTRIM(CHEM_PRICE_CODE))
FROM Customer
	INNER JOIN oldhamchem_com.dbo.Customer AS C ON C.CustomerNumber = Customer.CUSTOMER_NO
	INNER JOIN oldhamchem_com.dbo.Customer_CustomerRole_Mapping AS CCRM ON CCRM.Customer_Id = C.Id
	INNER JOIN oldhamchem_com.dbo.CustomerRole AS CR ON CR.Id = CCRM.CustomerRole_Id AND CR.[Name] LIKE @PriceLevelRoleNamePrefix + '%'
WHERE
	RTRIM(LTRIM(Customer.CHEM_PRICE_CODE)) IN ('1', '2', '3', '4')		 
	AND RTRIM(LTRIM(Customer.CHEM_PRICE_CODE)) <> REPLACE(CR.[Name], @PriceLevelRoleNamePrefix, ''))

DECLARE @NumberRecords int
DECLARE @RowCount int

SET @NumberRecords = (SELECT MAX(RowId) FROM @PriceLevelUpdates)
SET @RowCount = 1

WHILE @RowCount <= @NumberRecords
BEGIN

	DECLARE @CustomerId int
	DECLARE @CustomerNumber nvarchar(10)
	DECLARE @CustomerPriceLevel nvarchar(25)

	SELECT 
		@CustomerId = CustomerId,
		@CustomerNumber = CustomerNumber,
		@CustomerPriceLevel = PriceLevel
	FROM @PriceLevelUpdates 
	WHERE
		RowId = @RowCount

	-- Update Price Level
	DECLARE @CurrentPriceLevelCustomerRoleId int

	SET @CurrentPriceLevelCustomerRoleId = (SELECT
			 TOP 1 CustomerRole_Id
		FROM [oldhamchem_com].dbo.Customer_CustomerRole_Mapping AS CCRM
			INNER JOIN [oldhamchem_com].dbo.CustomerRole AS CR ON CR.Id = CCRM.CustomerRole_Id
		WHERE
			CCRM.Customer_Id = @CustomerId
			AND CR.[Name] LIKE @PriceLevelRoleNamePrefix + '%')

	IF @CurrentPriceLevelCustomerRoleId IS NOT NULL
	BEGIN
	
		DELETE FROM [oldhamchem_com].dbo.Customer_CustomerRole_Mapping WHERE Customer_Id = @CustomerId AND CustomerRole_Id = @CurrentPriceLevelCustomerRoleId

	END

	DECLARE @PriceLevelCustomerRoleId int
	
	SELECT
		@PriceLevelCustomerRoleId = Id
	FROM [oldhamchem_com].dbo.CustomerRole
	WHERE
		[Name] = @CustomerPriceLevel

	IF @PriceLevelCustomerRoleId IS NOT NULL
	BEGIN
		
		INSERT INTO [oldhamchem_com].dbo.Customer_CustomerRole_Mapping(
			Customer_Id,
			CustomerRole_Id)
		VALUES (
			@CustomerId,
			@PriceLevelCustomerRoleId)

	END
	
	SET @RowCount = @RowCount + 1
	
END
