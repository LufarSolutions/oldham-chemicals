﻿CREATE PROCEDURE [dbo].[AddAndUpdateStoreAddressesSinceDate]
	@DateSinceLastUpdate datetime
AS

	DECLARE @DateCurrentUpdate datetime = getutcdate()

	DECLARE @NewAddresses TABLE (
		RowId int IDENTITY(1,1),
		CUSTOMER_NO varchar(7) NULL,
		CUST_SHIP_TO_CONTACT varchar(24),
		SHIP_TO_NO varchar(4),
		CUST_SHIP_TO_NAME varchar(24),
		CUST_SHIP_TO_ADDR1 varchar(24),
		CUST_SHIP_TO_ADDR2 varchar(24),
		CUST_SHIP_TO_CITY varchar(24),
		StateProvinceId int,
		CUST_SHIP_TO_ZIP varchar(10)
	)

	INSERT INTO @NewAddresses(
		CUSTOMER_NO,
		CUST_SHIP_TO_CONTACT,
		SHIP_TO_NO,
		CUST_SHIP_TO_NAME,
		CUST_SHIP_TO_ADDR1,
		CUST_SHIP_TO_ADDR2,
		CUST_SHIP_TO_CITY,
		StateProvinceId,
		CUST_SHIP_TO_ZIP)
	(SELECT
		RTRIM(LTRIM(CUSTOMER_NO)),
		dbo.ToProperCase(RTRIM(LTRIM(CUST_SHIP_TO_CONTACT))),
		RTRIM(LTRIM(SHIP_TO_NO)),
		dbo.ToProperCase(RTRIM(LTRIM(CUST_SHIP_TO_NAME))),
		dbo.ToProperCase(RTRIM(LTRIM(CUST_SHIP_TO_ADDR1))),
		dbo.ToProperCase(RTRIM(LTRIM(CUST_SHIP_TO_ADDR2))),
		dbo.ToProperCase(RTRIM(LTRIM(CUST_SHIP_TO_CITY))),
		SP.Id,
		RTRIM(LTRIM(CUST_SHIP_TO_ZIP))
	FROM CustomerAddresses
		INNER JOIN oldhamchem_com.dbo.StateProvince AS SP ON SP.Abbreviation = CustomerAddresses.CUST_SHIP_TO_STATE
	WHERE
		DATE_TIME_CHANGED >= @DateSinceLastUpdate)

	DECLARE @NumberRecords int
	DECLARE @RowCount int

	SET @NumberRecords = @@ROWCOUNT
	SET @RowCount = 1

	WHILE @RowCount <= @NumberRecords
	BEGIN
	
		DECLARE @CustomerNumber nvarchar(10)
		DECLARE @CustomerName nvarchar(max)
		DECLARE @ShipToNumber nvarchar(5)
		DECLARE @Address1 nvarchar(MAX)
		DECLARE @Address2 nvarchar(MAX)
		DECLARE @Company nvarchar(MAX)
		DECLARE @City nvarchar(MAX)
		DECLARE @StateProvinceId int
		DECLARE @ZipPostalCode nvarchar(MAX)
		DECLARE @AddressId int = NULL
		DECLARE @CustomerId int = NULL
		DECLARE @CustomerFirstName nvarchar(MAX) = NULL
		DECLARE @CustomerLastName nvarchar(MAX) = NULL

		SELECT
			@CustomerNumber = CUSTOMER_NO,
			@ShipToNumber = SHIP_TO_NO,
			@CustomerName = CUST_SHIP_TO_CONTACT,
			@Company = CUST_SHIP_TO_NAME,
			@Address1 = CUST_SHIP_TO_ADDR1,
			@Address2 = CUST_SHIP_TO_ADDR2,
			@City = CUST_SHIP_TO_CITY,
			@StateProvinceId = StateProvinceId,
			@ZipPostalCode = CUST_SHIP_TO_ZIP
		FROM @NewAddresses
		WHERE
			RowId = @RowCount

		IF @CustomerName IS NOT NULL
		BEGIN		

			SET @CustomerFirstName = RTRIM(LTRIM(SUBSTRING(@CustomerName, 1, CHARINDEX(' ', @CustomerName))))
			SET @CustomerLastName = RTRIM(LTRIM(SUBSTRING(@CustomerName, CHARINDEX(' ', @CustomerName) + 1, LEN(@CustomerName))))

		END

		SELECT 
			@AddressId = A.Id,
			@CustomerId = Customer.Id
		FROM oldhamchem_com.dbo.[Address] AS A
			INNER JOIN oldhamchem_com.dbo.CustomerAddresses ON A.Id = CustomerAddresses.Address_Id
			INNER JOIN oldhamchem_com.dbo.Customer ON Customer.Id = CustomerAddresses.Customer_Id
		WHERE
			Customer.CustomerNumber = @CustomerNumber
			AND Customer.Deleted = 0
			AND A.ShipToNumber = @ShipToNumber

		-- If the address already exists, update it else add it
		IF @AddressId IS NOT NULL
		BEGIN

			UPDATE oldhamchem_com.dbo.[Address]
			SET
				FirstName = @CustomerFirstName,
				LastName = @CustomerLastName,
				Company = @Company,
				Address1 = @Address1,
				Address2 = @Address2,
				City = @City,
				Latitude = NULL,
				Longitude = NULL,
				StateProvinceId = @StateProvinceId,
				ZipPostalCode = @ZipPostalCode,
				DateUpdated = @DateCurrentUpdate
			WHERE
				Id = @AddressId

		END
		ELSE
		BEGIN
		
			DECLARE @CustomerEmail nvarchar(max)

			SELECT
				@CustomerId = Id,
				@CustomerEmail = Email
			FROM oldhamchem_com.dbo.Customer
			WHERE
				CustomerNumber = @CustomerNumber

			IF @CustomerId IS NOT NULL
			BEGIN

				INSERT INTO oldhamchem_com.dbo.[Address](
					FirstName,
					LastName,
					Email,
					Company,
					Address1,
					Address2,
					City,
					StateProvinceId,
					ZipPostalCode,
					ShipToNumber,
					CreatedOnUtc)
				VALUES (
					@CustomerFirstName,
					@CustomerLastName,
					@CustomerEmail,
					@Company,
					@Address1,
					@Address2,
					@City,
					@StateProvinceId,
					@ZipPostalCode,
					@ShipToNumber,
					@DateCurrentUpdate)
					
				SET @AddressId = SCOPE_IDENTITY()

				INSERT INTO oldhamchem_com.dbo.CustomerAddresses(
					Customer_Id,
					Address_Id)
				VALUES (
					@CustomerId,
					@AddressId)
			
			END

		END

		SET @RowCount = @RowCount + 1

	END

	RETURN 0