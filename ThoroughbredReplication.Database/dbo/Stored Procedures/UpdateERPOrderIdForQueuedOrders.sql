﻿CREATE PROCEDURE [dbo].[UpdateERPOrderIdForQueuedOrders]
AS

	UPDATE OnlineOrder
	SET
		OnlineOrder.ERPOrderId = OrderQueue.OrderNumber
	FROM
		[oldhamchem_com].dbo.[Order] AS OnlineOrder
		INNER JOIN OrderQueue ON OrderQueue.OnlineOrderNumber = OnlineOrder.Id
	WHERE
		OnlineOrder.DateQueuedForERP IS NOT NULL
		AND OnlineOrder.ERPOrderId IS NULL
		
RETURN 0
