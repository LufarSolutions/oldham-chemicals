﻿CREATE PROCEDURE [dbo].[UpdateStoreProductPrices]
AS

	DECLARE @PriceLevelRoleNamePrefix nvarchar(25) = 'Price Level '
	DECLARE @ProductId int
	DECLARE @TierPriceId int
	DECLARE @PriceLevelName nvarchar(25)
	DECLARE @Tier1Price decimal(9,2)
	DECLARE @Tier2Price decimal(9,2)
	DECLARE @Tier3Price decimal(9,2)
	DECLARE @Tier4Price decimal(9,2)

	DECLARE @ProductPrices TABLE(
		RowId int IDENTITY(1,1),
		ProductId int,
		TierPriceId int,
		PriceLevelName nvarchar(25),
		[UNIT_PRICE_1] [decimal](9, 2) NULL,
		[UNIT_PRICE_2] [decimal](8, 2) NULL,
		[UNIT_PRICE_3] [decimal](8, 2) NULL,
		[UNIT_PRICE_4] [decimal](9, 2) NULL)

	INSERT INTO @ProductPrices(
		ProductId,
		TierPriceId,
		PriceLevelName,
		UNIT_PRICE_1,
		UNIT_PRICE_2,
		UNIT_PRICE_3,
		UNIT_PRICE_4)
	(SELECT
			P.Id,
			TP.Id,
			CR.[Name],
			Item.UNIT_PRICE_1,
			Item.UNIT_PRICE_2,
			Item.UNIT_PRICE_3,
			Item.UNIT_PRICE_4
		FROM Item
			INNER JOIN oldhamchem_com.dbo.Product AS P ON P.Sku = Item.ITEM_CODE
			INNER JOIN oldhamchem_com.dbo.TierPrice AS TP ON TP.ProductId = P.Id
			INNER JOIN oldhamchem_com.dbo.CustomerRole AS CR ON CR.Id = TP.CustomerRoleId
		WHERE
			(CR.[Name] = @PriceLevelRoleNamePrefix + '1' AND TP.Price <> UNIT_PRICE_1)
			OR (CR.[Name] = @PriceLevelRoleNamePrefix + '2' AND TP.Price <> UNIT_PRICE_2)
			OR (CR.[Name] = @PriceLevelRoleNamePrefix + '3' AND TP.Price <> UNIT_PRICE_3)
			OR (CR.[Name] = @PriceLevelRoleNamePrefix + '4' AND TP.Price <> UNIT_PRICE_4))


	
	DECLARE @NumberRecords int
	DECLARE @RowCount int

	SET @NumberRecords = (SELECT MAX(RowId) FROM @ProductPrices)
	SET @RowCount = 1

	WHILE @RowCount <= @NumberRecords
	BEGIN

		SELECT
			@ProductId = ProductId,
			@TierPriceId = TierPriceId,
			@PriceLevelName = PriceLevelName,
			@Tier1Price = UNIT_PRICE_1,
			@Tier2Price = UNIT_PRICE_2,
			@Tier3Price = UNIT_PRICE_3,
			@Tier4Price = UNIT_PRICE_4
		FROM @ProductPrices
		WHERE
			RowId = @RowCount			
		
		DECLARE @TierPrice decimal(9,2)
				
		IF @PriceLevelName = @PriceLevelRoleNamePrefix + '1'
		BEGIN
			SET @TierPrice = @Tier1Price
		END
		ELSE IF @PriceLevelName = @PriceLevelRoleNamePrefix + '2'
		BEGIN
			SET @TierPrice = @Tier2Price
		END
		ELSE IF @PriceLevelName = @PriceLevelRoleNamePrefix + '3'
		BEGIN
			SET @TierPrice = @Tier3Price
		END
		ELSE IF @PriceLevelName = @PriceLevelRoleNamePrefix + '4'
		BEGIN
			SET @TierPrice = @Tier4Price
		END

		UPDATE [oldhamchem_com].dbo.TierPrice
		SET
			Price = @TierPrice
		WHERE
			Id = @TierPriceId
	
		-- Update the product price for customers without tier pricing
		IF @Tier1Price IS NOT NULL
		BEGIN		
			UPDATE oldhamchem_com.dbo.Product SET Price = @Tier1Price WHERE Id = @ProductId
		END
		
		SET @RowCount = @RowCount + 1

	END
	
	UPDATE oldhamchem_com.dbo.Product SET SpecialPrice = NULL WHERE SpecialPrice IS NOT NULL