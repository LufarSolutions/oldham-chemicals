﻿
CREATE PROCEDURE [dbo].[UpdateStoreContractPrices]
AS

	BEGIN -- Add New Contract Prices

		DECLARE @DateCreated datetime = GETUTCDATE()

		BEGIN -- Add Individual Contract Prices
	
			INSERT INTO oldhamchem_com.dbo.ContractPrice(
				DateCreated,
				ProductId,
				StoreId,
				CustomerId,
				Quantity,
				Price)
			(SELECT
				@DateCreated,
				Product.Id,
				1,
				Customer.Id,
				1,
				CASE WHEN CHARINDEX('AGENCY', MFGNUM, 0) > 0 THEN Item.UNIT_PRICE_1 ELSE CP.CONTRACT_PRICE END
			FROM ContractPrice AS CP
				INNER JOIN Item ON Item.ITEM_CODE = CP.ITEM_CODE
				INNER JOIN oldhamchem_com.dbo.Customer ON CP.CUSTOMER_NO = Customer.CustomerNumber
				INNER JOIN oldhamchem_com.dbo.Product ON CP.ITEM_CODE = Product.Sku
				LEFT JOIN oldhamchem_com.dbo.ContractPrice ON ContractPrice.ProductId = Product.Id AND ContractPrice.CustomerId = Customer.Id
			WHERE
				ContractPrice.Price IS NULL)
	
		END

		BEGIN -- Add Shared Contract Prices

			INSERT INTO oldhamchem_com.dbo.ContractPrice(
				DateCreated,
				ProductId,
				StoreId,
				CustomerId,
				Quantity,
				Price)
			(SELECT
				@DateCreated,
				Product.Id,
				1,
				Customer.Id,
				1,
				CASE WHEN CHARINDEX('AGENCY', MFGNUM, 0) > 0 THEN Item.UNIT_PRICE_1 ELSE CP.CONTRACT_PRICE END
			FROM ContractPrice AS CP
				INNER JOIN Item ON Item.ITEM_CODE = CP.ITEM_CODE
				INNER JOIN Customer AS C ON CP.CUSTOMER_NO = C.CONTRACT_CUSTOMER_NO
				INNER JOIN oldhamchem_com.dbo.Customer ON C.CUSTOMER_NO = Customer.CustomerNumber
				INNER JOIN oldhamchem_com.dbo.Product ON CP.ITEM_CODE = Product.Sku
				LEFT JOIN oldhamchem_com.dbo.ContractPrice ON ContractPrice.ProductId = Product.Id AND ContractPrice.CustomerId = Customer.Id
			WHERE 
				CONTRACT_CUSTOMER_NO IS NOT NULL
				AND ContractPrice.Price IS NULL)

		END

	END
	
	BEGIN -- Update Current Contract Prices

		BEGIN -- Update Individual Contract Prices

			DECLARE @DateUpdated datetime = GETUTCDATE()

			UPDATE oldhamchem_com.dbo.ContractPrice
			SET
				DateUpdated = @DateUpdated,
				Price = CASE WHEN CHARINDEX('AGENCY', MFGNUM, 0) > 0 THEN Item.UNIT_PRICE_1 ELSE CP.CONTRACT_PRICE END
			--SELECT 
			--	@DateUpdated,
			--	CP.CONTRACT_PRICE
			FROM ContractPrice AS CP
				INNER JOIN Item ON Item.ITEM_CODE = CP.ITEM_CODE
				INNER JOIN oldhamchem_com.dbo.Customer ON CP.CUSTOMER_NO = Customer.CustomerNumber
				INNER JOIN oldhamchem_com.dbo.Product ON CP.ITEM_CODE = Product.Sku
				LEFT JOIN oldhamchem_com.dbo.ContractPrice ON ContractPrice.ProductId = Product.Id AND ContractPrice.CustomerId = Customer.Id
			WHERE
				CP.CONTRACT_PRICE <> ContractPrice.Price
				OR (Item.MFGNUM LIKE '%agency%' AND cp.CONTRACT_PRICE <> Item.UNIT_PRICE_1)

		END			

		BEGIN -- Update Shared Contract Prices

			UPDATE oldhamchem_com.dbo.ContractPrice
			SET
				DateUpdated = @DateUpdated,
				Price = CASE WHEN CHARINDEX('AGENCY', MFGNUM, 0) > 0 THEN Item.UNIT_PRICE_1 ELSE CP.CONTRACT_PRICE END
			FROM ContractPrice AS CP
				INNER JOIN Item ON Item.ITEM_CODE = CP.ITEM_CODE
				INNER JOIN Customer AS C ON CP.CUSTOMER_NO = C.CONTRACT_CUSTOMER_NO
				INNER JOIN oldhamchem_com.dbo.Customer ON C.CUSTOMER_NO = Customer.CustomerNumber
				INNER JOIN oldhamchem_com.dbo.Product ON CP.ITEM_CODE = Product.Sku
				LEFT JOIN oldhamchem_com.dbo.ContractPrice ON ContractPrice.ProductId = Product.Id AND ContractPrice.CustomerId = Customer.Id
			WHERE
				CONTRACT_CUSTOMER_NO IS NOT NULL
				AND CP.CONTRACT_PRICE <> ContractPrice.Price
				OR (Item.MFGNUM LIKE '%agency%' AND cp.CONTRACT_PRICE <> Item.UNIT_PRICE_1)

		END

	END
GO