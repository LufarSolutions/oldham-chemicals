﻿CREATE PROCEDURE [dbo].[UpdateShipments]
AS
BEGIN
	DECLARE @ShipmentUpdates TABLE (
		RowId int IDENTITY(1,1),
		InvoiceId nvarchar(7),
		LineNumber nvarchar(3),
		OrderId int,
		ShippingMethod nvarchar(max),
		TrackingNumber nvarchar(max),
		DateShipped datetime
	)

	INSERT INTO @ShipmentUpdates(
		InvoiceId,
		LineNumber,
		TrackingNumber,
		DateShipped)
	(SELECT 
		 RTRIM(LTRIM(INV_NO)),
		 RTRIM(LTRIM(LINE_NO)),
		 RTRIM(LTRIM(TRACKING_NUMBER)),
		 SHIP_DATE
	FROM InvoiceTracking
	WHERE
		INV_NO NOT LIKE 'D%'
		AND LINE_NO = '001' --Only process 1 tracking number per invoice
		AND DATE_TIME_ADDED >= '9/11/2023'
		AND DATE_TIME_PROCESSED IS NULL)

	DECLARE @NumberRecords int
	DECLARE @RowCount int

	SET @NumberRecords = (SELECT MAX(RowId) FROM @ShipmentUpdates)
	SET @RowCount = 1

	WHILE @RowCount <= @NumberRecords
	BEGIN
		DECLARE @InvoiceId nvarchar(7)
		DECLARE @OrderNumber nvarchar(25)
		DECLARE @LineNumber nvarchar(3)
		DECLARE @OrderId int
		DECLARE @DateShipped datetime
		DECLARE @TrackingNumber nvarchar(max)
		DECLARE @ShippingMethod nvarchar(max)

		SET @InvoiceId = (SELECT InvoiceId FROM @ShipmentUpdates WHERE RowId = @RowCount)

		SET @OrderNumber = (SELECT RTRIM(LTRIM(ORDER_NO)) FROM Invoice WHERE INV_NO = @InvoiceId)
		
		SET @OrderId = (SELECT Id FROM oldhamchem_com.dbo.[Order] WHERE Deleted = 0 AND ERPOrderId = @OrderNumber)

		SET @LineNumber = (SELECT LineNumber FROM @ShipmentUpdates WHERE RowId = @RowCount)
		SET @DateShipped = (SELECT DateShipped FROM @ShipmentUpdates WHERE RowId = @RowCount)
		SET @TrackingNumber = (SELECT TrackingNumber FROM @ShipmentUpdates WHERE RowId = @RowCount)
		SET @ShippingMethod = (SELECT SHIP_VIA FROM Invoice WHERE INV_NO = @InvoiceId)

		IF @OrderId IS NOT NULL
		BEGIN
			-- Add the shipment
			INSERT INTO oldhamchem_com.dbo.Shipment (
				OrderId,
				ErpInvoiceId,
				TrackingNumber,
				ShippedDateUtc,
				CreatedOnUtc)
			VALUES(
				@OrderId,
				@InvoiceId,
				@TrackingNumber,
				@DateShipped,
				GETUTCDATE())

			DECLARE @ShipmentId int
			SET @ShipmentId = (SELECT id FROM oldhamchem_com.dbo.Shipment WHERE TrackingNumber = @TrackingNumber AND ErpInvoiceId = @InvoiceId)
			
			--Update order shipping method and status
			UPDATE oldhamchem_com.dbo.[Order] SET ShippingMethod = @ShippingMethod, ShippingStatusId = 30 WHERE Id = @OrderId

			--Update the ERP InvoiceTracking table to indicate it was processed.
			UPDATE InvoiceTracking SET DATE_TIME_PROCESSED = GETUTCDATE() WHERE INV_NO = @InvoiceId AND LINE_NO = @LineNumber
		END
		ELSE IF DATEDIFF(day, @DateShipped, GETUTCDATE()) >= 30
		BEGIN
			-- Update the ERP InvoiceTracking table to indicate it was processed after 30 days of not matching an order.
			UPDATE InvoiceTracking SET DATE_TIME_PROCESSED = GETUTCDATE() WHERE INV_NO = @InvoiceId AND LINE_NO = @LineNumber
		END

		SET @RowCount = @RowCount + 1
	END
END
GO


