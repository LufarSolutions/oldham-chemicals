﻿CREATE PROCEDURE [dbo].[UpdateStoreAddressesShipToNumberWhereNULL]
AS

	-- UPDATE Customer Addresseses WHERE ShipToNumber IS NULL
	UPDATE A
	SET
		A.ShipToNumber = TCA.SHIP_TO_NO
	FROM oldhamchem_com.dbo.[Address] AS A
			INNER JOIN oldhamchem_com.dbo.CustomerAddresses ON CustomerAddresses.Address_Id = A.Id	
			INNER JOIN oldhamchem_com.dbo.Customer ON CustomerAddresses.Customer_Id = Customer.Id
			INNER JOIN oldhamchem_com.dbo.StateProvince ON StateProvince.Id = A.StateProvinceId
			LEFT JOIN CustomerAddresses AS TCA ON TCA.CUST_SHIP_TO_ADDR1 = A.Address1
				AND TCA.CUST_SHIP_TO_CITY = A.City
				AND TCA.CUST_SHIP_TO_STATE = StateProvince.Abbreviation
				AND TCA.CUSTOMER_NO = Customer.CustomerNumber
	WHERE
		A.ShipToNumber IS NULL
		AND TCA.SHIP_TO_NO IS NOT NULL

	-- UPDATE Billing Addresseses WHERE ShipToNumber IS NULL
	UPDATE BA
	SET
		BA.ShipToNumber = TCA.SHIP_TO_NO
	FROM oldhamchem_com.dbo.[Address] AS BA
		INNER JOIN oldhamchem_com.dbo.[Order] ON [Order].BillingAddressId = BA.Id
		INNER JOIN oldhamchem_com.dbo.Customer ON [Order].CustomerId = Customer.Id
		INNER JOIN oldhamchem_com.dbo.StateProvince ON StateProvince.Id = BA.StateProvinceId
			LEFT JOIN CustomerAddresses AS TCA ON TCA.CUST_SHIP_TO_ADDR1 = BA.Address1
				AND TCA.CUST_SHIP_TO_CITY = BA.City
				AND TCA.CUST_SHIP_TO_STATE = StateProvince.Abbreviation
				AND TCA.CUSTOMER_NO = Customer.CustomerNumber
	WHERE
		BA.ShipToNumber IS NULL
		AND TCA.SHIP_TO_NO IS NOT NULL

	-- UPDATE Shipping Addresseses WHERE ShipToNumber IS NULL
	UPDATE SA
	SET
		SA.ShipToNumber = TCA.SHIP_TO_NO
	FROM oldhamchem_com.dbo.[Address] AS SA
		INNER JOIN oldhamchem_com.dbo.[Order] ON [Order].ShippingAddressId = SA.Id
		INNER JOIN oldhamchem_com.dbo.Customer ON [Order].CustomerId = Customer.Id
		INNER JOIN oldhamchem_com.dbo.StateProvince ON StateProvince.Id = SA.StateProvinceId
			LEFT JOIN CustomerAddresses AS TCA ON TCA.CUST_SHIP_TO_ADDR1 = SA.Address1
				AND TCA.CUST_SHIP_TO_CITY = SA.City
				AND TCA.CUST_SHIP_TO_STATE = StateProvince.Abbreviation
				AND TCA.CUSTOMER_NO = Customer.CustomerNumber
	WHERE
		SA.ShipToNumber IS NULL
		AND TCA.SHIP_TO_NO IS NOT NULL

RETURN 0
