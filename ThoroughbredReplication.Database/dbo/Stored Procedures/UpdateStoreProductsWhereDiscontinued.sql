﻿CREATE PROCEDURE [dbo].[UpdateStoreProductsWhereDiscontinued]

AS

	UPDATE oldhamchem_com.dbo.Product
	SET
		Published = 0
	WHERE
		Id IN (
			SELECT
				DISTINCT P.Id
				--Item.*
				--(SELECT MP.Sku FROM oldhamchem_com.dbo.Product AS MP WHERE LEFT(MP.Sku, LEN(MP.Sku) - CHARINDEX('-', REVERSE(MP.Sku) + '-')) = LEFT(Item.ITEM_CODE, LEN(Item.ITEM_CODE) - CHARINDEX('-', REVERSE(Item.ITEM_CODE) + '-')))	
			FROM Item
				INNER JOIN oldhamchem_com.dbo.Product AS P ON P.Sku = RTRIM(LTRIM(Item.ITEM_CODE))
			WHERE
				Item.DISCONTINUED = 'Y'
				AND P.Published = 1
		)
