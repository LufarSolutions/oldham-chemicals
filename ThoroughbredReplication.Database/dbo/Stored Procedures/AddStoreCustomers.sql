﻿CREATE PROCEDURE [dbo].[AddStoreCustomers]
AS

DECLARE @NewCustomers TABLE (
	RowId int IDENTITY(1,1),
	CUSTOMER_NO varchar(7) NOT NULL,
	CHEM_PRICE_CODE varchar(1) NULL,
	CUST_NAME varchar(24) NULL,
	CUST_CONTACT varchar(24) NULL,
	CUST_ADDR1 varchar(24) NULL,
	CUST_CITY varchar(24) NULL,
	CUST_STATE varchar(2) NULL,
	CUST_ZIP varchar(10) NULL,
	CUST_PHONE varchar(10) NULL
)

INSERT INTO @NewCustomers(
	CUSTOMER_NO,
	CHEM_PRICE_CODE,
	CUST_NAME,
	CUST_CONTACT,
	CUST_ADDR1,
	CUST_CITY,
	CUST_STATE,
	CUST_ZIP,
	CUST_PHONE)
(SELECT 
		CUSTOMER_NO,
		CHEM_PRICE_CODE,
		CUST_NAME,
		CUST_CONTACT,
		CUST_ADDR1,
		CUST_CITY,
		CUST_STATE,
		CUST_ZIP,
		CUST_PHONE
	FROM Customer
		LEFT JOIN oldhamchem_com.dbo.Customer AS C ON C.CustomerNumber = Customer.CUSTOMER_NO
	WHERE
		C.Id IS NULL)

DECLARE @NumberRecords int
DECLARE @RowCount int

SET @NumberRecords = @@ROWCOUNT
SET @RowCount = 1

WHILE @RowCount <= @NumberRecords
BEGIN

	DECLARE @CustomerGuid uniqueidentifier = NEWID()
	DECLARE @SaltForPassword varchar(25) = 'watman3798'
	DECLARE @CurrentDateAndTime datetime = GETUTCDATE()
	DECLARE @CustomerNumber nvarchar(10)
	DECLARE @CustomerPriceLevel nvarchar(10)
	DECLARE @Company nvarchar(50)
	DECLARE @CustomerName nvarchar(50)
	DECLARE @StreetAddress nvarchar(50)
	DECLARE @City nvarchar(50)
	DECLARE @State nvarchar(50)
	DECLARE @ZipCode nvarchar(50)
	DECLARE @PhoneNumber nvarchar(50)

	SELECT
		@CustomerNumber = CUSTOMER_NO,
		@CustomerPriceLevel = CHEM_PRICE_CODE, 
		@Company = CUST_CONTACT,
		@CustomerName = CUST_NAME,
		@StreetAddress = CUST_ADDR1,
		@City = CUST_CITY, 
		@State = CUST_STATE,
		@ZipCode = CUST_ZIP,
		@PhoneNumber = CUST_PHONE
	FROM @NewCustomers
	WHERE
		RowId = @RowCount

	INSERT INTO oldhamchem_com.dbo.Customer(
		CustomerGuid,
		CustomerNumber,
		Username,
		Email,
		[Password],
		PasswordFormatId,
		PasswordSalt,
		AdminComment,
		IsTaxExempt,
		AffiliateId,
		VendorId,
		HasShoppingCartItems,
		Active,
		Deleted,
		IsSystemAccount,
		SystemName,
		LastIpAddress,
		CreatedOnUtc,
		LastLoginDateUtc,
		LastActivityDateUtc,
		BillingAddress_Id,
		ShippingAddress_Id,
		PictureId)
	(SELECT
		@CustomerGuid,
		Customer.CUSTOMER_NO,
		(Customer.CUSTOMER_NO + '@oldhamchem.com'),
		Customer.EMAIL,
		[oldhamchem_com].dbo.GenerateSHA1HashedPassword(Customer.CUSTOMER_NO, @SaltForPassword),
		1,
		@SaltForPassword,
		NULL,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		NULL,
		NULL,
		@CurrentDateAndTime,
		NULL,
		@CurrentDateAndTime,
		NULL,
		NULL,
		0
	FROM Customer
		LEFT JOIN oldhamchem_com.dbo.Customer AS C ON C.CustomerNumber = Customer.CUSTOMER_NO
	WHERE
		Customer.CUSTOMER_NO = @CustomerNumber)

	DECLARE @CustomerId int
	DECLARE @RegisteredCustomerRoleId int
	DECLARE @PriceLevelCustomerRoleId int
	DECLARE @PriceLevelRoleName nvarchar(255) = 'Price Level ' + LTRIM(RTRIM(@CustomerPriceLevel))

	SELECT @CustomerId = Id FROM [oldhamchem_com].dbo.Customer WHERE CustomerGuid = @CustomerGuid

	-- Setup Attributes and Roles
	IF @CustomerId IS NOT NULL
	BEGIN
		
		SELECT @RegisteredCustomerRoleId =  (SELECT Id FROM [oldhamchem_com].dbo.CustomerRole WHERE [Name] = 'Registered')

		IF @RegisteredCustomerRoleId IS NOT NULL
		BEGIN

			INSERT INTO [oldhamchem_com].dbo.Customer_CustomerRole_Mapping(
				Customer_Id,
				CustomerRole_Id)
			VALUES (
				@CustomerId,
				@RegisteredCustomerRoleId)

		END

		SELECT
			@PriceLevelCustomerRoleId = Id
		FROM [oldhamchem_com].dbo.CustomerRole
		WHERE
			[Name] = @PriceLevelRoleName

		IF @PriceLevelCustomerRoleId IS NOT NULL
		BEGIN
		
			INSERT INTO [oldhamchem_com].dbo.Customer_CustomerRole_Mapping(
				Customer_Id,
				CustomerRole_Id)
			VALUES (
				@CustomerId,
				@PriceLevelCustomerRoleId)

		END
		

		-- Company Name
		IF @Company IS NOT NULL
			INSERT INTO [oldhamchem_com].dbo.GenericAttribute (EntityId, KeyGroup, [Key], [Value], StoreId) VALUES (@CustomerId, 'Customer', 'Company', dbo.ToProperCase(RTRIM(LTRIM(@Company))), 0)
		
		-- Name
		IF @CustomerName IS NOT NULL
		BEGIN
			DECLARE @SpaceIndex int = CHARINDEX(' ', @CustomerName)
			DECLARE @NameLength int = LEN(@CustomerName)

			IF @SpaceIndex > 0
			BEGIN
				DECLARE @FirstName nvarchar(100) = dbo.ToProperCase(RTRIM(LTRIM(SUBSTRING(@CustomerName, 0, @SpaceIndex - 1))))
				DECLARE @LastName nvarchar(100) = dbo.ToProperCase(RTRIM(LTRIM(SUBSTRING(@CustomerName, @SpaceIndex + 1, @NameLength - @SpaceIndex))))

				IF @FirstName IS NOT NULL
					INSERT INTO [oldhamchem_com].dbo.GenericAttribute (EntityId, KeyGroup, [Key], [Value], StoreId) VALUES (@CustomerId, 'Customer', 'FirstName', @FirstName, 0)

				IF @LastName IS NOT NULL
					INSERT INTO [oldhamchem_com].dbo.GenericAttribute (EntityId, KeyGroup, [Key], [Value], StoreId) VALUES (@CustomerId, 'Customer', 'LastName', @LastName, 0)
			END
		END

		IF @StreetAddress IS NOT NULL
		BEGIN
			DECLARE @StateProvinceId int = (SELECT Id FROM [oldhamchem_com].dbo.StateProvince WHERE Abbreviation = RTRIM(LTRIM(@State)))
			
			IF @StreetAddress IS NOT NULL
				INSERT INTO [oldhamchem_com].dbo.GenericAttribute (EntityId, KeyGroup, [Key], [Value], StoreId) VALUES (@CustomerId, 'Customer', 'StreetAddress', dbo.ToProperCase(RTRIM(LTRIM(@StreetAddress))), 0)

			IF @City IS NOT NULL
				INSERT INTO [oldhamchem_com].dbo.GenericAttribute (EntityId, KeyGroup, [Key], [Value], StoreId) VALUES (@CustomerId, 'Customer', 'City', dbo.ToProperCase(RTRIM(LTRIM(@City))), 0)

			IF @ZipCode IS NOT NULL
				INSERT INTO [oldhamchem_com].dbo.GenericAttribute (EntityId, KeyGroup, [Key], [Value], StoreId) VALUES (@CustomerId, 'Customer', 'ZipPostalCode', RTRIM(LTRIM(@ZipCode)), 0)

			IF @StateProvinceId IS NOT NULL
				INSERT INTO [oldhamchem_com].dbo.GenericAttribute (EntityId, KeyGroup, [Key], [Value], StoreId) VALUES (@CustomerId, 'Customer', 'StateProvinceId', @StateProvinceId, 0)

			INSERT INTO [oldhamchem_com].dbo.GenericAttribute (EntityId, KeyGroup, [Key], [Value], StoreId) VALUES (@CustomerId, 'Customer', 'CountryId', 1, 0)
		END

		IF @PhoneNumber IS NOT NULL
		BEGIN
			INSERT INTO [oldhamchem_com].dbo.GenericAttribute (EntityId, KeyGroup, [Key], [Value], StoreId) VALUES (@CustomerId, 'Customer', 'Phone', RTRIM(LTRIM(@PhoneNumber)), 0)
		END
	END
	
	SET @RowCount = @RowCount + 1

END