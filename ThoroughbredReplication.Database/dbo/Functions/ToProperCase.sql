﻿CREATE FUNCTION [dbo].[ToProperCase]
(
	@Text nvarchar(MAX)
)
RETURNS nvarchar(MAX)
AS
BEGIN

	declare @Reset bit;
	declare @Ret nvarchar(MAX);
	declare @i int;
	declare @c char(1);

	IF @Text IS NOT NULL
	BEGIN

		select @Reset = 1, @i=1, @Ret = '';

		while (@i <= len(@Text))
			select @c= substring(@Text,@i,1),
						@Ret = @Ret + case when @Reset=1 then UPPER(@c) else LOWER(@c) end,
						@Reset = case when @c like '[a-zA-Z]' then 0 else 1 end,
						@i = @i +1

	END
	
	return @Ret

END
