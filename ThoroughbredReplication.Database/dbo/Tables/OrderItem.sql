﻿CREATE TABLE [dbo].[OrderItem](
	[ORDER_NO] [varchar](7) NULL,
	[LINE_NO] [varchar](3) NULL,
	[LINE_TYPE] [varchar](1) NULL,
	[LINE_MESSAGE] [varchar](30) NULL,
	[UOM] [varchar](4) NULL,
	[PRICE_CODE_CHEM] [varchar](1) NULL,
	[PRICE_CODE_EQUIP] [varchar](1) NULL,
	[TAXABLE] [varchar](1) NULL,
	[COMMISIONABLE] [varchar](1) NULL,
	[DROP_SHIP] [varchar](1) NULL,
	[VEND_NO] [varchar](6) NULL,
	[VEND_INDEX] [varchar](5) NULL,
	[LOCATION] [varchar](10) NULL,
	[PROD_CODE] [varchar](3) NULL,
	[GL_CODE] [varchar](3) NULL,
	[LOT_CONTROL] [varchar](1) NULL,
	[BOM_TYPE] [varchar](1) NULL,
	[KIT_FLAG] [varchar](1) NULL,
	[PROD_CODE_2] [varchar](3) NULL,
	[CHEM] [varchar](1) NULL,
	[RESTRICTED] [varchar](1) NULL,
	[PROD_CODE_3] [varchar](3) NULL,
	[ITEM_CODE] [varchar](24) NULL,
	[WHSE_CODE] [varchar](3) NULL,
	[ORDER_TODAY] [decimal](8, 1) NULL,
	[SHIP_TODAY] [decimal](8, 1) NULL,
	[BACKORD_TODAY] [decimal](8, 0) NULL,
	[UNIT_PRICE] [decimal](10, 2) NULL,
	[EXT_PRICE] [decimal](10, 2) NULL,
	[UNIT_COST] [decimal](10, 2) NULL,
	[DISC_RATE] [decimal](6, 2) NULL,
	[ORDERED] [decimal](9, 0) NULL,
	[COMM_RATE] [decimal](5, 2) NULL,
	[EXT_COST] [decimal](13, 2) NULL,
	[NUM_UOM] [decimal](5, 0) NULL,
	[DATE_TIME_CHANGED] [datetime] NULL
) ON [PRIMARY]