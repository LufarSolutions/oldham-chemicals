﻿CREATE TABLE [dbo].[Invoice] (
    [INV_NO]          VARCHAR (7)     NOT NULL,
    [CUST_NO]         VARCHAR (7)     NULL,
    [INV_DATE]        DATETIME    NULL,
    [ORDER_NO]        VARCHAR (7)     NULL,
    [TERM_DESC]       VARCHAR (28)    NULL,
    [DUE_DATE]        DATETIME    NULL,
    [WAREHOUSE]       VARCHAR (3)     NULL,
	[CUST_PO_NUM] varchar(20) NULL,
    [INVOICE_AMT]     DECIMAL (10, 2) NULL,
    [INVOICE_BALANCE] DECIMAL (10, 2) NULL,
    [SALES_TAX_AMT]   DECIMAL (9, 2)  NULL,
    [FREIGHT_AMT]     DECIMAL (9, 2)  NULL, 
	[SHIP_TO_CODE] [varchar](4) NULL,
	[SHIP_VIA] [varchar](10) NULL,
	[ORDER_DATE] [datetime] NULL,
	[SHIP_DATE] [datetime] NULL,
	[SHIP_TO_NAME] [varchar](24) NULL,
	[SHIP_TO_ADDR1] [varchar](24) NULL,
	[SHIP_TO_ADDR2] [varchar](24) NULL,
	[SHIP_TO_ADDR3] [varchar](24) NULL,
	[SHIP_TO_ADDR4] [varchar](24) NULL,
	[SHIP_TO_ADDR5] [varchar](24) NULL,
    [SHIP_TO_CITY] [varchar](24) NULL,
    [SHIP_TO_STATE] [varchar](2) NULL,
    [SHIP_TO_ZIP] [varchar](10) NULL,
	[TRACKING_NUMBER] [varchar](25) NULL,
    [DATE_TIME_ADDED] DATETIME NULL DEFAULT GETUTCDATE(),
    [DATE_TIME_CHANGED] DATETIME NULL
    CONSTRAINT [PK_Invoice] PRIMARY KEY ([INV_NO])
);


GO
--CREATE UNIQUE CLUSTERED INDEX [I_Invoice_0]
--    ON [dbo].[Invoice]([INV_NO] ASC);

