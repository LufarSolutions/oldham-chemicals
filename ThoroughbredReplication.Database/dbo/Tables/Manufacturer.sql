﻿CREATE TABLE [dbo].[Manufacturer] (
    [VEND_NO]				VARCHAR (6)		NOT NULL,
    [VEND_NAME]				VARCHAR (24)	NULL,
    [VEND_ADDR1]			VARCHAR (24)	NULL,
    [VEND_ADDR2]			VARCHAR (24)	NULL,
    [VEND_CITY]				VARCHAR (24)	NULL,
    [VEND_STATE]			VARCHAR (2)		NULL,
    [VEND_ZIP]				VARCHAR (10)	NULL, 
	[MFG_IC_CODE]		VARCHAR (20)	NULL,
    [DATE_TIME_ADDED]		DATETIME		NULL DEFAULT GETUTCDATE(),
    [DATE_TIME_CHANGED]		DATETIME		NULL
    CONSTRAINT [PK_Manufacturer] PRIMARY KEY ([VEND_NO])
);


GO
--CREATE UNIQUE CLUSTERED INDEX [I_Manufacturer_0]
--    ON [dbo].[Manufacturer]([VEND_NO] ASC);

