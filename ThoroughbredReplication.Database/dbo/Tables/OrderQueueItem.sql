﻿CREATE TABLE [dbo].[OrderQueueItem]
(
	[OnlineOrderNumber]		VARCHAR(10) NOT NULL,
	[ItemCode]				VARCHAR(24) NOT NULL,
	[LineNumber]			INT NOT NULL DEFAULT 1,
	[Quantity]				DECIMAL(8) NOT NULL,
	[Price]					DECIMAL(10,2) NOT NULL DEFAULT 0,
	[UnitOfMeasure]			VARCHAR(5) NULL, 
    CONSTRAINT [PK_OrderQueueItem] PRIMARY KEY (OnlineOrderNumber ASC, ItemCode ASC, LineNumber ASC),
    CONSTRAINT [FK_OrderQueueItem_OrderQueue] FOREIGN KEY ([OnlineOrderNumber]) REFERENCES [OrderQueue]([OnlineOrderNumber]) ON DELETE CASCADE
)
