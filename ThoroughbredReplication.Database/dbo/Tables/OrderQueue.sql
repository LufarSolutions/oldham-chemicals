﻿CREATE TABLE [dbo].[OrderQueue]
(
	[OnlineOrderNumber]		varchar(10)		NOT NULL,
	[CustomerNumber]		varchar(7)		NOT NULL,
	[Error]					varchar(255)	NULL,
	[DateCreated]			datetime		NOT NULL DEFAULT GETUTCDATE(),
	[DateProcessed]			datetime		NULL,
	[Notes]					varchar(90)		NULL,
	[NotificationEmails]	varchar(255)	NULL,
	[OrderNumber]			varchar(7)		NULL,
	[PurchaseOrderNumber]	varchar(50)		NULL,
	[ShipToNumber]			varchar(4)		NULL, 
    [WarehouseCode]			VARCHAR(3)				NULL, 
    CONSTRAINT [PK_OrderQueue] PRIMARY KEY ([OnlineOrderNumber]),
)
