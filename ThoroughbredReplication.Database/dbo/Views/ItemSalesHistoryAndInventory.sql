﻿CREATE VIEW [dbo].[ItemSalesHistoryAndInventory] WITH SCHEMABINDING
AS
SELECT        dbo.Manufacturer.VEND_NO, dbo.Item.ITEM_CODE, dbo.Item.DISCONTINUED, dbo.ItemInventory.WHSE_CODE, DATEADD(MONTH, DATEDIFF(MONTH, 0, dbo.Invoice.INV_DATE), 0) AS InvoiceDate, YEAR(dbo.Invoice.INV_DATE) 
                         AS InvoiceYear, MONTH(dbo.Invoice.INV_DATE) AS InvoiceMonth, SUM(CASE WHEN Invoice.WAREHOUSE = ItemInventory.WHSE_Code THEN InvoiceItem.SHIP_TODAY ELSE 0 END) AS Units
FROM            dbo.InvoiceItem LEFT OUTER JOIN
                         dbo.ItemInventory ON dbo.ItemInventory.ITEM_CODE = dbo.InvoiceItem.ITEM_CODE LEFT OUTER JOIN
                         dbo.Item ON dbo.Item.PROD_CODE = dbo.ItemInventory.PROD_CODE AND dbo.Item.ITEM_CODE = dbo.ItemInventory.ITEM_CODE LEFT OUTER JOIN
                         dbo.Warehouse ON dbo.Warehouse.WHSE_CODE = dbo.ItemInventory.WHSE_CODE INNER JOIN
                         dbo.Invoice ON dbo.InvoiceItem.INV_NO = dbo.Invoice.INV_NO LEFT OUTER JOIN
                         dbo.Manufacturer ON dbo.Manufacturer.VEND_NO = dbo.Item.VENDOR
GROUP BY dbo.Manufacturer.VEND_NO, dbo.Item.ITEM_CODE, dbo.Item.DISCONTINUED, dbo.ItemInventory.WHSE_CODE, DATEADD(MONTH, DATEDIFF(MONTH, 0, dbo.Invoice.INV_DATE), 0), YEAR(dbo.Invoice.INV_DATE), 
                         MONTH(dbo.Invoice.INV_DATE), dbo.ItemInventory.QTY_ON_HAND


GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "InvoiceItem"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemInventory"
            Begin Extent = 
               Top = 6
               Left = 303
               Bottom = 136
               Right = 530
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Item"
            Begin Extent = 
               Top = 6
               Left = 568
               Bottom = 136
               Right = 795
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Warehouse"
            Begin Extent = 
               Top = 6
               Left = 833
               Bottom = 136
               Right = 1060
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Invoice"
            Begin Extent = 
               Top = 6
               Left = 1098
               Bottom = 136
               Right = 1325
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Manufacturer"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ItemSalesHistoryAndInventory'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'      Alias = 1335
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ItemSalesHistoryAndInventory'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ItemSalesHistoryAndInventory'
GO


