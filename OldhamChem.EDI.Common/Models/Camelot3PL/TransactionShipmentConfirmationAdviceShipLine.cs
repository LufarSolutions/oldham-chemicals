﻿namespace OldhamChem.EDI.Common.Models.Camelot3PL
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TransactionShipmentConfirmationAdviceShipLine
    {

        private string itemNumberField;

        private byte qtyOrderedField;

        private byte qtyShippedField;

        private string uOMField;

        private string itemDesc1Field;

        private string itemDesc2Field;

        private string lotField;

        private string lineReferenceField;

        private string subPart1NumberField;

        private string subPart2NumberField;

        private string serialNumberField;

        private string nMFCCodeField;

        private string freightClassField;

        private string lineUser1Field;

        private string lineUser2Field;

        private string lineUser3Field;

        private string lineUser4Field;

        private string lineUser5Field;

        private byte lGrsWeightField;

        private byte lNetWeightField;

        /// <remarks/>
        public string ItemNumber {
            get {
                return this.itemNumberField;
            }
            set {
                this.itemNumberField = value;
            }
        }

        /// <remarks/>
        public byte QtyOrdered {
            get {
                return this.qtyOrderedField;
            }
            set {
                this.qtyOrderedField = value;
            }
        }

        /// <remarks/>
        public byte QtyShipped {
            get {
                return this.qtyShippedField;
            }
            set {
                this.qtyShippedField = value;
            }
        }

        /// <remarks/>
        public string UOM {
            get {
                return this.uOMField;
            }
            set {
                this.uOMField = value;
            }
        }

        /// <remarks/>
        public string ItemDesc1 {
            get {
                return this.itemDesc1Field;
            }
            set {
                this.itemDesc1Field = value;
            }
        }

        /// <remarks/>
        public string ItemDesc2 {
            get {
                return this.itemDesc2Field;
            }
            set {
                this.itemDesc2Field = value;
            }
        }

        /// <remarks/>
        public string Lot {
            get {
                return this.lotField;
            }
            set {
                this.lotField = value;
            }
        }

        /// <remarks/>
        public string LineReference {
            get {
                return this.lineReferenceField;
            }
            set {
                this.lineReferenceField = value;
            }
        }

        /// <remarks/>
        public string SubPart1Number {
            get {
                return this.subPart1NumberField;
            }
            set {
                this.subPart1NumberField = value;
            }
        }

        /// <remarks/>
        public string SubPart2Number {
            get {
                return this.subPart2NumberField;
            }
            set {
                this.subPart2NumberField = value;
            }
        }

        /// <remarks/>
        public string SerialNumber {
            get {
                return this.serialNumberField;
            }
            set {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>
        public string NMFCCode {
            get {
                return this.nMFCCodeField;
            }
            set {
                this.nMFCCodeField = value;
            }
        }

        /// <remarks/>
        public string FreightClass {
            get {
                return this.freightClassField;
            }
            set {
                this.freightClassField = value;
            }
        }

        /// <remarks/>
        public string LineUser1 {
            get {
                return this.lineUser1Field;
            }
            set {
                this.lineUser1Field = value;
            }
        }

        /// <remarks/>
        public string LineUser2 {
            get {
                return this.lineUser2Field;
            }
            set {
                this.lineUser2Field = value;
            }
        }

        /// <remarks/>
        public string LineUser3 {
            get {
                return this.lineUser3Field;
            }
            set {
                this.lineUser3Field = value;
            }
        }

        /// <remarks/>
        public string LineUser4 {
            get {
                return this.lineUser4Field;
            }
            set {
                this.lineUser4Field = value;
            }
        }

        /// <remarks/>
        public string LineUser5 {
            get {
                return this.lineUser5Field;
            }
            set {
                this.lineUser5Field = value;
            }
        }

        /// <remarks/>
        public byte LGrsWeight {
            get {
                return this.lGrsWeightField;
            }
            set {
                this.lGrsWeightField = value;
            }
        }

        /// <remarks/>
        public byte LNetWeight {
            get {
                return this.lNetWeightField;
            }
            set {
                this.lNetWeightField = value;
            }
        }
    }


}
