﻿namespace OldhamChem.EDI.Common.Models.Camelot3PL
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TransactionShipmentConfirmationAdviceShipFrom
    {

        private string shipFromCodeField;

        private string shipFromNameField;

        /// <remarks/>
        public string ShipFromCode {
            get {
                return this.shipFromCodeField;
            }
            set {
                this.shipFromCodeField = value;
            }
        }

        /// <remarks/>
        public string ShipFromName {
            get {
                return this.shipFromNameField;
            }
            set {
                this.shipFromNameField = value;
            }
        }
    }


}
