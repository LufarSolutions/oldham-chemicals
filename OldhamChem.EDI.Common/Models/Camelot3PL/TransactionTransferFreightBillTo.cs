﻿namespace OldhamChem.EDI.Common.Models.Camelot3PL
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TransactionTransferFreightBillTo
    {

        private string frtBillToCodeField;

        private string frtBillToNameField;

        private string frtBillToAddr1Field;

        private string frtBillToAddr2Field;

        private string frtBillToCityField;

        private string frtBillToStateField;

        private string frtBillToZipField;

        private string frtBillToCountryField;

        /// <remarks/>
        public string FrtBillToCode {
            get {
                return this.frtBillToCodeField;
            }
            set {
                this.frtBillToCodeField = value;
            }
        }

        /// <remarks/>
        public string FrtBillToName {
            get {
                return this.frtBillToNameField;
            }
            set {
                this.frtBillToNameField = value;
            }
        }

        /// <remarks/>
        public string FrtBillToAddr1 {
            get {
                return this.frtBillToAddr1Field;
            }
            set {
                this.frtBillToAddr1Field = value;
            }
        }

        /// <remarks/>
        public string FrtBillToAddr2 {
            get {
                return this.frtBillToAddr2Field;
            }
            set {
                this.frtBillToAddr2Field = value;
            }
        }

        /// <remarks/>
        public string FrtBillToCity {
            get {
                return this.frtBillToCityField;
            }
            set {
                this.frtBillToCityField = value;
            }
        }

        /// <remarks/>
        public string FrtBillToState {
            get {
                return this.frtBillToStateField;
            }
            set {
                this.frtBillToStateField = value;
            }
        }

        /// <remarks/>
        public string FrtBillToZip {
            get {
                return this.frtBillToZipField;
            }
            set {
                this.frtBillToZipField = value;
            }
        }

        /// <remarks/>
        public string FrtBillToCountry {
            get {
                return this.frtBillToCountryField;
            }
            set {
                this.frtBillToCountryField = value;
            }
        }
    }
}
