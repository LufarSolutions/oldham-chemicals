﻿namespace OldhamChem.EDI.Common.Models.Camelot3PL
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TransactionTransferShipTo
    {

        private string shipToCodeField;

        private string shipToNameField;

        private string shipToName2Field;

        private string shipToAddr1Field;

        private string shipToAddr2Field;

        private string shipToCityField;

        private string shipToStateField;

        private string shipToZipField;

        private string shipToCountryField;

        /// <remarks/>
        public string ShipToCode {
            get {
                return this.shipToCodeField;
            }
            set {
                this.shipToCodeField = value;
            }
        }

        /// <remarks/>
        public string ShipToName {
            get {
                return this.shipToNameField;
            }
            set {
                this.shipToNameField = value;
            }
        }

        /// <remarks/>
        public string ShipToName2 {
            get {
                return this.shipToName2Field;
            }
            set {
                this.shipToName2Field = value;
            }
        }

        /// <remarks/>
        public string ShipToAddr1 {
            get {
                return this.shipToAddr1Field;
            }
            set {
                this.shipToAddr1Field = value;
            }
        }

        /// <remarks/>
        public string ShipToAddr2 {
            get {
                return this.shipToAddr2Field;
            }
            set {
                this.shipToAddr2Field = value;
            }
        }

        /// <remarks/>
        public string ShipToCity {
            get {
                return this.shipToCityField;
            }
            set {
                this.shipToCityField = value;
            }
        }

        /// <remarks/>
        public string ShipToState {
            get {
                return this.shipToStateField;
            }
            set {
                this.shipToStateField = value;
            }
        }

        /// <remarks/>
        public string ShipToZip {
            get {
                return this.shipToZipField;
            }
            set {
                this.shipToZipField = value;
            }
        }

        /// <remarks/>
        public string ShipToCountry {
            get {
                return this.shipToCountryField;
            }
            set {
                this.shipToCountryField = value;
            }
        }
    }
}
