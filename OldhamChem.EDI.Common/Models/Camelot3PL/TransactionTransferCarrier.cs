﻿namespace OldhamChem.EDI.Common.Models.Camelot3PL
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TransactionTransferCarrier
    {

        private string carrierCodeField;

        private string carrierNameField;

        /// <remarks/>
        public string CarrierCode {
            get {
                return this.carrierCodeField;
            }
            set {
                this.carrierCodeField = value;
            }
        }

        /// <remarks/>
        public string CarrierName {
            get {
                return this.carrierNameField;
            }
            set {
                this.carrierNameField = value;
            }
        }
    }
}
