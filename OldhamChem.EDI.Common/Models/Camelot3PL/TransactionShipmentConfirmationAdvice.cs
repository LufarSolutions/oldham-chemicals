﻿namespace OldhamChem.EDI.Common.Models.Camelot3PL
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TransactionShipmentConfirmationAdvice
    {

        private string depositorCodeField;

        private string orderRefNumberField;

        private string purchOrderNumberField;

        private string shipmentIDField;

        private string orderDateField;

        private string shipDateField;

        private string expDeliveryDateField;

        private string cancelDateField;

        private TransactionShipmentConfirmationAdviceShipTo shipToField;

        private TransactionShipmentConfirmationAdviceShipFrom shipFromField;

        private TransactionShipmentConfirmationAdviceCarrier carrierField;

        private string proNumberField;

        private string shipmentMethodCodeField;

        private string freightBillTypeField;

        private TransactionShipmentConfirmationAdviceFreightBillTo freightBillToField;

        private decimal freightCostField;

        private string hdrUser1Field;

        private string hdrUser2Field;

        private string hdrUser3Field;

        private string hdrUser4Field;

        private string hdrUser5Field;

        private string orderStatusField;

        private byte totalQtyOrderedField;

        private byte grsWeightField;

        private byte netWeightField;

        private TransactionShipmentConfirmationAdviceShipLine shipLineField;

        /// <remarks/>
        public string DepositorCode {
            get {
                return this.depositorCodeField;
            }
            set {
                this.depositorCodeField = value;
            }
        }

        /// <remarks/>
        public string OrderRefNumber {
            get {
                return this.orderRefNumberField;
            }
            set {
                this.orderRefNumberField = value;
            }
        }

        /// <remarks/>
        public string PurchOrderNumber {
            get {
                return this.purchOrderNumberField;
            }
            set {
                this.purchOrderNumberField = value;
            }
        }

        /// <remarks/>
        public string ShipmentID {
            get {
                return this.shipmentIDField;
            }
            set {
                this.shipmentIDField = value;
            }
        }

        /// <remarks/>
        public string OrderDate {
            get {
                return this.orderDateField;
            }
            set {
                this.orderDateField = value;
            }
        }

        /// <remarks/>
        public string ShipDate {
            get {
                return this.shipDateField;
            }
            set {
                this.shipDateField = value;
            }
        }

        /// <remarks/>
        public string ExpDeliveryDate {
            get {
                return this.expDeliveryDateField;
            }
            set {
                this.expDeliveryDateField = value;
            }
        }

        /// <remarks/>
        public string CancelDate {
            get {
                return this.cancelDateField;
            }
            set {
                this.cancelDateField = value;
            }
        }

        /// <remarks/>
        public TransactionShipmentConfirmationAdviceShipTo ShipTo {
            get {
                return this.shipToField;
            }
            set {
                this.shipToField = value;
            }
        }

        /// <remarks/>
        public TransactionShipmentConfirmationAdviceShipFrom ShipFrom {
            get {
                return this.shipFromField;
            }
            set {
                this.shipFromField = value;
            }
        }

        /// <remarks/>
        public TransactionShipmentConfirmationAdviceCarrier Carrier {
            get {
                return this.carrierField;
            }
            set {
                this.carrierField = value;
            }
        }

        /// <remarks/>
        public string ProNumber {
            get {
                return this.proNumberField;
            }
            set {
                this.proNumberField = value;
            }
        }

        /// <remarks/>
        public string ShipmentMethodCode {
            get {
                return this.shipmentMethodCodeField;
            }
            set {
                this.shipmentMethodCodeField = value;
            }
        }

        /// <remarks/>
        public string FreightBillType {
            get {
                return this.freightBillTypeField;
            }
            set {
                this.freightBillTypeField = value;
            }
        }

        /// <remarks/>
        public TransactionShipmentConfirmationAdviceFreightBillTo FreightBillTo {
            get {
                return this.freightBillToField;
            }
            set {
                this.freightBillToField = value;
            }
        }

        /// <remarks/>
        public decimal FreightCost {
            get {
                return this.freightCostField;
            }
            set {
                this.freightCostField = value;
            }
        }

        /// <remarks/>
        public string HdrUser1 {
            get {
                return this.hdrUser1Field;
            }
            set {
                this.hdrUser1Field = value;
            }
        }

        /// <remarks/>
        public string HdrUser2 {
            get {
                return this.hdrUser2Field;
            }
            set {
                this.hdrUser2Field = value;
            }
        }

        /// <remarks/>
        public string HdrUser3 {
            get {
                return this.hdrUser3Field;
            }
            set {
                this.hdrUser3Field = value;
            }
        }

        /// <remarks/>
        public string HdrUser4 {
            get {
                return this.hdrUser4Field;
            }
            set {
                this.hdrUser4Field = value;
            }
        }

        /// <remarks/>
        public string HdrUser5 {
            get {
                return this.hdrUser5Field;
            }
            set {
                this.hdrUser5Field = value;
            }
        }

        /// <remarks/>
        public string OrderStatus {
            get {
                return this.orderStatusField;
            }
            set {
                this.orderStatusField = value;
            }
        }

        /// <remarks/>
        public byte TotalQtyOrdered {
            get {
                return this.totalQtyOrderedField;
            }
            set {
                this.totalQtyOrderedField = value;
            }
        }

        /// <remarks/>
        public byte GrsWeight {
            get {
                return this.grsWeightField;
            }
            set {
                this.grsWeightField = value;
            }
        }

        /// <remarks/>
        public byte NetWeight {
            get {
                return this.netWeightField;
            }
            set {
                this.netWeightField = value;
            }
        }

        /// <remarks/>
        public TransactionShipmentConfirmationAdviceShipLine ShipLine {
            get {
                return this.shipLineField;
            }
            set {
                this.shipLineField = value;
            }
        }
    }


}
