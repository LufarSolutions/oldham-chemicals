﻿namespace OldhamChem.EDI.Common.Models.Camelot3PL
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Transaction
    {
        private TransactionShipmentConfirmationAdvice shipmentConfirmationAdviceField;
        private TransactionTransfer transferField;

        private string typeField;
        public TransactionShipmentConfirmationAdvice ShipmentConfirmationAdvice {
            get {
                return this.shipmentConfirmationAdviceField;
            }
            set {
                this.shipmentConfirmationAdviceField = value;
            }
        }

        /// <remarks/>
        public TransactionTransfer Transfer {
            get {
                return this.transferField;
            }
            set {
                this.transferField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
            }
        }
    }
}
