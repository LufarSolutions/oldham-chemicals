﻿namespace OldhamChem.EDI.Common.Models.Camelot3PL
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TransactionTransfer
    {

        private string depositorCodeField;

        private string orderRefNumberField;

        private string purchOrderNumberField;

        private string orderDateField;

        private string shipDateField;

        private string expDeliveryDateField;

        private string cancelDateField;

        private TransactionTransferShipTo shipToField;

        private TransactionTransferShipFrom shipFromField;

        private string soldToCodeField;

        private TransactionTransferCarrier carrierField;

        private string proNumberField;

        private string shipmentMethodCodeField;

        private string freightBillTypeField;

        private TransactionTransferFreightBillTo freightBillToField;

        private string hdrUser1Field;

        private string hdrUser2Field;

        private string hdrUser3Field;

        private string hdrUser4Field;

        private string hdrUser5Field;

        private string orderStatusField;

        private int totalQtyOrderedField;

        private TransactionTransferTransferLine transferLineField;

        private TransactionTransferComments commentsField;

        /// <remarks/>
        public string DepositorCode {
            get {
                return this.depositorCodeField;
            }
            set {
                this.depositorCodeField = value;
            }
        }

        /// <remarks/>
        public string OrderRefNumber {
            get {
                return this.orderRefNumberField;
            }
            set {
                this.orderRefNumberField = value;
            }
        }

        /// <remarks/>
        public string PurchOrderNumber {
            get {
                return this.purchOrderNumberField;
            }
            set {
                this.purchOrderNumberField = value;
            }
        }

        /// <remarks/>
        public string OrderDate {
            get {
                return this.orderDateField;
            }
            set {
                this.orderDateField = value;
            }
        }

        /// <remarks/>
        public string ShipDate {
            get {
                return this.shipDateField;
            }
            set {
                this.shipDateField = value;
            }
        }

        /// <remarks/>
        public string ExpDeliveryDate {
            get {
                return this.expDeliveryDateField;
            }
            set {
                this.expDeliveryDateField = value;
            }
        }

        /// <remarks/>
        public string CancelDate {
            get {
                return this.cancelDateField;
            }
            set {
                this.cancelDateField = value;
            }
        }

        /// <remarks/>
        public TransactionTransferShipTo ShipTo {
            get {
                return this.shipToField;
            }
            set {
                this.shipToField = value;
            }
        }

        /// <remarks/>
        public TransactionTransferShipFrom ShipFrom {
            get {
                return this.shipFromField;
            }
            set {
                this.shipFromField = value;
            }
        }

        /// <remarks/>
        public string SoldToCode {
            get {
                return this.soldToCodeField;
            }
            set {
                this.soldToCodeField = value;
            }
        }

        /// <remarks/>
        public TransactionTransferCarrier Carrier {
            get {
                return this.carrierField;
            }
            set {
                this.carrierField = value;
            }
        }

        /// <remarks/>
        public string ProNumber {
            get {
                return this.proNumberField;
            }
            set {
                this.proNumberField = value;
            }
        }

        /// <remarks/>
        public string ShipmentMethodCode {
            get {
                return this.shipmentMethodCodeField;
            }
            set {
                this.shipmentMethodCodeField = value;
            }
        }

        /// <remarks/>
        public string FreightBillType {
            get {
                return this.freightBillTypeField;
            }
            set {
                this.freightBillTypeField = value;
            }
        }

        /// <remarks/>
        public TransactionTransferFreightBillTo FreightBillTo {
            get {
                return this.freightBillToField;
            }
            set {
                this.freightBillToField = value;
            }
        }

        /// <remarks/>
        public string HdrUser1 {
            get {
                return this.hdrUser1Field;
            }
            set {
                this.hdrUser1Field = value;
            }
        }

        /// <remarks/>
        public string HdrUser2 {
            get {
                return this.hdrUser2Field;
            }
            set {
                this.hdrUser2Field = value;
            }
        }

        /// <remarks/>
        public string HdrUser3 {
            get {
                return this.hdrUser3Field;
            }
            set {
                this.hdrUser3Field = value;
            }
        }

        /// <remarks/>
        public string HdrUser4 {
            get {
                return this.hdrUser4Field;
            }
            set {
                this.hdrUser4Field = value;
            }
        }

        /// <remarks/>
        public string HdrUser5 {
            get {
                return this.hdrUser5Field;
            }
            set {
                this.hdrUser5Field = value;
            }
        }

        /// <remarks/>
        public string OrderStatus {
            get {
                return this.orderStatusField;
            }
            set {
                this.orderStatusField = value;
            }
        }

        /// <remarks/>
        public int TotalQtyOrdered {
            get {
                return this.totalQtyOrderedField;
            }
            set {
                this.totalQtyOrderedField = value;
            }
        }

        /// <remarks/>
        public TransactionTransferTransferLine TransferLine {
            get {
                return this.transferLineField;
            }
            set {
                this.transferLineField = value;
            }
        }

        /// <remarks/>
        public TransactionTransferComments Comments {
            get {
                return this.commentsField;
            }
            set {
                this.commentsField = value;
            }
        }
    }
}
