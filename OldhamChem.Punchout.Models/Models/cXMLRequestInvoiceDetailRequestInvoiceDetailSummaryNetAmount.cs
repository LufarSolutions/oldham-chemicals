﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryNetAmount
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryNetAmountMoney moneyField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryNetAmountMoney Money
        {
            get
            {
                return this.moneyField;
            }
            set
            {
                this.moneyField = value;
            }
        }
    }
}
