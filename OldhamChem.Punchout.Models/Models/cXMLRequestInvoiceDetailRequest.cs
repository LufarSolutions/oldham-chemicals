﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequest
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeader invoiceDetailRequestHeaderField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrder invoiceDetailOrderField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummary invoiceDetailSummaryField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeader InvoiceDetailRequestHeader
        {
            get
            {
                return this.invoiceDetailRequestHeaderField;
            }
            set
            {
                this.invoiceDetailRequestHeaderField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrder InvoiceDetailOrder
        {
            get
            {
                return this.invoiceDetailOrderField;
            }
            set
            {
                this.invoiceDetailOrderField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummary InvoiceDetailSummary
        {
            get
            {
                return this.invoiceDetailSummaryField;
            }
            set
            {
                this.invoiceDetailSummaryField = value;
            }
        }
    }
}
