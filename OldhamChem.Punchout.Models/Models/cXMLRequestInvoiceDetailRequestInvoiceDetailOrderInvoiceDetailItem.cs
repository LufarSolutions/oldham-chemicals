﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItem
    {

        private string unitOfMeasureField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemUnitPrice unitPriceField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemInvoiceDetailItemReference invoiceDetailItemReferenceField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemSubtotalAmount subtotalAmountField;

        private string invoiceLineNumberField;

        private string quantityField;

        /// <remarks/>
        public string UnitOfMeasure
        {
            get
            {
                return this.unitOfMeasureField;
            }
            set
            {
                this.unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemUnitPrice UnitPrice
        {
            get
            {
                return this.unitPriceField;
            }
            set
            {
                this.unitPriceField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemInvoiceDetailItemReference InvoiceDetailItemReference
        {
            get
            {
                return this.invoiceDetailItemReferenceField;
            }
            set
            {
                this.invoiceDetailItemReferenceField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemSubtotalAmount SubtotalAmount
        {
            get
            {
                return this.subtotalAmountField;
            }
            set
            {
                this.subtotalAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string invoiceLineNumber
        {
            get
            {
                return this.invoiceLineNumberField;
            }
            set
            {
                this.invoiceLineNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }
    }
}
