﻿/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryShippingAmount
{

    private cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryShippingAmountMoney moneyField;

    /// <remarks/>
    public cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryShippingAmountMoney Money {
        get {
            return this.moneyField;
        }
        set {
            this.moneyField = value;
        }
    }
}

