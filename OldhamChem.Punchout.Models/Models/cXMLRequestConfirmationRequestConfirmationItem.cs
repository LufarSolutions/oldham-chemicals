﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestConfirmationRequestConfirmationItem
    {

        private string unitOfMeasureField;

        private cXMLRequestConfirmationRequestConfirmationItemConfirmationStatus confirmationStatusField;

        private int quantityField;

        private int lineNumberField;

        /// <remarks/>
        public string UnitOfMeasure
        {
            get
            {
                return this.unitOfMeasureField;
            }
            set
            {
                this.unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestConfirmationRequestConfirmationItemConfirmationStatus ConfirmationStatus
        {
            get
            {
                return this.confirmationStatusField;
            }
            set
            {
                this.confirmationStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int lineNumber
        {
            get
            {
                return this.lineNumberField;
            }
            set
            {
                this.lineNumberField = value;
            }
        }
    }


}
