﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartner
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContact contactField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContact Contact
        {
            get
            {
                return this.contactField;
            }
            set
            {
                this.contactField = value;
            }
        }
    }
}
