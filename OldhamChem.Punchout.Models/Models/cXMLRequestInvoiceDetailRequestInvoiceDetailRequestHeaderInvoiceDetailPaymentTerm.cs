﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailPaymentTerm
    {

        private string payInNumberOfDaysField;

        private byte percentageRateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string payInNumberOfDays
        {
            get
            {
                return this.payInNumberOfDaysField;
            }
            set
            {
                this.payInNumberOfDaysField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte percentageRate
        {
            get
            {
                return this.percentageRateField;
            }
            set
            {
                this.percentageRateField = value;
            }
        }
    }
}
