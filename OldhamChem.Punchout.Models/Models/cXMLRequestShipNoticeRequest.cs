﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Punchout.Common.Models
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class cXMLRequestShipNoticeRequest
    {

        private cXMLRequestShipNoticeRequestShipNoticeHeader shipNoticeHeaderField;

        private cXMLRequestShipNoticeRequestShipControl shipControlField;

        private cXMLRequestShipNoticeRequestShipNoticePortion shipNoticePortionField;

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticeHeader ShipNoticeHeader
        {
            get
            {
                return this.shipNoticeHeaderField;
            }
            set
            {
                this.shipNoticeHeaderField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipControl ShipControl
        {
            get
            {
                return this.shipControlField;
            }
            set
            {
                this.shipControlField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticePortion ShipNoticePortion
        {
            get
            {
                return this.shipNoticePortionField;
            }
            set
            {
                this.shipNoticePortionField = value;
            }
        }
    }
}