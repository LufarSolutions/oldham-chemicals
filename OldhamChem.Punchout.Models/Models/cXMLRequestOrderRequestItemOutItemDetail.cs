﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequestItemOutItemDetail
    {

        private cXMLRequestOrderRequestItemOutItemDetailUnitPrice unitPriceField;

        private cXMLRequestOrderRequestItemOutItemDetailDescription descriptionField;

        private string unitOfMeasureField;

        private cXMLRequestOrderRequestItemOutItemDetailClassification classificationField;

        private string manufacturerPartIDField;

        private string manufacturerNameField;

        /// <remarks/>
        public cXMLRequestOrderRequestItemOutItemDetailUnitPrice UnitPrice
        {
            get
            {
                return this.unitPriceField;
            }
            set
            {
                this.unitPriceField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestOrderRequestItemOutItemDetailDescription Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public string UnitOfMeasure
        {
            get
            {
                return this.unitOfMeasureField;
            }
            set
            {
                this.unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestOrderRequestItemOutItemDetailClassification Classification
        {
            get
            {
                return this.classificationField;
            }
            set
            {
                this.classificationField = value;
            }
        }

        /// <remarks/>
        public string ManufacturerPartID
        {
            get
            {
                return this.manufacturerPartIDField;
            }
            set
            {
                this.manufacturerPartIDField = value;
            }
        }

        /// <remarks/>
        public string ManufacturerName
        {
            get
            {
                return this.manufacturerNameField;
            }
            set
            {
                this.manufacturerNameField = value;
            }
        }
    }
}