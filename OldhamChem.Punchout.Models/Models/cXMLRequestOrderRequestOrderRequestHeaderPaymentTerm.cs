﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequestOrderRequestHeaderPaymentTerm
    {
        private string payInNumberOfDaysField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string payInNumberOfDays {
            get {
                return this.payInNumberOfDaysField;
            }
            set {
                this.payInNumberOfDaysField = value;
            }
        }
    }

}