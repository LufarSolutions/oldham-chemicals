﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequestOrderRequestHeaderTotal
    {

        private cXMLRequestOrderRequestOrderRequestHeaderTotalMoney moneyField;

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderTotalMoney Money
        {
            get
            {
                return this.moneyField;
            }
            set
            {
                this.moneyField = value;
            }
        }
    }
}