﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequestOrderRequestHeaderShipToAddressFaxTelephoneNumber
    {

        private cXMLRequestOrderRequestOrderRequestHeaderShipToAddressFaxTelephoneNumberCountryCode countryCodeField;

        private string areaOrCityCodeField;

        private string numberField;

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderShipToAddressFaxTelephoneNumberCountryCode CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        /// <remarks/>
        public string AreaOrCityCode
        {
            get
            {
                return this.areaOrCityCodeField;
            }
            set
            {
                this.areaOrCityCodeField = value;
            }
        }

        /// <remarks/>
        public string Number
        {
            get
            {
                return this.numberField;
            }
            set
            {
                this.numberField = value;
            }
        }
    }
}