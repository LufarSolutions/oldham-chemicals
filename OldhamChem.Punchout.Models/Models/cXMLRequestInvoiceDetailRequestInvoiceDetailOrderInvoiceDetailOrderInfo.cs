﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfo
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfoOrderReference orderReferenceField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfoMasterAgreementIDInfo masterAgreementIDInfoField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfoOrderReference OrderReference
        {
            get
            {
                return this.orderReferenceField;
            }
            set
            {
                this.orderReferenceField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfoMasterAgreementIDInfo MasterAgreementIDInfo
        {
            get
            {
                return this.masterAgreementIDInfoField;
            }
            set
            {
                this.masterAgreementIDInfoField = value;
            }
        }
    }
}
