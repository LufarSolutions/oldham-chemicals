﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetailUnitPrice
    {

        private cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetailUnitPriceMoney moneyField;

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetailUnitPriceMoney Money
        {
            get
            {
                return this.moneyField;
            }
            set
            {
                this.moneyField = value;
            }
        }
    }

}
