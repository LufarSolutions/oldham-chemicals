﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryGrossAmount
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryGrossAmountMoney moneyField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryGrossAmountMoney Money
        {
            get
            {
                return this.moneyField;
            }
            set
            {
                this.moneyField = value;
            }
        }
    }
}
