﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestConfirmationRequestOrderReference
    {

        private cXMLRequestConfirmationRequestOrderReferenceDocumentReference documentReferenceField;

        private string orderIDField;

        /// <remarks/>
        public cXMLRequestConfirmationRequestOrderReferenceDocumentReference DocumentReference
        {
            get
            {
                return this.documentReferenceField;
            }
            set
            {
                this.documentReferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string orderID
        {
            get
            {
                return this.orderIDField;
            }
            set
            {
                this.orderIDField = value;
            }
        }
    }


}
