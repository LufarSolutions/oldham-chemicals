﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequestOrderRequestHeaderTotalMoney
    {
        private string alternateAmountField;

        private string alternateCurrencyField;

        private string currencyField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string alternateAmount
        {
            get
            {
                return this.alternateAmountField;
            }
            set
            {
                this.alternateAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string alternateCurrency
        {
            get
            {
                return this.alternateCurrencyField;
            }
            set
            {
                this.alternateCurrencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }
}