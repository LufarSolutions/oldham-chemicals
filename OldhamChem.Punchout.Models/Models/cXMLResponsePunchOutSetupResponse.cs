﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Models
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLResponsePunchOutSetupResponse
    {

        private cXMLResponsePunchOutSetupResponseStartPage startPageField;

        /// <remarks/>
        public cXMLResponsePunchOutSetupResponseStartPage StartPage
        {
            get
            {
                return this.startPageField;
            }
            set
            {
                this.startPageField = value;
            }
        }
    }
}