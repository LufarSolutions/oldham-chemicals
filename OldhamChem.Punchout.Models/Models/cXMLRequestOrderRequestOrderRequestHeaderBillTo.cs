﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequestOrderRequestHeaderBillTo
    {

        private cXMLRequestOrderRequestOrderRequestHeaderBillToAddress addressField;

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderBillToAddress Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }
}