﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhone
    {

        private cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhoneTelephoneNumber telephoneNumberField;

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhoneTelephoneNumber TelephoneNumber
        {
            get
            {
                return this.telephoneNumberField;
            }
            set
            {
                this.telephoneNumberField = value;
            }
        }
    }
}