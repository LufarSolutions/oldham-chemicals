﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequestOrderRequestHeader
    {

        private cXMLRequestOrderRequestOrderRequestHeaderTotal totalField;

        private cXMLRequestOrderRequestOrderRequestHeaderShipTo shipToField;

        private cXMLRequestOrderRequestOrderRequestHeaderBillTo billToField;

        private cXMLRequestOrderRequestOrderRequestHeaderPaymentTerm paymentTermField;

        private System.DateTime orderDateField;

        private string orderIDField;

        private string orderTypeField;

        private string orderVersionField;

        private string typeField;

        public cXMLRequestOrderRequestOrderRequestHeaderPaymentTerm PaymentTerm {
            get {
                return this.paymentTermField;
            }
            set {
                this.paymentTermField = value;
            }
        }

        public string Comments { get; set; }

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderTotal Total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderShipTo ShipTo
        {
            get
            {
                return this.shipToField;
            }
            set
            {
                this.shipToField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderBillTo BillTo
        {
            get
            {
                return this.billToField;
            }
            set
            {
                this.billToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime orderDate
        {
            get
            {
                return this.orderDateField;
            }
            set
            {
                this.orderDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string orderID
        {
            get
            {
                return this.orderIDField;
            }
            set
            {
                this.orderIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string orderType
        {
            get
            {
                return this.orderTypeField;
            }
            set
            {
                this.orderTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string orderVersion
        {
            get
            {
                return this.orderVersionField;
            }
            set
            {
                this.orderVersionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }
}