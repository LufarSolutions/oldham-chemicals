﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetail
    {

        private cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetailUnitPrice unitPriceField;

        private cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetailDescription descriptionField;

        private string unitOfMeasureField;

        private string manufacturerPartIDField;

        private string manufacturerNameField;

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetailUnitPrice UnitPrice
        {
            get
            {
                return this.unitPriceField;
            }
            set
            {
                this.unitPriceField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetailDescription Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public string UnitOfMeasure
        {
            get
            {
                return this.unitOfMeasureField;
            }
            set
            {
                this.unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        public string ManufacturerPartID
        {
            get
            {
                return this.manufacturerPartIDField;
            }
            set
            {
                this.manufacturerPartIDField = value;
            }
        }

        /// <remarks/>
        public string ManufacturerName
        {
            get
            {
                return this.manufacturerNameField;
            }
            set
            {
                this.manufacturerNameField = value;
            }
        }
    }

}
