﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestShipNoticeRequestShipNoticeHeaderContact
    {

        private cXMLRequestShipNoticeRequestShipNoticeHeaderContactName nameField;

        private cXMLRequestShipNoticeRequestShipNoticeHeaderContactPostalAddress postalAddressField;

        private cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhone phoneField;

        private string roleField;

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticeHeaderContactName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticeHeaderContactPostalAddress PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhone Phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string role
        {
            get
            {
                return this.roleField;
            }
            set
            {
                this.roleField = value;
            }
        }
    }
}