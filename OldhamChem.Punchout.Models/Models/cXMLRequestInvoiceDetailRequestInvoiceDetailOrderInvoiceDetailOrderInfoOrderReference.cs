﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfoOrderReference
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfoOrderReferenceDocumentReference documentReferenceField;

        private string orderIDField;

        private string orderDateField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfoOrderReferenceDocumentReference DocumentReference
        {
            get
            {
                return this.documentReferenceField;
            }
            set
            {
                this.documentReferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string orderID
        {
            get
            {
                return this.orderIDField;
            }
            set
            {
                this.orderIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string orderDate
        {
            get
            {
                return this.orderDateField;
            }
            set
            {
                this.orderDateField = value;
            }
        }
    }
}
