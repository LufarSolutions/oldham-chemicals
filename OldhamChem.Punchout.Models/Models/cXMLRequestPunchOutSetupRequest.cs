﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Models
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestPunchOutSetupRequest
    {

        private string buyerCookieField;

        private cXMLRequestPunchOutSetupRequestExtrinsic[] extrinsicField;

        private cXMLRequestPunchOutSetupRequestBrowserFormPost browserFormPostField;

        private cXMLRequestPunchOutSetupRequestSupplierSetup supplierSetupField;

        private cXMLRequestPunchOutSetupRequestSelectedItem selectedItemField;

        private string operationField;

        /// <remarks/>
        public string BuyerCookie
        {
            get
            {
                return this.buyerCookieField;
            }
            set
            {
                this.buyerCookieField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Extrinsic")]
        public cXMLRequestPunchOutSetupRequestExtrinsic[] Extrinsic
        {
            get
            {
                return this.extrinsicField;
            }
            set
            {
                this.extrinsicField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestPunchOutSetupRequestBrowserFormPost BrowserFormPost
        {
            get
            {
                return this.browserFormPostField;
            }
            set
            {
                this.browserFormPostField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestPunchOutSetupRequestSupplierSetup SupplierSetup
        {
            get
            {
                return this.supplierSetupField;
            }
            set
            {
                this.supplierSetupField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestPunchOutSetupRequestSelectedItem SelectedItem
        {
            get
            {
                return this.selectedItemField;
            }
            set
            {
                this.selectedItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string operation
        {
            get
            {
                return this.operationField;
            }
            set
            {
                this.operationField = value;
            }
        }
    }
}