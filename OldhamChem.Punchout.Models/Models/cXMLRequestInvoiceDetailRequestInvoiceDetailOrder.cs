﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailOrder
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfo invoiceDetailOrderInfoField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItem[] invoiceDetailItemField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfo InvoiceDetailOrderInfo
        {
            get
            {
                return this.invoiceDetailOrderInfoField;
            }
            set
            {
                this.invoiceDetailOrderInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("InvoiceDetailItem")]
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItem[] InvoiceDetailItem
        {
            get
            {
                return this.invoiceDetailItemField;
            }
            set
            {
                this.invoiceDetailItemField = value;
            }
        }
    }
}
