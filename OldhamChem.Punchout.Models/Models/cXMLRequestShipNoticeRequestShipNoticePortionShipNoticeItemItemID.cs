﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemItemID
    {

        private string supplierPartIDField;

        private string supplierPartAuxiliaryIDField;

        /// <remarks/>
        public string SupplierPartID
        {
            get
            {
                return this.supplierPartIDField;
            }
            set
            {
                this.supplierPartIDField = value;
            }
        }

        /// <remarks/>
        public string SupplierPartAuxiliaryID
        {
            get
            {
                return this.supplierPartAuxiliaryIDField;
            }
            set
            {
                this.supplierPartAuxiliaryIDField = value;
            }
        }
    }

}
