﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Models
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestPunchOutSetupRequestSelectedItem
    {

        private cXMLRequestPunchOutSetupRequestSelectedItemItemOut itemOutField;

        /// <remarks/>
        public cXMLRequestPunchOutSetupRequestSelectedItemItemOut ItemOut
        {
            get
            {
                return this.itemOutField;
            }
            set
            {
                this.itemOutField = value;
            }
        }
    }
}