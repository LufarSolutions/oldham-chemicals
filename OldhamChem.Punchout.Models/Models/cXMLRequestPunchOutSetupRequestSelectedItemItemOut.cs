﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Models
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestPunchOutSetupRequestSelectedItemItemOut
    {

        private cXMLRequestPunchOutSetupRequestSelectedItemItemOutItemID itemIDField;

        private byte quantityField;

        /// <remarks/>
        public cXMLRequestPunchOutSetupRequestSelectedItemItemOutItemID ItemID
        {
            get
            {
                return this.itemIDField;
            }
            set
            {
                this.itemIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }
    }
}