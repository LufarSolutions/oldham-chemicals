﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequestOrderRequestHeaderShipToAddressPhoneTelephoneNumber
    {

        private cXMLRequestOrderRequestOrderRequestHeaderShipToAddressPhoneTelephoneNumberCountryCode countryCodeField;

        private string areaOrCityCodeField;

        private string numberField;

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderShipToAddressPhoneTelephoneNumberCountryCode CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        /// <remarks/>
        public string AreaOrCityCode
        {
            get
            {
                return this.areaOrCityCodeField;
            }
            set
            {
                this.areaOrCityCodeField = value;
            }
        }

        /// <remarks/>
        public string Number
        {
            get
            {
                return this.numberField;
            }
            set
            {
                this.numberField = value;
            }
        }
    }
}