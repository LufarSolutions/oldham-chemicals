﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfoMasterAgreementIDInfo
    {

        private string agreementIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string agreementID
        {
            get
            {
                return this.agreementIDField;
            }
            set
            {
                this.agreementIDField = value;
            }
        }
    }
}
