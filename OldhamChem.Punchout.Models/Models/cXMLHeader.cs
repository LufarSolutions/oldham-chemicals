﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLHeader
    {

        private cXMLHeaderFrom fromField;

        private cXMLHeaderTo toField;

        private cXMLHeaderSender senderField;

        /// <remarks/>
        public cXMLHeaderFrom From
        {
            get
            {
                return this.fromField;
            }
            set
            {
                this.fromField = value;
            }
        }

        /// <remarks/>
        public cXMLHeaderTo To
        {
            get
            {
                return this.toField;
            }
            set
            {
                this.toField = value;
            }
        }

        /// <remarks/>
        public cXMLHeaderSender Sender
        {
            get
            {
                return this.senderField;
            }
            set
            {
                this.senderField = value;
            }
        }
    }
}