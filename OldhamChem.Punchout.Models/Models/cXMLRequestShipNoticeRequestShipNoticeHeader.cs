﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestShipNoticeRequestShipNoticeHeader
    {

        private cXMLRequestShipNoticeRequestShipNoticeHeaderContact[] contactField;

        private cXMLRequestShipNoticeRequestShipNoticeHeaderComments commentsField;

        private string shipmentIDField;

        private string noticeDateField;

        private string shipmentDateField;

        private string shipmentTypeField;

        private string operationField;

        private string deliveryDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Contact")]
        public cXMLRequestShipNoticeRequestShipNoticeHeaderContact[] Contact
        {
            get
            {
                return this.contactField;
            }
            set
            {
                this.contactField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticeHeaderComments Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string shipmentID
        {
            get
            {
                return this.shipmentIDField;
            }
            set
            {
                this.shipmentIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string noticeDate
        {
            get
            {
                return this.noticeDateField;
            }
            set
            {
                this.noticeDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string shipmentDate
        {
            get
            {
                return this.shipmentDateField;
            }
            set
            {
                this.shipmentDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string shipmentType
        {
            get
            {
                return this.shipmentTypeField;
            }
            set
            {
                this.shipmentTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string deliveryDate
        {
            get
            {
                return this.deliveryDateField;
            }
            set
            {
                this.deliveryDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string operation
        {
            get
            {
                return this.operationField;
            }
            set
            {
                this.operationField = value;
            }
        }
    }
}