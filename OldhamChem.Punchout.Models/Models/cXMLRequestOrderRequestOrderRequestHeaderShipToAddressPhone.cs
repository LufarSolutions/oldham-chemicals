﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequestOrderRequestHeaderShipToAddressPhone
    {

        private cXMLRequestOrderRequestOrderRequestHeaderShipToAddressPhoneTelephoneNumber telephoneNumberField;

        private string nameField;

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderShipToAddressPhoneTelephoneNumber TelephoneNumber
        {
            get
            {
                return this.telephoneNumberField;
            }
            set
            {
                this.telephoneNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }
}