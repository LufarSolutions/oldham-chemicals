﻿namespace OldhamChem.Punchout.Common.Models
{
    public enum cXMLDocumentType
    {
        OrderRequest = 10,
        OrderConfirmation = 20,
        Invoice = 30,
        ShipNotice = 40

    }
}
