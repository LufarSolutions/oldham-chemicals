﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequest
    {
        private cXMLRequestInvoiceDetailRequest invoiceDetailRequest;
        private cXMLRequestConfirmationRequest confirmationRequestField;
        private cXMLRequestOrderRequest orderRequestField;
        private cXMLRequestShipNoticeRequest shipNoticeRequestField;

        private string deploymentModeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string deploymentMode
        {
            get
            {
                return this.deploymentModeField;
            }
            set
            {
                this.deploymentModeField = value;
            }
        }

        public cXMLRequestInvoiceDetailRequest InvoiceDetailRequest
        {
            get
            {
                return this.invoiceDetailRequest;
            }
            set
            {
                this.invoiceDetailRequest = value;
            }
        }

        /// <remarks/>
        public cXMLRequestOrderRequest OrderRequest
        {
            get
            {
                return this.orderRequestField;
            }
            set
            {
                this.orderRequestField = value;
            }
        }

        public cXMLRequestConfirmationRequest ConfirmationRequest
        {
            get
            {
                return this.confirmationRequestField;
            }
            set
            {
                this.confirmationRequestField = value;
            }
        }

        public cXMLRequestShipNoticeRequest ShipNoticeRequest
        {
            get
            {
                return this.shipNoticeRequestField;
            }
            set
            {
                this.shipNoticeRequestField = value;
            }
        }

    }
}