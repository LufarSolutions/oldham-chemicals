﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestShipNoticeRequestShipControl
    {

        private cXMLRequestShipNoticeRequestShipControlCarrierIdentifier[] carrierIdentifierField;

        private string shipmentIdentifierField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CarrierIdentifier")]
        public cXMLRequestShipNoticeRequestShipControlCarrierIdentifier[] CarrierIdentifier
        {
            get
            {
                return this.carrierIdentifierField;
            }
            set
            {
                this.carrierIdentifierField = value;
            }
        }

        /// <remarks/>
        public string ShipmentIdentifier
        {
            get
            {
                return this.shipmentIdentifierField;
            }
            set
            {
                this.shipmentIdentifierField = value;
            }
        }
    }
}