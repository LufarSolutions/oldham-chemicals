﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestShipNoticeRequestShipNoticePortion
    {

        private cXMLRequestShipNoticeRequestShipNoticePortionOrderReference orderReferenceField;

        private cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItem[] shipNoticeItemField;

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticePortionOrderReference OrderReference
        {
            get
            {
                return this.orderReferenceField;
            }
            set
            {
                this.orderReferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ShipNoticeItem")]
        public cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItem[] ShipNoticeItem
        {
            get
            {
                return this.shipNoticeItemField;
            }
            set
            {
                this.shipNoticeItemField = value;
            }
        }
    }
}