﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhoneTelephoneNumber
    {

        private cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhoneTelephoneNumberCountryCode countryCodeField;

        private string areaOrCityCodeField;

        private string numberField;

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhoneTelephoneNumberCountryCode CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        /// <remarks/>
        public string AreaOrCityCode
        {
            get
            {
                return this.areaOrCityCodeField;
            }
            set
            {
                this.areaOrCityCodeField = value;
            }
        }

        /// <remarks/>
        public string Number
        {
            get
            {
                return this.numberField;
            }
            set
            {
                this.numberField = value;
            }
        }
    }
}