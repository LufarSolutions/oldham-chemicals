﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemInvoiceDetailItemReference
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemInvoiceDetailItemReferenceItemID itemIDField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemInvoiceDetailItemReferenceDescription descriptionField;

        private string lineNumberField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemInvoiceDetailItemReferenceItemID ItemID
        {
            get
            {
                return this.itemIDField;
            }
            set
            {
                this.itemIDField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemInvoiceDetailItemReferenceDescription Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lineNumber
        {
            get
            {
                return this.lineNumberField;
            }
            set
            {
                this.lineNumberField = value;
            }
        }
    }
}
