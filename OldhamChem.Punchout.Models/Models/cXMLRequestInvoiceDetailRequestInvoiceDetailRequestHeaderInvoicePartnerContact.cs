﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContact
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactName nameField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactPostalAddress postalAddressField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactEmail emailField;

        private string roleField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactPostalAddress PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactEmail Email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string role
        {
            get
            {
                return this.roleField;
            }
            set
            {
                this.roleField = value;
            }
        }
    }
}
