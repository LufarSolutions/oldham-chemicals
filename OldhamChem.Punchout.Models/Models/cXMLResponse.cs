﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLResponse
    {

        private cXMLResponseStatus statusField;
        
        /// <remarks/>
        public cXMLResponseStatus Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }
}