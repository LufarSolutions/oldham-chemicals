﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemUnitPrice
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemUnitPriceMoney moneyField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemUnitPriceMoney Money
        {
            get
            {
                return this.moneyField;
            }
            set
            {
                this.moneyField = value;
            }
        }
    }
}
