﻿[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailLineIndicator
{

    private string isTaxInLineField;

    private string isShippingInLineField;

    private string isAccountingInLineField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string isTaxInLine {
        get {
            return this.isTaxInLineField;
        }
        set {
            this.isTaxInLineField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string isShippingInLine {
        get {
            return this.isShippingInLineField;
        }
        set {
            this.isShippingInLineField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string isAccountingInLine {
        get {
            return this.isAccountingInLineField;
        }
        set {
            this.isAccountingInLineField = value;
        }
    }
}

