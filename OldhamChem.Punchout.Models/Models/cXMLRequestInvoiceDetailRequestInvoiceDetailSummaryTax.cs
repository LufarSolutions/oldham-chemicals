﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryTax
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryTaxMoney moneyField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryTaxDescription descriptionField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryTaxMoney Money
        {
            get
            {
                return this.moneyField;
            }
            set
            {
                this.moneyField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryTaxDescription Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
    }
}
