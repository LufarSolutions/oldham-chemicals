﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Punchout.Common.Models
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShipping
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContact[] contactField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Contact")]
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContact[] Contact
        {
            get
            {
                return this.contactField;
            }
            set
            {
                this.contactField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContact
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactName nameField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactPostalAddress postalAddressField;

        private string roleField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactPostalAddress PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string role
        {
            get
            {
                return this.roleField;
            }
            set
            {
                this.roleField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactName
    {

        private string langField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactPostalAddress
    {

        private string streetField;

        private string cityField;

        private string stateField;

        private string postalCodeField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactPostalAddressCountry countryField;

        /// <remarks/>
        public string Street
        {
            get
            {
                return this.streetField;
            }
            set
            {
                this.streetField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactPostalAddressCountry Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactPostalAddressCountry
    {

        private string isoCountryCodeField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string isoCountryCode
        {
            get
            {
                return this.isoCountryCodeField;
            }
            set
            {
                this.isoCountryCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }


}
