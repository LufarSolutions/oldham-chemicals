﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderContact
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderContactName nameField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderContactPostalAddress postalAddressField;

        private string roleField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderContactName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderContactPostalAddress PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string role
        {
            get
            {
                return this.roleField;
            }
            set
            {
                this.roleField = value;
            }
        }
    }
}
