﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailSummarySubtotalAmount
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummarySubtotalAmountMoney moneyField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummarySubtotalAmountMoney Money
        {
            get
            {
                return this.moneyField;
            }
            set
            {
                this.moneyField = value;
            }
        }
    }
}
