﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItem
    {
        private cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemItemID itemIDField;

        private cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetail shipNoticeItemDetailField;

        private string unitOfMeasureField;

        private string shipNoticeLineNumberField;

        private string lineNumberField;

        private string quantityField;

        private cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemItemID shipNoticeItemIdField;

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemItemID ItemID
        {
            get
            {
                return this.itemIDField;
            }
            set
            {
                this.itemIDField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetail ShipNoticeItemDetail
        {
            get
            {
                return this.shipNoticeItemDetailField;
            }
            set
            {
                this.shipNoticeItemDetailField = value;
            }
        }

        /// <remarks/>
        public string UnitOfMeasure
        {
            get
            {
                return this.unitOfMeasureField;
            }
            set
            {
                this.unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string shipNoticeLineNumber
        {
            get
            {
                return this.shipNoticeLineNumberField;
            }
            set
            {
                this.shipNoticeLineNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lineNumber
        {
            get
            {
                return this.lineNumberField;
            }
            set
            {
                this.lineNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }
    }
}