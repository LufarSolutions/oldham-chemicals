﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequestOrderRequestHeaderShipToAddress
    {

        private cXMLRequestOrderRequestOrderRequestHeaderShipToAddressName nameField;

        private cXMLRequestOrderRequestOrderRequestHeaderShipToAddressPostalAddress postalAddressField;

        private cXMLRequestOrderRequestOrderRequestHeaderShipToAddressEmail emailField;

        private cXMLRequestOrderRequestOrderRequestHeaderShipToAddressPhone phoneField;

        private cXMLRequestOrderRequestOrderRequestHeaderShipToAddressFax faxField;

        private string addressIDField;

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderShipToAddressName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderShipToAddressPostalAddress PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderShipToAddressEmail Email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderShipToAddressPhone Phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderShipToAddressFax Fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string addressID
        {
            get
            {
                return this.addressIDField;
            }
            set
            {
                this.addressIDField = value;
            }
        }
    }
}