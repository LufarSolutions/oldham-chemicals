﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequest
    {

        private cXMLRequestOrderRequestOrderRequestHeader orderRequestHeaderField;

        private cXMLRequestOrderRequestItemOut[] itemOutField;

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeader OrderRequestHeader
        {
            get
            {
                return this.orderRequestHeaderField;
            }
            set
            {
                this.orderRequestHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ItemOut")]
        public cXMLRequestOrderRequestItemOut[] ItemOut
        {
            get
            {
                return this.itemOutField;
            }
            set
            {
                this.itemOutField = value;
            }
        }
    }
}