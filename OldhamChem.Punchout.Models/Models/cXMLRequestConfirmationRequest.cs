﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Punchout.Common.Models
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class cXMLRequestConfirmationRequest
    {
        private cXMLRequestConfirmationRequestConfirmationHeader confirmationHeaderField;

        private cXMLRequestConfirmationRequestOrderReference orderReferenceField;

        private cXMLRequestConfirmationRequestConfirmationItem[] confirmationItemField;

        /// <remarks/>
        public cXMLRequestConfirmationRequestConfirmationHeader ConfirmationHeader
        {
            get
            {
                return this.confirmationHeaderField;
            }
            set
            {
                this.confirmationHeaderField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestConfirmationRequestOrderReference OrderReference
        {
            get
            {
                return this.orderReferenceField;
            }
            set
            {
                this.orderReferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConfirmationItem")]
        public cXMLRequestConfirmationRequestConfirmationItem[] ConfirmationItem
        {
            get
            {
                return this.confirmationItemField;
            }
            set
            {
                this.confirmationItemField = value;
            }
        }
    }


}
