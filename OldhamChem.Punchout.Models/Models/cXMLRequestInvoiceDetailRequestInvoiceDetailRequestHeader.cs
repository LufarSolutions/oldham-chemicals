﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeader
    {

        private object invoiceDetailHeaderIndicatorField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailLineIndicator invoiceDetailLineIndicatorField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartner[] invoicePartnerField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShipping invoiceDetailShippingField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailPaymentTerm invoiceDetailPaymentTermField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderExtrinsic[] extrinsicField;

        private string invoiceDateField;

        private string invoiceIDField;

        private string invoiceOriginField;

        private string operationField;

        private string purposeField;

        /// <remarks/>
        public object InvoiceDetailHeaderIndicator
        {
            get
            {
                return this.invoiceDetailHeaderIndicatorField;
            }
            set
            {
                this.invoiceDetailHeaderIndicatorField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailLineIndicator InvoiceDetailLineIndicator
        {
            get
            {
                return this.invoiceDetailLineIndicatorField;
            }
            set
            {
                this.invoiceDetailLineIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("InvoicePartner")]
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartner[] InvoicePartner
        {
            get
            {
                return this.invoicePartnerField;
            }
            set
            {
                this.invoicePartnerField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlArrayItemAttribute("In", IsNullable = false)]
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShipping InvoiceDetailShipping
        {
            get
            {
                return this.invoiceDetailShippingField;
            }
            set
            {
                this.invoiceDetailShippingField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailPaymentTerm InvoiceDetailPaymentTerm
        {
            get
            {
                return this.invoiceDetailPaymentTermField;
            }
            set
            {
                this.invoiceDetailPaymentTermField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Extrinsic")]
        public cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderExtrinsic[] Extrinsic
        {
            get
            {
                return this.extrinsicField;
            }
            set
            {
                this.extrinsicField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string invoiceDate
        {
            get
            {
                return this.invoiceDateField;
            }
            set
            {
                this.invoiceDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string invoiceID
        {
            get
            {
                return this.invoiceIDField;
            }
            set
            {
                this.invoiceIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string invoiceOrigin
        {
            get
            {
                return this.invoiceOriginField;
            }
            set
            {
                this.invoiceOriginField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string operation
        {
            get
            {
                return this.operationField;
            }
            set
            {
                this.operationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string purpose
        {
            get
            {
                return this.purposeField;
            }
            set
            {
                this.purposeField = value;
            }
        }
    }
}
