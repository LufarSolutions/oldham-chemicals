﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailSummary
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummarySubtotalAmount subtotalAmountField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryTax taxField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryShippingAmount shippingAmountField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryGrossAmount grossAmountField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryNetAmount netAmountField;

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryDueAmount dueAmountField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummarySubtotalAmount SubtotalAmount
        {
            get
            {
                return this.subtotalAmountField;
            }
            set
            {
                this.subtotalAmountField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryTax Tax
        {
            get
            {
                return this.taxField;
            }
            set
            {
                this.taxField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryShippingAmount ShippingAmount {
            get {
                return this.shippingAmountField;
            }
            set {
                this.shippingAmountField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryGrossAmount GrossAmount
        {
            get
            {
                return this.grossAmountField;
            }
            set
            {
                this.grossAmountField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryNetAmount NetAmount
        {
            get
            {
                return this.netAmountField;
            }
            set
            {
                this.netAmountField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryDueAmount DueAmount
        {
            get
            {
                return this.dueAmountField;
            }
            set
            {
                this.dueAmountField = value;
            }
        }
    }
}
