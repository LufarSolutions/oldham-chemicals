﻿namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryDueAmount
    {

        private cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryDueAmountMoney moneyField;

        /// <remarks/>
        public cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryDueAmountMoney Money
        {
            get
            {
                return this.moneyField;
            }
            set
            {
                this.moneyField = value;
            }
        }
    }
}
