﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldhamChem.Punchout.Common.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class cXMLRequestOrderRequestOrderRequestHeaderBillToAddress
    {
        public int addressId { get; set; }

        public string isoCountryCode { get; set; }

        private cXMLRequestOrderRequestOrderRequestHeaderBillToAddressName nameField;

        private cXMLRequestOrderRequestOrderRequestHeaderBillToAddressPostalAddress postalAddressField;

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderBillToAddressName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public cXMLRequestOrderRequestOrderRequestHeaderBillToAddressPostalAddress PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }
    }
}