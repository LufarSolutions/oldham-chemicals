﻿using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Stores;
using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.ERP;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace OldhamChem.Punchout.Common.Helpers
{
    public class NopHelper
    {
        public const string CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_SHARED_SECRET = "Ariba Shared Secret";
        public const string CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_TRANSACTION_ENDPOINT = "Ariba Transaction Endpoint";
        public const string CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_SUPPLIER_NETWORKID = "Ariba Supplier NetworkId";
        public const string CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_VENDOR_NETWORKID = "Ariba Vendor NetworkId";
        public const string CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_SEND_CXML_CONFIRM_INVOICE = "Ariba Send cXML Confirm/Invoice";
        public const string CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_SEND_CXML_SHIP_NOTICE = "Ariba Send cXML Ship Notice";
        public const string SYSTEM_NAME_FOR_PAYMENT_METHOD_PURCHASE_ORDER = "Payments.PurchaseOrder";
        public const string SYSTEM_NAME_FOR_PUNCHOUT_ROLE = "Punchout";

        private IEngine NopEngineContext;
        public IAddressService AddressService;
        public ICustomerAttributeParser CustomerAttributeParser;
        public ICustomerService CustomerService;
        public IErpService ErpService;
        public IDownloadService DownloadService;
        public IOrderService OrderService;
        public IProductService ProductService;
        public IShippingService ShippingService;
        private IShipmentService ShipmentService;
        public IStoreService StoreService;

        public int StoreId { get; private set; }

        public NopHelper()
        {
            this.StoreId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["StoreId"]);

            // Initialize the NopEngine.
            NopEngineContext = EngineContext.Initialize(false);
            this.AddressService = EngineContext.Current.Resolve<IAddressService>();
            this.CustomerAttributeParser = EngineContext.Current.Resolve<ICustomerAttributeParser>();
            this.CustomerService = EngineContext.Current.Resolve<ICustomerService>();
            this.ErpService = EngineContext.Current.Resolve<IErpService>();
            this.DownloadService = EngineContext.Current.Resolve<IDownloadService>();
            this.OrderService = EngineContext.Current.Resolve<IOrderService>();
            this.ProductService = EngineContext.Current.Resolve<IProductService>();
            this.ShippingService = EngineContext.Current.Resolve<IShippingService>();
            this.ShipmentService = EngineContext.Current.Resolve<IShipmentService>();
            this.StoreService = EngineContext.Current.Resolve<IStoreService>();
        }

        public string GetCustomersSharedSecret(Customer customer)
        {
            string SharedSecret = null;

            string CustomAttributesXML = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes);

            List<CustomerAttribute> CustomerAttributes = CustomerAttributeParser.ParseCustomerAttributes(CustomAttributesXML).ToList();

            CustomerAttribute SharedSecretAttribute = CustomerAttributes.FirstOrDefault(f => f.Name == CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_SHARED_SECRET);

            if (SharedSecretAttribute != null)
            {
                List<string> CustomerAttributeValues = CustomerAttributeParser.ParseValues(CustomAttributesXML, SharedSecretAttribute.Id).ToList();

                if (CustomerAttributeValues != null && CustomerAttributeValues.Count > 0)
                {
                    SharedSecret = CustomerAttributeValues.FirstOrDefault();
                }
            }

            return SharedSecret;
        }

        public string GetCustomersTransactionEndpoint(Customer customer)
        {
            string TransactionEndpoint = null;

            string CustomAttributesXML = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes);

            List<CustomerAttribute> CustomerAttributes = CustomerAttributeParser.ParseCustomerAttributes(CustomAttributesXML).ToList();

            CustomerAttribute TransactionEndpointAttribute = CustomerAttributes.FirstOrDefault(f => f.Name == CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_TRANSACTION_ENDPOINT);

            if (TransactionEndpointAttribute != null)
            {
                List<string> CustomerAttributeValues = CustomerAttributeParser.ParseValues(CustomAttributesXML, TransactionEndpointAttribute.Id).ToList();

                if (CustomerAttributeValues != null && CustomerAttributeValues.Count > 0)
                {
                    TransactionEndpoint = CustomerAttributeValues.FirstOrDefault();
                }
            }

            return TransactionEndpoint;
        }

        public List<Customer> GetPunchoutCustomers() {
            CustomerSearchParameters SearchParameters = new CustomerSearchParameters();
            SearchParameters.Roles.Add(NopHelper.SYSTEM_NAME_FOR_PUNCHOUT_ROLE);

            int CustomerCount = 0;

            return CustomerService
                .SearchCustomers(null, null, null, SearchParameters, null, out CustomerCount)
                .ToList();
        }

        public XmlDocument GetcXmlDocFromDownload(int downloadId)
        {
            XmlDocument xmlDoc = null;

            var download = this.DownloadService.GetDownloadById(downloadId);

            if (download != null)
            {
                xmlDoc = GenerateCxmlDocFromBinary(download.DownloadBinary);
            }

            return xmlDoc;
        }

        public string GetPurchaseOrderNumber(Order order)
        {
            Dictionary<string, object> OrderCustomValues = order.DeserializeCustomValues();

            object PurchaseOrderNumber = OrderCustomValues
                .Where(w => w.Key == "PO Number")
                .Select(s => s.Value)
                .FirstOrDefault();

            return PurchaseOrderNumber != null ? PurchaseOrderNumber.ToString() : null;
        }

        public Address GetStoreAddress()
        {
            Store MainStore = this.StoreService.GetStoreById(StoreId);

            return AddressService.GetAddressById(MainStore.BillingAddressId.Value);
        }

        public string GetSupplierNetworkId(Customer customer)
        {
            string NetworkId = null;

            string CustomAttributesXML = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes);

            List<CustomerAttribute> CustomerAttributes = CustomerAttributeParser.ParseCustomerAttributes(CustomAttributesXML).ToList();

            CustomerAttribute SharedSecretAttribute = CustomerAttributes.FirstOrDefault(f => f.Name == CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_SUPPLIER_NETWORKID);

            if (SharedSecretAttribute != null)
            {
                List<string> CustomerAttributeValues = CustomerAttributeParser.ParseValues(CustomAttributesXML, SharedSecretAttribute.Id).ToList();

                if (CustomerAttributeValues != null && CustomerAttributeValues.Count > 0)
                {
                    NetworkId = CustomerAttributeValues.FirstOrDefault();
                }
            }

            return NetworkId;
        }

        public string GetVendorNetworkId(Customer customer)
        {
            string NetworkId = null;

            string CustomAttributesXML = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes);

            List<CustomerAttribute> CustomerAttributes = CustomerAttributeParser.ParseCustomerAttributes(CustomAttributesXML).ToList();

            CustomerAttribute SharedSecretAttribute = CustomerAttributes.FirstOrDefault(f => f.Name == CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_VENDOR_NETWORKID);

            if (SharedSecretAttribute != null)
            {
                List<string> CustomerAttributeValues = CustomerAttributeParser.ParseValues(CustomAttributesXML, SharedSecretAttribute.Id).ToList();

                if (CustomerAttributeValues != null && CustomerAttributeValues.Count > 0)
                {
                    NetworkId = CustomerAttributeValues.FirstOrDefault();
                }
            }

            return NetworkId;
        }

        public bool GetSendConfirmInvoiceSetting(Customer customer)
        {
            bool send = false;

            string CustomAttributesXML = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes);

            List<CustomerAttribute> CustomerAttributes = CustomerAttributeParser.ParseCustomerAttributes(CustomAttributesXML).ToList();

            CustomerAttribute sendConfirmInvoiceSettingAttribute = CustomerAttributes.FirstOrDefault(f => f.Name == CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_SEND_CXML_CONFIRM_INVOICE);

            if (sendConfirmInvoiceSettingAttribute != null)
            {
                List<string> CustomerAttributeValues = CustomerAttributeParser.ParseValues(CustomAttributesXML, sendConfirmInvoiceSettingAttribute.Id).ToList();                
                send = CustomerAttributeValues != null && CustomerAttributeValues.Count > 0;
            }

            return send;
        }

        public bool GetSendShipNoticeSetting(Customer customer)
        {
            bool send = false;

            string CustomAttributesXML = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes);

            List<CustomerAttribute> CustomerAttributes = CustomerAttributeParser.ParseCustomerAttributes(CustomAttributesXML).ToList();

            CustomerAttribute sendShipNoticeSettingAttribute = CustomerAttributes.FirstOrDefault(f => f.Name == CUSTOMER_ATTRIBUTE_NAME_FOR_ARIBA_SEND_CXML_SHIP_NOTICE);

            if (sendShipNoticeSettingAttribute != null)
            {
                List<string> CustomerAttributeValues = CustomerAttributeParser.ParseValues(CustomAttributesXML, sendShipNoticeSettingAttribute.Id).ToList();
                send = CustomerAttributeValues != null && CustomerAttributeValues.Count > 0;
            }

            return send;
        }

        public OrderNote SavecXMLRequestAsOrderNote(Order order, XmlDocument cXmlDoc, string punchoutDocumentType)
        {
            byte[] cXmlBinary = GenerateBinaryFromXmlDoc(cXmlDoc);

            Download OrderNoteAttachment = SaveXmlFile(cXmlBinary, punchoutDocumentType);

            OrderNote CxmlOrderNote = new OrderNote();

            CxmlOrderNote.Note = punchoutDocumentType;
            CxmlOrderNote.DisplayToCustomer = false;
            CxmlOrderNote.DownloadId = OrderNoteAttachment.Id;
            CxmlOrderNote.CreatedOnUtc = DateTime.UtcNow;

            order.OrderNotes.Add(CxmlOrderNote);

            this.OrderService.UpdateOrder(order);

            return CxmlOrderNote;
        }

        public Download SaveXmlFile(byte[] xmlBinary, string documentType)
        {
            Download OrderNoteAttachment = new Download();
            OrderNoteAttachment.ContentType = "text/xml";
            OrderNoteAttachment.DownloadBinary = xmlBinary;
            OrderNoteAttachment.DownloadGuid = Guid.NewGuid();
            OrderNoteAttachment.DownloadUrl = null;
            OrderNoteAttachment.Extension = ".xml";
            OrderNoteAttachment.Filename = documentType.Replace(" ", "");
            OrderNoteAttachment.UseDownloadUrl = false;

            DownloadService.InsertDownload(OrderNoteAttachment);

            return OrderNoteAttachment;
        }
        

        private static byte[] GenerateBinaryFromXmlDoc(XmlDocument xmlDoc)
        {
            byte[] cXmlBinary = null;

            using (MemoryStream memory = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(memory))
                {
                    writer.Write(xmlDoc.OuterXml);
                    writer.Flush();
                    memory.Flush();

                    cXmlBinary = memory.ToArray();
                }
            }

            return cXmlBinary;
        }

        private static XmlDocument GenerateCxmlDocFromBinary(byte[] buffer)
        {
            XmlDocument xmlDoc = new XmlDocument();
            MemoryStream ms = new MemoryStream(buffer);
            xmlDoc.Load(ms);

            return xmlDoc;
        }
    }
}
