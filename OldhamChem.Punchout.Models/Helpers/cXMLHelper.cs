﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using OldhamChem.Punchout.Common.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace OldhamChem.Punchout.Common.Helpers
{
    public class cXMLHelper
    {
        public const string CXML_VERSION = "1.2.044";
        public const string CXML_DTD_URL = "http://xml.cxml.org/schemas/cXML/{0}/cXML.dtd";
        public const string CXML_FULFILL_DTD_URL = "http://xml.cXML.org/schemas/cXML/{0}/Fulfill.dtd";
        public const string CXML_INVOICE_DTD_URL = "http://xml.cXML.org/schemas/cXML/{0}/InvoiceDetail.dtd";
        public const string CXML_CUSTOMER_EMAIL_SUFFIX = "@punchout.oldhamchem.com";

        private NopHelper NopHelper;

        public cXMLHelper()
        {
            NopHelper = new NopHelper();
        }

        public cXML BuildConfirmationRequestForOrder(Order order)
        {
            cXML ConfirmationRequest = null;

            if (order != null)
            {
                string PurchaseOrderNumber = this.NopHelper.GetPurchaseOrderNumber(order);

                ConfirmationRequest = new cXML();

                ConfirmationRequest.payloadID = GeneratePayloadID();
                ConfirmationRequest.timestamp = GenerateTimeStamp();
                ConfirmationRequest.version = CXML_VERSION;

                ConfirmationRequest.Header = new cXMLHeader();

                var vendorNetworkId = this.NopHelper.GetVendorNetworkId(order.Customer);
                var supplierNetworkId = this.NopHelper.GetSupplierNetworkId(order.Customer);

                ConfirmationRequest.Header.From = new cXMLHeaderFrom();
                ConfirmationRequest.Header.From.Credential = new cXMLHeaderFromCredential();
                ConfirmationRequest.Header.From.Credential.domain = "NetworkId";
                ConfirmationRequest.Header.From.Credential.Identity = supplierNetworkId;
                
                ConfirmationRequest.Header.To = new cXMLHeaderTo();
                ConfirmationRequest.Header.To.Credential = new cXMLHeaderToCredential();
                ConfirmationRequest.Header.To.Credential.domain = "NetworkId";
                ConfirmationRequest.Header.To.Credential.Identity = vendorNetworkId;

                ConfirmationRequest.Header.Sender = new cXMLHeaderSender();
                ConfirmationRequest.Header.Sender.Credential = new cXMLHeaderSenderCredential();
                ConfirmationRequest.Header.Sender.Credential.domain = "NetworkId";
                ConfirmationRequest.Header.Sender.Credential.Identity = supplierNetworkId;
                ConfirmationRequest.Header.Sender.Credential.SharedSecret = this.NopHelper.GetCustomersSharedSecret(order.Customer);
                ConfirmationRequest.Header.Sender.UserAgent = "Supplier";

                ConfirmationRequest.Request = new cXMLRequest();
                ConfirmationRequest.Request.deploymentMode = "test";

                ConfirmationRequest.Request.ConfirmationRequest = new cXMLRequestConfirmationRequest();
                ConfirmationRequest.Request.ConfirmationRequest.ConfirmationHeader = new cXMLRequestConfirmationRequestConfirmationHeader();
                ConfirmationRequest.Request.ConfirmationRequest.ConfirmationHeader.operation = "new";
                ConfirmationRequest.Request.ConfirmationRequest.ConfirmationHeader.type = "detail";
                ConfirmationRequest.Request.ConfirmationRequest.ConfirmationHeader.noticeDate = GenerateTimeStamp();

                ConfirmationRequest.Request.ConfirmationRequest.OrderReference = new cXMLRequestConfirmationRequestOrderReference();
                ConfirmationRequest.Request.ConfirmationRequest.OrderReference.orderID = $"PO{PurchaseOrderNumber}";
                ConfirmationRequest.Request.ConfirmationRequest.OrderReference.DocumentReference = new cXMLRequestConfirmationRequestOrderReferenceDocumentReference();
                ConfirmationRequest.Request.ConfirmationRequest.OrderReference.DocumentReference.payloadID = this.GetOrderRequestPayloadIdFromOrder(order);

                List<cXMLRequestConfirmationRequestConfirmationItem> ConfirmationItems = new List<cXMLRequestConfirmationRequestConfirmationItem>();
                foreach (var orderItem in order.OrderItems)
                {
                    cXMLRequestConfirmationRequestConfirmationItem ConfirmationItem = new cXMLRequestConfirmationRequestConfirmationItem();
                    ConfirmationItem.quantity = orderItem.Quantity;

                    var lineNumber = GetOrderRequestItemLineNumber(order, orderItem.Product.Sku, orderItem.Quantity);
                    ConfirmationItem.lineNumber = !String.IsNullOrWhiteSpace(lineNumber) ? int.Parse(lineNumber) : 1;

                    var unitOfMeasure = orderItem.Product.ProductSpecificationAttributes.Where(w => w.SpecificationAttributeOption.SpecificationAttribute.Name == "Unit of Measure").FirstOrDefault();
                    var uom = ConvertUnitOfMeasureToAbbreviation(unitOfMeasure);                                       

                    ConfirmationItem.UnitOfMeasure = uom;

                    ConfirmationItem.ConfirmationStatus = new cXMLRequestConfirmationRequestConfirmationItemConfirmationStatus();
                    ConfirmationItem.ConfirmationStatus.quantity = orderItem.Quantity;
                    ConfirmationItem.ConfirmationStatus.type = "accept";

                    //Don't need to send the actual unit of measure. The catalog was only setup with EA so just send EA
                    ConfirmationItem.ConfirmationStatus.UnitOfMeasure = uom;

                    ConfirmationItems.Add(ConfirmationItem);
                }

                ConfirmationRequest.Request.ConfirmationRequest.ConfirmationItem = ConfirmationItems.ToArray();

            }

            return ConfirmationRequest;
        }

        public List<cXML> BuildInvoiceRequestForOrder(Order order)
        {
            List<cXML> InvoiceRequests = new List<cXML>();

            if (order != null)
            {
                var buildInvoices = false;
                var invoiceIdsToBuild = new List<string>();
                Thoroughbred.Data.Entities.Invoice invoiceToBuild = null;

                // Run this regardless of InvoiceStatus to make sure you don't resend any cXML Invoices
                invoiceIdsToBuild = GetUnsentInvoices(order);

                if (!invoiceIdsToBuild.IsNullOrEmpty()) {
                    buildInvoices = true;

                    //This will only process one invoice at a time. In the off chance that there are multiple invoices that need to be processed it will get process on the next run.
                    invoiceToBuild = NopHelper.ErpService.GetInvoice(invoiceIdsToBuild.First());
                }

                if (buildInvoices)
                {
                    Address StoreAddress = NopHelper.GetStoreAddress();

                    foreach(var invoiceIdToBuild in invoiceIdsToBuild) {
                        invoiceToBuild = NopHelper.ErpService.GetInvoice(invoiceIdToBuild);

                        if (invoiceToBuild != null) {
                            string PurchaseOrderNumber = this.NopHelper.GetPurchaseOrderNumber(order);
                            Warehouse WarehouseForInvoice = NopHelper.ShippingService.GetWarehouseByCode(invoiceToBuild.WAREHOUSE.Trim());

                            var InvoiceRequest = new cXML();

                            InvoiceRequest.payloadID = GeneratePayloadID();
                            InvoiceRequest.timestamp = GenerateTimeStamp();
                            InvoiceRequest.version = CXML_VERSION;

                            InvoiceRequest.Header = new cXMLHeader();

                            var vendorNetworkId = this.NopHelper.GetVendorNetworkId(order.Customer);
                            var supplierNetworkId = this.NopHelper.GetSupplierNetworkId(order.Customer);

                            InvoiceRequest.Header.From = new cXMLHeaderFrom();
                            InvoiceRequest.Header.From.Credential = new cXMLHeaderFromCredential();
                            InvoiceRequest.Header.From.Credential.domain = "NetworkId";
                            InvoiceRequest.Header.From.Credential.Identity = supplierNetworkId;

                            InvoiceRequest.Header.To = new cXMLHeaderTo();
                            InvoiceRequest.Header.To.Credential = new cXMLHeaderToCredential();
                            InvoiceRequest.Header.To.Credential.domain = "NetworkId";
                            InvoiceRequest.Header.To.Credential.Identity = vendorNetworkId;

                            InvoiceRequest.Header.Sender = new cXMLHeaderSender();
                            InvoiceRequest.Header.Sender.Credential = new cXMLHeaderSenderCredential();
                            InvoiceRequest.Header.Sender.Credential.domain = "NetworkId";
                            InvoiceRequest.Header.Sender.Credential.Identity = supplierNetworkId;
                            InvoiceRequest.Header.Sender.Credential.SharedSecret = this.NopHelper.GetCustomersSharedSecret(order.Customer);
                            InvoiceRequest.Header.Sender.UserAgent = "Supplier";

                            InvoiceRequest.Request = new cXMLRequest();
                            InvoiceRequest.Request.deploymentMode = "test";

                            InvoiceRequest.Request.InvoiceDetailRequest = new cXMLRequestInvoiceDetailRequest();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeader();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.invoiceDate = GenerateTimeStamp();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.invoiceID = invoiceToBuild.INV_NO;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.invoiceOrigin = "supplier";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.operation = "new";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.purpose = "standard";

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailHeaderIndicator = new object();

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailLineIndicator = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailLineIndicator();

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartner[2];
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0] = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartner();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContact();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.role = "remitTo";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.Name = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactName();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.Name.lang = "en";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.Name.Value = StoreAddress.Company;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.PostalAddress = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactPostalAddress();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.PostalAddress.Street = StoreAddress.Address1;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.PostalAddress.City = StoreAddress.City;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.PostalAddress.State = StoreAddress.StateProvince.Abbreviation;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.PostalAddress.PostalCode = StoreAddress.ZipPostalCode;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.PostalAddress.Country = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactPostalAddressCountry();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.PostalAddress.Country.isoCountryCode = StoreAddress.Country.TwoLetterIsoCode;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[0].Contact.PostalAddress.Country.Value = StoreAddress.Country.Name;

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1] = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartner();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContact();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.role = "soldTo";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.Name = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactName();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.Name.lang = "en";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.Name.Value = order.BillingAddress.Company;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.PostalAddress = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactPostalAddress();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.PostalAddress.Street = order.BillingAddress.Address1 + (!String.IsNullOrWhiteSpace(order.BillingAddress.Address2) ? ", " + order.BillingAddress.Address2 : "");
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.PostalAddress.City = order.BillingAddress.City;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.PostalAddress.State = order.BillingAddress.StateProvince.Abbreviation;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.PostalAddress.PostalCode = order.BillingAddress.ZipPostalCode;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.PostalAddress.Country = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoicePartnerContactPostalAddressCountry();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.PostalAddress.Country.isoCountryCode = order.BillingAddress.Country.TwoLetterIsoCode;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoicePartner[1].Contact.PostalAddress.Country.Value = order.BillingAddress.Country.Name;

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShipping();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContact[2];
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0] = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContact();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].role = "shipTo";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].Name = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactName();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].Name.lang = "en";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].Name.Value = order.ShippingAddress.Company;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].PostalAddress = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactPostalAddress();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].PostalAddress.Street = order.ShippingAddress.Address1 + (!String.IsNullOrWhiteSpace(order.ShippingAddress.Address2) ? ", " + order.ShippingAddress.Address2 : "");
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].PostalAddress.City = order.ShippingAddress.City;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].PostalAddress.State = order.ShippingAddress.StateProvince.Abbreviation;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].PostalAddress.PostalCode = order.ShippingAddress.ZipPostalCode;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].PostalAddress.Country = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactPostalAddressCountry();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].PostalAddress.Country.isoCountryCode = order.ShippingAddress.Country.TwoLetterIsoCode;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[0].PostalAddress.Country.Value = order.ShippingAddress.Country.Name;

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1] = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContact();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].role = "shipFrom";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].Name = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactName();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].Name.lang = "en";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].Name.Value = StoreAddress.Company + " (" + WarehouseForInvoice.Name + ")";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].PostalAddress = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactPostalAddress();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].PostalAddress.Street = WarehouseForInvoice.Address.Address1 + (!String.IsNullOrWhiteSpace(WarehouseForInvoice.Address.Address2) ? ", " + WarehouseForInvoice.Address.Address2 : "");
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].PostalAddress.City = WarehouseForInvoice.Address.City;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].PostalAddress.State = WarehouseForInvoice.Address.StateProvince.Abbreviation;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].PostalAddress.PostalCode = WarehouseForInvoice.Address.ZipPostalCode;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].PostalAddress.Country = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailShippingContactPostalAddressCountry();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].PostalAddress.Country.isoCountryCode = WarehouseForInvoice.Address.Country.TwoLetterIsoCode;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailShipping.Contact[1].PostalAddress.Country.Value = WarehouseForInvoice.Address.Country.Name;

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailPaymentTerm = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderInvoiceDetailPaymentTerm();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailPaymentTerm.payInNumberOfDays = this.GetOrderRequestPaymentTermsPayInNumberOfDays(order); // InvoiceForOrder.TERM_DESC.Trim();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.InvoiceDetailPaymentTerm.percentageRate = 0;

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.Extrinsic = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderExtrinsic[2];
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.Extrinsic[0] = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderExtrinsic();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.Extrinsic[0].name = "invoiceSubmissionMethod";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.Extrinsic[0].Value = "EDI";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.Extrinsic[1] = new cXMLRequestInvoiceDetailRequestInvoiceDetailRequestHeaderExtrinsic();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.Extrinsic[1].name = "invoiceSourceDocument";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.Extrinsic[1].Value = "PurchaseOrder";

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailOrder = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrder();

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailOrder.InvoiceDetailOrderInfo = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfo();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailOrder.InvoiceDetailOrderInfo.OrderReference = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfoOrderReference();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailOrder.InvoiceDetailOrderInfo.OrderReference.orderID = $"PO{PurchaseOrderNumber}"; ;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailOrder.InvoiceDetailOrderInfo.OrderReference.orderDate = order.CreatedOnUtc.ToString("yyyy-MM-ddTHH:mm:ssK");
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailOrder.InvoiceDetailOrderInfo.OrderReference.DocumentReference = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfoOrderReferenceDocumentReference();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailOrder.InvoiceDetailOrderInfo.OrderReference.DocumentReference.payloadID = this.GetOrderRequestPayloadIdFromOrder(order);
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailOrder.InvoiceDetailOrderInfo.MasterAgreementIDInfo = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailOrderInfoMasterAgreementIDInfo();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailOrder.InvoiceDetailOrderInfo.MasterAgreementIDInfo.agreementID = $"PO{PurchaseOrderNumber}"; ;

                            List<cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItem> InvoiceItems = new List<cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItem>();

                            foreach (var invoiceItem in invoiceToBuild.InvoiceItems.Where(w => !String.IsNullOrWhiteSpace(w.ITEM_CODE) && (w.SHIP_TODAY.HasValue && w.SHIP_TODAY.Value > 0))) {
                                var orderItem = order.OrderItems.FirstOrDefault(f => f.Product.Sku == invoiceItem.ITEM_CODE.Trim());

                                if (orderItem != null) {
                                    cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItem InvoiceDetailItem = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItem();

                                    var originalLineNumber = GetOrderRequestItemLineNumber(order, invoiceItem.ITEM_CODE.Trim(), orderItem.Quantity);

                                    var unitOfMeasure = orderItem.Product.ProductSpecificationAttributes.Where(w => w.SpecificationAttributeOption.SpecificationAttribute.Name == "Unit of Measure").FirstOrDefault();
                                    var uom = ConvertUnitOfMeasureToAbbreviation(unitOfMeasure);

                                    InvoiceDetailItem.quantity = invoiceItem.SHIP_TODAY.ToString();
                                    InvoiceDetailItem.invoiceLineNumber = int.Parse(invoiceItem.LINE_NO).ToString();
                                    InvoiceDetailItem.UnitOfMeasure = uom;
                                    InvoiceDetailItem.UnitPrice = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemUnitPrice();
                                    InvoiceDetailItem.UnitPrice.Money = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemUnitPriceMoney();
                                    InvoiceDetailItem.UnitPrice.Money.currency = "USD";
                                    InvoiceDetailItem.UnitPrice.Money.Value = invoiceItem.UNIT_PRICE.Value;

                                    InvoiceDetailItem.InvoiceDetailItemReference = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemInvoiceDetailItemReference();
                                    InvoiceDetailItem.InvoiceDetailItemReference.lineNumber = !String.IsNullOrWhiteSpace(originalLineNumber) ? originalLineNumber : int.Parse(invoiceItem.LINE_NO).ToString();
                                    InvoiceDetailItem.InvoiceDetailItemReference.ItemID = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemInvoiceDetailItemReferenceItemID();
                                    InvoiceDetailItem.InvoiceDetailItemReference.ItemID.SupplierPartID = invoiceItem.ITEM_CODE.Trim();
                                    InvoiceDetailItem.InvoiceDetailItemReference.Description = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemInvoiceDetailItemReferenceDescription();
                                    InvoiceDetailItem.InvoiceDetailItemReference.Description.lang = "en";
                                    InvoiceDetailItem.InvoiceDetailItemReference.Description.Value = orderItem != null && orderItem.Product != null ? orderItem.Product.Name : invoiceItem.LINE_MESSAGE.Trim();

                                    InvoiceDetailItem.SubtotalAmount = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemSubtotalAmount();
                                    InvoiceDetailItem.SubtotalAmount.Money = new cXMLRequestInvoiceDetailRequestInvoiceDetailOrderInvoiceDetailItemSubtotalAmountMoney();
                                    InvoiceDetailItem.SubtotalAmount.Money.currency = "USD";
                                    InvoiceDetailItem.SubtotalAmount.Money.Value = invoiceItem.EXT_PRICE.Value;

                                    InvoiceItems.Add(InvoiceDetailItem);
                                }
                            }

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailOrder.InvoiceDetailItem = InvoiceItems.ToArray();

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummary();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.SubtotalAmount = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummarySubtotalAmount();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.SubtotalAmount.Money = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummarySubtotalAmountMoney();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.SubtotalAmount.Money.currency = "USD";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.SubtotalAmount.Money.Value = invoiceToBuild.INVOICE_AMT.Value - invoiceToBuild.SALES_TAX_AMT.Value - invoiceToBuild.FREIGHT_AMT.Value;

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.Tax = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryTax();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.Tax.Money = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryTaxMoney();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.Tax.Money.currency = "USD";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.Tax.Money.Value = invoiceToBuild.SALES_TAX_AMT.Value;
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.Tax.Description = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryTaxDescription();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.Tax.Description.lang = "en";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.Tax.Description.Value = "Sales Tax";

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.ShippingAmount = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryShippingAmount();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.ShippingAmount.Money = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryShippingAmountMoney();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.ShippingAmount.Money.currency = "USD";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.ShippingAmount.Money.Value = invoiceToBuild.FREIGHT_AMT.Value;

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.GrossAmount = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryGrossAmount();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.GrossAmount.Money = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryGrossAmountMoney();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.GrossAmount.Money.currency = "USD";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.GrossAmount.Money.Value = invoiceToBuild.INVOICE_AMT.Value;

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.NetAmount = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryNetAmount();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.NetAmount.Money = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryNetAmountMoney();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.NetAmount.Money.currency = "USD";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.NetAmount.Money.Value = invoiceToBuild.INVOICE_AMT.Value;

                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.DueAmount = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryDueAmount();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.DueAmount.Money = new cXMLRequestInvoiceDetailRequestInvoiceDetailSummaryDueAmountMoney();
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.DueAmount.Money.currency = "USD";
                            InvoiceRequest.Request.InvoiceDetailRequest.InvoiceDetailSummary.DueAmount.Money.Value = invoiceToBuild.INVOICE_AMT.Value;

                            InvoiceRequests.Add(InvoiceRequest);
                        }
                    }
                }
            }

            return InvoiceRequests;
        }

        public cXML BuildNotificationForShipment(Shipment shipment)
        {
            cXML shipNotificationRequest = null;

            if (shipment != null)
            {                
                var matchingInvoice = NopHelper.ErpService.GetInvoice(shipment.ErpInvoiceId);

                if (matchingInvoice != null)
                {
                    var payloadId = GeneratePayloadID();
                    var storeAddress = NopHelper.GetStoreAddress();
                    string purchaseOrderNumber = this.NopHelper.GetPurchaseOrderNumber(shipment.Order);

                    shipNotificationRequest = new cXML();

                    shipNotificationRequest.payloadID = payloadId;
                    shipNotificationRequest.timestamp = GenerateTimeStamp();
                    shipNotificationRequest.version = CXML_VERSION;

                    shipNotificationRequest.Header = new cXMLHeader();

                    var vendorNetworkId = this.NopHelper.GetVendorNetworkId(shipment.Order.Customer);
                    var supplierNetworkId = this.NopHelper.GetSupplierNetworkId(shipment.Order.Customer);

                    shipNotificationRequest.Header.From = new cXMLHeaderFrom();
                    shipNotificationRequest.Header.From.Credential = new cXMLHeaderFromCredential();
                    shipNotificationRequest.Header.From.Credential.domain = "NetworkId";
                    shipNotificationRequest.Header.From.Credential.Identity = supplierNetworkId;

                    shipNotificationRequest.Header.To = new cXMLHeaderTo();
                    shipNotificationRequest.Header.To.Credential = new cXMLHeaderToCredential();
                    shipNotificationRequest.Header.To.Credential.domain = "NetworkId";
                    shipNotificationRequest.Header.To.Credential.Identity = vendorNetworkId;

                    shipNotificationRequest.Header.Sender = new cXMLHeaderSender();
                    shipNotificationRequest.Header.Sender.Credential = new cXMLHeaderSenderCredential();
                    shipNotificationRequest.Header.Sender.Credential.domain = "NetworkId";
                    shipNotificationRequest.Header.Sender.Credential.Identity = supplierNetworkId;
                    shipNotificationRequest.Header.Sender.Credential.SharedSecret = this.NopHelper.GetCustomersSharedSecret(shipment.Order.Customer);
                    shipNotificationRequest.Header.Sender.UserAgent = "Supplier";

                    shipNotificationRequest.Request = new cXMLRequest();
                    shipNotificationRequest.Request.deploymentMode = "test";

                    shipNotificationRequest.Request.ShipNoticeRequest = new cXMLRequestShipNoticeRequest();

                    // Ship Notice Header
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader = new cXMLRequestShipNoticeRequestShipNoticeHeader();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.shipmentID = shipment.Id.ToString();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.noticeDate = GenerateTimeStamp();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.shipmentDate = GenerateTimeStamp(shipment.ShippedDateUtc);
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.shipmentType = "actual";
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.operation = "new";

                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact = new cXMLRequestShipNoticeRequestShipNoticeHeaderContact[2];

                    // Ship From
                    //Don't have this so hoping it is optional
                    //shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.deliveryDate = GenerateTimeStamp(shipment.DeliveryDateUtc);
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0] = new cXMLRequestShipNoticeRequestShipNoticeHeaderContact();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].role = "shipFrom";
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].Name = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactName();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].Name.lang = "en";
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].Name.Value = storeAddress.Company;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].PostalAddress = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactPostalAddress();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].PostalAddress.Street = storeAddress.Address1;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].PostalAddress.City = storeAddress.City;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].PostalAddress.State = storeAddress.StateProvince.Abbreviation;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].PostalAddress.PostalCode = storeAddress.ZipPostalCode;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].PostalAddress.Country = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactPostalAddressCountry();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].PostalAddress.Country.isoCountryCode = storeAddress.Country.TwoLetterIsoCode;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].PostalAddress.Country.Value = storeAddress.Country.Name;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].Phone = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhone();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].Phone.TelephoneNumber = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhoneTelephoneNumber();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].Phone.TelephoneNumber.CountryCode = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhoneTelephoneNumberCountryCode();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].Phone.TelephoneNumber.CountryCode.isoCountryCode = storeAddress.Country.TwoLetterIsoCode;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].Phone.TelephoneNumber.AreaOrCityCode = storeAddress.PhoneNumber.Substring(0, 3);
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[0].Phone.TelephoneNumber.Number = storeAddress.PhoneNumber.Substring(3);

                    // Ship To                
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1] = new cXMLRequestShipNoticeRequestShipNoticeHeaderContact();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].role = "shipTo";
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].Name = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactName();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].Name.lang = "en";
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].Name.Value = shipment.Order.ShippingAddress.Company;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].PostalAddress = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactPostalAddress();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].PostalAddress.Street = shipment.Order.ShippingAddress.Address1;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].PostalAddress.City = shipment.Order.ShippingAddress.City;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].PostalAddress.State = shipment.Order.ShippingAddress.StateProvince.Abbreviation;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].PostalAddress.PostalCode = shipment.Order.ShippingAddress.ZipPostalCode;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].PostalAddress.Country = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactPostalAddressCountry();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].PostalAddress.Country.isoCountryCode = shipment.Order.ShippingAddress.Country.TwoLetterIsoCode;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].PostalAddress.Country.Value = shipment.Order.ShippingAddress.Country.Name;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].Phone = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhone();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].Phone.TelephoneNumber = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhoneTelephoneNumber();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].Phone.TelephoneNumber.CountryCode = new cXMLRequestShipNoticeRequestShipNoticeHeaderContactPhoneTelephoneNumberCountryCode();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].Phone.TelephoneNumber.CountryCode.isoCountryCode = shipment.Order.ShippingAddress.Country.TwoLetterIsoCode;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].Phone.TelephoneNumber.AreaOrCityCode = shipment.Order.ShippingAddress.PhoneNumber.Substring(0, 3);
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticeHeader.Contact[1].Phone.TelephoneNumber.Number = shipment.Order.ShippingAddress.PhoneNumber.Substring(3);

                    //Ship Control
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipControl = new cXMLRequestShipNoticeRequestShipControl();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipControl.CarrierIdentifier = new cXMLRequestShipNoticeRequestShipControlCarrierIdentifier[1];
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipControl.CarrierIdentifier[0] = new cXMLRequestShipNoticeRequestShipControlCarrierIdentifier();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipControl.CarrierIdentifier[0].domain = shipment.Order.ShippingMethod;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipControl.CarrierIdentifier[0].Value = shipment.Order.ShippingMethod;
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipControl.ShipmentIdentifier = shipment.TrackingNumber;

                    //Ship Notice Portion
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion = new cXMLRequestShipNoticeRequestShipNoticePortion();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.OrderReference = new cXMLRequestShipNoticeRequestShipNoticePortionOrderReference();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.OrderReference.orderID = $"PO{purchaseOrderNumber}";
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.OrderReference.DocumentReference = new cXMLRequestShipNoticeRequestShipNoticePortionOrderReferenceDocumentReference();
                    shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.OrderReference.DocumentReference.payloadID = this.GetOrderRequestPayloadIdFromOrder(shipment.Order);

                    //Ship Notice Item
                    var actualInvoiceItems = matchingInvoice
                        .InvoiceItems
                        .Where(w => !String.IsNullOrWhiteSpace(w.ITEM_CODE) && w.SHIP_TODAY > 0)
                        .OrderBy(ob => ob.LINE_NO)
                        .ToList();
                    
                    var invoiceItemCount = actualInvoiceItems.Count();

                    if (invoiceItemCount > 0)
                    {
                        shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem = new cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItem[invoiceItemCount];

                        var itemIndex = 0;
                        foreach (var invoiceItem in actualInvoiceItems)
                        {
                            var product = NopHelper.ProductService.GetProductBySku(invoiceItem.ITEM_CODE.Trim());

                            if (product != null)
                            {
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex] = new cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItem();
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].shipNoticeLineNumber = (itemIndex + 1).ToString();
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].lineNumber = (itemIndex + 1).ToString();
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].quantity = invoiceItem.SHIP_TODAY.ToString();

                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].ItemID = new cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemItemID();
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].ItemID.SupplierPartID = invoiceItem.ITEM_CODE.Trim();

                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].ShipNoticeItemDetail = new cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetail();
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].ShipNoticeItemDetail.UnitPrice = new cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetailUnitPrice();
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].ShipNoticeItemDetail.UnitPrice.Money = new cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetailUnitPriceMoney();
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].ShipNoticeItemDetail.UnitPrice.Money.currency = "USD";
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].ShipNoticeItemDetail.UnitPrice.Money.Value = invoiceItem.EXT_PRICE.Value;

                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].ShipNoticeItemDetail.Description = new cXMLRequestShipNoticeRequestShipNoticePortionShipNoticeItemShipNoticeItemDetailDescription();
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].ShipNoticeItemDetail.Description.lang = "en-US";
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].ShipNoticeItemDetail.Description.Value = product.Name;

                                var unitOfMeasure = product.ProductSpecificationAttributes.Where(w => w.SpecificationAttributeOption.SpecificationAttribute.Name == "Unit of Measure").FirstOrDefault();
                                var uom = ConvertUnitOfMeasureToAbbreviation(unitOfMeasure);
                                shipNotificationRequest.Request.ShipNoticeRequest.ShipNoticePortion.ShipNoticeItem[itemIndex].UnitOfMeasure = uom;

                                itemIndex++;
                            }                           
                        }
                    }
                    else
                    {
                        shipNotificationRequest = null;
                    }
                }                
            }

            return shipNotificationRequest;
        }

        public cXML CreatePunchoutResponse(HttpStatusCode statusCode)
        {
            cXML Response = new cXML();

            Response.payloadID = GeneratePayloadID();
            Response.timestamp = GenerateTimeStamp();
            Response.version = CXML_VERSION;

            Response.Response = new cXMLResponse()
            {
                Status = new cXMLResponseStatus()
                {
                    code = ((int)statusCode).ToString(),
                    text = statusCode.ToString()                    
                }                
            };

            return Response;
        }

        public cXML DeserializeXmlDocumentToCxml(XmlDocument xmlDoc)
        {
            cXML CxmlDoc = null;

            if (xmlDoc.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
            {
                xmlDoc.RemoveChild(xmlDoc.FirstChild);
            }

            if (xmlDoc.FirstChild.NodeType == XmlNodeType.DocumentType)
            {
                xmlDoc.RemoveChild(xmlDoc.FirstChild);
            }

            XmlSerializer Serializer = new XmlSerializer(typeof(cXML));
            CxmlDoc = (cXML)Serializer.Deserialize(new StringReader(xmlDoc.OuterXml));

            return CxmlDoc;
        }

        public string GeneratePayloadID()
        {
            string PayloadId = Guid.NewGuid().ToString() + CXML_CUSTOMER_EMAIL_SUFFIX;

            return PayloadId;
        }

        public string GetOrderRequestPayloadIdFromOrder(Order order)
        {
            string payloadId = null;

            var orderRequestNote = order.OrderNotes.FirstOrDefault(f => f.Note == "Punchout Order Request");

            if (orderRequestNote != null)
            {
                var xmlDoc = this.NopHelper.GetcXmlDocFromDownload(orderRequestNote.DownloadId);

                if (xmlDoc != null)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(cXML));

                    StringReader reader = new StringReader(xmlDoc.InnerXml);
                    var cXML = (cXML)serializer.Deserialize(reader);

                    if (cXML != null)
                    {
                        payloadId = cXML.payloadID;
                    }
                }
            }

            return payloadId;
        }

        public string GetOrderRequestPaymentTermsPayInNumberOfDays(Order order)
        {
            string paymentTerms = null;

            var orderRequestNote = order.OrderNotes.FirstOrDefault(f => f.Note == "Punchout Order Request");

            if (orderRequestNote != null)
            {
                var xmlDoc = this.NopHelper.GetcXmlDocFromDownload(orderRequestNote.DownloadId);

                if (xmlDoc != null)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(cXML));

                    StringReader reader = new StringReader(xmlDoc.InnerXml);
                    var cXML = (cXML)serializer.Deserialize(reader);

                    if (cXML != null)
                    {
                        paymentTerms = cXML.Request.OrderRequest.OrderRequestHeader.PaymentTerm.payInNumberOfDays;
                    }
                }
            }

            return paymentTerms;
        }

        public string GetOrderRequestItemLineNumber(Order order, string sku, int requestedQuantity)
        {
            string lineNumber = null;

            var orderRequestNote = order.OrderNotes.FirstOrDefault(f => f.Note == "Punchout Order Request");

            if (orderRequestNote != null)
            {
                var xmlDoc = this.NopHelper.GetcXmlDocFromDownload(orderRequestNote.DownloadId);

                if (xmlDoc != null)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(cXML));

                    StringReader reader = new StringReader(xmlDoc.InnerXml);
                    var cXML = (cXML)serializer.Deserialize(reader);

                    if (cXML != null)
                    {
                        var orderItem = cXML.Request.OrderRequest.ItemOut.FirstOrDefault(f => f.ItemID.SupplierPartID.ToLower() == sku.ToLower() && f.quantity == requestedQuantity.ToString());
                        
                        if (orderItem != null) {
                            lineNumber = orderItem.lineNumber;
                        }
                    }
                }
            }

            return lineNumber;
        }

        public string GetLastCXMLInvoiceNumberFromOrderNotes(Order order)
        {
            string invoiceNumber = null;

            var orderInvoiceNote = order.OrderNotes.OrderByDescending(obd => obd.CreatedOnUtc).FirstOrDefault(f => f.Note.StartsWith("Punchout Invoice"));

            if (orderInvoiceNote != null)
            {
                var xmlDoc = this.NopHelper.GetcXmlDocFromDownload(orderInvoiceNote.DownloadId);

                if (xmlDoc != null)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(cXML));

                    StringReader reader = new StringReader(xmlDoc.InnerXml);
                    var cXML = (cXML)serializer.Deserialize(reader);

                    if (cXML != null)
                    {
                        invoiceNumber = cXML.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.invoiceID;
                    }
                }
            }

            return invoiceNumber;
        }

        public List<string> GetUnsentInvoices(Order order) {
            var unsentInvoiceIds = new List<string>();

            var invoiceIdsToCheck = order
                .ERPInvoiceId
                .Split('|')
                .OrderBy(ob => ob)
                .ToList();

            foreach(var invoiceId in invoiceIdsToCheck) {
                var sent = order.OrderNotes.Any(a => a.Note == $"Punchout Invoice (#{invoiceId})");

                if (!sent) {
                    unsentInvoiceIds.Add(invoiceId);
                }
            }

            return unsentInvoiceIds;
        }

        public string GenerateTimeStamp(DateTime? date = null)
        {
            var timeStamp = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssK");
            
            if (date.HasValue)
            {
                timeStamp = date.Value.ToString("yyyy-MM-ddTHH:mm:sszzz");
            }

            return timeStamp;
        }

        public XmlDocument SerializeCxmlToXmlDocument(cXML cXMLDocument, cXMLDocumentType cXMLDocumentType)
        {
            XmlDocument XmlDoc = new XmlDocument();

            XmlDeclaration Declaration = XmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlDoc.AppendChild(Declaration);

            string systemId = GetSystemId(cXMLDocumentType);

            XmlDocumentType DocType = XmlDoc.CreateDocumentType("cXML", null, systemId, null);
            XmlDoc.AppendChild(DocType);

            XPathNavigator XPathNav = XmlDoc.CreateNavigator();

            using (XmlWriter writer = XPathNav.AppendChild())
            {
                XmlSerializer Serializer = new XmlSerializer(typeof(cXML));

                XmlSerializerNamespaces EmptyNamespace = new XmlSerializerNamespaces();
                EmptyNamespace.Add("", "");

                Serializer.Serialize(writer, cXMLDocument, EmptyNamespace);
            }

            return XmlDoc;
        }

        public static string GetSystemId(cXMLDocumentType cXMLDocumentType)
        {
            string systemId;
            switch (cXMLDocumentType)
            {
                case cXMLDocumentType.OrderRequest:
                    systemId = cXMLHelper.CXML_DTD_URL;
                    break;
                case cXMLDocumentType.OrderConfirmation:
                    systemId = cXMLHelper.CXML_FULFILL_DTD_URL;
                    break;
                case cXMLDocumentType.Invoice:
                    systemId = cXMLHelper.CXML_INVOICE_DTD_URL;
                    break;
                case cXMLDocumentType.ShipNotice:
                    systemId = cXMLHelper.CXML_FULFILL_DTD_URL;
                    break;
                default:
                    systemId = cXMLHelper.CXML_DTD_URL;
                    break;
            }

            return String.Format(systemId, cXMLHelper.CXML_VERSION);
        }

        public IRestResponse SendcXML(XmlDocument cXML, string endpoint)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var client = new RestClient(endpoint);
            client.Timeout = -1;

            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/xml");
            request.AddParameter("application/xml", cXML.InnerXml, ParameterType.RequestBody);
            var response = client.Execute(request);

            return response;
        }

        private string ConvertUnitOfMeasureToAbbreviation(ProductSpecificationAttribute unitOfMeasureAttribute) {
            var uom = "EA";

            if (unitOfMeasureAttribute != null && unitOfMeasureAttribute.SpecificationAttributeOption != null) {
                var unitOfMeasure = unitOfMeasureAttribute.SpecificationAttributeOption.Name.Trim().ToUpper();

                switch (unitOfMeasure) {
                    case "BAG":
                        uom = "BG";
                        break;
                    case "CASE":
                        uom = "CS";
                        break;
                    case "CTN": 
                        uom = "CT";
                        break;
                    case "EACH":
                        uom = "EA";
                        break;
                    default:
                        uom = "EA";
                        break;
                }
            }

            return uom;
        }
    }
}
