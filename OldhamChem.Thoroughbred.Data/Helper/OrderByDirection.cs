﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Helper
{
    public enum OrderByDirection
    {
        ASC,
        DESC
    }
}
