﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Helper
{
    public class OrderByExpression<T>
    {
        public Expression<Func<T, object>> Expression { get; set; }

        public OrderByDirection Direction { get; set; }
    }
}
