﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Helper
{
    public static class QueryableHelper
    {
        public static IOrderedQueryable<TSource> OrderByHelper<TSource>(this IQueryable<TSource> source, OrderByExpression<TSource> orderByExpression)// Expression<Func<TSource, object>> keySelector)
        {
            return OrderByExpressionHelper(source, orderByExpression.Expression, false, orderByExpression.Direction);
        }

        public static IOrderedQueryable<TSource> ThenByHelper<TSource>(this IQueryable<TSource> source, OrderByExpression<TSource> orderByExpression)
        {
            return OrderByExpressionHelper(source, orderByExpression.Expression, true, orderByExpression.Direction);
        }

        /// <summary>
        /// Allows for using a dynamic order by clause that has a parameter output type of object
        /// http://stackoverflow.com/questions/29972051/specification-pattern-using-entity-framework-order-by-property
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        private static IOrderedQueryable<TSource> OrderByExpressionHelper<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, object>> keySelector, bool isSecondary = false, OrderByDirection orderByDirection = OrderByDirection.ASC)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (keySelector == null)
                throw new ArgumentNullException("keySelector");

            // While the return type of keySelector is object, the "type" of 
            // keySelector.Body is the "real" type *or* it is a
            // Convert(body). We rebuild a new Expression with this "correct" 
            // Body (removing the Convert if present). The return type is
            // automatically chosen from the type of the keySelector.Body .
            Expression body = keySelector.Body;

            if (body.NodeType == ExpressionType.Convert)
            {
                body = ((UnaryExpression)body).Operand;
            }

            LambdaExpression keySelector2 = Expression.Lambda(body, keySelector.Parameters);

            Type tkey = keySelector2.ReturnType;

            string OrderByMethodName = "OrderBy";

            if (isSecondary)
                OrderByMethodName = "ThenBy";

            if (orderByDirection == OrderByDirection.DESC)
                OrderByMethodName += "Descending";

            MethodInfo orderbyMethod = (from x in typeof(Queryable).GetMethods()
                                        where x.Name == OrderByMethodName
                                        let parameters = x.GetParameters()
                                        where parameters.Length == 2
                                        let generics = x.GetGenericArguments()
                                        where generics.Length == 2
                                        // BEGIN: These lines were commented out from the original code.
                                        //        They worked fine for OrderBy but prevented a method from being returned when looking for ThenBy or ThenByDescending
                                        //        After breaking apart the query I was only returned 1 method for both OrderBy(s) and ThenBy(s) at this point.
                                        //where parameters[0].ParameterType == typeof(IQueryable<>).MakeGenericType(generics[0]) &&
                                        //    parameters[1].ParameterType == typeof(Expression<>).MakeGenericType(typeof(Func<,>).MakeGenericType(generics[0], generics[1]))
                                        // END:
                                        select x).Single();

            Type[] OrderByType = new Type[]
            {
                typeof(TSource),
                tkey
            };

            MethodInfo GenericOrderByMethod = orderbyMethod.MakeGenericMethod(OrderByType);

            Expression[] TypeSpecificExpression = new Expression[]
            {
                        source.Expression,
                        Expression.Quote(keySelector2)
            };

            return (IOrderedQueryable<TSource>)source.Provider.CreateQuery<TSource>(Expression.Call(null, GenericOrderByMethod, TypeSpecificExpression));
            
        }       

    }
}
