using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    [Table("OrderQueueItem")]
    public partial class OrderQueueItem
    {
        [ForeignKey("OrderQueue")]
        [Key]
        [Column(Order = 0)]
        [StringLength(10)]
        public string OnlineOrderNumber { get; set; }

        [Required]
        [StringLength(24)]
        [Key]
        [Column(Order = 1)]
        public string ItemCode { get; set; }

        [Required]
        [Key]
        [Column(Order = 2)]
        public int LineNumber { get; set; }

        public decimal Price { get; set; }

        public decimal Quantity { get; set; }

        [Required]
        [StringLength(5)]
        public string UnitOfMeasure { get; set; }

        [ForeignKey("OnlineOrderNumber")]
        public virtual OrderQueue OrderQueue { get; set; }
    }
}
