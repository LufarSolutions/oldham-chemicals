﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    [Table("OpenPO")]
    public partial class OpenPO
    {
        [Key]
        [Column(Order = 0)]
        public string VEND_NO { get; set; }

        [Key]
        [Column(Order = 1)]
        public string ITEM_CODE { get; set; }

        [Key]
        [Column(Order = 2)]
        public string WHSE_CODE { get; set; }

        public decimal QTY_ON_ORDER { get; set; }
    }
}
