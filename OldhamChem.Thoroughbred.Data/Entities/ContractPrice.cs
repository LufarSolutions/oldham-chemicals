namespace OldhamChem.Thoroughbred.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ContractPrice")]
    public partial class ContractPrice
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(7)]
        public string CustomerNumber { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string PROD_CODE { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(24)]
        public string ITEM_CODE { get; set; }

        public decimal? CONTRACT_PRICE { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }
    }
}
