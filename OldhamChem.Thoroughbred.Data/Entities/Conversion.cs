﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    public partial class Conversion
    {
        [Key]
        public string ITEM_CODE { get; set; }

        public string CONV_ITEM_CODE { get; set; }

        public decimal CONV_FACTOR { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }
    }
}
