using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    [Table("Item")]
    public partial class Item
    {
        public string PROD_CODE { get; set; }

        //[ForeignKey("InvoiceItem")]
        [Key]
        [StringLength(24)]
        public string ITEM_CODE { get; set; }

        [StringLength(30)]
        public string ITEM_DESC { get; set; }

        [StringLength(4)]
        public string UOM { get; set; }

        [StringLength(1)]
        public string TAXABLE { get; set; }

        [StringLength(1)]
        public string DISCONTINUED { get; set; }

        [StringLength(6)]
        public string MFGNUM { get; set; }

        [StringLength(15)]
        public string PROD_TYPE { get; set; }

        [StringLength(30)]
        public string PROD_DESC { get; set; }

        [StringLength(15)]
        public string VENDOR { get; set; }

        public decimal? COST_DIVISOR { get; set; }

        public decimal? UNIT_PRICE_1 { get; set; }

        public decimal? UNIT_PRICE_2 { get; set; }

        public decimal? UNIT_PRICE_3 { get; set; }

        public decimal? UNIT_PRICE_4 { get; set; }

        [StringLength(1)]
        public string AGENCY { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }
        
        [ForeignKey("VENDOR")]
        public virtual Manufacturer Manufacturer { get; set; }
    }
}
