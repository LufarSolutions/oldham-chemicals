namespace OldhamChem.Thoroughbred.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CustomerAddress
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(7)]
        public string CustomerNumber { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(4)]
        public string SHIP_TO_NO { get; set; }

        [StringLength(24)]
        public string CUST_SHIP_TO_NAME { get; set; }

        [StringLength(24)]
        public string CUST_SHIP_TO_ADDR1 { get; set; }

        [StringLength(24)]
        public string CUST_SHIP_TO_ADDR2 { get; set; }

        [StringLength(24)]
        public string CUST_SHIP_TO_CITY { get; set; }

        [StringLength(2)]
        public string CUST_SHIP_TO_STATE { get; set; }

        [StringLength(10)]
        public string CUST_SHIP_TO_ZIP { get; set; }

        [StringLength(24)]
        public string CUST_SHIP_TO_CONTACT { get; set; }

        [StringLength(5)]
        public string TAX_CODE { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }
    }
}
