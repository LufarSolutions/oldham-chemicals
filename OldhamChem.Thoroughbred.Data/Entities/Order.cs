﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    [Table("Order")]
    public partial class Order
    {
        [Key]
        [StringLength(7)]
        public string ORDER_NO { get; set; }

        public string ORDER_TYPE { get; set; }
        
        public string BATCH_NO { get; set; }
        
        public string REPRINT { get; set; }
        
        public string DS_SPEC { get; set; }
        
        public string FREIGHT_CHG { get; set; }
        
        public string CUST_DIV_NO { get; set; }
        
        public string CUST_NO { get; set; }
        
        public string CONTRACT_PRICE_LINK { get; set; }
        
        public string SHIP_TO_CODE { get; set; }
        
        public string CUST_ORDER_NO { get; set; }
        
        public string SHIP_VIA { get; set; }
        
        public string F_O_B { get; set; }
        
        public DateTime? ORDER_DATE { get; set; }
        
        public DateTime? SHIP_DATE { get; set; }
        
        public DateTime? INVOICE_DATE { get; set; }
        
        public string TIMES_UPD { get; set; }
        
        public string LAST_INV_NO_UPD { get; set; }
        
        public string SALESMAN_NO { get; set; }
        
        public string TERM_DESC { get; set; }
        
        public DateTime? DUE_DATE { get; set; }
        
        public string INVOICED_BY { get; set; }
        
        public string FRT_TAXABLE { get; set; }
        
        public string WAREHOUSE { get; set; }
        
        public string DUE_DAYS { get; set; }
        
        public string CHEM_PRICE_CODE { get; set; }
        
        public string DISC_DAYS { get; set; }
        
        public string EQUIPMENT_PRICE { get; set; }
        
        public string NEW_TAX_CODE { get; set; }
        
        public string CUST_PO_NUM { get; set; }
        
        public string MASTER_BOL_NUMBER { get; set; }
        
        public string WORK_ORDER { get; set; }
        
        public string TERMIDOR_ORDER { get; set; }
        
        public string OPERATOR_CODE { get; set; }
        
        public string TICKET_WHSE_FROM { get; set; }
        
        public decimal? GROSS_SALES_1 { get; set; }
        
        public decimal? GROSS_SALES_2 { get; set; }
        
        public decimal? GROSS_SALES_3 { get; set; }
        
        public decimal? GROSS_SALES_4 { get; set; }
        
        public decimal? OTHER { get; set; }
        
        public decimal? COST_SALES { get; set; }
        
        public decimal? MISC_CHGS { get; set; }
        
        public decimal? TAXABLE_TOTAL { get; set; }
        
        public decimal? DISCOUNT_AMT { get; set; }
        
        public decimal? SALES_TAX_AMT { get; set; }
        
        public decimal? FREIGHT_AMT { get; set; }
        
        public decimal? INVOICE_AMT { get; set; }
        
        public decimal? COMM_BASE { get; set; }
        
        public decimal? TAX_RATE { get; set; }
        
        public decimal? DISC_RATE { get; set; }
        
        public decimal? COMM_RATE { get; set; }
        
        public decimal? TERM_DISC_RATE { get; set; }
        
        public string SHIP_TO_NAME { get; set; }
        
        public string SHIP_TO_ADDR1 { get; set; }
        
        public string SHIP_TO_ADDR2 { get; set; }
        
        public string SHIP_TO_ADDR3 { get; set; }
        
        public string SHIP_TO_ADDR4 { get; set; }
        
        public string SHIP_TO_ADDR5 { get; set; }
        
        public string SHIP_TO_CITY { get; set; }
        
        public string SHIP_TO_STATE { get; set; }
        
        public string SHIP_TO_ZIP { get; set; }
        
        public DateTime? DATE_TIME_CHANGED { get; set; }
                
        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }
}
