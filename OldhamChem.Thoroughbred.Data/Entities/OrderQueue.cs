using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace OldhamChem.Thoroughbred.Data.Entities
{

    [Table("OrderQueue")]
    public partial class OrderQueue
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OrderQueue()
        {
            OrderQueueItems = new HashSet<OrderQueueItem>();
        }

        [Key]
        [Required]
        [StringLength(10)]
        public string OnlineOrderNumber { get; set; }

        [Required]
        [StringLength(7)]
        public string CustomerNumber { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateProcessed { get; set; }

        public string Error { get; set; }

        [StringLength(90)]
        public string Notes { get; set; }

        [StringLength(255)]
        public string NotificationEmails { get; set; }

        [StringLength(7)]
        public string OrderNumber { get; set; }

        [StringLength(150)]
        public string PurchaseOrderNumber { get; set; }
        
        [StringLength(4)]
        public string ShipToNumber { get; set; }

        [StringLength(3)]
        public string WarehouseCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderQueueItem> OrderQueueItems { get; set; }

    }
}
