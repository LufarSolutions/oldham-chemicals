using System.Data.Entity;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    public partial class ThoroughbredDbContext : DbContext
    {
        public ThoroughbredDbContext() : base("name=ThoroughbredDbContext")
        {
            // Disable Lazy Loading to aid in performance
            this.Configuration.LazyLoadingEnabled = false;

            // Disable data migrations
            Database.SetInitializer<ThoroughbredDbContext>(null);
        }

        //public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<ContractPrice> ContractPrices { get; set; }
        public virtual DbSet<Conversion> Conversions { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerAddress> CustomerAddresses { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<InvoiceItem> InvoiceItems { get; set; }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<ItemInventory> ItemInventories { get; set; }
        public virtual DbSet<ItemVendor> ItemVendors { get; set; }
        public virtual DbSet<Manufacturer> Manufacturers { get; set; }
        public virtual DbSet<OpenPO> OpenPOs { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderQueue> OrderQueues { get; set; }
        public virtual DbSet<OrderQueueItem> OrderQueueItems { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }

        public virtual DbSet<ItemSalesHistoryAndInventory> ItemSalesHistoriesAndInventory { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //modelBuilder.Configurations.Add(new Mapping.CategoryMap());
            modelBuilder.Configurations.Add(new Mapping.ContractPriceMap());
            modelBuilder.Configurations.Add(new Mapping.ConversionMap());
            modelBuilder.Configurations.Add(new Mapping.CustomerMap());
            modelBuilder.Configurations.Add(new Mapping.CustomerAddressMap());
            modelBuilder.Configurations.Add(new Mapping.InvoiceMap());
            modelBuilder.Configurations.Add(new Mapping.InvoiceItemMap());
            modelBuilder.Configurations.Add(new Mapping.ItemMap());
            modelBuilder.Configurations.Add(new Mapping.ItemInventoryMap());
            modelBuilder.Configurations.Add(new Mapping.ItemVendorMap());
            modelBuilder.Configurations.Add(new Mapping.ManufacturerMap());
            modelBuilder.Configurations.Add(new Mapping.OpenPOMap());
            modelBuilder.Configurations.Add(new Mapping.OrderMap());
            modelBuilder.Configurations.Add(new Mapping.OrderQueueMap());
            modelBuilder.Configurations.Add(new Mapping.OrderQueueItemMap());
            modelBuilder.Configurations.Add(new Mapping.WarehouseMap());
            modelBuilder.Configurations.Add(new Mapping.ItemSalesHistoryAndInventoryMap());

        }
    }
}
