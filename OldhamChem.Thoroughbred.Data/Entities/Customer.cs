namespace OldhamChem.Thoroughbred.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Customer")]
    public partial class Customer
    {
        [Key]
        [StringLength(7)]
        public string CustomerNumber { get; set; }

        [StringLength(24)]
        public string CUST_NAME { get; set; }

        [StringLength(24)]
        public string CUST_ADDR1 { get; set; }

        [StringLength(24)]
        public string CUST_ADDR2 { get; set; }

        [StringLength(24)]
        public string CUST_CITY { get; set; }

        [StringLength(2)]
        public string CUST_STATE { get; set; }

        [StringLength(10)]
        public string CUST_ZIP { get; set; }

        [StringLength(10)]
        public string CUST_PHONE { get; set; }

        [StringLength(24)]
        public string CUST_CONTACT { get; set; }

        [StringLength(2)]
        public string SALES_REP { get; set; }

        [StringLength(3)]
        public string BRANCH { get; set; }

        [StringLength(1)]
        public string CHEM_PRICE_CODE { get; set; }

        [StringLength(60)]
        public string EMAIL { get; set; }

        public decimal? CURRENT { get; set; }

        [Column("30_DAYS")]
        public decimal? C30_DAYS { get; set; }

        [Column("60_DAYS")]
        public decimal? C60_DAYS { get; set; }

        [Column("90_DAYS")]
        public decimal? C90_DAYS { get; set; }

        [Column("120_DAYS")]
        public decimal? C120_DAYS { get; set; }

        public decimal? CREDIT_LIMIT { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }

        public virtual ICollection<ContractPrice> ContractPrices { get; set; }

        public virtual ICollection<CustomerAddress> CustomerAddresses { get; set; }
        
        public virtual ICollection<Invoice> Invoices { get; set; }
    }
}
