﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    /// <summary>
    /// This maps to a view. Due to the legacy setup of the database this seemed to be the only way to get the data out without an insane amount of trouble.
    /// </summary>
    public partial class ItemSalesHistoryAndInventory
    {
        [Key, Column(Order = 1)]
        [StringLength(6)]
        public string VEND_NO { get; set; }

        [Key, Column(Order = 2)]
        [StringLength(24)]
        public string ITEM_CODE { get; set; }

        [Key, Column(Order = 3)]
        [StringLength(3)]
        public string WHSE_CODE { get; set; }

        //public string UOM { get; set; }

        [Key, Column(Order = 4)]
        public DateTime InvoiceDate { get; set; }

        public int InvoiceYear { get; set; }

        public int InvoiceMonth { get; set; }

        public decimal Units { get; set; }

        public string DISCONTINUED { get; set; }

    }
}
