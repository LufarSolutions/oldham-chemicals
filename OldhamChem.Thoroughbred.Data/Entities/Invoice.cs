using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    [Table("Invoice")]
    public partial class Invoice
    {
        [Key]
        [StringLength(7)]
        public string INV_NO { get; set; }

        [StringLength(7)]
        public string CustomerNumber { get; set; }

        public DateTime? INV_DATE { get; set; }

        [StringLength(7)]
        public string ORDER_NO { get; set; }

        [StringLength(28)]
        public string TERM_DESC { get; set; }

        public DateTime? DUE_DATE { get; set; }

        [StringLength(3)]
        public string WAREHOUSE { get; set; }

        [StringLength(20)]
        public string CUST_PO_NUM { get; set; }

        public decimal? INVOICE_AMT { get; set; }

        public decimal? INVOICE_BALANCE { get; set; }

        public decimal? SALES_TAX_AMT { get; set; }

        public decimal? FREIGHT_AMT { get; set; }

        public string SHIP_TO_CODE { get; set; }

        public string SHIP_VIA { get; set; }

        public DateTime? ORDER_DATE { get; set; }

        public DateTime? SHIP_DATE { get; set; }

        public string SHIP_TO_ADDR1 { get; set; }

        public string SHIP_TO_ADDR2 { get; set; }

        public string SHIP_TO_ADDR3 { get; set; }

        public string SHIP_TO_ADDR4 { get; set; }

        public string SHIP_TO_ADDR5 { get; set; }

        public string TRACKING_NUMBER { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }

        public virtual ICollection<InvoiceItem> InvoiceItems { get; set; }

        public virtual ICollection<InvoiceTracking> InvoiceTracking { get; set; }

        public virtual Customer Customer { get; set; }
        
        [ForeignKey("WAREHOUSE")]
        public virtual Warehouse Warehouse { get; set; }
    }
}
