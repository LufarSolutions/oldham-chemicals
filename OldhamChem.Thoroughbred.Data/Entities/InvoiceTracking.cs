﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    [Table("InvoiceTracking")]
    public partial class InvoiceTracking
    {
        [ForeignKey("Invoice")]
        [Key]
        [Column(Order = 0)]
        [StringLength(7)]
        public string INV_NO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string LINE_NO { get; set; }

        [StringLength(25)]
        public string TRACKING_NUMBER { get; set; }

        public DateTime? SHIP_DATE { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }

        public virtual Invoice Invoice { get; set; }
    }
}
