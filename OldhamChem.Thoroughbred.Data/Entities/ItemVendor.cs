﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    public partial class ItemVendor
    {
        [Key]
        public string ITEM_CODE { get; set; }

        public string VEND_NO { get; set; }

        public string VEND_ITEM_CODE { get; set; }

        public string VEND_ITEM_DESC { get; set; }

        public string UOM { get; set; }

        public decimal UNITS_PACK_SIZE { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }
    }
}
