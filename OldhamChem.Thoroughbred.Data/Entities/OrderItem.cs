﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OldhamChem.Thoroughbred.Data.Entities
{
	[Table("OrderItem")]
	public partial class OrderItem
	{
		[ForeignKey("Order")]
		[Key]
		[Column(Order = 0)]
		[StringLength(7)]
		public string ORDER_NO { get; set; }

		[Key]
		[Column(Order = 1)]
		[StringLength(3)]
		public string LINE_NO { get; set; }
		
		public string LINE_TYPE { get; set; }
		
		public string LINE_MESSAGE { get; set; }
		
		public string UOM { get; set; }
		
		public string PRICE_CODE_CHEM { get; set; }
		
		public string PRICE_CODE_EQUIP { get; set; }
		
		public string TAXABLE { get; set; }
		
		public string COMMISIONABLE { get; set; }
		
		public string DROP_SHIP { get; set; }
		
		public string VEND_NO { get; set; }
		
		public string VEND_INDEX { get; set; }
		
		public string LOCATION { get; set; }
		
		public string PROD_CODE { get; set; }
		
		public string GL_CODE { get; set; }
		
		public string LOT_CONTROL { get; set; }
		
		public string BOM_TYPE { get; set; }
		
		public string KIT_FLAG { get; set; }
		
		public string PROD_CODE_2 { get; set; }
		
		public string CHEM { get; set; }
		
		public string RESTRICTED { get; set; }
		
		public string PROD_CODE_3 { get; set; }
		
		public string ITEM_CODE { get; set; }
		
		public string WHSE_CODE { get; set; }
		
		public decimal? ORDER_TODAY { get; set; }
		
		public decimal? SHIP_TODAY { get; set; }
		
		public decimal? BACKORD_TODAY { get; set; }
		
		public decimal? UNIT_PRICE { get; set; }
		
		public decimal? EXT_PRICE { get; set; }
		
		public decimal? UNIT_COST { get; set; }
		
		public decimal? DISC_RATE { get; set; }
		
		public decimal? ORDERED { get; set; }
		
		public decimal? COMM_RATE { get; set; }
		
		public decimal? EXT_COST { get; set; }
		
		public decimal? NUM_UOM { get; set; }
		
		public DateTime? DATE_TIME_CHANGED { get; set; }
				
		[ForeignKey("ORDER_NO")]
		public virtual Order Order { get; set; }
	}
}
