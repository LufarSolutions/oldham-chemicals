﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    [Table("Product")]
    public partial class Category
    {
        [Key]
        [StringLength(3)]
        public string PROD_CODE { get; set; }

        public string PROD_DESC { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }
    }
}
