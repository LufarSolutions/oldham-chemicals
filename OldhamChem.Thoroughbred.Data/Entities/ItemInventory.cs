namespace OldhamChem.Thoroughbred.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ItemInventory")]
    public partial class ItemInventory
    {
        public string PROD_CODE { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(24)]
        public string ITEM_CODE { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string WHSE_CODE { get; set; }

        public decimal? QTY_ON_HAND { get; set; }

        public decimal? ON_BACK_ORDER { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }

        public virtual Warehouse Warehouse { get; set; }

        [ForeignKey("ITEM_CODE")]
        public virtual Item Item { get; set; }
    }
}
