using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OldhamChem.Thoroughbred.Data.Entities
{
    [Table("InvoiceItem")]
    public partial class InvoiceItem
    {
        [ForeignKey("Invoice")]
        [Key]
        [Column(Order = 0)]
        [StringLength(7)]
        public string INV_NO { get; set; }
                
        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string LINE_NO { get; set; }

        [StringLength(24)]
        public string ITEM_CODE { get; set; }

        public string LINE_TYPE { get; set; }

        public string LINE_MESSAGE { get; set; }

        [StringLength(1)]
        public string TAXABLE { get; set; }

        public decimal? SHIP_TODAY { get; set; }

        public decimal? BACKORD_TODAY { get; set; }

        public decimal? UNIT_PRICE { get; set; }

        public decimal? EXT_PRICE { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }

        public virtual Invoice Invoice { get; set; }

        [ForeignKey("ITEM_CODE")]
        public virtual Item Item { get; set; }
    }
}
