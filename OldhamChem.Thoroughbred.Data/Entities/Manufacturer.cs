namespace OldhamChem.Thoroughbred.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Manufacturer")]
    public partial class Manufacturer
    {
        [Key]
        [StringLength(6)]
        public string VEND_NO { get; set; }

        [StringLength(24)]
        public string VEND_NAME { get; set; }

        [StringLength(24)]
        public string VEND_ADDR1 { get; set; }

        [StringLength(24)]
        public string VEND_ADDR2 { get; set; }

        [StringLength(24)]
        public string VEND_CITY { get; set; }

        [StringLength(2)]
        public string VEND_STATE { get; set; }

        [StringLength(10)]
        public string VEND_ZIP { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }
    }
}
