namespace OldhamChem.Thoroughbred.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Warehouse")]
    public partial class Warehouse
    {
        [Key]
        [StringLength(3)]
        public string WHSE_CODE { get; set; }

        [StringLength(24)]
        public string WHSE_DESC { get; set; }

        public string WHSE_ADDR1 { get; set; }

        public string WHSE_ADDR2 { get; set; }

        public string WHSE_ADDR3 { get; set; }

        public string WHSE_CITY { get; set; }

        public string WHSE_STATE { get; set; }

        public string WHSE_ZIP_CODE { get; set; }

        public DateTime? DATE_TIME_ADDED { get; set; }

        public DateTime? DATE_TIME_CHANGED { get; set; }
    }
}
