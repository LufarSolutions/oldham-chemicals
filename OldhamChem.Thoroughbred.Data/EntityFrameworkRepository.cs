﻿using OldhamChem.Thoroughbred.Data.Entities;
using OldhamChem.Thoroughbred.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace OldhamChem.Thoroughbred.Data
{
    public class EntityFrameworkRepository<T> :
        IGenericRepository<T> where T : class
    {
        #region " Properties "

        private ThoroughbredDbContext DbContext;

        #endregion

        #region " Constructors "

        public EntityFrameworkRepository()
        {
            this.DbContext = new ThoroughbredDbContext();
        }

        #endregion

        #region " Methods "

        public void Add(T entity)
        {
            DbContext.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            DbContext.Set<T>().Remove(entity);
        }

        public void Edit(T entity)
        {
            DbContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public IQueryable<T> FindBy(int? currentPageIndex, int? pageSize, List<Expression<Func<T, object>>> includeExpressions, List<Expression<Func<T, bool>>> whereExpressions, List<OrderByExpression<T>> orderByExpressions, out int recordCount)
        {
            IQueryable<T> Query = DbContext
                .Set<T>()
                .AsQueryable();

            if (includeExpressions != null && includeExpressions.Any())
            {
                foreach (var includeExpression in includeExpressions)
                {
                    Query = Query.Include(includeExpression);
                }
            }

            if (whereExpressions != null && whereExpressions.Any())
            {
                foreach (var whereExpression in whereExpressions)
                {
                    Query = Query.Where(whereExpression);
                }
            }

            // Get the count of ALL matching records
            recordCount = Query.Count();

            // if pagesize of zero specified, only return the count            
            if (pageSize.HasValue && pageSize.Value == 0)
            {
            }
            else
            {
                if (orderByExpressions != null && orderByExpressions.Any())
                {
                    OrderByExpression<T> InitialOrderByExpression = orderByExpressions.First();
                    orderByExpressions.RemoveAt(0);

                    Query = QueryableHelper.OrderByHelper(Query, InitialOrderByExpression);

                    foreach (var secondaryOrderByExpression in orderByExpressions)
                    {
                        Query = QueryableHelper.ThenByHelper(Query, secondaryOrderByExpression);
                    }                    
                }                    

                // if pageSize and currentPageIndex was provided, page the results otherwise return all
                if (currentPageIndex.HasValue && pageSize.HasValue)
                {
                    if (recordCount > pageSize)
                    {
                        int StartIndex = (currentPageIndex.Value * pageSize.Value);

                        Query = Query
                            .Skip(StartIndex)
                            .Take(pageSize.Value);
                    }
                }
            }

            return Query;
        }

        public virtual void Save()
        {
            DbContext.SaveChanges();
        }

        #endregion
    }
}
