﻿using OldhamChem.Thoroughbred.Data.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace OldhamChem.Thoroughbred.Data
{
    public interface IGenericRepository<T> where T : class
    {
        void Add(T entity);

        void Delete(T entity);

        void Edit(T entity);

        IQueryable<T> FindBy(int? currentPageIndex, int? pageSize, List<Expression<Func<T, object>>> includeExpressions, List<Expression<Func<T, bool>>> whereExpressions, List<OrderByExpression<T>> orderByExpressions, out int recordCount);

        void Save();
    }
}
