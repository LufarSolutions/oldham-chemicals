﻿using OldhamChem.Thoroughbred.Data.Entities;

namespace OldhamChem.Thoroughbred.Data
{
    public class Repository
    {
        #region " Properties "

        public bool EnableMocking { get; set; } = false;

        public IGenericRepository<ContractPrice> ContractPriceRepository { get; private set; }
        public IGenericRepository<Conversion> ConversionRepository { get; private set; }
        public IGenericRepository<Customer> Customer { get; private set; }
        public IGenericRepository<CustomerAddress> CustomerAddressRepository { get; private set; }
        public IGenericRepository<Invoice> InvoiceRepository { get; private set; }
        public IGenericRepository<InvoiceItem> InvoiceItemRepository { get; private set; }
        public IGenericRepository<InvoiceTracking> InvoiceTrackingRepository { get; private set; }
        public IGenericRepository<ItemVendor> ItemVendorRepository { get; private set; }
        public IGenericRepository<Manufacturer> ManufacturerRepository { get; private set; }
        public IGenericRepository<OpenPO> OpenPORepository { get; private set; }
        public IGenericRepository<Order> OrderRepository { get; private set; }
        public IGenericRepository<OrderItem> OrderItemRepository { get; private set; }
        public IGenericRepository<OrderQueue> OrderQueueRepository { get; private set; }
        public IGenericRepository<OrderQueueItem> OrderQueueItemRepository { get; private set; }
        public IGenericRepository<Item> ItemRepository { get; private set; }
        public IGenericRepository<ItemInventory> ItemInventoryRepository { get; private set; }
        public IGenericRepository<Warehouse> WarehouseRepository { get; private set; }

        public IGenericRepository<ItemSalesHistoryAndInventory> ProductSalesHistoryAndInventoryRepository { get; private set; }

        #endregion

        #region " Constructors "

        public Repository()
        {
        }

        #endregion

        #region " Private Methods "

        public void Initialize()
        {
            if (EnableMocking)
            {
            }
            else
            {
                this.ContractPriceRepository = new EntityFrameworkRepository<ContractPrice>();
                this.ConversionRepository = new EntityFrameworkRepository<Conversion>();
                this.Customer = new EntityFrameworkRepository<Customer>();
                this.CustomerAddressRepository = new EntityFrameworkRepository<CustomerAddress>();
                this.InvoiceRepository = new EntityFrameworkRepository<Invoice>();
                this.InvoiceItemRepository = new EntityFrameworkRepository<InvoiceItem>();
                this.InvoiceTrackingRepository = new EntityFrameworkRepository<InvoiceTracking>();
                this.ManufacturerRepository = new EntityFrameworkRepository<Manufacturer>();
                this.OpenPORepository = new EntityFrameworkRepository<OpenPO>();
                this.OrderRepository = new EntityFrameworkRepository<Order>();
                this.OrderItemRepository = new EntityFrameworkRepository<OrderItem>();
                this.OrderQueueRepository = new EntityFrameworkRepository<OrderQueue>();
                this.OrderQueueItemRepository = new EntityFrameworkRepository<OrderQueueItem>();
                this.ItemRepository = new EntityFrameworkRepository<Item>();
                this.ItemInventoryRepository = new EntityFrameworkRepository<ItemInventory>();
                this.ItemVendorRepository = new EntityFrameworkRepository<ItemVendor>();
                this.WarehouseRepository = new EntityFrameworkRepository<Warehouse>();

                this.ProductSalesHistoryAndInventoryRepository = new EntityFrameworkRepository<ItemSalesHistoryAndInventory>();
            }
        }

        #endregion
    }
}
