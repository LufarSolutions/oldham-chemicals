﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class ItemMap : EntityTypeConfiguration<Item>
    {

        public ItemMap()
        {
            this.ToTable("Item");

            this.Property(e => e.PROD_CODE)
               .IsUnicode(false);

            this.Property(e => e.ITEM_CODE)
                .IsUnicode(false);

            this.Property(e => e.ITEM_DESC)
                .IsUnicode(false);

            this.Property(e => e.UOM)
                .IsUnicode(false);

            this.Property(e => e.TAXABLE)
                .IsUnicode(false);

            this.Property(e => e.DISCONTINUED)
                .IsUnicode(false);

            this.Property(e => e.VENDOR)
                //.HasColumnName("VENDOR")
                .IsUnicode(false);

            this.Property(e => e.PROD_TYPE)
                .IsUnicode(false);

            this.Property(e => e.PROD_DESC)
                .IsUnicode(false);

            this.Property(e => e.MFGNUM)
                //.HasColumnName("MFGNUM")
                .IsUnicode(false);

            this.Property(e => e.COST_DIVISOR)
                .HasPrecision(9, 2);

            this.Property(e => e.UNIT_PRICE_1)
                .HasPrecision(9, 2);

            this.Property(e => e.UNIT_PRICE_2)
                .HasPrecision(8, 2);

            this.Property(e => e.UNIT_PRICE_3)
                .HasPrecision(8, 2);

            this.Property(e => e.UNIT_PRICE_4)
                .HasPrecision(9, 2);

            this.Property(e => e.AGENCY)
                .IsUnicode(false);
            

        }

    }
}
