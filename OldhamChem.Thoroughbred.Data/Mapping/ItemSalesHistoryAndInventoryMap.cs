﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class ItemSalesHistoryAndInventoryMap : EntityTypeConfiguration<ItemSalesHistoryAndInventory>
    {

        public ItemSalesHistoryAndInventoryMap()
        {
            this.ToTable("ItemSalesHistoryAndInventory");

            this.Property(e => e.WHSE_CODE)
                .IsUnicode(false);
        }

    }
}
