﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class ConversionMap : EntityTypeConfiguration<Conversion>
    {
        public ConversionMap()
        {
            this.ToTable("Conversion");

            this.Property(e => e.ITEM_CODE)
                .IsUnicode(false);

            this.Property(e => e.CONV_ITEM_CODE)
                .IsUnicode(false);

            this.Property(e => e.CONV_FACTOR)
                .HasPrecision(10, 4);
        }
    }
}
