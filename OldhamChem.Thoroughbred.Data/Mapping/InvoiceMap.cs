﻿using OldhamChem.Thoroughbred.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class InvoiceMap : EntityTypeConfiguration<Invoice>
    {

        public InvoiceMap()
        {
            this.ToTable("Invoice");

            this.Property(e => e.INV_NO)
                .IsUnicode(false);

            this.Property(e => e.CustomerNumber)
                .HasColumnName("CUST_NO")
                .IsUnicode(false);

            //this.Property(e => e.INV_DATE)
            //    .IsUnicode(false);

            this.Property(e => e.ORDER_NO)
                .IsUnicode(false);

            this.Property(e => e.TERM_DESC)
                .IsUnicode(false);

            //this.Property(e => e.DUE_DATE)
            //    .IsUnicode(false);

            this.Property(e => e.WAREHOUSE)
                //.HasColumnName("WAREHOUSE")
                .IsUnicode(false);

            this.Property(e => e.CUST_PO_NUM)
                .IsUnicode(false);

            this.Property(e => e.INVOICE_AMT)
                .HasPrecision(10, 2);

            this.Property(e => e.INVOICE_BALANCE)
                .HasPrecision(10, 2);

            this.Property(e => e.SALES_TAX_AMT)
                .HasPrecision(9, 2);

            this.Property(e => e.FREIGHT_AMT)
                .HasPrecision(9, 2);

            this.HasMany(e => e.InvoiceItems)
                .WithRequired(e => e.Invoice)
                .WillCascadeOnDelete(false);

            this.HasMany(e => e.InvoiceTracking)
                .WithRequired(e => e.Invoice)
                .WillCascadeOnDelete(false);
        }

    }
}
