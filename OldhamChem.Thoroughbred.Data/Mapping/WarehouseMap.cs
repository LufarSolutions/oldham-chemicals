﻿using OldhamChem.Thoroughbred.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class WarehouseMap : EntityTypeConfiguration<Warehouse>
    {
        public WarehouseMap()
        {
            this.ToTable("Warehouse");

            this.Property(e => e.WHSE_CODE)
                .IsUnicode(false);

            this.Property(e => e.WHSE_DESC)
                .IsUnicode(false);

            this.Property(e => e.WHSE_ADDR1)
                .IsUnicode(false);

            this.Property(e => e.WHSE_ADDR2)
                .IsUnicode(false);

            this.Property(e => e.WHSE_ADDR3)
                .IsUnicode(false);

            this.Property(e => e.WHSE_CITY)
                .IsUnicode(false);

            this.Property(e => e.WHSE_STATE)
                .IsUnicode(false);

            this.Property(e => e.WHSE_ZIP_CODE)
                .IsUnicode(false);
        }

    }
}
