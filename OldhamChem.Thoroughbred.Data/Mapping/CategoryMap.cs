﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class CategoryMap : EntityTypeConfiguration<Category>
    {

        public CategoryMap()
        {
            this.ToTable("Product");
            
        }

    }
}
