﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class InvoiceTrackingMap : EntityTypeConfiguration<InvoiceTracking>
    {
        public InvoiceTrackingMap()
        {
            this.ToTable("InvoiceTracking");

            this.Property(e => e.INV_NO)
                .IsUnicode(false);

            this.Property(e => e.LINE_NO)
                .IsUnicode(false);
        }
    }
}
