﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class OpenPOMap : EntityTypeConfiguration<OpenPO>
    {
        public OpenPOMap()
        {
            this.ToTable("OpenPO");

            this.Property(e => e.VEND_NO)
                .IsUnicode(false);            

            this.Property(e => e.ITEM_CODE)
                .IsUnicode(false);

            this.Property(e => e.WHSE_CODE)
                .IsUnicode(false);

            this.Property(e => e.QTY_ON_ORDER)
                .HasPrecision(7, 0);
        }
    }
}
