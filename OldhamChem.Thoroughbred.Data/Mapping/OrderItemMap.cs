﻿using OldhamChem.Thoroughbred.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class OrderItemMap : EntityTypeConfiguration<OrderItem>
    {
        public OrderItemMap()
        {
            this.ToTable("OrderItem");

            this.Property(e => e.ORDER_NO)
                .IsUnicode(false);

            this.Property(e => e.LINE_NO)
                .IsUnicode(false);
        }
    }
}