﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class CustomerMap : EntityTypeConfiguration<Customer>
    {

        public CustomerMap()
        {
            this.ToTable("Customer");

            this.Property(e => e.CustomerNumber)
                .HasColumnName("CUSTOMER_NO")
                .IsUnicode(false);

            this.Property(e => e.CUST_NAME)
                .IsUnicode(false);

            this.Property(e => e.CUST_ADDR1)
                .IsUnicode(false);

            this.Property(e => e.CUST_ADDR2)
                .IsUnicode(false);

            this.Property(e => e.CUST_CITY)
                .IsUnicode(false);

            this.Property(e => e.CUST_STATE)
                .IsUnicode(false);

            this.Property(e => e.CUST_ZIP)
                .IsUnicode(false);

            this.Property(e => e.CUST_PHONE)
                .IsUnicode(false);

            this.Property(e => e.CUST_CONTACT)
                .IsUnicode(false);

            this.Property(e => e.SALES_REP)
                .IsUnicode(false);

            this.Property(e => e.BRANCH)
                .IsUnicode(false);

            this.Property(e => e.CHEM_PRICE_CODE)
                .IsUnicode(false);

            this.Property(e => e.EMAIL)
                .IsUnicode(false);

            this.Property(e => e.CURRENT)
                .HasPrecision(11, 2);

            this.Property(e => e.C30_DAYS)
                .HasPrecision(11, 2);

            this.Property(e => e.C60_DAYS)
                .HasPrecision(11, 2);

            this.Property(e => e.C90_DAYS)
                .HasPrecision(11, 2);

            this.Property(e => e.C120_DAYS)
                .HasPrecision(11, 2);

            this.Property(e => e.CREDIT_LIMIT)
                .HasPrecision(10, 2);
        }

    }
}
