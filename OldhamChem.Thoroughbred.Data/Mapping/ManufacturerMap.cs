﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class ManufacturerMap : EntityTypeConfiguration<Manufacturer>
    {

        public ManufacturerMap()
        {
            this.ToTable("Manufacturer");

            this.Property(e => e.VEND_NO)
                //.HasColumnName("VEND_NO")
               .IsUnicode(false);

            this.Property(e => e.VEND_NAME)
                .IsUnicode(false);

            this.Property(e => e.VEND_ADDR1)
                .IsUnicode(false);

            this.Property(e => e.VEND_ADDR2)
                .IsUnicode(false);

            this.Property(e => e.VEND_CITY)
                .IsUnicode(false);

            this.Property(e => e.VEND_STATE)
                .IsUnicode(false);

            this.Property(e => e.VEND_ZIP)
                .IsUnicode(false);

        }

    }
}
