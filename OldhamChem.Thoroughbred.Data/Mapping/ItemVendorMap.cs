﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class ItemVendorMap : EntityTypeConfiguration<ItemVendor>
    {
        public ItemVendorMap()
        {
            this.ToTable("ItemVendor");

            this.Property(e => e.ITEM_CODE)
                .IsUnicode(false);

            this.Property(e => e.VEND_NO)
                .IsUnicode(false);

            this.Property(e => e.VEND_ITEM_CODE)
                .IsUnicode(false);

            this.Property(e => e.VEND_ITEM_DESC)
                .IsUnicode(false);

            this.Property(e => e.UOM)
                .IsUnicode(false);

            this.Property(e => e.UNITS_PACK_SIZE)
                .HasPrecision(12, 5);

        }

    }
}
