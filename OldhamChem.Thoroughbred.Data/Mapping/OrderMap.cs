﻿using OldhamChem.Thoroughbred.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class OrderMap : EntityTypeConfiguration<Order>
    {
        public OrderMap()
        {
            this.ToTable("Order");

            //this.Property(e => e.CustomerNumber)
            //    .IsUnicode(false);

            //this.Property(e => e.Error)
            //    .IsUnicode(false);

            this.Property(e => e.ORDER_NO)
                .IsUnicode(false);

            //this.Property(e => e.PurchaseOrderNumber)
            //    .IsUnicode(false);

            //this.Property(e => e.ShipToNumber)
            //    .IsUnicode(false);

            this.HasMany(e => e.OrderItems)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);
        }
    }
}