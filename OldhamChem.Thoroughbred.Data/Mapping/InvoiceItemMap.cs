﻿using OldhamChem.Thoroughbred.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class InvoiceItemMap : EntityTypeConfiguration<InvoiceItem>
    {

        public InvoiceItemMap()
        {
            this.ToTable("InvoiceItem");

            this.Property(e => e.INV_NO)
                .IsUnicode(false);

            this.Property(e => e.LINE_NO)
                .IsUnicode(false);

            //this.Property(e => e.PROD_CODE)
            //    .IsUnicode(false);

            this.Property(e => e.ITEM_CODE)
                //.HasColumnName("ITEM_CODE")
                .IsUnicode(false);

            this.Property(e => e.TAXABLE)
                .IsUnicode(false);

            this.Property(e => e.SHIP_TODAY)
                .HasPrecision(8, 0);

            this.Property(e => e.UNIT_PRICE)
                .HasPrecision(10, 2);

            this.Property(e => e.EXT_PRICE)
                .HasPrecision(13, 2);

            this.HasOptional(e => e.Item)
                .WithMany()
                .HasForeignKey(hf => hf.ITEM_CODE);

            //this.HasRequired<Product>(hr => hr.Product)
            //    .WithRequiredDependent()
        }

    }
}
