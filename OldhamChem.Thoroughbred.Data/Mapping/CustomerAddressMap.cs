﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class CustomerAddressMap : EntityTypeConfiguration<CustomerAddress>
    {

        public CustomerAddressMap()
        {
            this.ToTable("CustomerAddresses");

            this.Property(e => e.CustomerNumber)
                .HasColumnName("CUSTOMER_NO")
                .IsUnicode(false);

            this.Property(e => e.SHIP_TO_NO)
                .IsUnicode(false);

            this.Property(e => e.CUST_SHIP_TO_NAME)
                .IsUnicode(false);

            this.Property(e => e.CUST_SHIP_TO_ADDR1)
                .IsUnicode(false);

            this.Property(e => e.CUST_SHIP_TO_ADDR2)
                .IsUnicode(false);

            this.Property(e => e.CUST_SHIP_TO_CITY)
                .IsUnicode(false);

            this.Property(e => e.CUST_SHIP_TO_STATE)
                .IsUnicode(false);

            this.Property(e => e.CUST_SHIP_TO_ZIP)
                .IsUnicode(false);

            this.Property(e => e.CUST_SHIP_TO_CONTACT)
                .IsUnicode(false);

            this.Property(e => e.TAX_CODE)
                .IsUnicode(false);

        }

    }
}
