﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class ContractPriceMap : EntityTypeConfiguration<ContractPrice>
    {

        public ContractPriceMap()
        {
            this.ToTable("ContractPrice");

            this.Property(e => e.CustomerNumber)
                .HasColumnName("CUSTOMER_NO")
                .IsUnicode(false);

            this.Property(e => e.ITEM_CODE)
                .IsUnicode(false);

            this.Property(e => e.CONTRACT_PRICE)
                .HasPrecision(8, 2);
            
        }

    }
}
