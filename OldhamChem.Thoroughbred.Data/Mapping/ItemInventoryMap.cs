﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class ItemInventoryMap : EntityTypeConfiguration<ItemInventory>
    {

        public ItemInventoryMap()
        {
            this.ToTable("ItemInventory");

            this.Property(e => e.PROD_CODE)
                .IsUnicode(false);

            this.Property(e => e.ITEM_CODE)
                .IsUnicode(false);

            this.Property(e => e.WHSE_CODE)
                //.HasColumnName("WHSE_CODE")
                .IsUnicode(false);

            this.Property(e => e.QTY_ON_HAND)
                .HasPrecision(7, 0);

            this.Property(e => e.ON_BACK_ORDER)
                .HasPrecision(7, 0);
        }

    }
}
