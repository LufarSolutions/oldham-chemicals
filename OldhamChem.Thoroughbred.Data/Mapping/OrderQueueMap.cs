﻿using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldhamChem.Thoroughbred.Data.Mapping
{
    internal class OrderQueueMap : EntityTypeConfiguration<OrderQueue>
    {

        public OrderQueueMap()
        {
            this.ToTable("OrderQueue");

            this.Property(e => e.CustomerNumber)
                .IsUnicode(false);

            this.Property(e => e.Error)
                .IsUnicode(false);

            this.Property(e => e.OrderNumber)
                .IsUnicode(false);

            this.Property(e => e.PurchaseOrderNumber)
                .IsUnicode(false);

            this.Property(e => e.ShipToNumber)
                .IsUnicode(false);

            this.HasMany(e => e.OrderQueueItems)
                .WithRequired(e => e.OrderQueue)
                .WillCascadeOnDelete(false);
        }

    }
}
