﻿using OldhamChem.Thoroughbred.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace OldhamChem.Thoroughbred.Data.Mapping {
    internal class OrderQueueItemMap : EntityTypeConfiguration<OrderQueueItem>
    {

        public OrderQueueItemMap()
        {
            this.ToTable("OrderQueueItem");

            this.Property(e => e.ItemCode)
                .IsUnicode(false);

            this.Property(e => e.UnitOfMeasure)
                .IsUnicode(false);                            
        }

    }
}
