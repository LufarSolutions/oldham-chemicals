﻿using Nop.Core.Domain.Orders;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Nop.Admin.Models.Orders {
    public class RepairRequestsSearchModel : BaseNopModel 
    {
        public string AuthorizationNumber { get; set; }

        public string Customer { get; set; }

        public int CustomerId { get; set; } = 0;

        public string Product { get; set; }

        public List<SelectListItem> Statuses { get; set; } = new List<SelectListItem>();

        public int? WarehouseId { get; set; }

        public List<SelectListItem> Warehouses { get; set; } = new List<SelectListItem>();

        public RepairRequestsSearchModel() {
            Statuses.Add(new SelectListItem() { Selected = true, Text = ReturnRequestStatus.Pending.ToString(), Value = ((int)ReturnRequestStatus.Pending).ToString() });
            Statuses.Add(new SelectListItem() { Selected = true, Text = ReturnRequestStatus.Authorized.ToString(), Value = ((int)ReturnRequestStatus.Authorized).ToString() });
            Statuses.Add(new SelectListItem() { Selected = true, Text = ReturnRequestStatus.Inbound.ToString(), Value = ((int)ReturnRequestStatus.Inbound).ToString() });
            Statuses.Add(new SelectListItem() { Selected = true, Text = ReturnRequestStatus.Received.ToString(), Value = ((int)ReturnRequestStatus.Received).ToString() });
            Statuses.Add(new SelectListItem() { Selected = true, Text = ReturnRequestStatus.Outbound.ToString(), Value = ((int)ReturnRequestStatus.Outbound).ToString() });
            Statuses.Add(new SelectListItem() { Selected = false, Text = ReturnRequestStatus.Complete.ToString(), Value = ((int)ReturnRequestStatus.Complete).ToString() });
            Statuses.Add(new SelectListItem() { Selected = false, Text = ReturnRequestStatus.Cancelled.ToString(), Value = ((int)ReturnRequestStatus.Cancelled).ToString() });
            //Statuses.Add(new SelectListItem() { Selected = true, Text = ReturnRequestStatus.Rejected.ToString(), Value = ((int)ReturnRequestStatus.Rejected).ToString() });
        }
    }
}