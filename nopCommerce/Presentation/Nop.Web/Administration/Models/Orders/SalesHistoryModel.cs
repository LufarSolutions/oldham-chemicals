﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Orders
{
    public class SalesHistoryModel
    {
        public Customers.CustomerModel Customer { get; set; }

        public DateTime DateOfInvoice { get; set; }

        public decimal FreightCharge { get; set; }

        public decimal InvoiceAmount { get; set; }

        public string InvoiceNumber { get; set; }

        public string OrderNumber { get; set; }

        public decimal SalesTax { get; set; }

        public List<SalesHistoryItemModel> SalesHistoryItems { get; set; }

        public Shipping.WarehouseModel Warehouse { get; set; }

    }
}