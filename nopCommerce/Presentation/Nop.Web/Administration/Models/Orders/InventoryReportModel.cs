﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Orders
{
    public class InventoryReportModel
    {
        public InventoryReportForm InventoryReportForm { get; set; }
        public List<InventoryReportItemModel> InventoryReportItems { get; set; }

        public InventoryReportModel()
        {
            this.InventoryReportForm = new InventoryReportForm();
            this.InventoryReportItems = new List<InventoryReportItemModel>();
        }
    }
}