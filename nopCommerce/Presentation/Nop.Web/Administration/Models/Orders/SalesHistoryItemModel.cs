﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Orders
{
    public class SalesHistoryItemModel
    {
        public bool IsTaxable { get; set; }

        public decimal Price { get; set; }

        public Models.Catalog.ProductModel Product { get; set; }

        public int Quantity { get; set; }

        public decimal Total { get; set; }
    }
}