﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Orders
{
    public class OrderQueueModel
    {
        public List<OrderModel> OrdersNotQueued { get; set; }

        public List<OrderModel> QueuedOrders { get; set; }

        public OrderQueueSearchModel SearchFields { get; set; }

        public OrderQueueModel()
        {
            OrdersNotQueued = new List<OrderModel>();
            QueuedOrders = new List<OrderModel>();
            SearchFields = new OrderQueueSearchModel();
        }
    }
}