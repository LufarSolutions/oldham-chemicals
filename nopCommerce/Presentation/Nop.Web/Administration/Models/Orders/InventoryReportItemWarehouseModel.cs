﻿using Nop.Admin.Models.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Orders
{
    public class InventoryReportItemWarehouseModel
    {
        public string WarehouseCode { get; set; }

        public string WarehouseName { get; set; }

        public int CurrentInventory { get; set; }

        public int QuantityOnPO { get; set; }

        public int UnitsSoldYearToDate { get; set; }

        public int UnitsSold9MonthsToDate { get; set; }

        public int UnitsSold6MonthsToDate { get; set; }

        public int UnitsSold3MonthsToDate { get; set; }

        public List<KeyValuePair<string, decimal>> UnitsSoldPerMonth { get; set; }

        public InventoryReportItemWarehouseModel()
        {
            UnitsSoldPerMonth = new List<KeyValuePair<string, decimal>>();
        }
    }
}