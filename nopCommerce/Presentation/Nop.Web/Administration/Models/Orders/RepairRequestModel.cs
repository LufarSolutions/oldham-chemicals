﻿using Nop.Admin.Models.Catalog;
using Nop.Core.Domain.Orders;
using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Nop.Admin.Models.Orders {
    public class RepairRequestModel {

        [Display(Name = "Authorization #")]
        public string AuthorizationNumber { get; set; }

        [NopResourceDisplayName("Admin.Customers.Customers.Fields.Email")]
        [Required(ErrorMessage = "Please enter a valid e-mail for the customer.")]
        [EmailAddress(ErrorMessage = "The value entered for the customer e-mail address is invalid.")]
        public string CustomerEmail { get; set; }

        public int? CustomerId { get; set; }

        [Required(ErrorMessage = "Please select a valid customer from the list of suggestions.")]
        [NopResourceDisplayName("Admin.ReturnRequests.Fields.Customer")]
        public string CustomerName { get; set; }

        [Display(Name = "Company")]
        public string CustomerCompany { get; set; }

        [NopResourceDisplayName("Admin.Customers.Customers.Fields.Phone")]
        [Required(ErrorMessage = "Please enter a valid phone number for the customer.")]
        [Phone(ErrorMessage = "The value entered for the customer phone number is invalid.")]
        public string CustomerPhoneNumber { get; set; }

        [Display(Name = "Customer Comments")]
        public string CustomerComments { get; set; }

        public DateTime? DateCreated { get; set; }

        public int? ProductId { get; set; }

        [NopResourceDisplayName("Admin.ReturnRequests.Fields.Product")]
        [Required(ErrorMessage = "Please enter a product name or choose one from this list of suggestions.")]
        public string ProductName { get; set; }
        
        public string ProductPictureUrl { get; set; }

        public string ProductSku { get; set; }

        public int? RepairRequestId { get; set; }

        [Required(ErrorMessage = "Please select the warehouse where this item was accepted.")]
        [Display(Name = "Warehouse")]
        public int? SelectedWarehouseId { get; set; }

        public string SelectedWarehouseName { get; set; }

        [Display(Name = "Repair Notes")]
        public string StaffNotes { get; set; }

        public string Status { get; set; } = ReturnRequestStatus.Pending.ToString();

        [Display(Name = "In-Tracking #")]
        public string TrackingNumberInbound { get; set; }

        [Display(Name = "Out-Tracking #")]
        public string TrackingNumberOutbound { get; set; }

        public List<SelectListItem> Warehouses { get; set; } = new List<SelectListItem>();
    }
}