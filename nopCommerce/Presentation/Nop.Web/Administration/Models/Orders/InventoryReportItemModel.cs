﻿using Nop.Admin.Models.Catalog;
using Nop.Admin.Models.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Orders
{
    public class InventoryReportItemModel
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string Sku { get; set; }

        public string UnitOfMeasure { get; set; }

        public int TotalCurrentInventory { get; set; }

        public int TotalQuantityOnPO { get; set; }

        public int UnitsSoldYearToDate { get; set; }

        public int UnitsSold9MonthsToDate { get; set; }

        public int UnitsSold6MonthsToDate { get; set; }

        public int UnitsSold3MonthsToDate { get; set; }

        public List<KeyValuePair<string, decimal>> TotalUnitsSoldPerMonth { get; set; }

        public List<InventoryReportItemWarehouseModel> WarehouseInventories { get; set; }

        public InventoryReportItemModel()
        {
            TotalUnitsSoldPerMonth = new List<KeyValuePair<string, decimal>>();
            WarehouseInventories = new List<InventoryReportItemWarehouseModel>();
        }
    }
}