﻿using Nop.Admin.Models.Customers;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Nop.Admin.Models.Orders {
    //[Validator(typeof(ReturnRequestValidator))]
    public partial class ReturnRequestModel : BaseNopEntityModel
    {
        public string AuthorizationNumber { get; set; }

        [NopResourceDisplayName("Admin.ReturnRequests.Fields.Customer")]
        public CustomerModel Customer { get; set; }

        [Required(ErrorMessage = "Please select the warehouse where this item was accepted.")]
        [Display(Name = "Warehouse")]
        public int? SelectedWarehouseId { get; set; }

        public List<SelectListItem> Warehouses { get; set; } = new List<SelectListItem>();

        [NopResourceDisplayName("Admin.ReturnRequests.Fields.ID")]
        public override int Id { get; set; }

        [NopResourceDisplayName("Admin.ReturnRequests.Fields.Order")]
        public int? OrderId { get; set; }

        [NopResourceDisplayName("Admin.ReturnRequests.Fields.Customer")]
        public int? CustomerId { get; set; }

        public int? ProductId { get; set; }

        [NopResourceDisplayName("Admin.ReturnRequests.Fields.Product")]
        public string ProductName { get; set; }

        [NopResourceDisplayName("Admin.ReturnRequests.Fields.Quantity")]
        public int Quantity { get; set; }

        [AllowHtml]
        [NopResourceDisplayName("Admin.ReturnRequests.Fields.ReasonForReturn")]
        public string ReasonForReturn { get; set; }

        [AllowHtml]
        [NopResourceDisplayName("Admin.ReturnRequests.Fields.RequestedAction")]
        public string RequestedAction { get; set; }

        [AllowHtml]
        [NopResourceDisplayName("Admin.ReturnRequests.Fields.CustomerComments")]
        public string CustomerComments { get; set; }

        [AllowHtml]
        [NopResourceDisplayName("Admin.ReturnRequests.Fields.StaffNotes")]
        public string StaffNotes { get; set; }

        [NopResourceDisplayName("Admin.ReturnRequests.Fields.Status")]
        public int ReturnRequestStatusId { get; set; }
        [NopResourceDisplayName("Admin.ReturnRequests.Fields.Status")]
        public string ReturnRequestStatusStr { get; set; }

        [NopResourceDisplayName("Admin.ReturnRequests.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }
    }
}