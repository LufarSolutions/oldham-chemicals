﻿using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Orders
{
    public class OrderQueueSearchModel
    {
        public OrderQueueSearchModel()
        {
            this.CustomerServiceRepresentatives = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Admin.Fields.CustomerServiceRepresentatives")]
        public List<SelectListItem> CustomerServiceRepresentatives { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.Company")]
        public string Company { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.CustomerEmail")]
        public string CustomerEmail { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.CustomerFirstName")]
        public string CustomerFirstName { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.CustomerLastName")]
        public string CustomerLastName { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.CustomerNumber")]
        public string CustomerNumber { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.OrderPlacedStartDate")]
        [UIHint("DateNullable")]
        public DateTime? OrderPlacedStartDate { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.OrderPlacedEndDate")]
        [UIHint("DateNullable")]
        public DateTime? OrderPlacedEndDate { get; set; }
    }
}