﻿using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Orders
{
    public class SalesHistorySearchModel
    {
        public SalesHistorySearchModel()
        {
            this.Warehouses = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Admin.Orders.List.Company")]
        public string Company { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.CustomerEmail")]
        public string CustomerEmail { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.CustomerFirstName")]
        public string CustomerFirstName { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.CustomerLastName")]
        public string CustomerLastName { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.CustomerNumber")]
        public string CustomerNumber { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.InvoiceStartDate")]
        [UIHint("DateNullable")]
        public DateTime? InvoiceStartDate { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.InvoiceEndDate")]
        [UIHint("DateNullable")]
        public DateTime? InvoiceEndDate { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.InvoiceNumber")]
        public string InvoiceNumber { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.InvoiceNumbers")]
        public List<string> InvoiceNumbers { get; set; }

        public bool IsLoggedInAsVendor { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.ProductName")]
        public string ProductName { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.ProductNumber")]
        public string ProductNumber { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.Store")]
        public int StoreId { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.Vendor")]
        public int VendorId { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.Warehouses")]
        public List<SelectListItem> Warehouses { get; set; }
    }
}