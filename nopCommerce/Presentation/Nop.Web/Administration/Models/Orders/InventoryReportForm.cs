﻿using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Orders
{
    public class InventoryReportForm
    {
        [NopResourceDisplayName("Admin.Reports.Fields.IncludeDiscontinuedItems")]
        public bool IncludeDiscontinuedItems { get; set; }

        [NopResourceDisplayName("Admin.Reports.Fields.Manufacturer")]
        [Remote("DoesProductBelongToManufacturer", "Order", AdditionalFields = "ProductNameOrNumber", ErrorMessage = "The selected manufacturer does not have a product matching the entered product information.")]
        public string Manufacturer { get; set; }

        [Required(ErrorMessage = "Please choose a manufacturer from the list of suggestions.")]
        public string SelectedManufacturerCode { get; set; }

        [NopResourceDisplayName("Admin.Reports.Fields.ProductNameOrNumber")]
        [Remote("DoesProductBelongToManufacturer", "Order", AdditionalFields = "Manufacturer", ErrorMessage = "The entered product information does not correspond to the selected manufacturer.")]
        public string ProductNameOrNumber { get; set; }

        [NopResourceDisplayName("Admin.Orders.List.Warehouses")]
        public List<SelectListItem> Warehouses { get; set; }

        public InventoryReportForm()
        {
            this.IncludeDiscontinuedItems = false;
            this.Warehouses = new List<SelectListItem>();
        }

    }
}