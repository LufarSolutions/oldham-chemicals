﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Orders {
    public class RepairRequestsListModel : BaseNopModel {
        public RepairRequestsSearchModel RepairRequestsSearchModel { get; set; } = new RepairRequestsSearchModel();
    }
}