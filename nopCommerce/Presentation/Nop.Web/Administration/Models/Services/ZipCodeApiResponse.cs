﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Services
{
    public class ZipCodeApiResponse
    {
        public List<string> Zip_Codes { get; set; }
    }
}