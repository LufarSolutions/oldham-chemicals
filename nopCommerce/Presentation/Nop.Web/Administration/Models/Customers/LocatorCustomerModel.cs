﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Customers
{
    public class LocatorCustomerModel
    {
        public CustomerModel Customer { get; set; }

        public bool IsOnline { get; set; }

        public DateTime LastOrderDate { get; set; }

        public CustomerAddressModel CustomerAddress { get; set; }
    }
}