﻿using Nop.Admin.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Customers
{
    public class LocatorMapModel
    {
        public List<LocatorCustomerModel> LocatorCustomers { get; set; }
        
        public AddressModel WarehouseAddress { get; set; }

        public int Zoom { get; set; }

        public LocatorMapModel()
        {
            LocatorCustomers = new List<LocatorCustomerModel>();
            WarehouseAddress = new AddressModel();
            Zoom = 4;
        }

        public void CalculateZoomFromRadius(int radius)
        {
            switch (radius)
            {
                case int n when (n <= 32):
                    this.Zoom = 10;
                    break;
                case int n when (n <= 64):
                    this.Zoom = 9;
                    break;
                case int n when (n <= 128):
                    this.Zoom = 8;
                    break;
                case int n when (n <= 256):
                    this.Zoom = 7;
                    break;
                default:
                    this.Zoom = 4;
                    break;                        
            }


        }
    }
}