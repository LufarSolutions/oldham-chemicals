﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Customers
{
    public partial class ContractPriceModel : BaseNopEntityModel
    {
        public int ProductId { get; set; }

        public int CustomerId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.ContractPrices.Fields.Product")]
        public string Product { get; set; }

        public int StoreId { get; set; }
        [NopResourceDisplayName("Admin.Catalog.Products.ContractPrices.Fields.Store")]
        public string Store { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.ContractPrices.Fields.Quantity")]
        public int Quantity { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.ContractPrices.Fields.Price")]
        public decimal Price { get; set; }
    }    
    
}