﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Customers
{

    public partial class FavoriteProductModel : BaseNopEntityModel
    {
        public int ProductId { get; set; }

        public int CustomerId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.FavoriteProducts.Fields.Product")]
        public string Product { get; set; }

        public int StoreId { get; set; }
        [NopResourceDisplayName("Admin.Catalog.Products.FavoriteProducts.Fields.Store")]
        public string Store { get; set; }

        //[NopResourceDisplayName("Admin.Catalog.Products.FavoriteProducts.Fields.Quantity")]
        //public int Quantity { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.FavoriteProducts.Fields.Price")]
        public decimal Price { get; set; }
    }

}