﻿using Nop.Admin.Models.Common;
using Nop.Core.Domain.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Customers
{
    public class LocatorModel
    {
        //public List<LocatorCustomerModel> LocatorCustomers { get; set; }

        public LocatorFormModel LocatorForm { get; set; }
        
        public LocatorMapModel LocatorMap { get; set; }

        public LocatorModel()
        {
            //LocatorCustomers = new List<LocatorCustomerModel>();
            LocatorForm = new LocatorFormModel();
            LocatorMap = new LocatorMapModel();
        }
    }
}