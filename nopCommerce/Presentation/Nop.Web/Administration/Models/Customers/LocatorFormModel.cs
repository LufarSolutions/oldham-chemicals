﻿using Nop.Core.Domain.Common;
using Nop.Core.Domain.Shipping;
using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Customers
{
    public class LocatorFormModel
    {
        private const int DEFAULT_RADIUS = 50;
        private const string MEMPHIS_WAREHOUSE_CODE = "100";

        [NopResourceDisplayName("Admin.Customers.Labels.Radius")]
        public int SelectedRadius { get; set; }

        public List<SelectListItem> Radiuses { get; set; }

        public List<SelectListItem> Warehouses { get; set; }

        [NopResourceDisplayName("Admin.Customers.Labels.Warehouse")]
        public Warehouse SelectedWarehouse { get; set; }

        public Address SelectedWarehouseAddress { get; set; }

        public List<SelectListItem> LastOrderDateRanges { get; set; }

        [NopResourceDisplayName("Admin.Customers.Labels.LastOrder")]
        public DateRange SelectedLastOrderDateRange { get; set; }

        public LocatorFormModel()
        {
            this.SelectedRadius = DEFAULT_RADIUS;

            PopulateRadiuses();

            this.PopulateLastOrderDateRanges();

            Warehouses = new List<SelectListItem>();
        }

        private void PopulateLastOrderDateRanges()
        {
            this.LastOrderDateRanges = new List<SelectListItem>();

            DateTime DateTimeNow = DateTime.UtcNow;

            DateRange Last6Months = new DateRange();
            Last6Months.StartDate = DateTimeNow.AddMonths(-6);
            Last6Months.EndDate = DateTimeNow;
            this.LastOrderDateRanges.Add(new SelectListItem()
            {
                Selected = false,
                Text = "Within Last 6 Months",
                Value = Last6Months.ToShortDateRangeString()
            });

            DateRange Between12To6MonthsAgo = new DateRange();
            Between12To6MonthsAgo.StartDate = DateTimeNow.AddMonths(-12);
            Between12To6MonthsAgo.EndDate = DateTimeNow.AddMonths(-6);
            this.LastOrderDateRanges.Add(new SelectListItem()
            {
                Selected = true,
                Text = "Between 12 and 6 Months Ago",
                Value = Between12To6MonthsAgo.ToShortDateRangeString()
            });

            DateRange Between18To12MonthsAgo = new DateRange();
            Between18To12MonthsAgo.StartDate = DateTimeNow.AddMonths(-18);
            Between18To12MonthsAgo.EndDate = DateTimeNow.AddMonths(-12);
            this.LastOrderDateRanges.Add(new SelectListItem()
            {
                Selected = false,
                Text = "Between 18 and 12 Months Ago",
                Value = Between18To12MonthsAgo.ToShortDateRangeString()
            });

            DateRange Between24To18MonthsAgo = new DateRange();
            Between24To18MonthsAgo.StartDate = DateTimeNow.AddMonths(-24);
            Between24To18MonthsAgo.EndDate = DateTimeNow.AddMonths(-18);
            this.LastOrderDateRanges.Add(new SelectListItem()
            {
                Selected = false,
                Text = "Between 24 and 18 Months Ago",
                Value = Between24To18MonthsAgo.ToShortDateRangeString()
            });

            DateRange Over24MonthsAgo = new DateRange();
            Over24MonthsAgo.StartDate = DateTime.MinValue;
            Over24MonthsAgo.EndDate = DateTimeNow.AddMonths(-24);
            this.LastOrderDateRanges.Add(new SelectListItem()
            {
                Selected = false,
                Text = "Over 24 Months Ago",
                Value = Over24MonthsAgo.ToShortDateRangeString()
            });
        }

        public void PopulateWarehouses(List<Warehouse> warehouses, List<Address> warehouseAddresses)
        {
            // Loop thru each warehouse to get the address
            foreach (var Warehouse in warehouses)
            {
                Core.Domain.Common.Address WarehouseAddress = warehouseAddresses.FirstOrDefault(f => f.Id == Warehouse.AddressId);

                if (WarehouseAddress != null)
                {
                    string WarehouseName = Warehouse.Name + " (" + Warehouse.Code + ")";
                    bool IsSelected = false;

                    if (Warehouse.Code == MEMPHIS_WAREHOUSE_CODE)
                    {
                        IsSelected = true;
                        SelectedWarehouse = Warehouse;
                        SelectedWarehouseAddress = WarehouseAddress;
                    }

                    this.Warehouses.Add(new SelectListItem()
                    {
                        Selected = IsSelected,
                        Text = WarehouseName,
                        Value = Warehouse.Id.ToString()
                    });
                }
            }
        }

        private void PopulateRadiuses()
        {
            List<int> RadiusValues = new List<int>();
            RadiusValues.Add(25);
            RadiusValues.Add(50);
            RadiusValues.Add(100);
            RadiusValues.Add(150);

            this.Radiuses = new List<SelectListItem>();

            foreach(var RadiusValue in RadiusValues)
            {
                Radiuses.Add(new SelectListItem() { Selected = RadiusValue == DEFAULT_RADIUS, Text = RadiusValue.ToString(), Value = RadiusValue.ToString() });
            }
        }
    }
}