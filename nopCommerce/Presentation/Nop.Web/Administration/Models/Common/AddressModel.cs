﻿using System.Collections.Generic;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Admin.Validators.Common;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Text;
using Nop.Core.Domain.Common;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Nop.Admin.Models.Common
{
    [Validator(typeof(AddressValidator))]
    public partial class AddressModel : BaseNopEntityModel
    {
        public AddressModel()
        {
            AvailableCountries = new List<SelectListItem>();
            AvailableStates = new List<SelectListItem>();
            CustomAddressAttributes = new List<AddressAttributeModel>();
        }

        public void SetAddressHtml(AddressSettings addressSettings, bool showCountry)
        {
            var AddressHtmlSb = new StringBuilder();

            if ((addressSettings != null && addressSettings.CompanyEnabled) && !String.IsNullOrEmpty(this.Company))
                AddressHtmlSb.AppendFormat("{0}<br />", HttpUtility.HtmlEncode(this.Company));

            if (addressSettings.StreetAddressEnabled && !String.IsNullOrEmpty(this.Address1))
                AddressHtmlSb.AppendFormat("{0}<br />", HttpUtility.HtmlEncode(this.Address1));

            if (addressSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(this.Address2))
                AddressHtmlSb.AppendFormat("{0}<br />", HttpUtility.HtmlEncode(this.Address2));

            if (addressSettings.CityEnabled && !String.IsNullOrEmpty(this.City))
                AddressHtmlSb.AppendFormat("{0},&nbsp;", HttpUtility.HtmlEncode(this.City));

            if (addressSettings.StateProvinceEnabled && !String.IsNullOrEmpty(this.StateProvinceName))
                AddressHtmlSb.AppendFormat("{0}&nbsp;", HttpUtility.HtmlEncode(this.StateProvinceName));

            if (addressSettings.ZipPostalCodeEnabled && !String.IsNullOrEmpty(this.ZipPostalCode))
                AddressHtmlSb.AppendFormat("{0}<br />", HttpUtility.HtmlEncode(this.ZipPostalCode));

            if (addressSettings.CountryEnabled && !String.IsNullOrEmpty(this.CountryName) && showCountry)
                AddressHtmlSb.AppendFormat("{0}", HttpUtility.HtmlEncode(this.CountryName));

            this.AddressHtml = AddressHtmlSb.ToString();
        }

        public bool CanModify { get; set; } = true;

        [Display(Name = "First Name")]
        [AllowHtml]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [AllowHtml]
        public string LastName { get; set; }

        [NopResourceDisplayName("Admin.Address.Fields.Email")]
        [AllowHtml]
        public string Email { get; set; }

        [NopResourceDisplayName("Admin.Address.Fields.Company")]
        [AllowHtml]
        public string Company { get; set; }

        [NopResourceDisplayName("Admin.Address.Fields.Country")]
        public int? CountryId { get; set; }

        [NopResourceDisplayName("Admin.Address.Fields.Country")]
        [AllowHtml]
        public string CountryName { get; set; }

        [NopResourceDisplayName("Admin.CreatedOn")]
        public DateTime CreatedOnUtc { get; set; }

        [NopResourceDisplayName("Admin.LastUpdatedOn")]
        public DateTime? DateUpdated { get; set; }

        [Display(Name = "State")]
        public int? StateProvinceId { get; set; }

        [NopResourceDisplayName("Admin.Address.Fields.StateProvince")]
        [AllowHtml]
        public string StateProvinceName { get; set; }

        [NopResourceDisplayName("Admin.Address.Fields.City")]
        [AllowHtml]
        public string City { get; set; }

        [NopResourceDisplayName("Admin.Address.Fields.Address1")]
        [AllowHtml]
        public string Address1 { get; set; }

        [NopResourceDisplayName("Admin.Address.Fields.Address2")]
        [AllowHtml]
        public string Address2 { get; set; }

        [Display(Name = "Postal Code")]
        [AllowHtml]
        public string ZipPostalCode { get; set; }

        [NopResourceDisplayName("Admin.Address.Fields.Latitude")]
        [AllowHtml]
        public decimal? Latitude { get; set; }

        [NopResourceDisplayName("Admin.Address.Fields.Longitude")]
        [AllowHtml]
        public decimal? Longitude { get; set; }

        [Display(Name = "Phone #")]
        [AllowHtml]
        public string PhoneNumber { get; set; }

        [Display(Name = "Fax #")]
        [AllowHtml]
        public string FaxNumber { get; set; }

        [Display(Name = "Ship To #")]
        public string ShipToNumber { get; set; }

        //address in HTML format (usually used in grids)
        [NopResourceDisplayName("Admin.Address")]
        public string AddressHtml { get; set; }

        //formatted custom address attributes
        public string FormattedCustomAddressAttributes { get; set; }
        public IList<AddressAttributeModel> CustomAddressAttributes { get; set; }


        public IList<SelectListItem> AvailableCountries { get; set; }
        public IList<SelectListItem> AvailableStates { get; set; }



        public bool FirstNameEnabled { get; set; }
        public bool FirstNameRequired { get; set; }
        public bool LastNameEnabled { get; set; }
        public bool LastNameRequired { get; set; }
        public bool EmailEnabled { get; set; }
        public bool EmailRequired { get; set; }
        public bool CompanyEnabled { get; set; }
        public bool CompanyRequired { get; set; }
        public bool CountryEnabled { get; set; }
        public bool StateProvinceEnabled { get; set; }
        public bool CityEnabled { get; set; }
        public bool CityRequired { get; set; }
        public bool StreetAddressEnabled { get; set; }
        public bool StreetAddressRequired { get; set; }
        public bool StreetAddress2Enabled { get; set; }
        public bool StreetAddress2Required { get; set; }
        public bool ZipPostalCodeEnabled { get; set; }
        public bool ZipPostalCodeRequired { get; set; }
        public bool PhoneEnabled { get; set; }
        public bool PhoneRequired { get; set; }
        public bool FaxEnabled { get; set; }
        public bool FaxRequired { get; set; }


        #region Nested classes

        public partial class AddressAttributeModel : BaseNopEntityModel
        {
            public AddressAttributeModel()
            {
                Values = new List<AddressAttributeValueModel>();
            }

            public string Name { get; set; }

            public bool IsRequired { get; set; }

            /// <summary>
            /// Selected value for textboxes
            /// </summary>
            public string DefaultValue { get; set; }

            public AttributeControlType AttributeControlType { get; set; }

            public IList<AddressAttributeValueModel> Values { get; set; }
        }

        public partial class AddressAttributeValueModel : BaseNopEntityModel
        {
            public string Name { get; set; }

            public bool IsPreSelected { get; set; }
        }

        #endregion
    }
}