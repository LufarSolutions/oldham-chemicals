﻿using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Common
{
    public class AddressEditModel : BaseNopModel
    {
        public AddressModel Address { get; set; }

        public int? CustomerId { get; set; }

        public bool HasLatitudeAndLongitude { get; set; }

        public int? OrderId { get; set; }
    }
}