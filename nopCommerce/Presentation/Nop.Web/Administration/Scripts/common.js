﻿var common = (function () {

    return {

        convertJsonDateToFriendlyDate: function (jsonDate) {

            if (jsonDate === null)
                return null;

            var dateRegExp = /^\/Date\((.*?)\)\/$/;
            var date = dateRegExp.exec(jsonDate);

            return new Date(parseInt(date[1]));
        },

        getSelectedCheckboxIds: function (checkBoxListClassName) {

            var SelectedCheckboxIds = [];

            $("." + checkBoxListClassName + ":checked").each(function () {

                SelectedCheckboxIds.push(this.id);

            });


            return SelectedCheckboxIds;
        },

        htmlEncode(value) {
            return $('<div/>').text(value).html();
        },

        htmlDecode(value) {
            return $('<div/>').html(value).text();
        },

    };

})();