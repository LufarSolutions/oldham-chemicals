﻿var erpQueue = (function () {

    var CheckAllPendingOrdersCheckBoxId = "CheckAllPendingOrdersCheckBox"
    var OrdersNotQueuedCheckboxClassName = "OrdersNotQueued_DateQueuedForERP";
    var ButtonQueueSelectedOrdersId = "ButtonQueueSelectedOrders";
    var ManuallyAssociatePendingOrderButtonClassName = "ManuallyAssociatePendingOrderButton";
    var ManuallyAssociatePendingOrderTextboxClassName = "ManuallyAssociatePendingOrderTextbox";
    var ManuallyAssociateQueuedOrderButtonClassName = "ManuallyAssociateQueuedOrderButton";
    var ManuallyAssociateQueuedOrderTextboxClassName = "ManuallyAssociateQueuedOrderTextbox";
    var RemoveQueuedOrderClassName = "RemoveQueuedOrder";
    var RemovePendingOrderClassName = "RemovePendingOrder";
    var SearchQueuedOrdersButtonId = "SearchQueuedOrdersButton";
    var SearchFormId = "SearchForm";

    return {

        initializeView: function () {         

            ux.setPageRefresh(299000);
            
            var TooltipTemplate = "<div class=\"tooltip tooltip-danger\" role=\"tooltip\"><div class=\"tooltip-arrow\"></div><div class=\"tooltip-inner\" style=\"text-align: left;\"></div></div>"
            $('[data-toggle="tooltip"]').tooltip({
                template: TooltipTemplate,
            });

            var PopoverTemplate = "<div class=\"popover\" role=\"tooltip\"><div class=\"arrow\"></div><div class=\"text-danger\" style=\"font-weight: bold; margin: 10px;\">Remove Order?</div><div style=\"margin: 10px;\"><a class=\"RemoveOrder text-danger\" onclick=\"erpQueue.removeOrder(this);\" style=\"font-weight: normal;\">Yes, remove order.</a></div></div>";
            $('[data-toggle="popover"]').popover({
                template: PopoverTemplate,
            });

            $("#" + ButtonQueueSelectedOrdersId).click(function (e) {
                
                erpQueue.queueSelectedOrders();

            });

            $("#" + CheckAllPendingOrdersCheckBoxId).click(function (e) {

                var IsChecked = $("#" + CheckAllPendingOrdersCheckBoxId).prop("checked");

                $("." + OrdersNotQueuedCheckboxClassName).prop("checked", IsChecked);

            });

            $("." + ManuallyAssociatePendingOrderButtonClassName).click(function (e) {

                var orderId = $(this).data("orderid");

                var erpIdTextbox = $("." + ManuallyAssociatePendingOrderTextboxClassName + "[data-orderid='" + orderId + "']");
                
                erpQueue.manuallyAssociatePendingOrder(erpIdTextbox.val(), orderId);

            });

            $("." + ManuallyAssociateQueuedOrderButtonClassName).click(function (e) {

                var orderId = $(this).data("orderid");

                var erpIdTextbox = $("." + ManuallyAssociateQueuedOrderTextboxClassName + "[data-orderid='" + orderId + "']");

                erpQueue.manuallyAssociateQueuedOrder(erpIdTextbox.val(), orderId);

            });

        },

        manuallyAssociatePendingOrder: function (erpOrderId, orderIdToAssociate) {

            if (erpOrderId === null || erpOrderId === "") {

                ux.displayAlert("Please enter a valid ERP Order Id to associate order # " + orderIdToAssociate + ".", "warning", true);

            }
            else {

                ux.showProgressBar();

                $.ajax({
                    url: "/Admin/Order/ManuallyAssociatePendingOrder",
                    data: {
                        erpOrderId: erpOrderId,
                        orderIdToAssociate: orderIdToAssociate
                    },
                    dataType: "json",
                    traditional: true,
                    type: "GET",
                    error: function () {

                        //ux.hideProgressBar();

                        ux.displayAlert("Unable to associate order.", "danger", true);

                        erpQueue.searchQueue();

                        ux.scrollToTop();

                    },
                    success: function (errors) {

                        if (errors !== null && errors.length > 0) {

                            var ErrorMessage = "The order was not successfully associated:<ul>"

                            $.each(errors, function () {

                                ErrorMessage += "<li>[Order ID: " + this.Key + "] " + this.Value + "</li>"

                            });

                            ErrorMessage += "</ul>";

                            ux.displayAlert(ErrorMessage, "danger", true);
                        }
                        else {

                            ux.displayAlert("The order was successfully associated.", "success", true);
                        }

                        erpQueue.searchQueue();

                        ux.scrollToTop();
                    },

                });
            }

        },

        manuallyAssociateQueuedOrder: function (erpOrderId, orderIdToAssociate) {

            if (erpOrderId === null || erpOrderId === "") {

                ux.displayAlert("Please enter a valid ERP Order Id to associate order # " + orderIdToAssociate + ".", "warning", true);

            }
            else {

                ux.showProgressBar();

                $.ajax({
                    url: "/Admin/Order/ManuallyAssociateQueuedOrder",
                    data: {
                        erpOrderId: erpOrderId,
                        orderIdToAssociate: orderIdToAssociate
                    },
                    dataType: "json",
                    traditional: true,
                    type: "GET",
                    error: function () {

                        //ux.hideProgressBar();

                        ux.displayAlert("Unable to associate order.", "danger", true);

                        erpQueue.searchQueue();

                        ux.scrollToTop();

                    },
                    success: function (errors) {

                        if (errors !== null && errors.length > 0) {

                            var ErrorMessage = "The order was not successfully associated:<ul>"

                            $.each(errors, function () {

                                ErrorMessage += "<li>[Order ID: " + this.Key + "] " + this.Value + "</li>"

                            });

                            ErrorMessage += "</ul>";

                            ux.displayAlert(ErrorMessage, "danger", true);
                        }
                        else {

                            ux.displayAlert("The order was successfully associated.", "success", true);
                        }

                        erpQueue.searchQueue();

                        ux.scrollToTop();
                    },

                });
            }

        },

        postSearchActions: function () {

            ux.hideProgressBar();
            
            erpQueue.initializeView();
        },

        removeOrder: function (e) {
            
            var PopoverId = $(e).closest(".popover").attr("id");

            var RemoveButton = $("a[aria-describedby='" + PopoverId + "'");

            var OrderIdToRemove = RemoveButton.data("orderid");
            var RemoveButtonClassName = RemoveButton.attr("class");

            if (RemoveButtonClassName.includes(RemoveQueuedOrderClassName)) {

                erpQueue.removeQueuedOrder(OrderIdToRemove);

            }
            else if (RemoveButtonClassName.includes(RemovePendingOrderClassName)) {

                erpQueue.removePendingOrder(OrderIdToRemove);

            }
        },

        queueSelectedOrders: function () {

            ux.showProgressBar();

            var SelectedCheckboxIds = common.getSelectedCheckboxIds(OrdersNotQueuedCheckboxClassName);

            var SelectedOrderIds = [];

            $.each(SelectedCheckboxIds, function () {

                var OrderId = $("#" + this).data("orderid");

                SelectedOrderIds.push(OrderId);

            });

            $.ajax({
                url: "/Admin/Order/QueueSelectedOrders",
                data: {
                    selectedOrderIds: SelectedOrderIds
                }, 
                dataType: "json",
                traditional: true,
                type: "GET",
                error: function () {

                    ux.displayAlert("Unable to queue order(s).", "danger", true);

                    ux.scrollToTop();

                    erpQueue.searchQueue();

                    ux.scrollToTop();

                },
                success: function (errors) {

                    if (errors !== null && errors.length > 0) {

                        var ErrorMessage = "Not all selected orders were successfully queued:<ul>"

                        $.each(errors, function () {

                            ErrorMessage += "<li>[Order ID: " + this.Key + "] " + this.Value + "</li>"

                        });

                        ErrorMessage += "</ul>";

                        ux.displayAlert(ErrorMessage, "danger", true);
                    }
                    else {

                        ux.displayAlert("All selected orders were successfully queued for conversion.", "success", true);
                    }

                    erpQueue.searchQueue();

                    ux.scrollToTop();
                },

            });
        },

        removeQueuedOrder: function (queuedOrderId)
        {
            ux.showProgressBar();

            $.ajax({
                url: "/Admin/Order/RemoveQueuedOrder",
                data: {
                    queuedOrderId: queuedOrderId
                },
                dataType: "json",
                traditional: true,
                type: "GET",
                error: function (message) {

                    ux.hideProgressBar();

                    ux.displayAlert("An unexpected error occurred when removing the order.", "danger", true);

                },
                success: function (message) {

                    erpQueue.searchQueue();

                    ux.displayAlert("The order was successfully removed.", "success", true);              
                },

            });
        },

        removePendingOrder: function (selectedOrderId) {
            ux.showProgressBar();

            $.ajax({
                url: "/Admin/Order/RemovePendingOrder",
                data: {
                    onlineOrderId: selectedOrderId
                },
                dataType: "json",
                traditional: true,
                type: "GET",
                error: function (message) {

                    ux.hideProgressBar();

                    ux.displayAlert("An unexpected error occurred when removing the order.", "danger", true);
                },
                success: function (message) {

                    erpQueue.searchQueue();

                    ux.displayAlert("The order was successfully removed.", "success", true); 
                },

            });
        },

        searchQueue: function () {
            
            var SearchForm = $("#" + SearchFormId);

            SearchForm.submit();

            return true;

        },

    }

})();