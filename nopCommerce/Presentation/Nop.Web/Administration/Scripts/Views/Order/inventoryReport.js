﻿var inventoryReport = (function () {

    var ExportToExcelButtonId = "ExportToExcelButton";
    var ReportGridContainerId = "ReportGrid";
    var ManufacturerTextBoxId = "InventoryReportForm_Manufacturer";
    var SelectedManufacturerCodeId = "InventoryReportForm_SelectedManufacturerCode";
    var SearchButtonId = "SearchButton";
    var SearchFormId = "SearchForm";

    var detailExportPromises = [];
    
    return {

        properties: {

            CheckboxAllWarehousesId: "CheckboxAllWarehouses",
            CheckboxWarehouseClassName: "CheckboxWarehouse"

        },

        destroyGrid: function () {

            var Grid = $("#" + ReportGridContainerId).data("kendoGrid");

            if (Grid) {

                Grid.destroy();
            }
        },

        exportChildData: function (warehouseInventories, rowIndex) {

            if (warehouseInventories.length > 0) {

                var deferred = $.Deferred();

                detailExportPromises.push(deferred);

                // add a spacer property so that the alignment is correct on export
                warehouseInventories.forEach(function (warehouseInventory) {
                    warehouseInventory.Spacer = " ";
                });

                var DetailColumns = inventoryReport.generateDetailColumns(warehouseInventories[0]);

                var SpacerColumn = {
                    field: "Spacer",
                    //template: "#=WarehouseName# (#=WarehouseCode#)",
                    title: " ",
                };

                DetailColumns.splice(0, 0, SpacerColumn);

                var exporter = new kendo.ExcelExporter({
                    columns: DetailColumns,
                    dataSource: warehouseInventories
                });

                exporter.workbook().then(function (book, data) {
                    deferred.resolve({
                        masterRowIndex: rowIndex,
                        sheet: book.sheets[0]
                    });
                });

            }
        },

        generateDetailColumns: function (reportDetailData) {

            var Columns = [
                {
                    attributes: {
                        style: "text-align: right;"
                    },
                    field: "WarehouseName",
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: "#=WarehouseName# (#=WarehouseCode#)",
                    title: "Warehouse"
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "QuantityOnPO",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "On PO",
                    width: 70
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "CurrentInventory",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "On Hand",
                    width: 70
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "UnitsSoldYearToDate",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "12MTD",
                    width: 70
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "UnitsSold9MonthsToDate",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "9MTD",
                    width: 70
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "UnitsSold6MonthsToDate",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "6MTD",
                    width: 70
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "UnitsSold3MonthsToDate",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "3MTD",
                    width: 70
                }
            ];

            for (var i = 0; i < reportDetailData.UnitsSoldPerMonth.length; i++) {
                var ColumnName = reportDetailData.UnitsSoldPerMonth[i].Key;
                var Field = "UnitsSoldPerMonth[" + i + "].Value";

                Columns.push(
                    {
                        attributes: {
                            style: "text-align: center;"
                        },
                        field: Field,
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        title: ColumnName,
                        width: 75
                    });
            }

            return Columns;
        },

        generateHeaderColumns: function (reportData) {
            //console.log(reportData);

            var Columns = [
                {
                    field: "ProductName",
                    title: "Product",
                    template: "<a href='/Admin/Product/Edit/#=ProductId#' target='_blank'>#=ProductName#</a> (#=UnitOfMeasure#)",
                    //width: 300
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "Sku",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "Sku",
                    width: 100
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "TotalQuantityOnPO",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "On PO",
                    width: 70
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "TotalCurrentInventory",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "On Hand",
                    width: 74
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "UnitsSoldYearToDate",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "12MTD",
                    width: 74
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "UnitsSold9MonthsToDate",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "9MTD",
                    width: 74
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "UnitsSold6MonthsToDate",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "6MTD",
                    width: 74
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "UnitsSold3MonthsToDate",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    title: "3MTD",
                    width: 74
                }
            ];

            for (var i = 0; i < reportData.TotalUnitsSoldPerMonth.length; i++) {
                var ColumnName = reportData.TotalUnitsSoldPerMonth[i].Key;
                var Field = "TotalUnitsSoldPerMonth[" + i + "].Value";

                Columns.push(
                    {
                        attributes: {
                            style: "text-align: center;"
                        },
                        field: Field,
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        title: ColumnName,
                        width: 74
                    });
            }

            return Columns;
        },

        generateReport: function (reportData) {

            inventoryReport.destroyGrid();

            var ReportColumns = inventoryReport.generateHeaderColumns(reportData[0]);

            $("#" + ReportGridContainerId).kendoGrid({
                //toolbar: ["excel"],
                excel: {
                    allPages: true,
                    fileName: "Inventory.xlsx",
                    //proxyURL: "/Admin/Order/ExportInventoryReportToExcel",
                    //filterable: true
                },
                dataSource: {
                    data: reportData,
                    cache: false,
                    type: "json"
                },
                columns: ReportColumns,
                detailInit: inventoryReport.generateWarehouseRows,
                dataBound: function () {

                    detailExportPromises = [];
                    //    this.expandRow(this.tbody.find("tr.k-master-row"));
                },
                scrollable: false,
                excelExport: function (e) {

                    e.preventDefault();

                    var workbook = e.workbook;

                    detailExportPromises = [];

                    var masterData = e.data;

                    for (var rowIndex = 0; rowIndex < masterData.length; rowIndex++) {
                        inventoryReport.exportChildData(masterData[rowIndex].WarehouseInventories, rowIndex);
                    }

                    $.when.apply(null, detailExportPromises)
                        .then(function () {
                            // get the export results
                            var detailExports = $.makeArray(arguments);

                            // sort by masterRowIndex
                            detailExports.sort(function (a, b) {
                                return a.masterRowIndex - b.masterRowIndex;
                            });

                            // add an empty column
                            workbook.sheets[0].columns.unshift({
                                width: 30
                            });

                            // prepend an empty cell to each row
                            for (var i = 0; i < workbook.sheets[0].rows.length; i++) {
                                workbook.sheets[0].rows[i].cells.unshift({});
                            }

                            // merge the detail export sheet rows with the master sheet rows
                            // loop backwards so the masterRowIndex doesn't need to be updated
                            for (var i = detailExports.length - 1; i >= 0; i--) {
                                var masterRowIndex = detailExports[i].masterRowIndex + 1; // compensate for the header row

                                var sheet = detailExports[i].sheet;

                                // prepend an empty cell to each row
                                for (var ci = 0; ci < sheet.rows.length; ci++) {
                                    if (sheet.rows[ci].cells[0].value) {
                                        sheet.rows[ci].cells.unshift({});
                                    }
                                }

                                // insert the detail sheet rows after the master row
                                [].splice.apply(workbook.sheets[0].rows, [masterRowIndex + 1, 0].concat(sheet.rows));
                            }

                            // save the workbook
                            kendo.saveAs({
                                dataURI: new kendo.ooxml.Workbook(workbook).toDataURL(),
                                fileName: "Export.xlsx"
                            });


                        });

                }
            });

        },
        
        generateWarehouseRows: function (reportData) {
            
            if (reportData.data.WarehouseInventories.length > 0) {

                var DetailColumns = inventoryReport.generateDetailColumns(reportData.data.WarehouseInventories[0]);

                $("<div/>").appendTo(reportData.detailCell).kendoGrid({
                    dataSource: {
                        data: reportData.data.WarehouseInventories,
                        cache: false,
                        type: "json"
                    },
                    excel: {
                        allPages: true
                    },
                    columns: DetailColumns,
                    scrollable: false

                });            
            }
        },

        getReportData: function () {

            var ReportData;

            $.ajax({
                url: "/Admin/Order/GetInventoryReport",
                type: "POST",
                dataType: "json",
                data: $("#" + SearchFormId).serialize(),
                success: function (result) {

                    inventoryReport.postGetDataSuccessOperations(result.Data);

                }
            });

            return ReportData;
        },

        initializeManufacturerAutoComplete: function () {

            $("#" + ManufacturerTextBoxId).autocomplete({
                delay: 500,
                minLength: 2,
                source: "/Admin/Manufacturer/AutoComplete",
                select: function (event, ui) {

                    $("#" + ManufacturerTextBoxId).val(ui.item.label);
                    $("#" + SelectedManufacturerCodeId).val(ui.item.code);

                    return false;
                }
            });
        },

        initializeView: function () {

            inventoryReport.initializeManufacturerAutoComplete();

            $("#" + inventoryReport.properties.CheckboxAllWarehousesId).click(function (e) {
                
                inventoryReport.toggleWarehouseCheckboxes($(this).is(":checked"));

            });

            $("." + inventoryReport.properties.CheckboxWarehouseClassName).click(function (e) {

                var SelectedWarehouseCheckboxes = common.getSelectedCheckboxIds(inventoryReport.properties.CheckboxWarehouseClassName);
                var NumberOfWarehouseCheckboxes = $("." + inventoryReport.properties.CheckboxWarehouseClassName).length;
                var AllWarehouseCheckboxesAreChecked = SelectedWarehouseCheckboxes.length == NumberOfWarehouseCheckboxes;

                $("#" + inventoryReport.properties.CheckboxAllWarehousesId).prop("checked", AllWarehouseCheckboxesAreChecked);

            });

            $("#" + ExportToExcelButtonId).click(function (e) {

                e.preventDefault();

                $("#" + ReportGridContainerId).getKendoGrid().saveAsExcel();

            });

            $("#" + SearchButtonId).click(function (e) {

                e.preventDefault();

                if ($("#" + SearchFormId).valid()) {

                    ux.showProgressBar();

                    inventoryReport.getReportData();
                }

            });

        },

        postGetDataSuccessOperations: function (reportData) {

            inventoryReport.generateReport(reportData);

            //$(".k-grid tbody .k-grid .k-grid-header").hide();

            $("#" + ExportToExcelButtonId).show();

            ux.hideProgressBar();

        },

        toggleWarehouseCheckboxes: function (isChecked) {

            console.log(isChecked);

            $("." + inventoryReport.properties.CheckboxWarehouseClassName).prop("checked", isChecked);
        },

        warehouse: function (selected, text, value) {

            this.Selected = selected;
            this.Value = value;
            this.Text = text;
        },

    }

})();