﻿export default class RepairRequestController {
    constructor() {
        $(document).ready(() => {
            this.initialize();
        });
    }

    authorizeRepairRequest() {
        const repairRequestId = $('#RepairRequestId').val();

        $.ajax({
            url: '/Admin/ReturnRequest/AuthorizeRepairRequest',
            data: {
                repairRequestId: repairRequestId,
                __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            },
            type: 'POST',
            error: () => {
                this.displayModalMessage('An unexpected error occurred when authorizing the repair request.', 'danger', true);
            },
            success: (success) => {
                this.displayModalMessage('The repair request was successfully authorized.', 'success', true);

                this.loadRepairRequestModal(repairRequestId);
            },
        });
    }

    bindActions() {
        buttonResetSearch
        $('#buttonResetSearch').unbind('click');
        $('#buttonResetSearch').click((e) => {
            e.preventDefault();

            $('#FormSearch')[0].reset();

            this.initializeGrid();
        });

        $('#buttonSearch').unbind('click');
        $('#buttonSearch').click((e) => {
            e.preventDefault();

            this.initializeGrid();
        });

        $('#ButtonNewRepairRequest').unbind('click');
        $('#ButtonNewRepairRequest').click((e) => {
            e.preventDefault();

            this.loadRepairRequestModal(null);
        });
    }

    cancelRepairRequest() {
        const repairRequestId = $('#RepairRequestId').val();

        $.ajax({
            url: '/Admin/ReturnRequest/CancelRepairRequest',
            data: {
                repairRequestId: repairRequestId,
                __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            },
            type: 'POST',
            error: () => {
                this.displayModalMessage('An unexpected error occurred when cancelling the repair request.', 'danger', true);
            },
            success: (success) => {
                this.displayModalMessage('The repair request was successfully cancelled.', 'success', true);

                this.loadRepairRequestModal(repairRequestId);
            },
        });
    }

    completeRepairRequest() {
        const repairRequestId = $('#RepairRequestId').val();

        $.ajax({
            url: '/Admin/ReturnRequest/CompleteRepairRequest',
            data: {
                repairRequestId: repairRequestId,
                __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            },
            type: 'POST',
            error: () => {
                this.displayModalMessage('An unexpected error occurred when completing the repair request.', 'danger', true);
            },
            success: (success) => {
                this.displayModalMessage('The repair request was successfully completed.', 'success', true);

                this.loadRepairRequestModal(repairRequestId);
            },
        });
    }

    displayModalMessage(message, type, removeExistingAlerts) {
        if (removeExistingAlerts) {
            $('#ModalRepairRequestMessageContainer').html('');
        }

        var alertHtml = "<div class=\"text-" + type + "\" role=\"alert\" style=\"margin-bottom: 0px;\">";

        alertHtml += message;

        alertHtml += "</div>";

        $('#ModalRepairRequestMessageContainer').prepend(alertHtml);
    }

    getSearchData() {
        var statuses = [];
        $('#RepairRequestsSearchModel_Statuses:checked').each((i, e) => {
            statuses.push({ text: '', value: $(e).val() });
        });

        var data = {
            repairRequestsSearchModel: {
                authorizationNumber: $('#RepairRequestsSearchModel_AuthorizationNumber').val(),
                customer: $('#RepairRequestsSearchModel_Customer').val(),
                customerId: $('#RepairRequestsSearchModel_CustomerId').val(),
                product: $('#RepairRequestsSearchModel_Product').val(),
                statuses: statuses,  
                warehouseId: $('#RepairRequestsSearchModel_WarehouseId').val(),
            }
        };

        addAntiForgeryToken(data);

        return data;
    }

    inboundRepairRequest() {
        const repairRequestId = $('#RepairRequestId').val();

        $.ajax({
            url: '/Admin/ReturnRequest/InboundRepairRequest',
            data: {
                repairRequestId: repairRequestId,
                trackingNumberInbound: $('#TrackingNumberInbound').val(),
                __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            },
            type: 'POST',
            error: () => {

                this.displayModalMessage('An unexpected error occurred when shipping the repair request.', 'danger', true);
            },
            success: (success) => {
                this.displayModalMessage('The repair request was successfully shipped.', 'success', true);

                this.loadRepairRequestModal(repairRequestId);
            },
        });
    }

    initialize() {
        this.bindActions();

        $('#RepairRequestsSearchModel_Customer').autocomplete({
            delay: 500,
            minLength: 3,
            source: '/Admin/Customer/Autocomplete',
            appendTo: '.search-box',
            change: (e, ui) => {
                if (ui.item === null) {
                    $('#RepairRequestsSearchModel_Customer').val('');
                    $('#RepairRequestsSearchModel_CustomerId').val('0');
                }

                if ($('#RepairRequestsSearchModel_Customer').val().length === 0) {
                    $('#RepairRequestsSearchModel_CustomerId').val('0');
                }
            },
            select: (event, ui) => {
                $('#RepairRequestsSearchModel_Customer').val(ui.item.customername);
                $('#RepairRequestsSearchModel_CustomerId').val(ui.item.customerid);

                return false;
            }
        })
        .data('ui-autocomplete')._renderItem = (ul, item) => {
            var t = item.label;
            //html encode
            t = common.htmlEncode(t);
            return $('<li></li>')
                .data('item.autocomplete', item)
                .append(`<a style="margin: 5px;">${item.customername}</a>`)
                .appendTo(ul);
        };

        $('#RepairRequestsSearchModel_Customer').attr('autocomplete', 'chrome-off');

        $('#RepairRequestsSearchModel_Product').autocomplete({
            delay: 500,
            minLength: 3,
            source: '/Admin/ReturnRequest/SearchProductsForRepairRequests',
            appendTo: '.search-box',
            change: (e, ui) => {
                if (ui.item === null) {
                    $('#RepairRequestsSearchModel_Product').val();
                }
            },
            select: (event, ui) => {
                $('#RepairRequestsSearchModel_Product').val(ui.item.label);

                return false;
            }
        })
        .data('ui-autocomplete')._renderItem = (ul, item) => {
            var t = item.label;
            //html encode
            t = common.htmlEncode(t);
            return $('<li></li>')
                .data('item.autocomplete', item)
                .append(`<a><img class="img" src="${item.productpictureurl}" style="height: 50px;"><span>${t}</span></a>`)
                .appendTo(ul);
        };

        this.initializeGrid();

        $('#ModalRepairRequest').on('hidden.bs.modal', (e) => {
            $('#ModalRepairRequestMessageContainer').html('');
            this.initializeGrid();
        })
    }

    initializeGrid() {
        var searchData = this.getSearchData();

        $("#returnrequests-grid").kendoGrid({
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: "/Admin/ReturnRequest/RepairRequests",
                        type: "POST",
                        dataType: "json",
                        data: searchData
                    }
                },
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors"
                },
                error: function (e) {
                    display_kendoui_grid_error(e);
                    // Cancel the changes
                    this.cancelChanges();
                },
                pageSize: 15,
                serverPaging: true,
                    serverFiltering: true,
                        serverSorting: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: [10,15,20,50,100]
                },
                dataBound: () => {            
                    $('.ButtonEditRepairRequest').unbind('click');
                    $('.ButtonEditRepairRequest').click((e) => {
                        e.preventDefault();

                        const repairRequestId = $(e.currentTarget).data('repair-request-id');
                        this.loadRepairRequestModal(repairRequestId);
                    });
                },
                editable: false,
                scrollable: false,
                    columns: [
                        {
                            field: "AuthorizationNumber",
                            title: "Authorization #",
                            width: 100
                        }, {
                            field: "ProductName",
                            title: "Product",
                            width: 200,
                            template: kendo.template($('#productTemplate').html())
                        },
                        {
                            field: "CustomerId",
                            title: "Customer",
                            width: 200,
                            template: '<a href="/Admin/Customer/Edit/#=CustomerId#">#:CustomerName#</a>'
                        },
                        {
                            field: "SelectedWarehouseName",
                            title: "Warehouse",
                            width: 100
                        },
                        {
                            field: "Status",
                            title: "Status",
                            width: 100
                        }, {
                            field: "TrackingNumberInbound",
                            title: "Inbound Tracking #",
                            width: 100,
                            template: kendo.template($('#trackingNumberInboundTemplate').html())
                        }, {
                            field: "TrackingNumberOutbound",
                            title: "Outbound Tracking #",
                            width: 100,
                            template: kendo.template($('#trackingNumberOutboundTemplate').html())
                        }, {
                            field: "DateCreated",
                            title: "Created On",
                            width: 100,
                            type: "date",
                            format: "{0:G}"
                        }, {
                            field: "RepairRequestId",
                            title: " ",
                            width: 50,
                            template: '<button type="button" class="btn btn-sm btn-info ButtonEditRepairRequest" data-repair-request-id="#=RepairRequestId#"><span class="glyphicon glyphicon-wrench"></span></button>' + 
                                '<a class="btn btn-sm btn-info" href="/Admin/ReturnRequest/PrintRepairRequest?repairRequestId=#=RepairRequestId#" style="margin-left: 5px;" target="_blank"><span class="glyphicon glyphicon-print"></span></a>'
                        }
                    ]
        });
    }

    initializeRepairRequestForm() {

        $('#ButtonAuthorize').unbind('click');
        $('#ButtonAuthorize').click((e) => {
            e.preventDefault();

            this.authorizeRepairRequest();
        });

        $('#ButtonCancel').unbind('click');
        $('#ButtonCancel').click((e) => {
            e.preventDefault();

            this.cancelRepairRequest();
        });

        $('#ButtonComplete').unbind('click');
        $('#ButtonComplete').click((e) => {
            e.preventDefault();

            this.completeRepairRequest();
        });

        $('#ButtonInbound').unbind('click');
        $('#ButtonInbound').click((e) => {
            e.preventDefault();

            this.inboundRepairRequest();
        });

        $('#ButtonOutnbound').unbind('click');
        $('#ButtonOutbound').click((e) => {
            e.preventDefault();

            this.outboundRepairRequest();
        });

        $('#ButtonReceive').unbind('click');
        $('#ButtonReceive').click((e) => {
            e.preventDefault();

            this.receiveRepairRequest();
        });

        $('#ButtonSubmitRepairRequest').unbind('click');
        $('#ButtonSubmitRepairRequest').click((e) => {
            e.preventDefault();

            this.submitRepairRequestForm(false);
        });

        $('#ButtonSubmitAndAuthorizeRepairRequest').unbind('click');
        $('#ButtonSubmitAndAuthorizeRepairRequest').click((e) => {
            e.preventDefault();

            this.submitRepairRequestForm(true);
        });

        $('#CustomerName').autocomplete({
            delay: 500,
            minLength: 3,
            source: '/Admin/Customer/Autocomplete',
            appendTo: '.search-box',
            change: (e, ui) => {
                if (ui.item === null) {
                    $('#CustomerId').val('');
                    $('#CustomerName').val();
                    $('#CustomerCompany').val();
                    $('#CustomerEmail').val();
                    $('#CustomerPhoneNumber').val();
                }
            },
            select: (event, ui) => {
                $('#CustomerId').val(ui.item.customerid);
                $('#CustomerName').val(ui.item.customername);
                $('#CustomerCompany').val(ui.item.company);
                $('#CustomerEmail').val(ui.item.email);
                $('#CustomerPhoneNumber').val(ui.item.phone);

                window.setTimeout((e) => {
                    $('#Product_Name').focus();
                }, 500);

                return false;
            }
        })
            .data('ui-autocomplete')._renderItem = (ul, item) => {
                var t = item.label;
                //html encode
                t = common.htmlEncode(t);
                return $('<li></li>')
                    .data('item.autocomplete', item)
                    .append(`<a style="margin: 5px;">${item.customername}</a>`)
                    .appendTo(ul);
            };

        $('#CustomerName').attr('autocomplete', 'chrome-off');

        $('#ProductName').autocomplete({
            delay: 500,
            minLength: 3,
            source: '/Admin/ReturnRequest/SearchProductsForRepairRequests',
            appendTo: '.search-box',
            change: (e, ui) => {
                if (ui.item === null) {
                    $('#ProductId').val('');
                    $('#LinkProduct').html('');
                    $('#LinkProduct').attr('href', '');
                    $('#ImageProduct').attr('src', '');
                    $('#ProductSku').html('');
                    $('#ContainerProduct').hide();
                }
            },
            select: (event, ui) => {
                $('#ProductId').val(ui.item.productid);
                $('#ProductName').val(ui.item.label);
                $('#LinkProduct').html(ui.item.label);
                $('#LinkProduct').attr('href', `/Admin/Product/Edit/${ui.item.productid}`);
                $('#ImageProduct').attr('src', ui.item.productpictureurl);
                $('#ProductSku').html(ui.item.sku);

                $('#ContainerProduct').show();

                window.setTimeout((e) => {
                    $('#CustomerComments').focus();
                }, 500);

                return false;
            }
        })
        .data('ui-autocomplete')._renderItem = (ul, item) => {
            var t = item.label;
            //html encode
            t = common.htmlEncode(t);
            return $('<li></li>')
                .data('item.autocomplete', item)
                .append(`<a><img class="img" src="${item.productpictureurl}" style="height: 50px;"><span>${t}</span></a>`)
                .appendTo(ul);
            };

        $('#FormSubmitRepairRequest').removeData('validator');
        $('#FormSubmitRepairRequest').removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse('#FormSubmitRepairRequest');
    }

    loadRepairRequestModal(repairRequestId) {
        $.ajax({
            url: '/Admin/ReturnRequest/GetRepairRequestForm',
            data: {
                repairRequestId: repairRequestId
            },
            type: 'GET',
            error: () => {
                ux.displayAlert('An unexpected error occurred when reloading the repair request.', 'danger', true);
            },
            success: (formHtml) => {
                $('#ContainerRepairRequest').replaceWith(formHtml);

                const repairRequestStatus = $('#ContainerStatus').html();

                if (repairRequestStatus === 'Pending') {
                    $('#ButtonSubmitAndAuthorizeRepairRequest').removeClass('disabled');
                }
                else {
                    $('#ButtonSubmitAndAuthorizeRepairRequest').addClass('disabled');
                }

                if (!$('#ModalRepairRequest').hasClass('in')) {
                    $('#ModalRepairRequest').modal('show');
                }

                this.initializeRepairRequestForm();
            },
        });
    }

    outboundRepairRequest() {
        const repairRequestId = $('#RepairRequestId').val();

        $.ajax({
            url: '/Admin/ReturnRequest/OutboundRepairRequest',
            data: {
                repairRequestId: repairRequestId,
                trackingNumberOutbound: $('#TrackingNumberOutbound').val(),
                __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            },
            type: 'POST',
            error: () => {

                this.displayModalMessage('An unexpected error occurred when returning the repair request.', 'danger', true);
            },
            success: (success) => {
                this.displayModalMessage('The repair request was successfully marked for return.', 'success', true);
                this.loadRepairRequestModal(repairRequestId);
            },
        });
    }

    receiveRepairRequest() {
        const repairRequestId = $('#RepairRequestId').val();

        $.ajax({
            url: '/Admin/ReturnRequest/ReceiveRepairRequest',
            data: {
                repairRequestId: repairRequestId,
                __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            },
            type: 'POST',
            error: () => {
                this.displayModalMessage('An unexpected error occurred when receiving the repair request.', 'danger', true);
            },
            success: (success) => {
                this.displayModalMessage('The repair request was successfully received.', 'success', true);

                this.loadRepairRequestModal(repairRequestId);
            },
        });
    }

    submitRepairRequestForm(authorize) {
        const formSubmitRepairRequest = $('#FormSubmitRepairRequest');

        if (formSubmitRepairRequest.valid()) {
            const data = {
                customerCompany: $('#CustomerCompany').val(),
                customerComments: $('#CustomerComments').val(),
                customerEmail: $('#CustomerEmail').val(),
                customerId: $('#CustomerId').val(),
                customerName: $('#CustomerName').val(),
                customerPhoneNumber: $('#CustomerPhoneNumber').val(),
                productId: $('#ProductId').val(),
                productName: $('#ProductName').val(),
                repairRequestId: $('#RepairRequestId').val(),
                selectedWarehouseId: $('#SelectedWarehouseId').val(),
                staffNotes: $('#StaffNotes').val(),
                trackingNumberInbound: $('#TrackingNumberInbound').val(),
                __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            };

            $.ajax({
                url: '/Admin/ReturnRequest/SaveRepairRequest',
                data: data,
                type: 'POST',
                error: () => {
                    this.displayModalMessage('An unexpected error occurred when submitting the repair request.', 'danger', true);
                },
                success: (response) => {
                    this.displayModalMessage('The repair request was successfully submitted.', 'success', true);

                    if (authorize) {
                        $('#RepairRequestId').val(response);
                        this.authorizeRepairRequest();
                    }

                    this.loadRepairRequestModal(response);
                },
            });
        }
    }
}