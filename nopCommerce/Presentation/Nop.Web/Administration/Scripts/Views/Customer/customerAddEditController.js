﻿export default class CustomerAddEdit {
    constructor() {
        $(document).ready(() => {
            this.initialize();
        });
    }

    bindEvents() {
        $('#buttonSendWelcomeMessage').unbind('click');
        $('#buttonSendWelcomeMessage').click((e) => {
            e.preventDefault();

            $.ajax({
                url: '/Admin/Customer/SendWelcomeMessage',
                data: {
                    customerId: $('#CustomerId').val(),
                    __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
                },
                type: 'POST',
                error: (error) => {
                    ux.displayAlert(error, "danger", true);
                },
                success: (success) => {
                    ux.displayAlert(success, "success", true);
                },
            });
        });
    }

    initialize() {
        this.bindEvents();
    }
}