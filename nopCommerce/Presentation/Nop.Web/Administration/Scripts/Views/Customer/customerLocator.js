﻿var customerLocator = (function () {

    return {

        properties: {

            ButtonExportToExcelId: "ButtonExportToExcel",
            ButtonLoadMapId: "ButtonLoadMap",
            ContainerCustomerGridId: "ContainerCustomerGrid",
            ContainerGoogleMapId: "ContainerGoogleMap",
            ContainerMapId: "ContainerMap",
            ContainerNumberOfCustomersId: "ContainerNumberOfCustomers",
            ContainerSelectedRadiusId: "ContainerSelectedRadius",
            ContainerSelectedWarehouseNameId: "ContainerSelectedWarehouseName",
            DropDownListSelecteRadiusId: "SelectedRadius",
            DropDownListSelectedWarehouseId: "SelectedWarehouse_Id",
            FormLocatorMapId: "FormLocatorMap",
            HiddenFieldCustomerCountId: "HiddenFieldCustomerCount",
            HiddenFieldMarkerListClassName: "MarkerList",
            HiddenFieldSelectedWarehouseLatitudeId: "WarehouseAddress_Latitude",
            HiddenFieldSelectedWarehouseLongitudeId: "WarehouseAddress_Longitude",
            HiddenFieldZoomId: "Zoom",

        },

        destroyGrid: function () {

            var Grid = $("#" + customerLocator.properties.ContainerCustomerGridId).data("kendoGrid");

            if (Grid) {

                Grid.destroy();
            }
        }, 

        generateGrid: function (gridData) {

            customerLocator.destroyGrid();

            var GridColumns = customerLocator.generateGridHeaders();

            $("#" + customerLocator.properties.ContainerCustomerGridId).kendoGrid({
                //toolbar: ["excel"],
                excel: {
                    allPages: true,
                    fileName: "CustomerLocator.xlsx",
                },
                columns: GridColumns,
                dataSource: {
                    data: gridData.ExtraData.LocatorCustomers,
                    cache: false,
                    type: "json",
                },
                height: 500,
                scrollable: true
                
            });

        },

        generateGridHeaders: function () {

            var AddressTemplate = "<small><address style=\"margin-bottom: 0\">" +
                //"#=CustomerAddress.Address.Address1#<br />" +
                //"#if (CustomerAddress.Address.Address2 !== null && CustomerAddress.Address.Address2 !== '') { ##=CustomerAddress.Address.Address2# </br /># }#" + 
                //"#=CustomerAddress.Address.City#,&nbsp;#=CustomerAddress.Address.StateProvinceName#,&nbsp;#=CustomerAddress.Address.ZipPostalCode#" +
                "#=CustomerAddress.Address.AddressHtml#" +
                "</address>" +
                "<input type='hidden' class='MarkerList' data-address-id='#=CustomerAddress.Address.Id#' data-address='#=CustomerAddress.Address.AddressHtml#' data-latitude='#=CustomerAddress.Address.Latitude#' data-longitude='#=CustomerAddress.Address.Longitude#'/>" + 
                "<div><a id='HyperLinkDirections_#=CustomerAddress.Address.Id#' href='https://www.google.com/maps/dir//#=CustomerAddress.Address.Address1#+#=CustomerAddress.Address.City#+#=CustomerAddress.Address.StateProvinceName#+#=CustomerAddress.Address.ZipPostalCode#' target='_blank'>Directions</a></div>"
                "</small > ";

            var Columns = [                
                {
                    field: "Customer.FullName",
                    title: "Customer",
                    template: "<small><a href='/Admin/Customer/Edit/#=Customer.Id#' target='_blank'>#=Customer.FullName# (#=Customer.CustomerNumber#)</a>" +
                        "<div>#=Customer.Company#</div>" + 
                    "<div><a href='mailto:'#=Customer.Email#'>#=Customer.Email#</a></div>" +
                    "#if(CustomerAddress.Address.PhoneNumber !== null && CustomerAddress.Address.PhoneNumber !== '') { #<div><a href='tel:#=CustomerAddress.Address.PhoneNumber#'>#=CustomerAddress.Address.PhoneNumber#</a></div># } #" +
                    "</small> ",
                },
                {
                    //attributes: {
                    //    style: "text-align: center;"
                    //},
                    field: "CustomerAddress.Address",
                    headerAttributes: {
                        style: "text-align: center;"
                    },

                    template: AddressTemplate,
                    title: "Address",
                    //width: 100
                },
                //{
                //    attributes: {
                //        style: "text-align: center;"
                //    },
                //    field: "Customer.LastActivityDate",
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    //template: "<small>#=Customer.LastActivityDate#</small>",
                //    title: "Last Active",
                //    //type: "date",
                //    //format: "{0:MM-dd-yyyy}",
                //    template: '<small>#= kendo.toString(common.convertJsonDateToFriendlyDate(Customer.LastActivityDate), "MM/dd/yyyy") #</small>'
                //    //width: 70
                //},
                {
                    field: "Customer.Email",
                    title: "Email",
                    hidden: true
                },
                {
                    field: "Customer.Company",
                    title: "Company",
                    hidden: true
                },
                {
                    field: "CustomerAddress.Address.Address1",
                    title: "Address1",
                    hidden: true
                },
                {
                    field: "CustomerAddress.Address.Address2",
                    title: "Address2",
                    hidden: true
                },
                {
                    field: "CustomerAddress.Address.City",
                    title: "City",
                    hidden: true
                },
                {
                    field: "CustomerAddress.Address.StateProvinceName",
                    title: "State",
                    hidden: true
                },
                {
                    field: "CustomerAddress.Address.ZipPostalCode",
                    title: "Postal Code",
                    hidden: true
                },
                {
                    field: "CustomerAddress.Address.PhoneNumber",
                    title: "Phone Number",
                    hidden: true
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "IsOnline",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    template: "#if (IsOnline){ #<span class='text-success' style='font-weight: bold;'>Yes</span># } else { #<span class='text-danger' style='font-weight: bold;'>No</span># }#",
                    title: "Online?"
                },
                {
                    attributes: {
                        style: "text-align: center;"
                    },
                    field: "CustomerAddress.HasLatitudeAndLongitude",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    template: "#if (CustomerAddress.HasLatitudeAndLongitude){ #<span class='text-success' style='font-weight: bold;'>Yes</span># } else { #<span class='text-danger' style='font-weight: bold;'>No</span># }#",
                    title: "Mapped?"
                }
            ];

            return Columns;
        },
        
        initializeView: function () {
                                   
            $("#" + customerLocator.properties.ButtonExportToExcelId).click(function (e) {

                ux.showProgressBar();
                
                var Grid = $("#" + customerLocator.properties.ContainerCustomerGridId).getKendoGrid();
                
                e.preventDefault();

                Grid.hideColumn(1)
                Grid.showColumn(2);
                Grid.showColumn(3);
                Grid.showColumn(4);
                Grid.showColumn(5);
                Grid.showColumn(6);
                Grid.showColumn(7);
                Grid.showColumn(8);
                Grid.showColumn(9);

                setTimeout(function () {

                    Grid.saveAsExcel();

                    Grid.showColumn(1)
                    Grid.hideColumn(2);
                    Grid.hideColumn(3);
                    Grid.hideColumn(4);
                    Grid.hideColumn(5);
                    Grid.hideColumn(6);
                    Grid.hideColumn(7);
                    Grid.hideColumn(8);
                    Grid.hideColumn(9);

                    ux.hideProgressBar();

                })
                
            });

            $("#" + customerLocator.properties.ButtonLoadMapId).click(function (e) {

                var SelectedWarehouseId = $("#" + customerLocator.properties.DropDownListSelectedWarehouseId).val();
                var SelectedRadius = $("#" + customerLocator.properties.DropDownListSelecteRadiusId).val();
                
                customerLocator.loadMap();
                //customerLocator.loadCustomers();

                e.preventDefault();

                return false;

            });

            customerLocator.loadMap();

        },

        loadCustomers: function () {

            ux.showProgressBar();

            $.ajax({
                url: "/Admin/Customer/GetLocatorCustomers",
                type: "POST",
                dataType: "json",
                data: $("#" + customerLocator.properties.FormLocatorMapId).serialize(),
                error: function () {

                    ux.hideProgressBar();

                    ux.displayAlert("<li>Failed to load the customers.</li>", "danger", true);

                },
                success: function (result) {
                                        
                    var SelectedWarehouseName = $("#" + customerLocator.properties.DropDownListSelectedWarehouseId + " option:selected").text();
                    var SelectedRadius = $("#" + customerLocator.properties.DropDownListSelecteRadiusId + " option:selected").text();
                    
                    $("#" + customerLocator.properties.ContainerSelectedRadiusId).html(SelectedRadius);
                    $("#" + customerLocator.properties.ContainerSelectedWarehouseNameId).html(SelectedWarehouseName);
                    $("#" + customerLocator.properties.ContainerNumberOfCustomersId).html(result.Total);

                    customerLocator.generateGrid(result);
                    customerLocator.renderMap();

                    ux.hideProgressBar();
                },

            });

        },

        loadMap: function () {
            
            $.ajax({
                url: "/Admin/Customer/GetLocatorMap",
                type: "POST",
                //dataType: "json",
                data: $("#" + customerLocator.properties.FormLocatorMapId).serialize(),
                error: function (xhr, status, error) {

                    ux.hideProgressBar();

                    ux.displayAlert("<li>Failed to load the map.</li>", "danger", true);

                },
                success: function (mapView) {
                    
                    $("#" + customerLocator.properties.ContainerMapId).html(mapView);

                    customerLocator.loadCustomers();
                },

            });            
            
        },

        renderMap: function () {

            var selectedLatitude = parseFloat($("#" + customerLocator.properties.HiddenFieldSelectedWarehouseLatitudeId).val());
            var selectedLongitude = parseFloat($("#" + customerLocator.properties.HiddenFieldSelectedWarehouseLongitudeId).val());
            var zoom = parseInt($("#" + customerLocator.properties.HiddenFieldZoomId).val());

            var selectedWarehouseLatAndLong = {
                lat: selectedLongitude,
                lng: selectedLatitude
            };

            var customerMap = new google.maps.Map(document.getElementById(customerLocator.properties.ContainerGoogleMapId), {
                zoom: zoom,
                center: selectedWarehouseLatAndLong
            });

            var warehouseMarker = new google.maps.Marker({
                position: selectedWarehouseLatAndLong,
                map: customerMap,
                icon: "https://www.oldhamchem.com/themes/oldhamchem1/content/images/gmap_marker.png"
            });

            $.each($("." + customerLocator.properties.HiddenFieldMarkerListClassName), function () {

                var customerLatitude = parseFloat($(this).data("latitude"));
                var customerLongitude = parseFloat($(this).data("longitude"));

                if (customerLatitude && customerLongitude && (customerLatitude !== 0 && customerLongitude !== 0)) {

                    var AddressId = $(this).data("address-id");
                    var WindowContent = "<small>" + $(this).data("address");
                    WindowContent += "<div><a href='" + $("#HyperLinkDirections_" + AddressId).attr("href") + "' target='_blank'>Directions</a></div>"
                        //'https://www.google.com/maps/dir//" + ($(this).data("address").replace(/<br\s*\/?>/gi, "+").replace("&nbsp;", "+").replace(",", "+")) + "' target= '_blank' > Directions</a ></div > "
                    WindowContent += "</small>"

                    var customerInfoWindow = new google.maps.InfoWindow({
                        content: WindowContent
                    });

                    var customerMarker = new google.maps.Marker({
                        position: {
                            lat: customerLatitude,
                            lng: customerLongitude
                        },
                        map: customerMap,
                        //icon: "https://www.oldhamchem.com/themes/oldhamchem1/content/images/gmap_marker.png"
                    });

                    customerMarker.addListener("click", function () {
                        customerInfoWindow.open(customerMap, customerMarker);
                    });
                }

            });

        },

    }

})();