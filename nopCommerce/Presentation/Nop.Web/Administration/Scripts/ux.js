﻿var ux = (function () {

    return {

        disableValidationOnHiddenFields: function () {
            
            $.validator.setDefaults({
                ignore: [],
                // any other default options and/or rules
            });

        },

        displayAlert: function (message, type, removeExistingAlerts) {

            if (removeExistingAlerts)
            {
                ux.removeAlerts();
            }

            var alertHtml = "<div class=\"alert alert-dismissable alert-" + type + "\" role=\"alert\" style=\"margin-bottom: 0px;\">";

            alertHtml += "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>";

            alertHtml += message;

            alertHtml += "</div>";
            
            $(".content").prepend(alertHtml);

        },

        displayConfirmationPopover: function (linkId) {


        },

        hideProgressBar: function () {

            $("#ModalProgressBar").modal("hide");

            return true;
        },

        removeAlerts: function () {

            $(".alert").remove();

        },

        scrollToTop: function () {

            $('body,html').animate({
                scrollTop: 0
            }, 800);

        },

        setPageRefresh: function (milliseconds) {

            setTimeout(function () {

                ux.showProgressBar();

                setTimeout(function () {

                    location.reload();

                }, 1000)

            }, milliseconds);

        },

        showProgressBar: function () {
            
            $("#ModalProgressBar").modal("show");

            return true;

        }

    };

})();