﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Nop.Admin.Infrastructure.Cache;
using Nop.Admin.Models.Catalog;
using Nop.Admin.Models.Orders;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;

namespace Nop.Admin.Controllers
{
    public partial class ReturnRequestController : BaseAdminController
    {
        #region Fields

        private readonly IReturnRequestService _returnRequestService;
        private readonly IOrderService _orderService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICustomerService _customerService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IPermissionService _permissionService;
        private readonly IShippingService _shippingService;
        private readonly CatalogSettings _catalogSettings;
        private readonly IProductService _productService;
        private readonly MediaSettings _mediaSettings;
        private readonly IPictureService _pictureService;

        #endregion Fields

        #region Constructors

        public ReturnRequestController(IReturnRequestService returnRequestService,
            IOrderService orderService,
            ICustomerAttributeParser customerAttributeParser,
            ICustomerService customerService,
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService,
            IWorkContext workContext,
            IWorkflowMessageService workflowMessageService,
            LocalizationSettings localizationSettings,
            ICustomerActivityService customerActivityService, 
            IPermissionService permissionService,
            IShippingService shippingService,
            CatalogSettings catalogSettings,
            IProductService productService,
            MediaSettings mediaSettings,
            IPictureService pictureService)
        {
            this._returnRequestService = returnRequestService;
            this._orderService = orderService;
            this._customerAttributeParser = customerAttributeParser;
            this._customerService = customerService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._workflowMessageService = workflowMessageService;
            this._localizationSettings = localizationSettings;
            this._customerActivityService = customerActivityService;
            this._permissionService = permissionService;
            this._shippingService = shippingService;
            this._catalogSettings = catalogSettings;
            this._productService = productService;
            this._mediaSettings = mediaSettings;
            this._pictureService = pictureService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual bool PrepareReturnRequestModel(ReturnRequestModel model, ReturnRequest returnRequest, bool excludeProperties)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            if (returnRequest == null)
                throw new ArgumentNullException("returnRequest");

            OrderItem orderItem = null;
            if (returnRequest.OrderItemId.HasValue)
            {
                orderItem = _orderService.GetOrderItemById(returnRequest.OrderItemId.Value);
            }
            //if (orderItem == null)
            //    return false;

            model.Id = returnRequest.Id;

            if (returnRequest.RequestedAction == "Repair")
            {
                model.ProductId = returnRequest.ProductId;
                model.ProductName = returnRequest.ProductDetails;
            }
            else if (orderItem != null)
            {
                model.ProductId = orderItem.ProductId;
                model.ProductName = orderItem.Product.Name;
                model.OrderId = orderItem.OrderId;
            }

            model.CustomerId = returnRequest.CustomerId.Value;
            var customer = returnRequest.Customer;
            model.Customer = new Models.Customers.CustomerModel {
                FullName = customer.GetFullName(),
                CustomerNumber = customer.CustomerNumber,
                Email = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest")
            };
            
            model.Quantity = returnRequest.Quantity;
            model.ReturnRequestStatusStr = returnRequest.ReturnRequestStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(returnRequest.CreatedOnUtc, DateTimeKind.Utc);
            
            if (!excludeProperties)
            {
                model.ReasonForReturn = returnRequest.ReasonForReturn;
                model.RequestedAction = returnRequest.RequestedAction;
                model.CustomerComments = returnRequest.CustomerComments;
                model.StaffNotes = returnRequest.StaffNotes;
                model.ReturnRequestStatusId = returnRequest.ReturnRequestStatusId;
            }

            //model is successfully prepared
            return true;
        }

        #endregion

        #region "Repair Requests"

        [HttpPost]
        public ActionResult AuthorizeRepairRequest(int repairRequestId) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var repairRequest = _returnRequestService.GetReturnRequestById(repairRequestId);

            if (repairRequest != null && repairRequest.ReturnRequestStatus != ReturnRequestStatus.Authorized) {
                var currentDate = DateTime.Now;

                // get repair requests for the same day at the same warehouse
                var currentRepairRequests = _returnRequestService.SearchReturnRequests(createdOn: currentDate.Date, warehouseId: repairRequest.WarehouseId);

                var sequence = currentRepairRequests.Where(w => !String.IsNullOrEmpty(w.AuthorizationNumber)).Count() + 1;

                repairRequest.ReturnRequestStatus = ReturnRequestStatus.Authorized;
                repairRequest.AuthorizationNumber = $"{currentDate.ToString("yyyyddMM")}-{repairRequest.Warehouse.Code}-{sequence}";

                _returnRequestService.UpdateReturnRequest(repairRequest);

                _workflowMessageService.SendRepairRequestAuthorizedNotification(repairRequest, _localizationSettings.DefaultAdminLanguageId);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult CancelRepairRequest(int repairRequestId) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var repairRequest = _returnRequestService.GetReturnRequestById(repairRequestId);

            if (repairRequest != null && repairRequest.ReturnRequestStatus != ReturnRequestStatus.Cancelled) {
                repairRequest.ReturnRequestStatus = ReturnRequestStatus.Cancelled;

                _returnRequestService.UpdateReturnRequest(repairRequest);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult CompleteRepairRequest(int repairRequestId) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var repairRequest = _returnRequestService.GetReturnRequestById(repairRequestId);

            if (repairRequest != null && repairRequest.ReturnRequestStatus != ReturnRequestStatus.Complete) {
                repairRequest.ReturnRequestStatus = ReturnRequestStatus.Complete;

                _returnRequestService.UpdateReturnRequest(repairRequest);

                _workflowMessageService.SendRepairRequestCompleteNotification(repairRequest, _localizationSettings.DefaultAdminLanguageId);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpGet]
        public ActionResult GetRepairRequestForm(int? repairRequestId) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            ReturnRequest repairRequest = null;

            if (repairRequestId.HasValue) {
                repairRequest = _returnRequestService.GetReturnRequestById(repairRequestId.Value);
            }

            var model = PrepareRepairRequestModel(repairRequest);

            return PartialView("_CreateOrUpdateRepairRequest", model);
        }
        [HttpPost]
        public ActionResult InboundRepairRequest(int repairRequestId, string trackingNumberInbound) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var repairRequest = _returnRequestService.GetReturnRequestById(repairRequestId);

            if (repairRequest != null && repairRequest.ReturnRequestStatus != ReturnRequestStatus.Cancelled && repairRequest.ReturnRequestStatus != ReturnRequestStatus.Inbound) {
                repairRequest.TrackingNumberInbound = String.IsNullOrWhiteSpace(trackingNumberInbound) ? null : trackingNumberInbound.Trim();
                repairRequest.ReturnRequestStatus = ReturnRequestStatus.Inbound;

                _returnRequestService.UpdateReturnRequest(repairRequest);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult OutboundRepairRequest(int repairRequestId, string trackingNumberOutbound) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var repairRequest = _returnRequestService.GetReturnRequestById(repairRequestId);

            if (repairRequest != null && repairRequest.ReturnRequestStatus != ReturnRequestStatus.Cancelled && repairRequest.ReturnRequestStatus != ReturnRequestStatus.Outbound) {
                repairRequest.TrackingNumberOutbound = String.IsNullOrWhiteSpace(trackingNumberOutbound) ? null : trackingNumberOutbound.Trim();
                repairRequest.ReturnRequestStatus = ReturnRequestStatus.Outbound;

                _returnRequestService.UpdateReturnRequest(repairRequest);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult PrintRepairRequest(int repairRequestId) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            ReturnRequest repairRequest = _returnRequestService.GetReturnRequestById(repairRequestId);

            var model = PrepareRepairRequestModel(repairRequest);

            return View(model);
        }

        [HttpPost]
        public ActionResult ReceiveRepairRequest(int repairRequestId) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var repairRequest = _returnRequestService.GetReturnRequestById(repairRequestId);

            if (repairRequest != null && repairRequest.ReturnRequestStatus == ReturnRequestStatus.Inbound) {

                repairRequest.ReturnRequestStatus = ReturnRequestStatus.Received;

                _returnRequestService.UpdateReturnRequest(repairRequest);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }


        public ActionResult RepairRequest(int? repairRequestId) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            ReturnRequest repairRequest = null;

            if (repairRequestId.HasValue) {
                repairRequest = _returnRequestService.GetReturnRequestById(repairRequestId.Value);
            }

            var model = PrepareRepairRequestModel(repairRequest);
            
            return View(model);
        }

        public ActionResult RepairRequests() {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var model = new RepairRequestsListModel();

            var warehouseCode = GetCustomerSpecificWarehouse();

            var warehouses = _shippingService.GetAllWarehouses();
            model.RepairRequestsSearchModel.Warehouses = warehouses
                .Select(s => new SelectListItem { Selected = s.Code == warehouseCode, Text = s.Name, Value = s.Id.ToString() })
                .ToList();

            model.RepairRequestsSearchModel.Warehouses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "" });

            return View(model);
        }

        [HttpPost]
        public ActionResult RepairRequests(DataSourceRequest command, RepairRequestsListModel model) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var repairRequests = _returnRequestService.SearchReturnRequests(0, model.RepairRequestsSearchModel.AuthorizationNumber, null, model.RepairRequestsSearchModel.CustomerId, 0, model.RepairRequestsSearchModel.Product, null, new List<string>() { "Repair" }, !model.RepairRequestsSearchModel.Statuses.IsNullOrEmpty() ? model.RepairRequestsSearchModel.Statuses.Select(s => int.Parse(s.Value)).ToList() : null, model.RepairRequestsSearchModel.WarehouseId, command.Page - 1, command.PageSize);

            var repairRequestModels = new List<RepairRequestModel>();

            if (!repairRequests.IsNullOrEmpty()) {
                repairRequestModels = repairRequests.Select(s => AutoMapper.Mapper.Map<RepairRequestModel>(s)).ToList();
            }

            var gridModel = new DataSourceResult {
                Data = repairRequestModels,
                Total = repairRequests.TotalCount,
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult SaveRepairRequest(RepairRequestModel model) {

            var repairRequestId = 0;

            var customerName = model.CustomerName.Split(' ');

            if (model.RepairRequestId.HasValue) {
                repairRequestId = model.RepairRequestId.Value;
                var repairRequest = _returnRequestService.GetReturnRequestById(repairRequestId);

                repairRequest.CustomerId = model.CustomerId;
                repairRequest.CustomerCompany = model.CustomerCompany;
                repairRequest.CustomerEmail = model.CustomerEmail;
                repairRequest.CustomerFirstName = customerName[0];
                repairRequest.CustomerLastName = customerName[1];
                repairRequest.CustomerPhoneNumber = model.CustomerPhoneNumber;
                repairRequest.CustomerComments = model.CustomerComments;
                repairRequest.ProductId = model.ProductId;
                repairRequest.ProductDetails = model.ProductName;
                repairRequest.StaffNotes = model.StaffNotes;
                repairRequest.TrackingNumberInbound = model.TrackingNumberInbound;

                if (repairRequest.ReturnRequestStatus != ReturnRequestStatus.Authorized) {
                    repairRequest.WarehouseId = model.SelectedWarehouseId;
                }

                _returnRequestService.UpdateReturnRequest(repairRequest);
            }
            else {
                var rrr = _returnRequestService.GetReturnRequestReasonById(3);
                var rra = _returnRequestService.GetReturnRequestActionById(1);

                var rr = new ReturnRequest {
                    CreatedOnUtc = DateTime.UtcNow,
                    CustomerId = model.CustomerId,
                    CustomerCompany = model.CustomerCompany,
                    CustomerEmail = model.CustomerEmail,
                    CustomerFirstName = customerName[0],
                    CustomerLastName = customerName[1],
                    CustomerPhoneNumber = model.CustomerPhoneNumber,
                    CustomerComments = model.CustomerComments,
                    ProductId = model.ProductId,
                    ProductDetails = model.ProductName,
                    Quantity = 1,
                    ReasonForReturn = rrr != null ? rrr.GetLocalized(x => x.Name) : "not available",
                    RequestedAction = rra != null ? rra.GetLocalized(x => x.Name) : "not available",
                    ReturnRequestStatus = ReturnRequestStatus.Pending,
                    StaffNotes = model.StaffNotes,
                    StoreId = 1,
                    TrackingNumberInbound = model.TrackingNumberInbound,
                    WarehouseId = model.SelectedWarehouseId,
                    UpdatedOnUtc = DateTime.UtcNow
                };

                _returnRequestService.AddReturnRequest(rr);

                var product = new Product() {
                    Id = model.ProductId.HasValue ? model.ProductId.Value : 0,
                    Name = model.ProductName
                };

                repairRequestId = rr.Id;
            }

            Response.StatusCode = repairRequestId > 0 ? 200 : 500;
            return Json(repairRequestId);
        }

        public ActionResult SearchProductsForRepairRequests(string term) {
            if (String.IsNullOrWhiteSpace(term) || term.Length < _catalogSettings.ProductSearchTermMinimumLength)
                return Content("");

            //products
            var productNumber = _catalogSettings.ProductSearchAutoCompleteNumberOfProducts > 0 ?
                _catalogSettings.ProductSearchAutoCompleteNumberOfProducts : 10;

            var products = _productService.SearchProducts(
                keywords: term,
                searchSku: true,
                languageId: _workContext.WorkingLanguage.Id,
                productType: ProductType.SimpleProduct,
                visibleIndividuallyOnly: false,
                pageSize: productNumber);

            var models = new List<ProductModel>();

            foreach (var product in products) {
                var defaultProductPicture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();

                var model = new ProductModel {
                    Id = product.Id,
                    Name = product.GetLocalized(x => x.Name),
                    ShortDescription = product.GetLocalized(x => x.ShortDescription),
                    FullDescription = product.GetLocalized(x => x.FullDescription),
                    PictureThumbnailUrl = _pictureService.GetPictureUrl(defaultProductPicture, 75, true),
                    Sku = product.Sku
                };

                models.Add(model);
            }

            var result = (from p in models
                          select new {
                              label = p.Name,
                              productid = p.Id,
                              //producturl = Url.RouteUrl("Product", new { SeName = p.SeName }),
                              productpictureurl = p.PictureThumbnailUrl,
                              sku = p.Sku
                          })
                          .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        protected virtual RepairRequestModel PrepareRepairRequestModel(ReturnRequest repairRequest) {
            var model = new RepairRequestModel();
            
            var warehouses = _shippingService.GetAllWarehouses();
            var warehouseCode = GetCustomerSpecificWarehouse();
            
            if (repairRequest != null) {
                model = AutoMapper.Mapper.Map<RepairRequestModel>(repairRequest);           

                if (repairRequest.Warehouse != null) {
                    warehouseCode = repairRequest.Warehouse.Code;
                }

                if (repairRequest.Product != null) {
                    var defaultProductPicture = _pictureService.GetPicturesByProductId(repairRequest.Product.Id, 1).FirstOrDefault();
                    model.ProductPictureUrl = _pictureService.GetPictureUrl(defaultProductPicture, 75, true);
                    model.ProductSku = repairRequest.Product.Sku;
                }
            } 

            model.Warehouses = warehouses
                .OrderBy(ob => ob.Code)
                .Select(s => new SelectListItem() {
                    Selected = !String.IsNullOrEmpty(warehouseCode) && warehouseCode == s.Code,
                    Text = $"{s.Name} (#{s.Code})",
                    Value = s.Id.ToString()
                })
                .ToList();

            return model;
        }


        #endregion

        #region Methods


        //delete
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var returnRequest = _returnRequestService.GetReturnRequestById(id);
            if (returnRequest == null)
                //No return request found with the specified id
                return RedirectToAction("List");

            _returnRequestService.DeleteReturnRequest(returnRequest);

            //activity log
            _customerActivityService.InsertActivity("DeleteReturnRequest", _localizationService.GetResource("ActivityLog.DeleteReturnRequest"), returnRequest.Id);

            SuccessNotification(_localizationService.GetResource("Admin.ReturnRequests.Deleted"));
            return RedirectToAction("List");
        }

        //edit
        public ActionResult Edit(int id) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var returnRequest = _returnRequestService.GetReturnRequestById(id);
            if (returnRequest == null)
                //No return request found with the specified id
                return RedirectToAction("List");

            var model = new ReturnRequestModel();
            PrepareReturnRequestModel(model, returnRequest, false);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public ActionResult Edit(ReturnRequestModel model, bool continueEditing) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var returnRequest = _returnRequestService.GetReturnRequestById(model.Id);
            if (returnRequest == null)
                //No return request found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid) {
                returnRequest.Quantity = model.Quantity;
                returnRequest.ReasonForReturn = model.ReasonForReturn;
                returnRequest.RequestedAction = model.RequestedAction;
                returnRequest.CustomerComments = model.CustomerComments;
                returnRequest.StaffNotes = model.StaffNotes;
                returnRequest.ReturnRequestStatusId = model.ReturnRequestStatusId;
                returnRequest.UpdatedOnUtc = DateTime.UtcNow;
                _customerService.UpdateCustomer(returnRequest.Customer);

                //activity log
                _customerActivityService.InsertActivity("EditReturnRequest", _localizationService.GetResource("ActivityLog.EditReturnRequest"), returnRequest.Id);

                SuccessNotification(_localizationService.GetResource("Admin.ReturnRequests.Updated"));
                return continueEditing ? RedirectToAction("Edit", new { id = returnRequest.Id }) : RedirectToAction("List");
            }


            //If we got this far, something failed, redisplay form
            PrepareReturnRequestModel(model, returnRequest, true);
            return View(model);
        }

        //list
        public ActionResult Index() {
            return RedirectToAction("List");
        }

        public ActionResult List() {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var returnRequests = _returnRequestService.SearchReturnRequests(0, null, null, 0, 0, null, null, null, null, null, command.Page - 1, command.PageSize);
            var returnRequestModels = new List<ReturnRequestModel>();
            foreach (var rr in returnRequests) {
                var m = new ReturnRequestModel();
                if (PrepareReturnRequestModel(m, rr, false))
                    returnRequestModels.Add(m);
            }
            var gridModel = new DataSourceResult {
                Data = returnRequestModels,
                Total = returnRequests.TotalCount,
            };

            return Json(gridModel);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("notify-customer")]
        public ActionResult NotifyCustomer(ReturnRequestModel model) {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var returnRequest = _returnRequestService.GetReturnRequestById(model.Id);
            if (returnRequest == null)
                //No return request found with the specified id
                return RedirectToAction("List");

            //var customer = returnRequest.Customer;
            var orderItem = _orderService.GetOrderItemById(returnRequest.OrderItemId.Value);
            int queuedEmailId = _workflowMessageService.SendReturnRequestStatusChangedCustomerNotification(returnRequest, orderItem, _localizationSettings.DefaultAdminLanguageId);
            if (queuedEmailId > 0)
                SuccessNotification(_localizationService.GetResource("Admin.ReturnRequests.Notified"));
            return RedirectToAction("Edit", new { id = returnRequest.Id });
        }
        #endregion

        private string GetCustomerSpecificWarehouse() {
            string WarehouseCode = null;

            string CustomAttributesXML = _workContext.CurrentCustomer.GetAttribute<string>(Core.Domain.Customers.SystemCustomerAttributeNames.CustomCustomerAttributes);

            List<Core.Domain.Customers.CustomerAttribute> CustomerAttributes = _customerAttributeParser.ParseCustomerAttributes(CustomAttributesXML).ToList();

            Core.Domain.Customers.CustomerAttribute WarehouseCodeAttribute = CustomerAttributes.FirstOrDefault(f => f.Name == "Warehouse Code");

            if (WarehouseCodeAttribute != null) {
                List<string> CustomerAttributeValues = _customerAttributeParser.ParseValues(CustomAttributesXML, WarehouseCodeAttribute.Id).ToList();

                if (CustomerAttributeValues != null && CustomerAttributeValues.Count > 0) {
                    WarehouseCode = CustomerAttributeValues.FirstOrDefault();
                }
            }

            return WarehouseCode;
        }
    }
}
