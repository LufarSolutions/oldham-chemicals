﻿using Nop.Admin.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Nop.Admin.Services
{
    public static class ZipCodeApiService
    {
        private static string Url = "https://www.zipcodeapi.com/rest/qot5j7lSfibAc6Ff8k33eLD4RJoqbh1N8SKkBqzrshr7xqGSEkp5PyEwMJP3ATBW/radius.json/{0}/{1}/mile";
        private static string UrlParams = "?minimal";

        public static List<string> GetZipCodesInRadiusOfZipCode(int radius, string zipCode)
        {
            List<string> ZipCodes = new List<string>();

            try
            {

                using (var client = new HttpClient())
                {
                    string RequestUrl = String.Format(Url, zipCode, radius);

                    client.BaseAddress = new Uri(RequestUrl);

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = client.GetAsync(UrlParams).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var Result = response.Content.ReadAsAsync<ZipCodeApiResponse>().Result;
                        ZipCodes = Result.Zip_Codes;
                    }
                }
            }
            catch
            {
            }

            return ZipCodes;
        }
    }
}