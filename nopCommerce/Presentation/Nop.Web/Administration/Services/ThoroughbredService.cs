﻿using Nop.Admin.Models.Orders;
using OldhamChem.Thoroughbred.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Services
{
    public class ThoroughbredService
    {
        private const string KEY = "ThoroughbredServiceSingleton";

        public Repository ThoroughbredRepository { get; private set; }

        public static ThoroughbredService Instance
        {
            get
            {
                if (HttpContext.Current.Session[KEY] == null)
                    HttpContext.Current.Session[KEY] = new ThoroughbredService();

                return HttpContext.Current.Session[KEY] as ThoroughbredService;

            }
        }

        private ThoroughbredService()
        {
            ThoroughbredRepository = new Repository();
            ThoroughbredRepository.Initialize();
        }
    }


}