﻿var OldhamChem = (function () {

    return {

        HideProgressBar: function() {

            $("#ModalProgressBar").modal("hide");

            return true;
        },

        ShowProgressBar: function () {

            $("#ModalProgressBar").modal("show");

            return true;

        }

    };

})();