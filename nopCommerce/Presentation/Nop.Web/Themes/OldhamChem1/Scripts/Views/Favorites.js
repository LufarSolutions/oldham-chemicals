﻿var favorites = (function () {
    
    return {

        properties: {

            ButtonAddProductToFavoritesClassName: "ButtonAddProductToFavorites",
            ButtonRemoveProductFromFavoritesClassName: "ButtonRemoveProductFromFavorites",
            FavoriteIconOnStateClassName:  "fa fa-heart ButtonRemoveProductFromFavorites",
            FavoriteIconOffStateClassName: "fa fa-heart-o ButtonAddProductToFavorites",

        },

        addFavorite: function (sku) {

            OldhamChem.ShowProgressBar();

            $.ajax({
                url: "/Catalog/AddFavorite",
                data: {
                    sku: sku
                },
                type: "GET",
                error: function () {

                    OldhamChem.HideProgressBar();

                    //ux.showAlert("An error occurred when loading the contact.", "danger");

                },
                success: function (success) {

                    var FavoriteIcon = $("a[data-sku='" + sku + "']");
                    
                    if (FavoriteIcon !== undefined) {
                        FavoriteIcon.attr("class", favorites.properties.FavoriteIconOnStateClassName);
                        favorites.bindFavoriteIconActions();
                    }

                    OldhamChem.HideProgressBar();

                },
            });

        },

        bindFavoriteIconActions: function () {

            $("." + favorites.properties.ButtonAddProductToFavoritesClassName).unbind("click");
            $("." + favorites.properties.ButtonAddProductToFavoritesClassName).click(function (e) {

                var sku = $(this).data("sku");

                favorites.addFavorite(sku);

                e.preventDefault();

            });

            $("." + favorites.properties.ButtonRemoveProductFromFavoritesClassName).unbind("click");
            $("." + favorites.properties.ButtonRemoveProductFromFavoritesClassName).click(function (e) {

                var sku = $(this).data("sku");

                favorites.removeFavorite(sku);

                e.preventDefault();

            });

        },

        initializeView: function () {

            favorites.bindFavoriteIconActions();

        },

        removeFavorite: function (sku) {

            OldhamChem.ShowProgressBar();
            
            $.ajax({
                url: "/Catalog/RemoveFavorite",
                data: {
                    sku: sku
                },
                type: "GET",
                error: function () {

                    OldhamChem.HideProgressBar();
                },
                success: function (success) {

                    var ProductContainer = $("div[data-productsku='" + sku + "']");

                    if (ProductContainer) {
                        ProductContainer.remove();
                    }

                    var FavoriteIcon = $("a[data-sku='" + sku + "']");

                    if (FavoriteIcon) {
                        FavoriteIcon.attr("class", favorites.properties.FavoriteIconOffStateClassName);
                        favorites.bindFavoriteIconActions();
                    }

                    OldhamChem.HideProgressBar();

                },
            });
    
        },

    };

})();