﻿var ContactUs = (function () {

    var PhoneNumberTextBoxId = "PhoneNumber";
    var PhoneNumberMask = "(000) 000-0000";
    var PhoneNumberMaskValue = "(___) ___-____";

    return {
        
        BindPhoneNumberMask: function () {

            $("#" + PhoneNumberTextBoxId).kendoMaskedTextBox({
                mask: PhoneNumberMask,
                unmaskOnPost: true,
                value: ""
            });

        },

        BindPhoneNumberCaretControl: function () {

            $("#" + PhoneNumberTextBoxId).focus(function () {

                ContactUs.SetPhoneNumberCaret();

            });
        },

        SetPhoneNumberCaret: function () {

            if ($("#" + PhoneNumberTextBoxId).val() === PhoneNumberMaskValue) {

                Helpers.SetCaretPosition(PhoneNumberTextBoxId, 1);
            }

        }

    };

})();