﻿//import AddBuyerModalController from '/Content/scripts/views/buyers/AddBuyerModalController.js';

export default class RepairRequestsController {
    constructor() {
        $(document).ready(() => {
            this.initialize();
        });
    }

    openRepairRequest(requestId) {
        $.ajax({
            url: '/ReturnRequest/GetRepairRequestModal',
            data: {
                requestId: requestId
                },
            type: 'GET',
            error: () => {
                displayBarNotification('An unexpected error occurred when retrieving the repair request.', 'error', 3500);
            },
            success: (modal) => {
                //insert modal
                $('body').append(modal);

                $('#ButtonCloseRepairRequestModal').click((e) => {
                    $('#ModalRepairRequest').modal('hide');

                    setTimeout(() => {
                        $('#ModalRepairRequest').remove();
                    }, 500);
                });

                //open modal
                $('#ModalRepairRequest').modal('show');
            },
        });
    }

    initialize() {
        $('.ButtonViewRequest').click((e) => {
            const requestId = $(e.currentTarget).data('request-id');
            this.openRepairRequest(requestId);
        });
    }
}