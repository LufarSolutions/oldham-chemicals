﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Web.Models.Order
{
    public class OrderHistoryModel
    {
        public decimal FreightCharge { get; set; }

        public string InvoiceNumber { get; set; }

        public decimal InvoiceAmount { get; set; }

        public string OrderNumber { get; set; }

        public DateTime DateOfInvoice { get; set; }

        public List<OrderHistoryItemModel> OrderHistoryItems { get; set; }

        public decimal SalesTax { get; set; }

    }
}
