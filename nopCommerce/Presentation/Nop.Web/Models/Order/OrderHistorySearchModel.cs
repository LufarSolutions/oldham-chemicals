﻿using Nop.Core.Domain.Orders;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Order
{
    public class OrderHistorySearchModel
    {
        public OrderHistorySearchFormModel OrderHistorySearchFormModel { get; set; }

        public List<Invoice> Invoices { get; set; }

        public SimplePagerModel Pagination { get; set; }

    }
}