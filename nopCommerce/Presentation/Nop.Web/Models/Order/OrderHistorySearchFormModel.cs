﻿using Nop.Core.Domain.Common;
using Nop.Web.Framework.UI.Paging;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Order
{
    public class OrderHistorySearchFormModel
    {
        public DateRange InvoiceDateRange { get; set; }

        public string InvoiceNumber { get; set; }

        public string OrderNumber { get; set; }

        public string ProductNumber { get; set; }

        public SimplePagerModel Pagination { get; set;  }

        public OrderHistorySearchFormModel()
        {
            this.Pagination = new SimplePagerModel()
            {
                PageNumber = 1,
                PageSize = 5
            };
        }
    }
}