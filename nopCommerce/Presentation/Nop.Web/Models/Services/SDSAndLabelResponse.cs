﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Services
{
    public class SDSAndLabelResponse
    {
        public string id { get; set; }
        public string api_version { get; set; }
        public bool merged { get; set; }
        public bool cover { get; set; }
        public string language { get; set; }
        public Product_Sds[] product_sds { get; set; }
    }

    public class Product_Sds
    {
        public string effective_date { get; set; }
        public string[] pdf_data { get; set; }
    }
}


//public class Rootobject
//{
//    public string id { get; set; }
//    public string api_version { get; set; }
//    public bool merged { get; set; }
//    public bool cover { get; set; }
//    public string language { get; set; }
//    public Product_Sds[] product_sds { get; set; }
//}

//public class Product_Sds
//{
//    public string effective_date { get; set; }
//    public object pdf_data { get; set; }
//}
