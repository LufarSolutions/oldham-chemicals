﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Customer;
using System;
using System.ComponentModel.DataAnnotations;
using static Nop.Web.Models.Order.CustomerReturnRequestsModel;

namespace Nop.Web.Models.ReturnRequest
{
    public class SubmitRepairRequestModel : BaseNopModel
    {
        public CustomerInfoModel CustomerInfo { get; set; } = new CustomerInfoModel();

        //public Guid? ProductId { get; set; }

        ////[Required]
        //[NopResourceDisplayName("returnrequests.repairrequests.productname")]
        //public string ProductName { get; set; }

        ////[Required]
        //[NopResourceDisplayName("returnrequests.repairrequests.problemdescription")]
        //public string ProblemDescription { get; set; }

        public ReturnRequestModel RepairRequest { get; set; } = new ReturnRequestModel();
    }
}