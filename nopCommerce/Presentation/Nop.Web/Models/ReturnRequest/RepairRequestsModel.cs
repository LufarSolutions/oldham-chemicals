﻿using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Customer;
using System.Collections.Generic;
using static Nop.Web.Models.Order.CustomerReturnRequestsModel;

namespace Nop.Web.Models.ReturnRequest
{
    public class RepairRequestsModel : BaseNopModel {

        public CustomerInfoModel CustomerInfo { get; set; } = new CustomerInfoModel();

        public ReturnRequestModel NewRepairRequest { get; set; } = new ReturnRequestModel();

        public List<ReturnRequestModel> RepairRequests { get; set; } = new List<ReturnRequestModel>();
    }
}