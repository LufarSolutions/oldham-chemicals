﻿using Nop.Web.Framework.UI.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Common
{
    public class SimplePagerModel : BasePageableModel
    {
        public int IndividualPagesToShow { get; set; }

        public SimplePagerModel()
        {
            this.IndividualPagesToShow = 5;
            this.PageNumber = 1;
            this.PageSize = 5;
        }

        public int GetFirstIndividualPageIndex()
        {
            if ((this.TotalPages < IndividualPagesToShow) ||
                ((this.PageIndex - (IndividualPagesToShow / 2)) < 0))
            {
                return 0;
            }
            if ((this.PageIndex + (IndividualPagesToShow / 2)) >= this.TotalPages)
            {
                return (this.TotalPages - IndividualPagesToShow);
            }
            return (this.PageIndex - (IndividualPagesToShow / 2));
        }
        public int GetLastIndividualPageIndex()
        {
            int num = IndividualPagesToShow / 2;
            if ((IndividualPagesToShow % 2) == 0)
            {
                num--;
            }
            if ((this.TotalPages < IndividualPagesToShow) ||
                ((this.PageIndex + num) >= this.TotalPages))
            {
                return (this.TotalPages - 1);
            }
            if ((this.PageIndex - (IndividualPagesToShow / 2)) < 0)
            {
                return (IndividualPagesToShow - 1);
            }
            return (this.PageIndex + num);
        }
    }
}