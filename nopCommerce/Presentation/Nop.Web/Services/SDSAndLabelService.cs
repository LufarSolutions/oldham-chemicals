﻿using Nop.Web.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Nop.Web.Services
{
    public class SDSAndLabelService
    {
        private const string API_KEY = "BWSSVUXUY8VJURX6XHD825LB1KL2SAU2";
        private const string API_URL = "http://sds-api.gsmsds.com/sds/{0}/{1}";

        public SDSAndLabelService()
        {

        }

        public string GetSDSDocument(string sku)
        {
            DateTime CurrentDate = DateTime.UtcNow;
            string SDSParameters = "?merged=false&version_date=" + CurrentDate.ToString("yyyyMMdd") + "&scan_type={0}";

            SDSAndLabelResponse Response = null;

            // Scan for GHS first
            Response = GetDocumentFromAPI(sku, String.Format(SDSParameters, "20"));

            string PdfData = null;

            if (Response == null)
            {
                // Scan for North American SDS
                Response = GetDocumentFromAPI(sku, String.Format(SDSParameters, "28"));
            }

            if (Response == null)
            {
                // Scan for Exception Letter
                Response = GetDocumentFromAPI(sku, String.Format(SDSParameters, "111"));
            }

            if (Response != null && Response.product_sds.Any())
            {
                PdfData = Response.product_sds.First().pdf_data.FirstOrDefault();
            }

            return PdfData;
        }

        public string GetLabelDocument(string sku)
        {
            string LabelParameters = "?language=en&merged=false&cover=false&scan_type={0}";

            SDSAndLabelResponse Response = null;

            // Scan for Lable first
            Response = GetDocumentFromAPI(sku, String.Format(LabelParameters, "110"));

            string PdfData = null;

            if (Response == null)
            {
                // Scan for Specimen Label
                Response = GetDocumentFromAPI(sku, String.Format(LabelParameters, "115"));
            }

            if (Response == null)
            {
                // Scan for Specimen Label
                Response = GetDocumentFromAPI(sku, String.Format(LabelParameters, "114"));
            }

            if (Response != null && Response.product_sds.Any())
            {
                PdfData = Response.product_sds.First().pdf_data.FirstOrDefault();
            }

            return PdfData;
        }

        private static SDSAndLabelResponse GetDocumentFromAPI(string sku, string parameters)
        {
            SDSAndLabelResponse SDSAndLabelResponse = null;

            using (var client = new HttpClient())
            {
                string RequestUrl = String.Format(API_URL, API_KEY, sku);

                client.BaseAddress = new Uri(RequestUrl);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));                
                
                HttpResponseMessage Response = client.GetAsync(parameters).Result;
                Response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                if (Response.IsSuccessStatusCode)
                {
                    SDSAndLabelResponse = Response.Content.ReadAsAsync<SDSAndLabelResponse>().Result;
                }
            }
            //}
            //catch
            //{
            //}
            return SDSAndLabelResponse;
        }
    }
}