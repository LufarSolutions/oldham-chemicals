﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Web.Extensions;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Order;
using Nop.Web.Models.Catalog;
using Nop.Services.ExportImport;
using Nop.Web.Framework;

namespace Nop.Web.Controllers
{
    public partial class OrderController : BasePublicController
    {
        #region Fields


        private readonly IOrderService _orderService;
        private readonly IShipmentService _shipmentService;
        private readonly IWorkContext _workContext;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPaymentService _paymentService;
        private readonly ILocalizationService _localizationService;
        private readonly IPdfService _pdfService;
        private readonly IShippingService _shippingService;
        private readonly ICountryService _countryService;
        private readonly IProductAttributeParser _productAttributeParser;
        //private readonly IWebHelper _webHelper;
        private readonly IDownloadService _downloadService;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly IStoreContext _storeContext;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IRewardPointService _rewardPointService;
        private readonly IProductService _productService;

        private readonly OrderSettings _orderSettings;
        private readonly TaxSettings _taxSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly AddressSettings _addressSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly PdfSettings _pdfSettings;

        private readonly IExportManager _exportManager;

        #endregion

        #region Constructors

        public OrderController(IOrderService orderService, 
            IShipmentService shipmentService, 
            IWorkContext workContext,
            ICurrencyService currencyService,
            IPriceFormatter priceFormatter,
            IOrderProcessingService orderProcessingService, 
            IDateTimeHelper dateTimeHelper,
            IPaymentService paymentService, 
            ILocalizationService localizationService,
            IPdfService pdfService, 
            IShippingService shippingService,
            ICountryService countryService, 
            IProductAttributeParser productAttributeParser,
            IWebHelper webHelper,
            IDownloadService downloadService,
            IProductService productService,
            IAddressAttributeFormatter addressAttributeFormatter,
            IStoreContext storeContext,
            IOrderTotalCalculationService orderTotalCalculationService,
            IRewardPointService rewardPointService,
            CatalogSettings catalogSettings,
            OrderSettings orderSettings,
            TaxSettings taxSettings,
            ShippingSettings shippingSettings, 
            AddressSettings addressSettings,
            RewardPointsSettings rewardPointsSettings,
            PdfSettings pdfSettings,
            IExportManager exportManager)
        {
            this._orderService = orderService;
            this._shipmentService = shipmentService;
            this._workContext = workContext;
            this._currencyService = currencyService;
            this._priceFormatter = priceFormatter;
            this._orderProcessingService = orderProcessingService;
            this._dateTimeHelper = dateTimeHelper;
            this._paymentService = paymentService;
            this._localizationService = localizationService;
            this._pdfService = pdfService;
            this._shippingService = shippingService;
            this._countryService = countryService;
            this._productAttributeParser = productAttributeParser;
            this._webHelper = webHelper;
            this._downloadService = downloadService;
            this._addressAttributeFormatter = addressAttributeFormatter;
            this._storeContext = storeContext;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._rewardPointService = rewardPointService;
            this._productService = productService;

            this._catalogSettings = catalogSettings;
            this._orderSettings = orderSettings;
            this._taxSettings = taxSettings;
            this._shippingSettings = shippingSettings;
            this._addressSettings = addressSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._pdfSettings = pdfSettings;

            this._exportManager = exportManager;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual CustomerOrderListModel PrepareCustomerOrderListModel()
        {
            var model = new CustomerOrderListModel();
            var orders = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                customerId: _workContext.CurrentCustomer.Id);
            foreach (var order in orders)
            {
                var orderModel = new CustomerOrderListModel.OrderDetailsModel
                {
                    Id = order.Id,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
                    OrderStatusEnum = order.OrderStatus,
                    OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                    PaymentStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext),
                    ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext),
                    IsReturnRequestAllowed = _orderProcessingService.IsReturnRequestAllowed(order)
                };
                var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
                orderModel.OrderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

                model.Orders.Add(orderModel);
            }

            var recurringPayments = _orderService.SearchRecurringPayments(_storeContext.CurrentStore.Id,
                _workContext.CurrentCustomer.Id);
            foreach (var recurringPayment in recurringPayments)
            {
                var recurringPaymentModel = new CustomerOrderListModel.RecurringOrderModel
                {
                    Id = recurringPayment.Id,
                    StartDate = _dateTimeHelper.ConvertToUserTime(recurringPayment.StartDateUtc, DateTimeKind.Utc).ToString(),
                    CycleInfo = string.Format("{0} {1}", recurringPayment.CycleLength, recurringPayment.CyclePeriod.GetLocalizedEnum(_localizationService, _workContext)),
                    NextPayment = recurringPayment.NextPaymentDate.HasValue ? _dateTimeHelper.ConvertToUserTime(recurringPayment.NextPaymentDate.Value, DateTimeKind.Utc).ToString() : "",
                    TotalCycles = recurringPayment.TotalCycles,
                    CyclesRemaining = recurringPayment.CyclesRemaining,
                    InitialOrderId = recurringPayment.InitialOrder.Id,
                    CanCancel = _orderProcessingService.CanCancelRecurringPayment(_workContext.CurrentCustomer, recurringPayment),
                };

                model.RecurringOrders.Add(recurringPaymentModel);
            }

            return model;
        }

        [NonAction]
        protected virtual OrderDetailsModel PrepareOrderDetailsModel(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            var model = new OrderDetailsModel();

            model.Id = order.Id;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc);
            model.OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.IsReOrderAllowed = _orderSettings.IsReOrderAllowed;
            model.IsReturnRequestAllowed = _orderProcessingService.IsReturnRequestAllowed(order);
            model.PdfInvoiceDisabled = _pdfSettings.DisablePdfInvoicesForPendingOrders && order.OrderStatus == OrderStatus.Pending;

            //shipping info
            model.ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext);
            if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
            {
                model.IsShippable = true;
                model.PickUpInStore = order.PickUpInStore;
                if (!order.PickUpInStore)
                {
                    model.ShippingAddress.PrepareModel(
                        address: order.ShippingAddress,
                        excludeProperties: false,
                        addressSettings: _addressSettings,
                        addressAttributeFormatter: _addressAttributeFormatter);
                }
                model.ShippingMethod = order.ShippingMethod;
   

                //shipments (only already shipped)
                var shipments = order.Shipments.Where(x => x.ShippedDateUtc.HasValue).OrderBy(x => x.CreatedOnUtc).ToList();
                foreach (var shipment in shipments)
                {
                    var shipmentModel = new OrderDetailsModel.ShipmentBriefModel
                    {
                        Id = shipment.Id,
                        TrackingNumber = shipment.TrackingNumber,
                    };
                    if (shipment.ShippedDateUtc.HasValue)
                        shipmentModel.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
                    if (shipment.DeliveryDateUtc.HasValue)
                        shipmentModel.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);
                    model.Shipments.Add(shipmentModel);
                }
            }


            //billing info
            model.BillingAddress.PrepareModel(
                address: order.BillingAddress,
                excludeProperties: false,
                addressSettings: _addressSettings,
                addressAttributeFormatter: _addressAttributeFormatter);

            //VAT number
            model.VatNumber = order.VatNumber;

            //payment method
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
            model.PaymentMethod = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id) : order.PaymentMethodSystemName;
            model.PaymentMethodStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.CanRePostProcessPayment = _paymentService.CanRePostProcessPayment(order);
            //custom values
            model.CustomValues = order.DeserializeCustomValues();

            //order subtotal
            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
            {
                //including tax

                //order subtotal
                var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //discount (applied to order subtotal)
                var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
                if (orderSubTotalDiscountInclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order subtotal
                var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //discount (applied to order subtotal)
                var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
                if (orderSubTotalDiscountExclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                //including tax

                //order shipping
                var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //payment method additional fee
                var paymentMethodAdditionalFeeInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeInclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeInclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order shipping
                var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //payment method additional fee
                var paymentMethodAdditionalFeeExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeExclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeExclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            //tax
            bool displayTax = true;
            bool displayTaxRates = true;
            if (_taxSettings.HideTaxInOrderSummary && order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                displayTax = false;
                displayTaxRates = false;
            }
            else
            {
                if (order.OrderTax == 0 && _taxSettings.HideZeroTax)
                {
                    displayTax = false;
                    displayTaxRates = false;
                }
                else
                {
                    displayTaxRates = _taxSettings.DisplayTaxRates && order.TaxRatesDictionary.Count > 0;
                    displayTax = !displayTaxRates;

                    var orderTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTax, order.CurrencyRate);
                    //TODO pass languageId to _priceFormatter.FormatPrice
                    model.Tax = _priceFormatter.FormatPrice(orderTaxInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

                    foreach (var tr in order.TaxRatesDictionary)
                    {
                        model.TaxRates.Add(new OrderDetailsModel.TaxRate
                        {
                            Rate = _priceFormatter.FormatTaxRate(tr.Key),
                            //TODO pass languageId to _priceFormatter.FormatPrice
                            Value = _priceFormatter.FormatPrice(_currencyService.ConvertCurrency(tr.Value, order.CurrencyRate), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                        });
                    }
                }
            }
            model.DisplayTaxRates = displayTaxRates;
            model.DisplayTax = displayTax;
            model.DisplayTaxShippingInfo = _catalogSettings.DisplayTaxShippingInfoOrderDetailsPage;
            model.PricesIncludeTax = order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax;

            //discount (applied to order total)
            var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
            if (orderDiscountInCustomerCurrency > decimal.Zero)
                model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderDiscountInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);


            //gift cards
            foreach (var gcuh in order.GiftCardUsageHistory)
            {
                model.GiftCards.Add(new OrderDetailsModel.GiftCard
                {
                    CouponCode = gcuh.GiftCard.GiftCardCouponCode,
                    Amount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(gcuh.UsedValue, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                });
            }

            //reward points           
            if (order.RedeemedRewardPointsEntry != null)
            {
                model.RedeemedRewardPoints = -order.RedeemedRewardPointsEntry.Points;
                model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(order.RedeemedRewardPointsEntry.UsedAmount, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);
            }

            //total
            var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
            model.OrderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

            //checkout attributes
            model.CheckoutAttributeInfo = order.CheckoutAttributeDescription;

            //order notes
            foreach (var orderNote in order.OrderNotes
                .Where(on => on.DisplayToCustomer)
                .OrderByDescending(on => on.CreatedOnUtc)
                .ToList())
            {
                model.OrderNotes.Add(new OrderDetailsModel.OrderNote
                {
                    Id = orderNote.Id,
                    HasDownload = orderNote.DownloadId > 0,
                    Note = orderNote.FormatOrderNoteText(),
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc)
                });
            }


            //purchased products
            model.ShowSku = _catalogSettings.ShowProductSku;
            var orderItems = _orderService.GetAllOrderItems(order.Id, null, null, null, null, null, null);
            foreach (var orderItem in orderItems)
            {
                var orderItemModel = new OrderDetailsModel.OrderItemModel
                {
                    Id = orderItem.Id,
                    OrderItemGuid = orderItem.OrderItemGuid,
                    Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                    ProductId = orderItem.Product.Id,
                    ProductName = orderItem.Product.GetLocalized(x => x.Name),
                    ProductSeName = orderItem.Product.GetSeName(),
                    Quantity = orderItem.Quantity,
                    AttributeInfo = orderItem.AttributeDescription,
                };
                //rental info
                if (orderItem.Product.IsRental)
                {
                    var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
                    orderItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }
                model.Items.Add(orderItemModel);

                //unit price, subtotal
                if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    //including tax
                    var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);

                    var priceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceInclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                }
                else
                {
                    //excluding tax
                    var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);

                    var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                }

                //downloadable products
                if (_downloadService.IsDownloadAllowed(orderItem))
                    orderItemModel.DownloadId = orderItem.Product.DownloadId;
                if (_downloadService.IsLicenseDownloadAllowed(orderItem))
                    orderItemModel.LicenseId = orderItem.LicenseDownloadId.HasValue ? orderItem.LicenseDownloadId.Value : 0;
            }

            return model;
        }

        [NonAction]
        protected virtual ShipmentDetailsModel PrepareShipmentDetailsModel(Shipment shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            var order = shipment.Order;
            if (order == null)
                throw new Exception("order cannot be loaded");
            var model = new ShipmentDetailsModel();
            
            model.Id = shipment.Id;
            if (shipment.ShippedDateUtc.HasValue)
                model.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
            if (shipment.DeliveryDateUtc.HasValue)
                model.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);
            
            //tracking number and shipment information
            if (!String.IsNullOrEmpty(shipment.TrackingNumber))
            {
                model.TrackingNumber = shipment.TrackingNumber;
                var srcm = _shippingService.LoadShippingRateComputationMethodBySystemName(order.ShippingRateComputationMethodSystemName);
                if (srcm != null &&
                    srcm.PluginDescriptor.Installed &&
                    srcm.IsShippingRateComputationMethodActive(_shippingSettings))
                {
                    var shipmentTracker = srcm.ShipmentTracker;
                    if (shipmentTracker != null)
                    {
                        model.TrackingNumberUrl = shipmentTracker.GetUrl(shipment.TrackingNumber);
                        if (_shippingSettings.DisplayShipmentEventsToCustomers)
                        {
                            var shipmentEvents = shipmentTracker.GetShipmentEvents(shipment.TrackingNumber);
                            if (shipmentEvents != null)
                            {
                                foreach (var shipmentEvent in shipmentEvents)
                                {
                                    var shipmentStatusEventModel = new ShipmentDetailsModel.ShipmentStatusEventModel();
                                    var shipmentEventCountry = _countryService.GetCountryByTwoLetterIsoCode(shipmentEvent.CountryCode);
                                    shipmentStatusEventModel.Country = shipmentEventCountry != null
                                                                           ? shipmentEventCountry.GetLocalized(x => x.Name)
                                                                           : shipmentEvent.CountryCode;
                                    shipmentStatusEventModel.Date = shipmentEvent.Date;
                                    shipmentStatusEventModel.EventName = shipmentEvent.EventName;
                                    shipmentStatusEventModel.Location = shipmentEvent.Location;
                                    model.ShipmentStatusEvents.Add(shipmentStatusEventModel);
                                }
                            }
                        }
                    }
                }
            }

            //products in this shipment
            model.ShowSku = _catalogSettings.ShowProductSku;
            foreach (var shipmentItem in shipment.ShipmentItems)
            {
                var orderItem = _orderService.GetOrderItemById(shipmentItem.OrderItemId);
                if (orderItem == null)
                    continue;

                var shipmentItemModel = new ShipmentDetailsModel.ShipmentItemModel
                {
                    Id = shipmentItem.Id,
                    Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                    ProductId = orderItem.Product.Id,
                    ProductName = orderItem.Product.GetLocalized(x => x.Name),
                    ProductSeName = orderItem.Product.GetSeName(),
                    AttributeInfo = orderItem.AttributeDescription,
                    QuantityOrdered = orderItem.Quantity,
                    QuantityShipped = shipmentItem.Quantity,
                };
                //rental info
                if (orderItem.Product.IsRental)
                {
                    var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
                    shipmentItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }
                model.Items.Add(shipmentItemModel);
            }

            //order details model
            model.Order = PrepareOrderDetailsModel(order);
            
            return model;
        }

        //[NonAction]
        //protected virtual ProductOverviewModel PrepareProductOverModels(IEnumerable<Product> products,
        //    bool preparePriceModel = true, bool preparePictureModel = true,
        //    int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
        //    bool forceRedirectionAfterAddingToCart = false)
        //{
        //    return this.PrepareProductOverviewModels(_workContext,
        //        _storeContext, _categoryService, _customerService, _productService, _specificationAttributeService,
        //        _priceCalculationService, _priceFormatter, _permissionService,
        //        _localizationService, _taxService, _currencyService,
        //        _pictureService, _webHelper, _cacheManager,
        //        _catalogSettings, _mediaSettings, products,
        //        preparePriceModel, preparePictureModel,
        //        productThumbPictureSize, prepareSpecificationAttributes,
        //        forceRedirectionAfterAddingToCart);
        //}

    

        #endregion

        #region Methods

        //My account / Orders
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult CustomerOrders()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            var model = PrepareCustomerOrderListModel();

            return View(model);
        }

        //My account / Orders / Cancel recurring order
        [HttpPost, ActionName("CustomerOrders")]
        [PublicAntiForgery]
        [FormValueRequired(FormValueRequirement.StartsWith, "cancelRecurringPayment")]
        public ActionResult CancelRecurringPayment(FormCollection form)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            //get recurring payment identifier
            int recurringPaymentId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("cancelRecurringPayment", StringComparison.InvariantCultureIgnoreCase))
                    recurringPaymentId = Convert.ToInt32(formValue.Substring("cancelRecurringPayment".Length));

            var recurringPayment = _orderService.GetRecurringPaymentById(recurringPaymentId);
            if (recurringPayment == null)
            {
                return RedirectToRoute("CustomerOrders");
            }

            if (_orderProcessingService.CanCancelRecurringPayment(_workContext.CurrentCustomer, recurringPayment))
            {
                var errors = _orderProcessingService.CancelRecurringPayment(recurringPayment);

                var model = PrepareCustomerOrderListModel();
                model.CancelRecurringPaymentErrors = errors;

                return View(model);
            }
            else
            {
                return RedirectToRoute("CustomerOrders");
            }
        }

        //My account / Reward points
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult CustomerRewardPoints()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            if (!_rewardPointsSettings.Enabled)
                return RedirectToRoute("CustomerInfo");

            var customer = _workContext.CurrentCustomer;

            var model = new CustomerRewardPointsModel();

            foreach (var rph in _rewardPointService.GetRewardPointsHistory(customer.Id))
            {
                model.RewardPoints.Add(new CustomerRewardPointsModel.RewardPointsHistoryModel
                {
                    Points = rph.Points,
                    PointsBalance = rph.PointsBalance,
                    Message = rph.Message,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(rph.CreatedOnUtc, DateTimeKind.Utc)
                });
            }
            //current amount/balance
            int rewardPointsBalance = _rewardPointService.GetRewardPointsBalance(customer.Id, _storeContext.CurrentStore.Id);
            decimal rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
            decimal rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, _workContext.WorkingCurrency);

            model.RewardPointsBalance = rewardPointsBalance;
            model.RewardPointsAmount = _priceFormatter.FormatPrice(rewardPointsAmount, true, false);
            
            //minimum amount/balance
            int minimumRewardPointsBalance = _rewardPointsSettings.MinimumRewardPointsToUse;
            decimal minimumRewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(minimumRewardPointsBalance);
            decimal minimumRewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(minimumRewardPointsAmountBase, _workContext.WorkingCurrency);

            model.MinimumRewardPointsBalance = minimumRewardPointsBalance;
            model.MinimumRewardPointsAmount = _priceFormatter.FormatPrice(minimumRewardPointsAmount, true, false);

            return View(model);
        }

        //My account / Order details page
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult Details(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var model = PrepareOrderDetailsModel(order);

            return View(model);
        }

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult ExportInvoicesToExcel(OrderHistorySearchModel model)
        {
            int TotalOrderHistoryCount = 0;
            
            List<OldhamChem.Thoroughbred.Data.Entities.Invoice> Invoices = SearchInvoices(model, out TotalOrderHistoryCount);

            List<Nop.Core.Domain.Orders.Invoice> InvoicesForExport = PrepareInvoiceDomainModels(Invoices);

            try
            {
                byte[] bytes;
                using (var stream = new MemoryStream())
                {
                    _exportManager.ExportInvoicesToXlsx(stream, InvoicesForExport);
                    bytes = stream.ToArray();
                }
                return File(bytes, "text/xls", "OrderHistory.xlsx");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Reporting");
            }
        }

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult OrderHistoryExportForm(OrderHistorySearchFormModel model)
        {
            return PartialView(model);
        }

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult OrderHistoryList(OrderHistorySearchFormModel model)
        {
            return PartialView(model);
        }

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult OrderHistorySearchForm()
        {
            OrderHistorySearchFormModel Model = new OrderHistorySearchFormModel();

            return PartialView(Model);
        }

        //My account / Order details page / Print
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult PrintOrderDetails(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var model = PrepareOrderDetailsModel(order);
            model.PrintMode = true;

            return View("Details", model);
        }

        //My account / Order details page / PDF invoice
        public ActionResult GetPdfInvoice(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var orders = new List<Order>();
            orders.Add(order);
            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", string.Format("order_{0}.pdf", order.Id));
        }

        //My account / Order details page / re-order
        public ActionResult ReOrder(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            _orderProcessingService.ReOrder(order);
            return RedirectToRoute("ShoppingCart");
        }

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult Reporting()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();
                        
            OrderHistorySearchFormModel orderHistorySearchFormModel = new OrderHistorySearchFormModel()
            {
                Pagination = new Models.Common.SimplePagerModel()
                {
                    FirstItem = 1,
                    HasPreviousPage = true,
                    HasNextPage = true,
                    PageSize = 25
                }
            };

            OrderHistorySearchModel Model = new OrderHistorySearchModel()
            {
                OrderHistorySearchFormModel = orderHistorySearchFormModel,
                Pagination = orderHistorySearchFormModel.Pagination
            };
            
            int TotalOrderHistoryCount = 0;
            List<OldhamChem.Thoroughbred.Data.Entities.Invoice> InvoiceSearchResults = SearchInvoices(Model, out TotalOrderHistoryCount);

            if (!InvoiceSearchResults.IsNullOrEmpty())
            {
                Model.Invoices = PrepareInvoiceDomainModels(InvoiceSearchResults);
                
                Model.Pagination.FirstItem = orderHistorySearchFormModel.Pagination.FirstItem;
                Model.Pagination.HasNextPage = orderHistorySearchFormModel.Pagination.HasNextPage;
                Model.Pagination.HasPreviousPage = orderHistorySearchFormModel.Pagination.HasPreviousPage;
                Model.Pagination.LastItem = Math.Min(TotalOrderHistoryCount, 0 + orderHistorySearchFormModel.Pagination.PageSize);
                Model.Pagination.PageNumber = orderHistorySearchFormModel.Pagination.PageNumber;
                Model.Pagination.PageSize = orderHistorySearchFormModel.Pagination.PageSize;
                Model.Pagination.TotalItems = TotalOrderHistoryCount;
                Model.Pagination.TotalPages = (TotalOrderHistoryCount + orderHistorySearchFormModel.Pagination.PageSize - 1) / orderHistorySearchFormModel.Pagination.PageSize;
            }

            return View(Model);
        }

        //My account / Order details page / Complete payment
        [HttpPost, ActionName("Details")]
        [PublicAntiForgery]
        [FormValueRequired("repost-payment")]
        public ActionResult RePostPayment(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            if (!_paymentService.CanRePostProcessPayment(order))
                return RedirectToRoute("OrderDetails", new { orderId = orderId });

            var postProcessPaymentRequest = new PostProcessPaymentRequest
            {
                Order = order
            };
            _paymentService.PostProcessPayment(postProcessPaymentRequest);

            if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
            {
                //redirection or POST has been done in PostProcessPayment
                return Content("Redirected");
            }

            //if no redirection has been done (to a third-party payment page)
            //theoretically it's not possible
            return RedirectToRoute("OrderDetails", new { orderId = orderId });
        }


        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult SearchOrderHistory(OrderHistorySearchFormModel model)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            OrderHistorySearchModel orderHistorySearchModel = new OrderHistorySearchModel()
            {
                OrderHistorySearchFormModel = model,
                Pagination = model.Pagination 
            };            

            int TotalOrderHistoryCount = 0;
            List<OldhamChem.Thoroughbred.Data.Entities.Invoice> InvoiceSearchResults = SearchInvoices(orderHistorySearchModel, out TotalOrderHistoryCount);

            if (!InvoiceSearchResults.IsNullOrEmpty())
            {
                orderHistorySearchModel.Invoices = PrepareInvoiceDomainModels(InvoiceSearchResults);
                
                orderHistorySearchModel.Pagination.LastItem = Math.Min(TotalOrderHistoryCount, 0 + model.Pagination.PageSize);
                orderHistorySearchModel.Pagination.PageNumber = model.Pagination.PageNumber;
                orderHistorySearchModel.Pagination.TotalItems = TotalOrderHistoryCount;
                orderHistorySearchModel.Pagination.TotalPages = (TotalOrderHistoryCount + model.Pagination.PageSize - 1) / model.Pagination.PageSize;
            }

            List<Invoice> Invoices = PrepareInvoiceDomainModels(InvoiceSearchResults);

            orderHistorySearchModel.Invoices = Invoices;

            return PartialView("_OrderHistoryList", orderHistorySearchModel);
        }

        private List<OldhamChem.Thoroughbred.Data.Entities.Invoice> SearchInvoices(OrderHistorySearchModel model, out int totalInvoiceCount)
        {
            int? CurrentPageIndex = null;
            int? PageSize = null;
            
            OldhamChem.Thoroughbred.Data.Repository ThoroughbredRepository = new OldhamChem.Thoroughbred.Data.Repository();
            ThoroughbredRepository.Initialize();

            List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, object>>> IncludeExpressions = new List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, object>>>();
            IncludeExpressions.Add(i => i.InvoiceItems);

            List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, bool>>> WhereExpressions = new List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, bool>>>();

            string CustomerNumber = _workContext.CurrentCustomer.CustomerNumber;
            WhereExpressions.Add(w => w.CustomerNumber == CustomerNumber);

            if (model.OrderHistorySearchFormModel.InvoiceDateRange != null && model.OrderHistorySearchFormModel.InvoiceDateRange.StartDate > DateTime.MinValue && model.OrderHistorySearchFormModel.InvoiceDateRange.EndDate < DateTime.MaxValue)
            {
                WhereExpressions.Add(w => w.INV_DATE >= model.OrderHistorySearchFormModel.InvoiceDateRange.StartDate && w.INV_DATE <= model.OrderHistorySearchFormModel.InvoiceDateRange.EndDate);
            }
            
            if (!String.IsNullOrWhiteSpace(model.OrderHistorySearchFormModel.ProductNumber))
            {
                WhereExpressions.Add(w => w.InvoiceItems.Any(a => a.ITEM_CODE != null && a.ITEM_CODE.Trim() == model.OrderHistorySearchFormModel.ProductNumber));
            }
                        
            if (!String.IsNullOrWhiteSpace(model.OrderHistorySearchFormModel.InvoiceNumber))
            {
                WhereExpressions.Add(w => w.INV_NO == model.OrderHistorySearchFormModel.InvoiceNumber);
            }

            if (!String.IsNullOrWhiteSpace(model.OrderHistorySearchFormModel.OrderNumber))
            {
                WhereExpressions.Add(w => w.ORDER_NO == model.OrderHistorySearchFormModel.OrderNumber);
            }

            List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OldhamChem.Thoroughbred.Data.Entities.Invoice>> OrderByExpressions = new List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OldhamChem.Thoroughbred.Data.Entities.Invoice>>();
            OrderByExpressions.Add(new OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OldhamChem.Thoroughbred.Data.Entities.Invoice>()
            {
                Expression = ob => ob.INV_DATE,
                Direction = OldhamChem.Thoroughbred.Data.Helper.OrderByDirection.DESC
            });


            if (model.Pagination != null)
            {
                CurrentPageIndex = model.Pagination.PageIndex;
                PageSize = model.Pagination.PageSize;
            }

            List<OldhamChem.Thoroughbred.Data.Entities.Invoice> SearchResults = ThoroughbredRepository
                .InvoiceRepository
                .FindBy(CurrentPageIndex, PageSize, IncludeExpressions, WhereExpressions, OrderByExpressions, out totalInvoiceCount)
                .ToList();

            return SearchResults;
        }

        //private List<OrderHistoryModel> PrepareOrderHistoryModels(List<Invoice> CompleteOrderHistory)
        //{
        //    List<OrderHistoryModel> OrderHistoryModels = new List<OrderHistoryModel>();

        //    if (CompleteOrderHistory != null && CompleteOrderHistory.Any())
        //    {
        //        List<string> DistinctSKUs = CompleteOrderHistory
        //            .SelectMany(sm => sm.OrderHistoryItems)
        //            .Select(s => s.ProductNumber)
        //            .Distinct()
        //            .ToList();

        //        List<Product> MatchingProducts = _productService
        //            .GetProductsBySku(DistinctSKUs)
        //            .ToList();

        //        foreach (var order in CompleteOrderHistory)
        //        {
        //            OrderHistoryModel OrderHistory = new OrderHistoryModel()
        //            {
        //                DateOfInvoice = order.DateInvoiceCreated,
        //                FreightCharge = order.FreightCharge,
        //                InvoiceAmount = order.InvoiceAmount,
        //                InvoiceNumber = order.InvoiceNumber,
        //                OrderNumber = order.OrderNumber,
        //                OrderHistoryItems = new List<OrderHistoryItemModel>(),
        //                SalesTax = order.SalesTax
        //            };

        //            if (order.OrderHistoryItems != null && order.OrderHistoryItems.Count > 0)
        //            {
        //                foreach (var Item in order.OrderHistoryItems)
        //                {
        //                    OrderHistoryItemModel OrderHistoryItem = new OrderHistoryItemModel()
        //                    {
        //                        Price = Item.UnitPrice,
        //                        Product = new ProductOverviewModel()
        //                        {
        //                            Sku = Item.ProductNumber
        //                        },
        //                        Quantity = Item.Quantity,
        //                        Total = Item.Total
        //                    };

        //                    Product MatchingProduct = MatchingProducts.Where(w => w.Sku == Item.ProductNumber).FirstOrDefault();

        //                    if (MatchingProduct != null)
        //                    {
        //                        OrderHistoryItem.Product.Id = MatchingProduct.Id;
        //                        OrderHistoryItem.Product.Name = MatchingProduct.GetLocalized(x => x.Name);
        //                        OrderHistoryItem.Product.ShortDescription = MatchingProduct.GetLocalized(x => x.ShortDescription);
        //                        OrderHistoryItem.Product.FullDescription = MatchingProduct.GetLocalized(x => x.FullDescription);
        //                        OrderHistoryItem.Product.SeName = MatchingProduct.GetSeName();
        //                    }

        //                    OrderHistory.OrderHistoryItems.Add(OrderHistoryItem);
        //                }
        //            }

        //            OrderHistoryModels.Add(OrderHistory);
        //        }

        //    }

        //    return OrderHistoryModels;
        //}

        private List<Core.Domain.Orders.Invoice> PrepareInvoiceDomainModels(List<OldhamChem.Thoroughbred.Data.Entities.Invoice> invoices)
        {
            List<Core.Domain.Orders.Invoice> OrderHistoryModels = new List<Core.Domain.Orders.Invoice>();

            if (invoices != null && invoices.Any())
            {

                Customer CurrentCustomer = _workContext.CurrentCustomer;

                List<Warehouse> AllWarehouses = _shippingService
                    .GetAllWarehouses()
                    .ToList();

                List<string> DistinctSKUs = invoices
                    .SelectMany(sm => sm.InvoiceItems)
                    .Where(w => !String.IsNullOrWhiteSpace(w.ITEM_CODE))
                    .Select(s => s.ITEM_CODE)
                    .Distinct()
                    .ToList();

                List<Product> MatchingProducts = _productService
                    .GetProductsBySku(DistinctSKUs)
                    .ToList();

                foreach (var order in invoices)
                {
                    Warehouse WarehouseForInvoice = AllWarehouses
                        .Where(w => w.Code == order.WAREHOUSE)
                        .FirstOrDefault();

                    Core.Domain.Orders.Invoice Invoice = new Core.Domain.Orders.Invoice()
                    {
                        Customer = CurrentCustomer,
                        DateInvoiceCreated = order.INV_DATE.Value,
                        ShipToNumber = order.SHIP_TO_CODE,
                        FreightCharge = order.FREIGHT_AMT.Value,
                        InvoiceAmount = order.INVOICE_AMT.Value,
                        InvoiceNumber = order.INV_NO,
                        OrderNumber = order.ORDER_NO,         
                        PoNumber = order.CUST_PO_NUM,
                        InvoiceItems = new List<Core.Domain.Orders.InvoiceItem>(),
                        SalesTax = order.SALES_TAX_AMT.Value,
                        Warehouse = WarehouseForInvoice
                    };

                    if (order.InvoiceItems != null && order.InvoiceItems.Count > 0)
                    {
                        foreach (var Item in order.InvoiceItems.Where(w => !String.IsNullOrWhiteSpace(w.ITEM_CODE)))
                        {
                            InvoiceItem InvoiceItem = new InvoiceItem()
                            {
                                UnitPrice = Item.UNIT_PRICE.Value,
                                Product = new Product()
                                {
                                    Sku = Item.ITEM_CODE.Trim()
                                },
                                Quantity = Convert.ToInt32(Item.SHIP_TODAY.Value),
                                Total = Item.EXT_PRICE.Value
                            };

                            Product MatchingProduct = MatchingProducts.Where(w => w.Sku == Item.ITEM_CODE.Trim()).FirstOrDefault();

                            if (MatchingProduct != null)
                            {
                                InvoiceItem.Product.Id = MatchingProduct.Id;
                                InvoiceItem.Product.Name = MatchingProduct.GetLocalized(x => x.Name);
                                InvoiceItem.Product.ShortDescription = MatchingProduct.GetLocalized(x => x.ShortDescription);
                                InvoiceItem.Product.FullDescription = MatchingProduct.GetLocalized(x => x.FullDescription);
                                InvoiceItem.Product.MetaDescription = MatchingProduct.GetSeName();
                            }

                            Invoice.InvoiceItems.Add(InvoiceItem);
                        }
                    }

                    OrderHistoryModels.Add(Invoice);
                }

            }

            return OrderHistoryModels;
        }

        //My account / Order details page / Shipment details page
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult ShipmentDetails(int shipmentId)
        {
            var shipment = _shipmentService.GetShipmentById(shipmentId);
            if (shipment == null)
                return new HttpUnauthorizedResult();

            var order = shipment.Order;
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var model = PrepareShipmentDetailsModel(shipment);

            return View(model);
        }

        #endregion
    }
}
