﻿using System.Web.Mvc;
using Nop.Web.Framework.Security;

namespace Nop.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
