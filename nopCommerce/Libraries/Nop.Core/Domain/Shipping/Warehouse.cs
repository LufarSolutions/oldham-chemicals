using Nop.Core.Domain.Common;
using Nop.Core.Domain.Orders;
using System.Collections.Generic;

namespace Nop.Core.Domain.Shipping
{
    /// <summary>
    /// Represents a shipment
    /// </summary>
    public partial class Warehouse : BaseEntity {

        private ICollection<ReturnRequest> _returnRequests;
        /// <summary>
        /// Gets or sets the warehouse name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the admin comment
        /// </summary>
        public string AdminComment { get; set; }

        /// <summary>
        /// Gets or sets the address identifier of the warehouse
        /// </summary>
        public int AddressId { get; set; }

        /// <summary>
        /// Gets or sets the warehouse code of the warehouse
        /// </summary>
        public string Code { get; set; }

        public virtual Address Address { get; set; }

        public virtual ICollection<ReturnRequest> ReturnRequests {
            get { return _returnRequests ?? (_returnRequests = new List<ReturnRequest>()); }
            protected set { _returnRequests = value; }
        }
    }
}