﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customers
{
    public partial class CustomerAccess_Mapping : BaseEntity
    {
        //[ForeignKey("Id")]
        public int CustomerId { get; set; }

        public int AccessibleCustomerId { get; set; }

        public int CustomerRoleId { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual CustomerRole CustomerRole { get; set; }

        public virtual Customer AccessibleCustomer { get; set; }

        //public virtual Customer Customer { get; set; }
    }
}
