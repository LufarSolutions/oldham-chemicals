using System;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Shipping;

namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents a return request
    /// </summary>
    public partial class ReturnRequest : BaseEntity
    {
        public string AuthorizationNumber { get; set; }

        public string CustomerComments { get; set; }

        public string CustomerCompany { get; set; }
        public int? CustomerId { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerLastName { get; set; }

        public string CustomerPhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the order item identifier
        /// </summary>
        public int? OrderItemId { get; set; }

        public int? ProductId { get; set; }

        public string ProductDetails { get; set; }

        public int Quantity { get; set; }

        public string ReasonForReturn { get; set; }

        public string RequestedAction { get; set; }

        public int ReturnRequestStatusId { get; set; }

        public string StaffNotes { get; set; }
        public int StoreId { get; set; }

        public string TrackingNumberInbound { get; set; }

        public string TrackingNumberOutbound { get; set; }

        public int? WarehouseId { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime UpdatedOnUtc { get; set; }
        
        public ReturnRequestStatus ReturnRequestStatus
        {
            get
            {
                return (ReturnRequestStatus)this.ReturnRequestStatusId;
            }
            set
            {
                this.ReturnRequestStatusId = (int)value;
            }
        }

        public virtual Customer Customer { get; set; }

        public virtual Product  Product { get; set; }

        public virtual Warehouse Warehouse { get; set; }
    }
}
