﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public partial class InvoiceItem : BaseEntity
    {
        #region Properties

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }

        public bool IsTaxable { get; set; }

        //public int OrderHistoryId { get; set; }

        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public decimal Total { get; set; }

        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Gets the product
        /// </summary>
        public virtual Product Product { get; set; }
        
        #endregion
    }
}
