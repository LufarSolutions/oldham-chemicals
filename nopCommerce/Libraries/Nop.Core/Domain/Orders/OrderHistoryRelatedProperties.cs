﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public class OrderHistoryRelatedProperties
    {
        public bool IncludeOrderHistoryItems { get; set; }

        public OrderHistoryRelatedProperties()
        {
            this.IncludeOrderHistoryItems = false;
        }
    }
}
