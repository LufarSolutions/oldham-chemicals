﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Shipping;
using System;
using System.Collections.Generic;

namespace Nop.Core.Domain.Orders
{
    public partial class Invoice : BaseEntity
    {
        #region Properties
        
        public int CustomerId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }

        public DateTime DateInvoiceCreated { get; set; }

        public DateTime? DateInvoiceUpdated { get; set; }

        public decimal FreightCharge { get; set; }

        public decimal InvoiceAmount { get; set; }

        public decimal InvoiceBalance { get; set; }

        public string InvoiceNumber { get; set; }

        public string OrderNumber { get; set; }

        public string PoNumber { get; set; }

        public decimal SalesTax { get; set; }

        public string ShipToNumber { get; set; }

        public string Terms { get; set; }

        public int WarehouseId { get; set; }

        #endregion

        #region Navigation Properties

        public virtual Customer Customer { get; set; }

        public virtual Warehouse Warehouse { get; set; }

        /// <summary>
        /// Gets or sets items for this order history
        /// </summary>
        public virtual ICollection<InvoiceItem> InvoiceItems { get; set; }

        #endregion
    }
}
