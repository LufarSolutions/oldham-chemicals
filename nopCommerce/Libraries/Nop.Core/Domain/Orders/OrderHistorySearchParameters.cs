﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public class OrderHistorySearchParameters
    {
        public List<string> CustomerNumbers { get; set; }

        public DateRange InvoiceDateRange { get; set; }

        public List<string> InvoiceNumbers { get; set; }

        public List<string> OrderNumbers { get; set; }

        public List<string> ProductNumbers { get; set; }

        public List<string> WarehouseCodes { get; set; }

        public OrderHistorySearchParameters()
        {
            this.CustomerNumbers = new List<string>();
            this.InvoiceNumbers = new List<string>();
            this.OrderNumbers = new List<string>();
            this.ProductNumbers = new List<string>();
            this.WarehouseCodes = new List<string>();
        }
    }
}
