﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public class OrderHistorySortParameter : SortParameter<OrderHistorySortParameterNames>
    {
        public OrderHistorySortParameter(OrderHistorySortParameterNames name, SortDirection direction)
        {
            this.Name = name;
            this.Direction = direction;

        }
    }
}
