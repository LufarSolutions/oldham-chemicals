﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Common
{
    public class SortParameter<T>
    {        
        public T Name { get; set; }

        public SortDirection Direction { get; set; }
    }
}
