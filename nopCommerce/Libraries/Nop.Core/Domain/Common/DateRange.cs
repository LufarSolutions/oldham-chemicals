﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Common
{
    public class DateRange
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateRange()
        {
            this.StartDate = DateTime.MinValue;
            this.EndDate = DateTime.MaxValue;
        }

        public string ToShortDateRangeString()
        {
            return this.StartDate.ToShortDateString() + "|" + this.EndDate.ToShortDateString();
        }
    }
}
