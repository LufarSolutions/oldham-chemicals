using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    public partial class ContractPriceMap : NopEntityTypeConfiguration<ContractPrice>
    {
        public ContractPriceMap()
        {
            this.ToTable("ContractPrice");
            this.HasKey(tp => tp.Id);
            this.Property(tp => tp.Price).HasPrecision(18, 4);

            this.HasRequired(tp => tp.Customer)
                .WithMany(p => p.ContractPrices)
                .HasForeignKey(tp => tp.CustomerId);

            //this.HasOptional(tp => tp.Customer)
            //    .WithMany()
            //    .HasForeignKey(tp => tp.CustomerId)
            //    .WillCascadeOnDelete(true);
        }
    }
}