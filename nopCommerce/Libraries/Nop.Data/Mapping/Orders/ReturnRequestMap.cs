using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.Orders
{
    public partial class ReturnRequestMap : NopEntityTypeConfiguration<ReturnRequest>
    {
        public ReturnRequestMap()
        {
            this.ToTable("ReturnRequest");

            this.HasKey(rr => rr.Id);
            
            this.Property(rr => rr.ReasonForReturn).IsRequired();
            this.Property(rr => rr.RequestedAction).IsRequired();

            this.Ignore(rr => rr.ReturnRequestStatus);

            this.HasOptional(rr => rr.Customer)
                .WithMany(c => c.ReturnRequests)
                .HasForeignKey(rr => rr.CustomerId);

            this.HasOptional(rr => rr.Product)
                .WithMany(c => c.ReturnRequests)
                .HasForeignKey(rr => rr.ProductId);

            this.HasOptional(rr => rr.Warehouse)
                .WithMany(c => c.ReturnRequests)
                .HasForeignKey(rr => rr.WarehouseId);
        }
    }
}