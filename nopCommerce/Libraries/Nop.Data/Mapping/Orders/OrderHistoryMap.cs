﻿using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Orders
{
    public partial class OrderHistoryMap : NopEntityTypeConfiguration<OrderHistory>
    {
        public OrderHistoryMap()
        {
            this.ToTable("OrderHistory");
            this.HasKey(tp => tp.Id);
            this.Property(tp => tp.InvoiceAmount).HasPrecision(8, 2);
            this.Property(tp => tp.InvoiceBalance).HasPrecision(8, 2);            
        }
    }
}
