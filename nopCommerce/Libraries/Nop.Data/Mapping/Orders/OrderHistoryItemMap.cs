﻿using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Orders
{
    public partial class OrderHistoryItemMap : NopEntityTypeConfiguration<OrderHistoryItem>
    {
        public OrderHistoryItemMap()
        {
            this.ToTable("OrderHistoryItem");
            this.HasKey(tp => tp.Id);
            this.Property(tp => tp.Total).HasPrecision(8, 2);
            this.Property(tp => tp.UnitPrice).HasPrecision(8, 2);

            //this.HasRequired(tp => tp.OrderHistory)
            //    .WithMany(p => p.OrderHistoryItems)
            //    .HasForeignKey(tp => tp.OrderHistoryId);

        }
    }
}
