﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Helpers
{
    public class SortParameter<T>
    {
        public T Parameter { get; set; }
        public System.Web.Helpers.SortDirection Direction { get; set; }
    }
}
