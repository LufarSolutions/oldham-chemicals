﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Customers
{
    public class CustomerIncludeParameters
    {
        public bool IncludeAddresses { get; set; }
        public bool IncludeRoles { get; set; }

        public CustomerIncludeParameters()
        {
            IncludeAddresses = false;
            IncludeRoles = false;
        }
    }
}
