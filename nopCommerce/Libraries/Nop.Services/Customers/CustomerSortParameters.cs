﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Customers
{
    public enum CustomerSortParameters
    {
        CustomerName,
        CustomerNumber,
        DateOfLastActivity
    }
}
