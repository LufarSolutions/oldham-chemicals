﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Customers
{
    public class CustomerSearchParameters
    {
        public string CustomerNameLike { get; set; }

        public string CustomerNumberLike { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }

        public DateRange LastOrderDate { get; set; }

        public List<string> PostalCodes { get; set; }

        public List<string> Roles  { get; set; }

        public CustomerSearchParameters()
        {            
            PostalCodes = new List<string>();
            Roles = new List<string>();
        }
    }
}
