﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Orders;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Orders;
using Nop.Services.Payments;
using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Nop.Services.ERP
{
    public partial class ThoroughbredService : IErpService
    {
        private const string PICKUP_IN_STORE_SHIP_TO_NUMBER = "000";
        private const string CUSTOMER_ATTRIBUTE_WAREHOUSE_CODE = "Warehouse Code";

        private OldhamChem.Thoroughbred.Data.Repository ThoroughbredRepository;

        private readonly ICustomerService _customerService;
        private readonly ICustomerAttributeParser CustomerAttributeParser;
        private readonly IOrderService _orderService;

        public ThoroughbredService(ICustomerService customerService, ICustomerAttributeParser customerAttributeParser, IOrderService orderService)
        {
            ThoroughbredRepository = new OldhamChem.Thoroughbred.Data.Repository();
            ThoroughbredRepository.Initialize();

            this._customerService = customerService;
            this._orderService = orderService;
            this.CustomerAttributeParser = customerAttributeParser;
        }

        public virtual Customer GetCustomerFromCustomerNumber(string customerNumber)
        {
            if (String.IsNullOrWhiteSpace(customerNumber))
                throw new ArgumentNullException("customerNumber");

            List<System.Linq.Expressions.Expression<Func<Customer, bool>>> WhereExpressions = new List<System.Linq.Expressions.Expression<Func<Customer, bool>>>();
            WhereExpressions.Add(w => w.CustomerNumber.Trim() == customerNumber);

            List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<Customer>> OrderByExpressions = new List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<Customer>>();
            OrderByExpressions.Add(new OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<Customer>(){ Expression = ob => ob.CustomerNumber, Direction = OldhamChem.Thoroughbred.Data.Helper.OrderByDirection.ASC });

            int CustomerCount = 0;

            return ThoroughbredRepository
                .Customer
                .FindBy(0, 1, null, WhereExpressions, OrderByExpressions, out CustomerCount)
                .FirstOrDefault();
        }

        public virtual OldhamChem.Thoroughbred.Data.Entities.Invoice GetInvoice(string invoiceNumber) {
            List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, object>>> IncludeExpressions = new List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, object>>>();
            IncludeExpressions.Add(i => i.InvoiceItems);
            IncludeExpressions.Add(i => i.InvoiceItems.Select(s => s.Item));

            List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, bool>>> WhereExpressions = new List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, bool>>>();
            WhereExpressions.Add(w => w.INV_NO.Trim() == invoiceNumber);

            List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OldhamChem.Thoroughbred.Data.Entities.Invoice>> OrderByExpressions = new List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OldhamChem.Thoroughbred.Data.Entities.Invoice>>();
            OrderByExpressions.Add(new OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OldhamChem.Thoroughbred.Data.Entities.Invoice>() { Direction = OldhamChem.Thoroughbred.Data.Helper.OrderByDirection.ASC, Expression = ob => ob.INV_NO });

            int InvoiceCount = 0;

            return ThoroughbredRepository
                .InvoiceRepository
                .FindBy(0, 1, IncludeExpressions, WhereExpressions, OrderByExpressions, out InvoiceCount)
                .FirstOrDefault();
        }

        public virtual OldhamChem.Thoroughbred.Data.Entities.Invoice GetInvoiceForOrder(string orderNumber)
        {
            List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, object>>> IncludeExpressions = new List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, object>>>();
            IncludeExpressions.Add(i => i.InvoiceItems);
            IncludeExpressions.Add(i => i.InvoiceItems.Select(s => s.Item));

            List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, bool>>> WhereExpressions = new List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Invoice, bool>>>();
            WhereExpressions.Add(w => w.ORDER_NO == orderNumber);

            List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OldhamChem.Thoroughbred.Data.Entities.Invoice>> OrderByExpressions = new List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OldhamChem.Thoroughbred.Data.Entities.Invoice>>();
            OrderByExpressions.Add(new OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OldhamChem.Thoroughbred.Data.Entities.Invoice>() { Direction = OldhamChem.Thoroughbred.Data.Helper.OrderByDirection.ASC, Expression = ob => ob.INV_NO });

            int InvoiceCount= 0;

            return ThoroughbredRepository
                .InvoiceRepository
                .FindBy(0, 1, IncludeExpressions, WhereExpressions, OrderByExpressions, out InvoiceCount)
                .FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="includeDiscontinuedItems"></param>
        /// <param name="InvoiceStartDate"></param>
        /// <param name="manufacturerCode"></param>
        /// <param name="skus"></param>
        /// <param name="warehouseCodes"></param>
        /// <returns></returns>
        public virtual List<ItemSalesHistoryAndInventory> GetInventoryReportData(bool includeDiscontinuedItems, DateTime InvoiceStartDate, string manufacturerCode, List<string> skus, List<string> warehouseCodes)
        {
            if (String.IsNullOrWhiteSpace(manufacturerCode))
                throw new ArgumentNullException("manufacturerCode");

            ThoroughbredDbContext DbContext = new OldhamChem.Thoroughbred.Data.Entities.ThoroughbredDbContext();

            // Had to use the SqlQuery method. Letting EF construct the query took up to 20 times as long as this.
            StringBuilder QueryBuilder = new StringBuilder();
            QueryBuilder.Append("SELECT * FROM ItemSalesHistoryAndInventory WHERE VEND_NO = '" + manufacturerCode + "' AND InvoiceDate >= '" + InvoiceStartDate.ToShortDateString() + "'");

            if (!includeDiscontinuedItems)
            {
                QueryBuilder.Append(" AND DISCONTINUED = 'N'");
            }

            if (skus != null && skus.Any())
            {
                QueryBuilder.Append(" AND ITEM_CODE IN ('" + String.Join("','", skus) + "')");
            }

            if (warehouseCodes != null && warehouseCodes.Any())
            {
                QueryBuilder.Append(" AND WHSE_CODE IN ('" + String.Join("','", warehouseCodes) + "')");
            }

            return DbContext
                .ItemSalesHistoriesAndInventory
                .SqlQuery(QueryBuilder.ToString())
                .ToList();
        }

        public virtual CustomerAddress GetMatchingCustomerAddress(Address address, string customerNumber)
        {
            List<System.Linq.Expressions.Expression<Func<CustomerAddress, bool>>> WhereExpressions = new List<System.Linq.Expressions.Expression<Func<CustomerAddress, bool>>>();
            WhereExpressions.Add(w => w.CustomerNumber == customerNumber);
            WhereExpressions.Add(w => w.CUST_SHIP_TO_ADDR1.Trim() == address.Address1);
            WhereExpressions.Add(w => w.CUST_SHIP_TO_CITY.Trim() == address.City);
            WhereExpressions.Add(w => w.CUST_SHIP_TO_STATE.Trim() == address.StateProvince.Abbreviation);
            WhereExpressions.Add(w => w.CUST_SHIP_TO_ZIP.Trim() == address.ZipPostalCode);

            List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<CustomerAddress>> OrderByExpressions = new List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<CustomerAddress>>();
            OrderByExpressions.Add(new OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<CustomerAddress>() { Expression = ob => ob.SHIP_TO_NO, Direction = OldhamChem.Thoroughbred.Data.Helper.OrderByDirection.ASC });

            int RecordCount = 0;
            CustomerAddress CustomerAddressForShipToNumber = ThoroughbredRepository
                .CustomerAddressRepository
                .FindBy(0, 1, null, WhereExpressions, OrderByExpressions, out RecordCount)
                .FirstOrDefault();

            return CustomerAddressForShipToNumber;
        }

        public virtual OldhamChem.Thoroughbred.Data.Entities.Order GetOrderByOrderNumber(string orderNumber)
        {
            var includeExpressions = new List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Order, object>>>();
            includeExpressions.Add(i => i.OrderItems);

            var whereExpressions = new List<System.Linq.Expressions.Expression<Func<OldhamChem.Thoroughbred.Data.Entities.Order, bool>>>();
            whereExpressions.Add(w => w.ORDER_NO == orderNumber);

            var recordCount = 0;

            return ThoroughbredRepository
                .OrderRepository
                .FindBy(0, 1, includeExpressions, whereExpressions, null, out recordCount)
                .FirstOrDefault();
        }

        public virtual List<OrderQueue> GetQueuedOrdersWithErrors()
        {
            List<System.Linq.Expressions.Expression<Func<OrderQueue, object>>> IncludeExpressions = new List<System.Linq.Expressions.Expression<Func<OrderQueue, object>>>();
            IncludeExpressions.Add(i => i.OrderQueueItems);

            List<System.Linq.Expressions.Expression<Func<OrderQueue, bool>>> WhereExpressions = new List<System.Linq.Expressions.Expression<Func<OrderQueue, bool>>>();
            WhereExpressions.Add(w => w.OrderNumber == null && !w.DateProcessed.HasValue);
            WhereExpressions.Add(w => w.Error != null || w.Error.Length > 0);

            List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OrderQueue>> OrderByExpressions = new List<OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OrderQueue>>();
            OrderByExpressions.Add(new OldhamChem.Thoroughbred.Data.Helper.OrderByExpression<OrderQueue>() { Expression = ob => ob.DateCreated, Direction = OldhamChem.Thoroughbred.Data.Helper.OrderByDirection.ASC });

            int QueuedOrderCount = 0;

            return ThoroughbredRepository
                .OrderQueueRepository
                .FindBy(null, null, IncludeExpressions, WhereExpressions, OrderByExpressions, out QueuedOrderCount)
                .ToList();
        }

        public virtual List<ItemInventory> GetWarehouseInventories(string manufacturerCode, List<string> warehouseCodes)
        {
            List<System.Linq.Expressions.Expression<Func<ItemInventory, bool>>> WhereExpressions = new List<System.Linq.Expressions.Expression<Func<ItemInventory, bool>>>();
            WhereExpressions.Add(w => w.Item.VENDOR.Trim() == manufacturerCode);

            if (warehouseCodes != null && warehouseCodes.Any())
            {
                WhereExpressions.Add(w => warehouseCodes.Contains(w.WHSE_CODE.Trim()));
            } 

            int WarehouseInventoryCount = 0;

            return ThoroughbredRepository
                .ItemInventoryRepository
                .FindBy(null, null, null, WhereExpressions, null, out WarehouseInventoryCount)
                .ToList();
        }

        public virtual List<OpenPO> GetWarehouseOpenPOs(string manufacturerCode, List<string> warehouseCodes)
        {
            List<System.Linq.Expressions.Expression<Func<OpenPO, bool>>> WhereExpressions = new List<System.Linq.Expressions.Expression<Func<OpenPO, bool>>>();
            WhereExpressions.Add(w => w.VEND_NO.Trim() == manufacturerCode);

            if (warehouseCodes != null && warehouseCodes.Any())
            {
                WhereExpressions.Add(w => warehouseCodes.Contains(w.WHSE_CODE.Trim()));
            }

            int WarehousePOCount = 0;

            return ThoroughbredRepository
                .OpenPORepository
                .FindBy(null, null, null, WhereExpressions, null, out WarehousePOCount)
                .ToList();
        }

        public virtual OrderQueue QueueOrderInERP(Core.Domain.Orders.Order orderToQueue)
        {
            if (orderToQueue == null)
                throw new ArgumentNullException("orderToQueue");
            
            OrderQueue OrderQueueToAdd = null;
            string OnlineOrderNumber = orderToQueue.Id.ToString();

            OrderQueueToAdd = new OldhamChem.Thoroughbred.Data.Entities.OrderQueue()
            {
                OnlineOrderNumber = OnlineOrderNumber,
                CustomerNumber = orderToQueue.Customer.CustomerNumber,
                DateCreated = DateTime.UtcNow,
                PurchaseOrderNumber = null,
                ShipToNumber = orderToQueue.PickUpInStore ? PICKUP_IN_STORE_SHIP_TO_NUMBER : null
            };

            // BEGIN: Customization to get additional order notification e-mail addresses for the specific customer
            List<int> CustomerServiceRepresentativesIds = orderToQueue
                .Customer
                .CustomersWithAccess
                .Where(w => w.CustomerRole.Name == "Customer Service")
                .Select(s => s.CustomerId)
                .ToList();

            if (CustomerServiceRepresentativesIds != null && CustomerServiceRepresentativesIds.Any())
            {
                List<Nop.Core.Domain.Customers.Customer> CustomerServiceRepresentatives = _customerService.GetCustomersByIds(CustomerServiceRepresentativesIds.ToArray())
                    .ToList();

                if (CustomerServiceRepresentatives != null && CustomerServiceRepresentatives.Any())
                {
                    OrderQueueToAdd.NotificationEmails = String.Join("|", CustomerServiceRepresentatives.Select(s => s.Email));
                }
            }

            // END: Customization to get additional order notification e-mail addresses for the specific customer

            Dictionary<string, object> OrderCustomValues = orderToQueue.DeserializeCustomValues();
            object PurchaseOrderNumber = OrderCustomValues.Where(w => w.Key == "PO Number").Select(s => s.Value).FirstOrDefault();

            if (PurchaseOrderNumber != null)
                OrderQueueToAdd.PurchaseOrderNumber = PurchaseOrderNumber.ToString();

            if (!orderToQueue.PickUpInStore)
            {
                OrderQueueToAdd.ShipToNumber = orderToQueue.ShippingAddress.ShipToNumber;

                var warehouseCode = GetCustomerSpecificWarehouse(orderToQueue.Customer);

                OrderQueueToAdd.WarehouseCode = !String.IsNullOrWhiteSpace(warehouseCode) ? warehouseCode : null;
            }

            if (!String.IsNullOrWhiteSpace(orderToQueue.CheckoutAttributeDescription))
            {
                string OrderNotes = orderToQueue
                        .CheckoutAttributeDescription
                        .Replace("&nbsp;", "")
                        .Replace("Special Instruction or Comments: ", "");

                if (OrderNotes.Length >= 90)
                {
                    OrderNotes = orderToQueue
                        .CheckoutAttributeDescription
                        .Substring(0, 90);
                }

                OrderQueueToAdd.Notes = OrderNotes;
            }

            var lineNumber = 0;
            foreach (var OrderItem in orderToQueue.OrderItems) {
                lineNumber++;

                ProductSpecificationAttribute UnitOfMeasure = OrderItem.Product.ProductSpecificationAttributes.Where(w => w.SpecificationAttributeOption.SpecificationAttribute.Name == "Unit of Measure").FirstOrDefault();

                OrderQueueToAdd.OrderQueueItems.Add(new OldhamChem.Thoroughbred.Data.Entities.OrderQueueItem()
                {
                    OnlineOrderNumber = OnlineOrderNumber,
                    ItemCode = OrderItem.Product.Sku,
                    LineNumber = lineNumber,
                    Quantity = OrderItem.Quantity,
                    Price = OrderItem.UnitPriceExclTax,
                    UnitOfMeasure = UnitOfMeasure != null ? UnitOfMeasure.SpecificationAttributeOption.Name : null
                });
            }
            
            // Verify all order items were added
            var orderItems = _orderService.GetAllOrderItems(orderToQueue.Id, null, null, null, null, null, null, false);
            
            if (orderItems.Count != OrderQueueToAdd.OrderQueueItems.Count)
            {
                OrderQueueToAdd.Error = "Mismatched number of order items.";
            }

            ThoroughbredRepository.OrderQueueRepository.Add(OrderQueueToAdd);

            ThoroughbredRepository.OrderQueueRepository.Save();

            orderToQueue.DateQueuedForERP = DateTime.UtcNow;

            _orderService.UpdateOrder(orderToQueue);

            return OrderQueueToAdd;
        }

        public virtual OrderQueue QueueOrderInERPById(int orderId)
        {
            var OrderToQueue = _orderService.GetOrderById(orderId);

            return QueueOrderInERP(OrderToQueue);
        }
        
        public virtual void RemoveQueuedOrder(int onlineOrderId)
        {
            List<System.Linq.Expressions.Expression<Func<OrderQueue, bool>>> WhereExpressions = new List<System.Linq.Expressions.Expression<Func<OrderQueue, bool>>>();
            WhereExpressions.Add(w => w.OnlineOrderNumber == onlineOrderId.ToString());

            int RecordCount= 0;

            OrderQueue QueuedOrderToRemove = ThoroughbredRepository
                .OrderQueueRepository
                .FindBy(0, 1, null, WhereExpressions, null, out RecordCount)
                .FirstOrDefault();

            if (QueuedOrderToRemove != null && String.IsNullOrWhiteSpace(QueuedOrderToRemove.OrderNumber))
            {
                int OnlineOrderId = int.Parse(QueuedOrderToRemove.OnlineOrderNumber);

                ThoroughbredRepository.OrderQueueRepository.Delete(QueuedOrderToRemove);

                ThoroughbredRepository.OrderQueueRepository.Save();

                var QueuedOrder = _orderService.GetOrderById(OnlineOrderId);

                QueuedOrder.ERPOrderId = "0";

                _orderService.UpdateOrder(QueuedOrder);
            }
        }

        public virtual List<string> ValidateOrderForERPConversion(Core.Domain.Orders.Order orderToConvert)
        {
            List<string> ConversionErrors = new List<string>();

            List<string> DistinctOrderItemSkus = orderToConvert
                .OrderItems
                .Select(s => s.Product.Sku)
                .Distinct()
                .ToList();

            List<System.Linq.Expressions.Expression<Func<Item, bool>>> ItemWhereExpressions = new List<System.Linq.Expressions.Expression<Func<Item, bool>>>();
            ItemWhereExpressions.Add(w => DistinctOrderItemSkus.Contains(w.ITEM_CODE.Trim()));

            int ItemCount = 0;

            List<Item> DistinctERPMatchingItems = ThoroughbredRepository
                .ItemRepository
                .FindBy(null, null, null, ItemWhereExpressions, null, out ItemCount)
                .ToList();

            // Check if customer # matches a number in the ERP system
            if (String.IsNullOrWhiteSpace(orderToConvert.Customer.CustomerNumber) )
            {
                ConversionErrors.Add("Customer does not have a valid Customer #.");
            }
            else
            {
                Customer CorrespondingERPCustomer = GetCustomerFromCustomerNumber(orderToConvert.Customer.CustomerNumber);

                if (CorrespondingERPCustomer == null)
                {
                    ConversionErrors.Add("Customer #" + orderToConvert.Customer.CustomerNumber + " does not correspond to a customer in your ERP system.");
                }
            }

            if (!String.IsNullOrWhiteSpace(orderToConvert.ERPOrderId))
            {
                ConversionErrors.Add("Order has already been converted.");
            }
            else if (orderToConvert.DateQueuedForERP.HasValue)
            {
                ConversionErrors.Add("Order was previously queued.");
            }

            // Check if the shipping address has a Ship To #"
            if (!orderToConvert.PickUpInStore && (orderToConvert.ShippingAddress == null || String.IsNullOrWhiteSpace(orderToConvert.ShippingAddress.ShipToNumber)))
            {
                ConversionErrors.Add("Shipping Address does not have a corresponding Ship To #.");
            }

            // Verify each produt matches a product in the ERP system AND has valid UOM
            foreach (var orderItem in orderToConvert.OrderItems)
            {
                ProductSpecificationAttribute UnitOfMeasure = orderItem.Product.ProductSpecificationAttributes.Where(w => w.SpecificationAttributeOption.SpecificationAttribute.Name == "Unit of Measure").FirstOrDefault();

                if (!DistinctERPMatchingItems.Any(a => a.ITEM_CODE.Trim() == orderItem.Product.Sku))
                {
                    ConversionErrors.Add("SKU #" + orderItem.Product.Sku + " does not correspond to an item in your ERP system.");
                }
                else if (UnitOfMeasure == null)
                {
                    ConversionErrors.Add("SKU #" + orderItem.Product.Sku + " does not have a valid Unit of Measure.");
                }
            }
            
            return ConversionErrors;
        }

        private string GetCustomerSpecificWarehouse(Nop.Core.Domain.Customers.Customer customer)
        {
            string WarehouseCode = null;

            string CustomAttributesXML = customer.GetAttribute<string>(Core.Domain.Customers.SystemCustomerAttributeNames.CustomCustomerAttributes);

            List<Core.Domain.Customers.CustomerAttribute> CustomerAttributes = CustomerAttributeParser.ParseCustomerAttributes(CustomAttributesXML).ToList();

            Core.Domain.Customers.CustomerAttribute WarehouseCodeAttribute = CustomerAttributes.FirstOrDefault(f => f.Name == CUSTOMER_ATTRIBUTE_WAREHOUSE_CODE);

            if (WarehouseCodeAttribute != null)
            {
                List<string> CustomerAttributeValues = CustomerAttributeParser.ParseValues(CustomAttributesXML, WarehouseCodeAttribute.Id).ToList();

                if (CustomerAttributeValues != null && CustomerAttributeValues.Count > 0)
                {
                    WarehouseCode = CustomerAttributeValues.FirstOrDefault();
                }
            }

            return WarehouseCode;
        }
    }
}
