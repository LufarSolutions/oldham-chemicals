﻿using Nop.Core.Domain.Common;
using OldhamChem.Thoroughbred.Data.Entities;
using System;
using System.Collections.Generic;

namespace Nop.Services.ERP
{
    public partial interface IErpService
    {
        Customer GetCustomerFromCustomerNumber(string customerNumber);

        List<ItemSalesHistoryAndInventory> GetInventoryReportData(bool includeDiscontinuedItems, DateTime InvoiceStartDate, string manufacturerCode, List<string> skus, List<string> warehouseCodes);

        Invoice GetInvoice(string invoiceNumber);

        Invoice GetInvoiceForOrder(string orderNumber);

        CustomerAddress GetMatchingCustomerAddress(Address address, string customerNumber);

        Order GetOrderByOrderNumber(string orderNumber);

        List<OrderQueue> GetQueuedOrdersWithErrors();

        List<ItemInventory> GetWarehouseInventories(string manufacturerCode, List<string> warehouseCodes);

        List<OpenPO> GetWarehouseOpenPOs(string manufacturerCode, List<string> warehouseCodes);

        OrderQueue QueueOrderInERPById(int orderId);

        OrderQueue QueueOrderInERP(Core.Domain.Orders.Order orderToQueue);

        void RemoveQueuedOrder(int onlineOrderId);

        List<string> ValidateOrderForERPConversion(Core.Domain.Orders.Order orderToConvert);
    }
}
