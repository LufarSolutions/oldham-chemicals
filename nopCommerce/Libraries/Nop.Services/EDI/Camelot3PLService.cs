﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Stores;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Stores;
using OldhamChem.EDI.Common.Models.Camelot3PL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace Nop.Services.EDI
{
    public partial class Camelot3PLService : IEdiService
    {
        private const int NICKEY_WAREHOUSE_CODE = 101;
        private const string CUSTOMER_ATTRIBUTE_WAREHOUSE_CODE = "Warehouse Code";

        //private OldhamChem.Thoroughbred.Data.Repository ThoroughbredRepository;

        private readonly ICustomerService _customerService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly IDownloadService _downloadService;
        private readonly IOrderService _orderService;
        public IStoreService _storeService;

        public Camelot3PLService(ICustomerService customerService, ICustomerAttributeParser customerAttributeParser, IDownloadService downloadService, IOrderService orderService, IStoreService storeService)
        {
            //ThoroughbredRepository = new OldhamChem.Thoroughbred.Data.Repository();
            //ThoroughbredRepository.Initialize();

            this._customerService = customerService;
            this._downloadService = downloadService;
            this._orderService = orderService;
            this._customerAttributeParser = customerAttributeParser;
            this._storeService = storeService;
        }

        public virtual Transaction BuildShipmentTransactionFromOrder(int orderId) {

            var order = _orderService.GetOrderById(orderId);

            var transaction = new Transaction();

            transaction.Transfer = new TransactionTransfer();
            transaction.Transfer.DepositorCode = "Oldham Chemicals";
            transaction.Transfer.OrderRefNumber = order.ERPOrderId;
            transaction.Transfer.PurchOrderNumber = ""; //TODO: Get PO Number from order
            transaction.Transfer.OrderDate = order.DateQueuedForERP.Value.ToShortDateString();
            transaction.Transfer.ShipDate = order.DateQueuedForERP.Value.ToShortDateString();
            //Is this required? Add 5 days? transaction.Transfer.ExpDeliveryDate
            //Is this required? Add 2 days?transaction.Transfer.CancelDate

            transaction.Transfer.ShipTo = new TransactionTransferShipTo();
            transaction.Transfer.ShipTo.ShipToCode = order.ShippingAddress.ShipToNumber;
            transaction.Transfer.ShipTo.ShipToName = order.ShippingAddress.Company;
            transaction.Transfer.ShipTo.ShipToName2 = $"{order.ShippingAddress.FirstName} {order.ShippingAddress.LastName}";
            transaction.Transfer.ShipTo.ShipToAddr1 = order.ShippingAddress.Address1;
            transaction.Transfer.ShipTo.ShipToAddr2 = order.ShippingAddress.Address2;
            transaction.Transfer.ShipTo.ShipToCity = order.ShippingAddress.City;
            transaction.Transfer.ShipTo.ShipToState = order.ShippingAddress.StateProvince.Abbreviation;
            transaction.Transfer.ShipTo.ShipToZip = order.ShippingAddress.ZipPostalCode;

            //TODO Load store Id correctly
            var store = _storeService.GetStoreById(1);
            transaction.Transfer.ShipFrom = new TransactionTransferShipFrom();
            transaction.Transfer.ShipFrom.ShipFromName = store.Name;

            //TODO: Where do I get this from?
            transaction.Transfer.Carrier = new TransactionTransferCarrier();

            //TODO: Is this correct?
            transaction.Transfer.ShipmentMethodCode = "M";

            //TODO: Is this correct?
            transaction.Transfer.FreightBillType = "Collect";

            //TODO: Is this correct?
            transaction.Transfer.OrderStatus = "N";

            var totalQtyOrdered = order.OrderItems.Sum(s => s.Quantity);
            transaction.Transfer.TotalQtyOrdered = totalQtyOrdered;

            foreach(var orderItem in order.OrderItems) {
                transaction.Transfer.TransferLine = new TransactionTransferTransferLine();
                transaction.Transfer.TransferLine.ItemNumber = orderItem.Product.Sku;
                transaction.Transfer.TransferLine.QtyOrdered = orderItem.Quantity;
                //TODO: Get UOM transaction.Transfer.TransferLine.UOM = 
                transaction.Transfer.TransferLine.ItemDesc1 = orderItem.Product.Name;
                //TODO: Anything else needed here?
            }

            //TODO: Comments?

            return transaction;
        }

        public virtual void TransmitTransaction(int orderId, Transaction transaction) {
            var xmlDoc = SerializeTransactionToXmlDocument(transaction);

            SaveXmlTransactionAsOrderNote(orderId, "EDI Shipment Import", xmlDoc);

            //TODO: Actually transmit the file via FTP
        }

        private OrderNote SaveXmlTransactionAsOrderNote(int orderId, string documentType, XmlDocument xmlDoc) {
            var order = _orderService.GetOrderById(orderId);

            byte[] xmlBinary = GenerateBinaryFromXmlDoc(xmlDoc);

            Download OrderNoteAttachment = SaveXmlFile(xmlBinary, documentType);
            
            OrderNote xmlOrderNote = new OrderNote();

            xmlOrderNote.Note = documentType;
            xmlOrderNote.DisplayToCustomer = false;
            xmlOrderNote.DownloadId = OrderNoteAttachment.Id;
            xmlOrderNote.CreatedOnUtc = DateTime.UtcNow;

            order.OrderNotes.Add(xmlOrderNote);

            this._orderService.UpdateOrder(order);

            return xmlOrderNote;
        }

        private Download SaveXmlFile(byte[] xmlBinary, string documentType) {
            Download OrderNoteAttachment = new Download();
            OrderNoteAttachment.ContentType = "text/xml";
            OrderNoteAttachment.DownloadBinary = xmlBinary;
            OrderNoteAttachment.DownloadGuid = Guid.NewGuid();
            OrderNoteAttachment.DownloadUrl = null;
            OrderNoteAttachment.Extension = ".xml";
            OrderNoteAttachment.Filename = documentType.Replace(" ", "");
            OrderNoteAttachment.UseDownloadUrl = false;

            _downloadService.InsertDownload(OrderNoteAttachment);

            return OrderNoteAttachment;
        }

        private XmlDocument SerializeTransactionToXmlDocument(Transaction transaction) {
            XmlDocument XmlDoc = new XmlDocument();

            XmlDeclaration Declaration = XmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlDoc.AppendChild(Declaration);

            XPathNavigator XPathNav = XmlDoc.CreateNavigator();

            using (XmlWriter writer = XPathNav.AppendChild()) {
                XmlSerializer Serializer = new XmlSerializer(typeof(Transaction));

                XmlSerializerNamespaces EmptyNamespace = new XmlSerializerNamespaces();
                EmptyNamespace.Add("", "");

                Serializer.Serialize(writer, transaction, EmptyNamespace);
            }

            return XmlDoc;
        }

        private static byte[] GenerateBinaryFromXmlDoc(XmlDocument xmlDoc) {
            byte[] xmlBinary = null;

            using (MemoryStream memory = new MemoryStream()) {
                using (StreamWriter writer = new StreamWriter(memory)) {
                    writer.Write(xmlDoc.OuterXml);
                    writer.Flush();
                    memory.Flush();

                    xmlBinary = memory.ToArray();
                }
            }

            return xmlBinary;
        }
    }
}
