﻿using Nop.Core.Domain.Orders;
using OldhamChem.EDI.Common.Models.Camelot3PL;
using System.Xml;

namespace Nop.Services.EDI {
    public partial interface IEdiService
    {
        Transaction BuildShipmentTransactionFromOrder(int orderId);

        void TransmitTransaction(int orderId, Transaction transaction);
    }
}
