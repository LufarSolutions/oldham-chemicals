﻿using System;
using System.Linq;

namespace OldhamChem.BackgroundJobs {
    class Program
    {
        private static Jobs.Orders OrderJobs = new Jobs.Orders();
        private static Jobs.Punchout PunchoutJobs = new Jobs.Punchout();

        static void Main(string[] args)
        {
            string MenuOption = null;

            if (args == null || args.Length == 0)
            {
                DisplayMenu();

                MenuOption = Console.ReadLine();
                
                while (MenuOption.ToUpper() != "X")
                {
                    ExecuteMenuOption(MenuOption);

                    DisplayMenu();

                    MenuOption = Console.ReadLine();
                }
            }
            else
            {
                MenuOption = args.First();

                ExecuteMenuOption(MenuOption);
            }
        }

        private static void DisplayMenu()
        {
            Console.WriteLine("Oldham Chemicals Background Job Application");
            Console.WriteLine("Enter a menu option. Type 'X' to quit.");
            Console.WriteLine();
            Console.WriteLine("1. Update Pending Orders to Processing");
            Console.WriteLine("2. Update Processing Orders to Completed");
            Console.WriteLine("3. Send Punchout Order Confirmations");
            Console.WriteLine("4. Convert Online Orders into ERP");
            Console.WriteLine("5. Send Punchout Invoices");
            Console.WriteLine("6. Update Orders In Pending Invoice Status");
            Console.WriteLine("7. Update Orders In Partial InvoiceStatus");
            Console.WriteLine("8. Send Punchout Ship Notices");
            Console.WriteLine("X. Exit");
        }

        private static void ExecuteMenuOption(string menuOption)
        {
            string mainMenuOption;
            int? menuParameter = null;

            if (menuOption.Contains("-")) 
            {
                var menuParameters = menuOption.Split('-');
                mainMenuOption = menuParameters[0].Trim();
                menuParameter = int.Parse(menuParameters[1].Trim());
            }
            else 
            {
                mainMenuOption = menuOption;
            }

            switch (mainMenuOption)
            {                 
                case "1":
                    OrderJobs.UpdateOrdersInPendingStatusToProcessingStatus();
                    break;
                case "2":
                    OrderJobs.UpdateOrdersInProcessingStatusToCompletedStatus();
                    break;
                case "3":
                    // menuParameter should be an OrderId
                    PunchoutJobs.SendOrderConfirmations(menuParameter);
                    break;
                case "4":
                    OrderJobs.QueueOrdersInErp();
                    break;
                case "5":
                    PunchoutJobs.SendInvoices(menuParameter);
                    break;
                case "6":
                    OrderJobs.UpdateOrdersInPendingInvoiceStatus(menuParameter);
                    break;
                case "7":
                    OrderJobs.UpdateOrdersInPartialInvoiceStatus();
                    break;
                case "8":
                    // menuParameter should be a ShipmentId
                    PunchoutJobs.SendShipNotices(menuParameter);
                    break;
                case "9":
                    OrderJobs.SendOrdersToExternalWarehouses();
                    break;
                default:
                    break;
            }            
        }
    }
}
