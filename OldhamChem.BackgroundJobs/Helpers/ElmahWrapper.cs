﻿using Elmah;
using System;
using System.Web;

namespace OldhamChem.BackgroundJobs.Helpers
{
    public class ElmahWrapper
    {
        private static HttpApplication httpApplication = null;
        private static ErrorFilterConsole errorFilter = new ErrorFilterConsole();

        public static ErrorMailModule ErrorEmail = new ErrorMailModule();
        public static ErrorLogModule ErrorLog = new ErrorLogModule();
        //public static ErrorTweetModule ErrorTweet = new ErrorTweetModule();

        public static void LogToElmah(Exception ex) {
            LogToElmah(null, ex);
        }

        public static void LogToElmah(string message, Exception ex)
        {
            if (HttpContext.Current != null)
            {
                if (!String.IsNullOrWhiteSpace(message)) {
                    ex.Data.Add("CustomeMessage", message);
                }

                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            else
            {
                if (httpApplication == null)
                    InitNoContext();

                ErrorSignal.Get(httpApplication).Raise(ex);
            }
        }

        private static void InitNoContext()
        {
            httpApplication = new HttpApplication();
            errorFilter.Init(httpApplication);

            (ErrorEmail as IHttpModule).Init(httpApplication);
            errorFilter.HookFiltering(ErrorEmail);

            (ErrorLog as IHttpModule).Init(httpApplication);
            errorFilter.HookFiltering(ErrorLog);

            //(ErrorTweet as IHttpModule).Init(httpApplication);  
        }

        private class ErrorFilterConsole : ErrorFilterModule
        {
            public void HookFiltering(IExceptionFiltering module)
            {
                module.Filtering += new ExceptionFilterEventHandler(base.OnErrorModuleFiltering);
            }
        }
    }
}