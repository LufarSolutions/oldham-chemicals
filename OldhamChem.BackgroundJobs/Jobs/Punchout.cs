﻿using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Infrastructure;
using Nop.Services.Customers;
using Nop.Services.ERP;
using Nop.Services.Orders;
using Nop.Services.Shipping;
using OldhamChem.BackgroundJobs.Helpers;
using OldhamChem.Punchout.Common.Helpers;
using OldhamChem.Punchout.Common.Models;
using RestSharp;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;

namespace OldhamChem.BackgroundJobs.Jobs
{
    public class Punchout : BaseJob
    {
        private cXMLHelper cXMLHelper;
        private NopHelper nopHelper;
        private IOrderService OrderService;
        private IOrderProcessingService OrderProcessingService;
        private IErpService ErpService;
        private IShipmentService ShipmentService;

        public Punchout()
        {
            cXMLHelper = new cXMLHelper();
            nopHelper = new NopHelper();
            this.OrderService = EngineContext.Current.Resolve<IOrderService>();
            this.ErpService = EngineContext.Current.Resolve<IErpService>();
            this.OrderProcessingService = EngineContext.Current.Resolve<IOrderProcessingService>();
            this.ShipmentService = EngineContext.Current.Resolve<IShipmentService>();
        }

        public List<Order> GetOrdersToInvoiceThroughPunchout(int? orderId)
        {
            List<Order> PunchoutOrders = new List<Order>();

            if (orderId.HasValue)
            {
                var order = nopHelper.OrderService.GetOrderById(orderId.Value);
                PunchoutOrders.Add(order);
            }
            else
            {
                CustomerSearchParameters SearchParameters = new CustomerSearchParameters();
                SearchParameters.Roles.Add(NopHelper.SYSTEM_NAME_FOR_PUNCHOUT_ROLE);

                int CustomerCount = 0;

                List<Customer> PunchoutCustomers = nopHelper
                    .CustomerService
                    .SearchCustomers(null, null, null, SearchParameters, null, out CustomerCount)
                    .ToList();

                foreach (var punchoutCustomer in PunchoutCustomers)
                {
                    var sendInvoiceRequest = this.nopHelper.GetSendConfirmInvoiceSetting(punchoutCustomer);

                    //Get Orders Where Punchout User, In ERP, Order Status: Completed, Payment Status: Pending
                    List<Order> punchoutOrdersForCustomer = nopHelper
                        .OrderService
                        .SearchOrders(customerId: punchoutCustomer.Id, orderNotInErp: false, orderInvoicedInErp: true, os: OrderStatus.Complete, ps: PaymentStatus.Pending, storeId: this.StoreId)
                        .ToList();

                    if (!punchoutOrdersForCustomer.IsNullOrEmpty())
                    {
                        // If Invoice Requests are enabled, send invoices for orders.
                        // This will allow the trigger for when invoices are sent to be changed in the admin UI
                        if (sendInvoiceRequest)
                        {
                            PunchoutOrders.AddRange(punchoutOrdersForCustomer);
                        }
                    }
                }
            }

            return PunchoutOrders;
        }

        public List<Order> GetPunchoutOrdersToConfirm(int? orderId)
        {
            List<Order> PunchoutOrders = new List<Order>();

            if (orderId.HasValue)
            {
                var order = nopHelper.OrderService.GetOrderById(orderId.Value);
                PunchoutOrders.Add(order);
            }
            else
            {
                var PunchoutCustomers = nopHelper.GetPunchoutCustomers();

                foreach (var PunchoutCustomer in PunchoutCustomers)
                {
                    List<Order> PunchoutOrdersForCustomer = nopHelper
                        .OrderService
                        .SearchOrders(customerId: PunchoutCustomer.Id, orderNotInErp: false, os: OrderStatus.Processing, storeId: this.StoreId)
                        .ToList();

                    if (!PunchoutOrdersForCustomer.IsNullOrEmpty())
                    {
                        var ordersNotConfirmed = PunchoutOrdersForCustomer.Where(w => !w.OrderNotes.Any(a => a.Note == "Punchout Order Confirmation")).ToList();
                        // Only get orders that have not previously been confirmed (i.e. don't have an a note containing the cXML for the order confirmation.)
                        PunchoutOrders.AddRange(ordersNotConfirmed);
                    }
                }
            }

            return PunchoutOrders;
        }

        public List<Shipment> GetPunchoutShipmentsForNotification(int? shipmentId)
        {
            var shipmentsForNotification = new List<Shipment>();

            if (shipmentId.HasValue)
            {
                var shipment = ShipmentService.GetShipmentById(shipmentId.Value);

                if (!shipment.PunchoutShipmentNotificationDateUtc.HasValue)
                {
                    shipmentsForNotification.Add(shipment);
                }
            }
            else
            {
                var PunchoutCustomers = nopHelper.GetPunchoutCustomers();

                foreach (var punchoutCustomer in PunchoutCustomers)
                {
                    //Get Shipments Where Punchout User, and no notification sent
                    var shipmentsForCustomer = ShipmentService.GetAllShipments(customerId: punchoutCustomer.Id, hasPunchoutShipmentNotification: false);

                    if (!shipmentsForCustomer.IsNullOrEmpty())
                    {
                        shipmentsForNotification.AddRange(shipmentsForCustomer);
                    }
                }
            }

            return shipmentsForNotification;
        }

        public void SendInvoices(int? orderId)
        {
            List<Order> PunchoutOrders = GetOrdersToInvoiceThroughPunchout(orderId);

#if DEBUG
            TextWriter tw = new StreamWriter(@"C:\Temp\PunchoutOrdersToInvoice.txt");

            foreach (var order in PunchoutOrders.OrderBy(ob => ob.Id))
                tw.WriteLine(order.Id.ToString());

            tw.Close();
#endif

            foreach (var punchoutOrder in PunchoutOrders)
            {
                try
                {
                    var cXmlInvoices = cXMLHelper.BuildInvoiceRequestForOrder(punchoutOrder);
                    foreach (var cXmlInvoice in cXmlInvoices)
                    {
                        string PunchoutTransactionEndpoint = nopHelper.GetCustomersTransactionEndpoint(punchoutOrder.Customer);

                        XmlDocument cXMLAsXml = cXMLHelper.SerializeCxmlToXmlDocument(cXmlInvoice, cXMLDocumentType.Invoice);

                        var sendCxml = this.nopHelper.GetSendConfirmInvoiceSetting(punchoutOrder.Customer);

                        IRestResponse response = new RestResponse()
                        {
                            StatusCode = HttpStatusCode.OK
                        };

                        if (sendCxml)
                        {
                            response = cXMLHelper.SendcXML(cXMLAsXml, PunchoutTransactionEndpoint);
                        }

                        var invoiceId = cXmlInvoice.Request.InvoiceDetailRequest.InvoiceDetailRequestHeader.invoiceID;

                        nopHelper.SavecXMLRequestAsOrderNote(punchoutOrder, cXMLAsXml, $"Punchout Invoice (#{invoiceId})");

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            if (punchoutOrder.InvoiceStatusId == (int)InvoiceStatus.Complete)
                                UpdateOnlinePaymenStatus(punchoutOrder, PaymentStatus.Authorized);
                        }
                        else
                        {
                            OrderNote CxmlOrderNote = new OrderNote();

                            CxmlOrderNote.Note = $"Punchout Error: {response.ErrorMessage}";
                            CxmlOrderNote.DisplayToCustomer = false;
                            CxmlOrderNote.CreatedOnUtc = DateTime.UtcNow;

                            punchoutOrder.OrderNotes.Add(CxmlOrderNote);

                            this.OrderService.UpdateOrder(punchoutOrder);
                        }
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = $"SendInvoices: Failed to send cXML Invoices for order ({punchoutOrder.Id.ToString()}).";
                    ElmahWrapper.LogToElmah(errorMessage, ex);
                }
            }
        }

        public void SendOrderConfirmations(int? orderId)
        {
            List<Order> PunchoutOrders = GetPunchoutOrdersToConfirm(orderId);

            foreach (var punchoutOrder in PunchoutOrders)
            {
                try
                {
                    cXML cxml = cXMLHelper.BuildConfirmationRequestForOrder(punchoutOrder);

                    string PunchoutTransactionEndpoint = nopHelper.GetCustomersTransactionEndpoint(punchoutOrder.Customer);

                    XmlDocument cXMLAsXml = cXMLHelper.SerializeCxmlToXmlDocument(cxml, cXMLDocumentType.OrderConfirmation);

                    var sendCxml = this.nopHelper.GetSendConfirmInvoiceSetting(punchoutOrder.Customer);

                    IRestResponse response = new RestResponse()
                    {
                        StatusCode = HttpStatusCode.OK
                    };

                    if (sendCxml)
                    {
                        response = cXMLHelper.SendcXML(cXMLAsXml, PunchoutTransactionEndpoint);
                    }

                    nopHelper.SavecXMLRequestAsOrderNote(punchoutOrder, cXMLAsXml, "Punchout Order Confirmation");

                    //Save the response if in error.                
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        OrderNote CxmlOrderNote = new OrderNote();

                        CxmlOrderNote.Note = $"Punchout Error: {response.ErrorMessage}";
                        CxmlOrderNote.DisplayToCustomer = false;
                        CxmlOrderNote.CreatedOnUtc = DateTime.UtcNow;

                        punchoutOrder.OrderNotes.Add(CxmlOrderNote);

                        this.OrderService.UpdateOrder(punchoutOrder);
                    }

                }
                catch (Exception ex)
                {
                    string errorMessage = $"SendOrderConfirmations: Failed to send cXML Order Confirmation for order ({punchoutOrder.Id.ToString()}).";
                    ElmahWrapper.LogToElmah(errorMessage, ex);
                }
            }
        }

        public void SendShipNotices(int? shipmentId)
        {
            var shipments = this.GetPunchoutShipmentsForNotification(shipmentId);

            if (!shipments.IsNullOrEmpty())
            {
                foreach(var shipment in shipments)
                {
                    try
                    {
                        var cxml = cXMLHelper.BuildNotificationForShipment(shipment);

                        string PunchoutTransactionEndpoint = nopHelper.GetCustomersTransactionEndpoint(shipment.Order.Customer);

                        XmlDocument cXMLAsXml = cXMLHelper.SerializeCxmlToXmlDocument(cxml, cXMLDocumentType.ShipNotice);

                        var sendCxml = this.nopHelper.GetSendShipNoticeSetting(shipment.Order.Customer);

                        IRestResponse response = new RestResponse()
                        {
                            StatusCode = HttpStatusCode.OK
                        };

                        if (sendCxml)
                        {
                            response = cXMLHelper.SendcXML(cXMLAsXml, PunchoutTransactionEndpoint);
                        }

                        nopHelper.SavecXMLRequestAsOrderNote(shipment.Order, cXMLAsXml, "Punchout Ship Notice");

                        //Save the response if in error.                
                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            OrderNote CxmlOrderNote = new OrderNote();

                            CxmlOrderNote.Note = $"Punchout Error: {response.ErrorMessage}";
                            CxmlOrderNote.DisplayToCustomer = false;
                            CxmlOrderNote.CreatedOnUtc = DateTime.UtcNow;

                            shipment.Order.OrderNotes.Add(CxmlOrderNote);

                            this.OrderService.UpdateOrder(shipment.Order);
                        }
                        else
                        {
                            // Update the shipment notification date so that it is processed again
                            shipment.PunchoutShipmentNotificationDateUtc = DateTime.UtcNow;
                            this.ShipmentService.UpdateShipment(shipment);
                        }
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = $"SendShipNotices: Failed to send cXML Ship Notice for shipment ({shipment.Id.ToString()}).";
                        ElmahWrapper.LogToElmah(errorMessage, ex);
                    }
                }
            }
        }

        private void UpdateOnlinePaymenStatus(Order order, PaymentStatus status)
        {
            var orderToAuthorize = this.OrderService.GetOrderById(order.Id);
            orderToAuthorize.PaymentStatusId = (int)status;

            //add a note
            orderToAuthorize.OrderNotes.Add(new OrderNote
            {
                Note = "Order has been marked as authorized",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });

            OrderService.UpdateOrder(orderToAuthorize);
        }
    }
}
