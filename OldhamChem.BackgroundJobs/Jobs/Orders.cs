﻿using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Infrastructure;
using Nop.Data;
using Nop.Services.Customers;
using Nop.Services.EDI;
using Nop.Services.ERP;
using Nop.Services.Orders;
using Nop.Web.Framework;
using OldhamChem.BackgroundJobs.Helpers;
using OldhamChem.Punchout.Common.Helpers;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace OldhamChem.BackgroundJobs.Jobs
{
    public class Orders : BaseJob
    {
        private ICustomerService _customerService;
        private IEdiService _ediService;
        private IErpService _erpService;
        private IOrderService _orderService;
        private IOrderProcessingService _orderProcessingService;
        private NopHelper nopHelper;

        public Orders()
        {
            nopHelper = new NopHelper();

            // Initialize the Order Service
            this._customerService = EngineContext.Current.Resolve<ICustomerService>();
            this._ediService = EngineContext.Current.Resolve<IEdiService>();
            this._erpService = EngineContext.Current.Resolve<IErpService>();
            this._orderProcessingService = EngineContext.Current.Resolve<IOrderProcessingService>();
            this._orderService = EngineContext.Current.Resolve<IOrderService>();
        }

        public void QueueOrdersInErp()
        {
            IPagedList<Order> OrdersForErp = _orderService
                .SearchOrders(
                    orderNotInErp: true,
                    os: OrderStatus.Pending);

            if (!OrdersForErp.IsNullOrEmpty())
            {
                foreach (var Order in OrdersForErp)
                {
                    List<string> ValidationErrors = _erpService.ValidateOrderForERPConversion(Order);

                    if (ValidationErrors.IsNullOrEmpty())
                    {
                        _erpService.QueueOrderInERPById(Order.Id);
                    }
                }
            }
        }

        public void SendOrdersToExternalWarehouses()
        {
            //1. Get Orders that need to go to external warehouses
            var orders = GetOrdersInErpByWarehouseAndShippingStatus(ShippingStatus.NotYetShipped, 101);

            foreach (var order in orders)
            {
                //2. Build External Warehouse Payload
                var shipmentTransaction = _ediService.BuildShipmentTransactionFromOrder(order.Id);

                //4. Send Payload to External Warehouse FTP
                _ediService.TransmitTransaction(order.Id, shipmentTransaction);

                //5. Update Order Shipping Status
                UpdateOnlineOrderShippingStatus(order, ShippingStatus.PreparingForShipment);
            }

            throw new NotImplementedException();
        }

        public void UpdateOrdersInPartialInvoiceStatus()
        {
            //Get Partial Orders and Update
            var partialOrders = GetOrdersInErpByInvoiceStatus(new List<InvoiceStatus> { InvoiceStatus.Partial });

            foreach (var partialOrder in partialOrders)
            {
                try
                {
                    var poNumber = this.nopHelper.GetPurchaseOrderNumber(partialOrder);
                    var orderWasChangedAfterReciept = false;

                    if (!String.IsNullOrWhiteSpace(poNumber))
                    {
                        var invoices = GetInvoicesForPONumbers(partialOrder.Customer.CustomerNumber, new List<string>() { poNumber });

                        if (!invoices.IsNullOrEmpty())
                        {

                            List<string> currentInvoiceIds = new List<string>();

                            if (partialOrder.ERPInvoiceId.Contains("|"))
                            {
                                currentInvoiceIds = partialOrder.ERPInvoiceId.Split('|').OrderBy(ob => ob).ToList();
                            }
                            else
                            {
                                currentInvoiceIds.Add(partialOrder.ERPInvoiceId);
                            }

                            var distinctOrderItemSkus = partialOrder.OrderItems.Select(s => s.Product.Sku).Distinct();
                            var distinctInvoiceItemSkus = invoices.SelectMany(sm => sm.InvoiceItems.Where(w => !String.IsNullOrWhiteSpace(w.ITEM_CODE))).Select(s => s.ITEM_CODE.Trim()).Distinct();
                            orderWasChangedAfterReciept = distinctInvoiceItemSkus.Any(a => !distinctOrderItemSkus.Contains(a));

                            //Check for new invoice and invoiced items for the current order
                            var newInvoices = invoices
                                .Where(w => w.CUST_PO_NUM.Trim() == poNumber && !currentInvoiceIds.Contains(w.INV_NO.Trim()))
                                .ToList();

                            if (!newInvoices.IsNullOrEmpty())
                            {

                                foreach (var newInvoice in newInvoices)
                                {
                                    partialOrder.ERPInvoiceId = partialOrder.ERPInvoiceId + "|" + newInvoice.INV_NO;
                                }

                                // Loop thru items that have not been completely fulfilled.
                                foreach (var orderItem in partialOrder.OrderItems.Where(w => w.QuantityInvoiced < w.Quantity))
                                {

                                    //Do the new invoices contain the item?
                                    var newInvoiceItems = newInvoices
                                        .Where(a => a.InvoiceItems.Any(a1 => !String.IsNullOrWhiteSpace(a1.ITEM_CODE) && a1.ITEM_CODE.Trim() == orderItem.Product.Sku))
                                        .SelectMany(s => s.InvoiceItems)
                                        .ToList();

                                    if (!newInvoiceItems.IsNullOrEmpty())
                                    {
                                        orderItem.QuantityInvoiced += newInvoiceItems.Sum(s => Decimal.ToInt32(s.SHIP_TODAY.Value));
                                    }
                                }
                            }

                            // Run this regardless of new invoices, if for whatever reason the order has been modified. It may no longer be a partial
                            var isStillPartial = partialOrder.OrderItems.Any(a => a.QuantityInvoiced < a.Quantity);
                            if (!isStillPartial || orderWasChangedAfterReciept)
                            {
                                partialOrder.InvoiceStatusId = (int)InvoiceStatus.Complete;

                                if (orderWasChangedAfterReciept)
                                {
                                    partialOrder.PaymentStatusId = (int)PaymentStatus.Authorized;
                                }
                            }

                            this._orderService.UpdateOrder(partialOrder);
                        }
                    }

                }
                catch (Exception ex)
                {
                    string errorMessage = $"UpdateOrdersInPartialInvoiceStatus: Failed to updated invoice status for order ({partialOrder.Id.ToString()}).";
                    ElmahWrapper.LogToElmah(errorMessage, ex);
                }
            }
        }

        public void UpdateOrdersInPendingInvoiceStatus(int? orderId)
        {
            var PunchoutCustomers = nopHelper.GetPunchoutCustomers();

            //Get Orders with Pending Status and Update
            var pendingOrders = new List<Order>();

            if (orderId.HasValue)
            {
                var order = _orderService.GetOrderById(orderId.Value);

                if (order.InvoiceStatusId == (int)InvoiceStatus.Pending)
                {
                    pendingOrders.Add(order);
                }
            }
            else
            {
                pendingOrders = GetOrdersInErpByInvoiceStatus(new List<InvoiceStatus> { InvoiceStatus.Pending });
            }

            var orderNumbers = pendingOrders
                .Select(s => s.ERPOrderId)
                .ToList();
                        
            var invoices = GetInvoicesForOrders(orderNumbers);
            var orderWasChangedAfterReciept = false;

            if (!invoices.IsNullOrEmpty())
            {
                var invoiceOrderNumbers = invoices.Select(s => s.ORDER_NO.Trim()).Distinct().ToList();

                foreach (var order in pendingOrders.Where(w => invoiceOrderNumbers.Contains(w.ERPOrderId)))
                {
                    // Try to process each order and catch if it fails on any so to not bomb out for the remaining orders.
                    try
                    {
                        var correspondingInvoices = invoices
                            .Where(f => f.ORDER_NO.Trim() == order.ERPOrderId)
                            .OrderBy(ob => ob.INV_DATE)
                            .ToList();

                        if (!correspondingInvoices.IsNullOrEmpty())
                        {
                            var distinctOrderItemSkus = order.OrderItems.Select(s => s.Product.Sku).Distinct();
                            var distinctInvoiceItemSkus = correspondingInvoices.SelectMany(sm => sm.InvoiceItems.Where(w => !String.IsNullOrWhiteSpace(w.ITEM_CODE))).Select(s => s.ITEM_CODE.Trim()).Distinct();
                            orderWasChangedAfterReciept = distinctInvoiceItemSkus.Any(a => !distinctOrderItemSkus.Contains(a));

                            var correspondingInvoice = correspondingInvoices.First();

                            var subTotal = correspondingInvoice
                                .InvoiceItems
                                .Where(w => !String.IsNullOrWhiteSpace(w.ITEM_CODE))
                                .Sum(s => (decimal)s.EXT_PRICE);

                            //If the order status has not previously been set to complete for whatever reason, update it
                            if (order.OrderStatus != OrderStatus.Complete)
                            {
                                order.OrderStatusId = (int)OrderStatus.Complete;
                            }

                            //UPDATE online order totals
                            order.ERPInvoiceId = correspondingInvoice.INV_NO;
                            order.OrderShippingInclTax = correspondingInvoice.FREIGHT_AMT.Value + correspondingInvoice.SALES_TAX_AMT.Value;
                            order.OrderShippingExclTax = correspondingInvoice.FREIGHT_AMT.Value;
                            order.OrderSubtotalInclTax = subTotal + correspondingInvoice.SALES_TAX_AMT.Value;
                            order.OrderSubtotalExclTax = subTotal;
                            order.OrderTax = correspondingInvoice.SALES_TAX_AMT.Value;
                            order.OrderTotal = correspondingInvoice.INVOICE_AMT.Value;

                            var workingInvoiceItems = correspondingInvoice.InvoiceItems.Where(w => !String.IsNullOrWhiteSpace(w.ITEM_CODE)).ToList();

                            foreach (var orderItem in order.OrderItems.OrderBy(ob => ob.Product.Sku).ThenBy(tb => tb.Quantity))
                            {
                                //Check to see if the item is backordered
                                var correspondingInvoiceItems = workingInvoiceItems.Where(w => w.SHIP_TODAY.HasValue && w.SHIP_TODAY.Value > 0 && w.ITEM_CODE.Trim() == orderItem.Product.Sku);

                                if (!correspondingInvoiceItems.IsNullOrEmpty())
                                {
                                    Thoroughbred.Data.Entities.InvoiceItem correspondingInvoiceItem = null;

                                    //TODO: Have to check if the item is on there multiple times if so, try to marry the correct item then remove it from the list
                                    if (correspondingInvoiceItems.Count() > 1)
                                    {
                                        //Check to see if an item has been fulfilled that matches the same quantity
                                        correspondingInvoiceItem = correspondingInvoiceItems.FirstOrDefault(f => f.SHIP_TODAY.Value == (decimal)orderItem.Quantity);

                                        // If an item has not been fulfilled in the same quantity, find one that has been fulfilled with a lesser quantity
                                        if (correspondingInvoiceItem == null)
                                        {
                                            correspondingInvoiceItem = correspondingInvoiceItems.OrderByDescending(ob => ob.SHIP_TODAY).FirstOrDefault(f => f.SHIP_TODAY.Value < (decimal)orderItem.Quantity);
                                        }
                                    }
                                    else
                                    {
                                        correspondingInvoiceItem = correspondingInvoiceItems.FirstOrDefault();
                                    }

                                    if (correspondingInvoiceItem != null)
                                    {
                                        orderItem.QuantityInvoiced = Decimal.ToInt32(correspondingInvoiceItem.SHIP_TODAY.Value);

                                        // Remove any processed invoice item. This will help with multiple line items of the same item and speed up the processing.
                                        workingInvoiceItems.RemoveAll(ra => ra.ITEM_CODE.Trim() == orderItem.Product.Sku);
                                    }
                                }
                            }

                            var isPunchoutCustomer = PunchoutCustomers.Any(a => a.Id == order.CustomerId);

                            //Only care about tracking partial invoices for punchout customers. For all other just move them to a complete invoice status
                            //There is not enough replicated tbred data to build logic to track non-punchout customer orders and it's not really necessary
                            var isPartial = isPunchoutCustomer && order.OrderItems.Any(a => a.QuantityInvoiced < a.Quantity);
                            if (!isPartial || orderWasChangedAfterReciept)
                            {
                                order.InvoiceStatusId = (int)InvoiceStatus.Complete;

                                if (orderWasChangedAfterReciept)
                                {
                                    order.PaymentStatusId = (int)PaymentStatus.Authorized;
                                }
                            }
                            else
                            {
                                order.InvoiceStatusId = (int)InvoiceStatus.Partial;
                            }

                            this._orderService.UpdateOrder(order);
                        }
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = $"UpdateOrdersInPendingInvoiceStatus: Failed to updated invoice status for order ({order.Id.ToString()}).";
                        ElmahWrapper.LogToElmah(errorMessage, ex);
                    }
                }
            }
        }

        public void UpdateOrdersInPendingStatusToProcessingStatus()
        {
            List<Order> orders = GetOrdersInErpByStatus(OrderStatus.Pending);

            foreach (var order in orders)
            {
                // Get the corresponding order in the ERP system
                var erpOrder = _erpService.GetOrderByOrderNumber(order.ERPOrderId);

                // Only update the status once the order exists in the replication data base and has been printed/confirmed
                if (erpOrder != null)
                {
                    if (!String.IsNullOrWhiteSpace(erpOrder.REPRINT))
                    {
                        //TODO: Possibly need to Reconcile Order prior to updating (verify order items, quantities, and prices. Update the online order to match the order in ERP

                        UpdateOnlineOrderStatus(order, OrderStatus.Processing);
                    }
                }
                else if (order.DateQueuedForERP < DateTime.UtcNow.AddDays(-2))
                {
                    //if it was submitted to the ERP over 2 days ago and the order is missing check for an invoice. If no Invoice or Order delete the order.
                    var invoice = _erpService.GetInvoiceForOrder(order.ERPOrderId);

                    if (invoice == null)
                    {
                        order.OrderNotes.Add(new OrderNote
                        {
                            Note = "Order has been deleted",
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow
                        });

                        order.Deleted = true;

                        _orderService.UpdateOrder(order);
                    }
                }
            }
        }

        public void UpdateOrdersInProcessingStatusToCompletedStatus()
        {
            List<Order> orders = GetOrdersInErpByStatus(OrderStatus.Processing);

            foreach (var order in orders)
            {
                if (order.ERPInvoiceId.HasValue())
                {
                    UpdateOnlineOrderStatus(order, OrderStatus.Complete);
                }
            }
        }

        #region Private Methods

        private List<Thoroughbred.Data.Entities.Invoice> GetInvoicesForOrders(List<string> orderNumbers)
        {
            Thoroughbred.Data.Entities.ThoroughbredDbContext DbContext = new Thoroughbred.Data.Entities.ThoroughbredDbContext();
            return DbContext
                .Invoices
                .IncludeProperties(i => i.InvoiceItems)
                .Where(w => orderNumbers.Contains(w.ORDER_NO) && w.InvoiceItems.Sum(s => s.SHIP_TODAY) != 0)
                .ToList();
        }

        private List<Thoroughbred.Data.Entities.Invoice> GetInvoicesForPONumbers(string customerNumber, List<string> poNumbers)
        {
            Thoroughbred.Data.Entities.ThoroughbredDbContext DbContext = new Thoroughbred.Data.Entities.ThoroughbredDbContext();
            return DbContext
                .Invoices
                .IncludeProperties(i => i.InvoiceItems)
                .Where(w => poNumbers.Contains(w.CUST_PO_NUM) && w.CustomerNumber == customerNumber)
                .ToList();
        }

        private List<Order> GetOrdersInErpByStatus(OrderStatus orderStatus)
        {
            return _orderService
                .SearchOrders(orderNotInErp: false, os: orderStatus)
                .ToList();
        }

        private List<Order> GetOrdersInErpByInvoiceStatus(List<InvoiceStatus> invoiceStatuses)
        {
            return _orderService
                .SearchOrders(orderNotInErp: false, invoiceStatuses: invoiceStatuses)
                .OrderBy(ob => ob.DateQueuedForERP)
                .ToList();
        }

        private List<Order> GetOrdersInErpByWarehouseAndShippingStatus(ShippingStatus shippingStatus, int warehouseId)
        {

            return _orderService
                .SearchOrders(orderNotInErp: false, ss: shippingStatus, warehouseId: warehouseId)
                .ToList();
        }

        private void UpdateOnlineOrderShippingStatus(Order order, ShippingStatus status)
        {
            //For some reason this did not work. Trying very explicit change.
            //OrderProcessingService.SetOrderStatus(order, status, false);

            order.ShippingStatusId = (int)status;

            //OrderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote
            {
                Note = string.Format("Shipping status has been edited. New status: {0}", status.ToString()),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });

            _orderService.UpdateOrder(order);
        }

        private void UpdateOnlineOrderStatus(Order order, OrderStatus status)
        {
            //For some reason this did not work. I think this is a context issue. Trying very explicit change.
            //OrderProcessingService.SetOrderStatus(order, status, false);

            order.OrderStatusId = (int)status;

            //OrderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote
            {
                Note = string.Format("Order status has been edited. New status: {0}", status.ToString()),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });

            _orderService.UpdateOrder(order);
        }

        #endregion
    }
}
