﻿using Nop.Core.Infrastructure;

namespace OldhamChem.BackgroundJobs.Jobs
{
    public class BaseJob
    {        
        private IEngine NopEngineContext;

        public int StoreId { get; private set; }

        public BaseJob()
        {
            // Initialize the NopEngine.
            NopEngineContext = EngineContext.Initialize(true);

            this.StoreId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["StoreId"]);
        }
    }
}
