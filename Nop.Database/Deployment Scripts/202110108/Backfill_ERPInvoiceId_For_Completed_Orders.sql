﻿UPDATE [Order]
SET
	ERPInvoiceId = (SELECT TOP 1 INV_NO FROM ThoroughbredReplication.dbo.Invoice WHERE ORDER_NO = ERPOrderId)
WHERE
	OrderStatusId = 30
	AND ERPInvoiceId IS NULL
	--AND DateQueuedForERP < '1/29/2021'
	AND Deleted = 0

UPDATE [Order]
SET
	InvoiceStatusId = 30
WHERE
	ERPInvoiceId IS NOT NULL
	AND InvoiceStatusId <> 30


UPDATE [Order]
SET
	OrderStatusId = 30,
	PaymentStatusId = 20,
	InvoiceStatusId = 30,
	ERPInvoiceId = '0000000'
WHERE
	(ERPInvoiceId IS NULL OR ERPInvoiceId = '0')
	AND ERPInvoiceId IS NULL
	AND (DateQueuedForERP < '1/1/2021' OR DateQueuedForERP IS NULL)
	AND Deleted = 0
