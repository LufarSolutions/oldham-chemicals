﻿DECLARE @StoreId int = 1

DECLARE @BillingAddressId int

SELECT @BillingAddressId = BillingAddressId FROM Store WHERE Id = @StoreId

IF @BillingAddressId IS NULL
BEGIN

	SET NOCOUNT ON

	INSERT INTO [Address] (
		Address1,
		City,
		Company,
		CreatedOnUtc,
		CountryId,
		Email,
		FaxNumber,
		PhoneNumber,
		Latitude,
		Longitude,
		StateProvinceId,
		ZipPostalCode)
	VALUES(
		'P.O. Box 18358',
		'Memphis',
		'Oldham Chemicals, Inc.',
		GETUTCDATE(),
		(SELECT Id FROM Country WHERE TwoLetterIsoCode = 'US'),
		'orders@oldhamchem.com',
		'9017948864',
		'8008885502',
		0,
		0,
		(SELECT Id FROM StateProvince WHERE Abbreviation = 'TN'),
		'38118')

	SET @BillingAddressId = SCOPE_IDENTITY();

	SET NOCOUNT OFF

	UPDATE Store SET BillingAddressId = @BillingAddressId WHERE Id = @StoreId
END
