﻿DECLARE @NoteDate datetime = getutcdate()

INSERT INTO OrderNote(
	OrderId,
	Note,
	DownloadId,
	DisplayToCustomer,
	CreatedOnUtc)
(SELECT
	Id,
	'Punchout Order Confirmation',
	0,
	0,
	@NoteDate
FROM [Order]
WHERE 
	CustomerId IN (246130, 11022812)
	AND OrderStatusId = 20
	AND DateQueuedForERP < '1/29/2021')

INSERT INTO OrderNote(
	OrderId,
	Note,
	DownloadId,
	DisplayToCustomer,
	CreatedOnUtc)
(SELECT
	Id,
	'Punchout Invoice',
	0,
	0,
	@NoteDateS
FROM [Order]
WHERE 
	CustomerId IN (246130, 11022812)
	AND ERPInvoiceId IS NOT NULL
	AND OrderStatusId = 30
	AND DateQueuedForERP < '1/29/2021')