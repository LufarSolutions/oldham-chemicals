﻿DECLARE @CustomerServiceRoleId int

SELECT @CustomerServiceRoleId = Id FROM CustomerRole WHERE Name = 'Customer Service'

IF NOT EXISTS (SELECT * FROM CustomerAccess_Mapping WHERE CustomerRoleId = @CustomerServiceRoleId)
BEGIN

	INSERT INTO CustomerAccess_Mapping(
		CustomerId,
		CustomerRoleId,
		AccessibleCustomerId)
	(SELECT
		--n.c.value('.', 'int'),
		CS.Id,
		@CustomerServiceRoleId,
		tbl.Id
	FROM (SELECT EntityId AS Id, CAST([Value] AS XML) as attribs FROM GenericAttribute WHERE [Key] = 'CustomCustomerAttributes') tbl
		outer apply tbl.attribs.nodes('/Attributes/CustomerAttribute[@ID="2"]/CustomerAttributeValue') as n(c)
		INNER JOIN CustomerAttributeValue ON CustomerAttributeValue.Id = n.c.value('.', 'int')
		INNER JOIN Customer AS CS ON CS.Email = CustomerAttributeValue.Name)
END