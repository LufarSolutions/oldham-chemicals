﻿IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Fields.CustomerServiceRepresentatives')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Fields.CustomerServiceRepresentatives',
		'Customer Service Representatives')
END

UPDATE LocaleStringResource
SET
	ResourceValue = REPLACE(ResourceValue, 'ANZIO', 'ERP')
WHERE
	ResourceValue LIKE '%ANZIO%'