﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

--:r ./UpdateOrderStatuses.sql

--:r ./PopulateERPFields.sql

:r ./AddNewLanguageResources.sql

:r ./Add_UnitOfMeasure_SpecificationAttribute_to_Products.sql