﻿DECLARE @UnitOfMeasureSpecificationAttributeId int

INSERT INTO SpecificationAttribute(
	Name,
	DisplayOrder)
VALUES(
	'Unit of Measure',
	0)

SELECT @UnitOfMeasureSpecificationAttributeId = Id FROM SpecificationAttribute WHERE Name = 'Unit of Measure'

INSERT INTO SpecificationAttributeOption(
	SpecificationAttributeId,
	Name,
	DisplayOrder)
(SELECT
	DISTINCT @UnitOfMeasureSpecificationAttributeId,
	UOM,
	0
FROM [ThoroughbredReplication].dbo.Item
WHERE
	UOM IS NOT NULL)


INSERT INTO Product_SpecificationAttribute_Mapping(
	ProductId,
	AttributeTypeId,
	SpecificationAttributeOptionId,
	CustomValue,
	AllowFiltering,
	ShowOnProductPage,
	DisplayOrder)
(SELECT
	Product.Id,
	0, 
	SAO.Id,
	'',
	0,
	0,
	0
FROM Product
	LEFT JOIN [ThoroughbredReplication].dbo.ITEM ON Product.Sku = RTRIM(ITEM.ITEM_CODE)
	LEFT JOIN SpecificationAttributeOption AS SAO ON SAO.SpecificationAttributeId = @UnitOfMeasureSpecificationAttributeId AND SAO.Name = ITEM.UOM
WHERE 
	ITEM.ITEM_CODE IS NOT NULL
	AND SAO.Id IS NOT NULL)

