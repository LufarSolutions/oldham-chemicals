﻿IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.ERPOrderProcessing')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.ERPOrderProcessing',
		'ANZIO Order Processing')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.ERPOrdersPendingProcessing')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.ERPOrdersPendingProcessing',
		'Orders Pending Processing')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.ERPOrdersQueuedForProcessing')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.ERPOrdersQueuedForProcessing',
		'Orders Queued In ANZIO')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Orders.List.OrderPlacedStartDate')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Orders.List.OrderPlacedStartDate',
		'Orders Placed Start Date')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Orders.List.OrderPlacedEndDate')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Orders.List.OrderPlacedEndDate',
		'Orders Placed End Date')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Reports')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Reports',
		'Reports')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Reports.InventoryReport')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Reports.InventoryReport',
		'Inventory Report')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Reports.Fields.Manufacturer')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Reports.Fields.Manufacturer',
		'Manufacturer')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Reports.Fields.Manufacturer.Hint')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Reports.Fields.Manufacturer.Hint',
		'Choose a manufacturer from the list of suggestions')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Reports.Fields.ProductNameOrNumber')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Reports.Fields.ProductNameOrNumber',
		'Product Name or #')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Reports.Fields.ProductNameOrNumber.Hint')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Reports.Fields.ProductNameOrNumber.Hint',
		'You may enter a product name or #')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Common.GenerateReport')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Common.GenerateReport',
		'Generate Report')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Reports.Fields.IncludeDiscontinuedItems')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Reports.Fields.IncludeDiscontinuedItems',
		'Include Discontinued Items')
END