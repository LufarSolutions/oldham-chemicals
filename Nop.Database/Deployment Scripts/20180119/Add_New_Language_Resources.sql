﻿IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'admin.customers.customers.favoriteproducts')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'admin.customers.customers.favoriteproducts',
		'Favorite Products')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'admin.label.product')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'admin.label.product',
		'Product')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'admin.label.store')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'admin.label.store',
		'Store')
END

UPDATE LocaleStringResource
SET
	ResourceValue = 'Go To Order #'
WHERE
	ResourceName = 'Admin.Orders.List.GoDirectlyToNumber'