﻿IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Address.Fields.ShipToNumber')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Address.Fields.ShipToNumber',
		'Ship To Number')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Catalog.Products.Fields.SpecialCustom')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Catalog.Products.Fields.SpecialCustom',
		'Custom Special')
END


IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Customers.Map')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Customers.Map',
		'Customer Locator Map')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Common.LoadMap')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Common.LoadMap',
		'Load Map')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Customers.Labels.Radius')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Customers.Labels.Radius',
		'Radius')
END

IF NOT EXISTS (SELECT LanguageId FROM LocaleStringResource WHERE ResourceName = 'Admin.Customers.Labels.Warehouse')
BEGIN
	INSERT INTO LocaleStringResource(
		LanguageId,
		ResourceName,
		ResourceValue)
	VALUES(
		1,
		'Admin.Customers.Labels.Warehouse',
		'Warehouse')
END