﻿UPDATE A
SET
	A.ShipToNumber = TCA.SHIP_TO_NO
FROM [Address] AS A
		INNER JOIN CustomerAddresses ON CustomerAddresses.Address_Id = A.Id	
		INNER JOIN Customer ON CustomerAddresses.Customer_Id = Customer.Id
		INNER JOIN StateProvince ON StateProvince.Id = A.StateProvinceId
		LEFT JOIN [40.84.189.183].ThoroughbredReplication.dbo.CustomerAddresses AS TCA ON TCA.CUST_SHIP_TO_ADDR1 = A.Address1
			AND TCA.CUST_SHIP_TO_CITY = A.City
			AND TCA.CUST_SHIP_TO_STATE = StateProvince.Abbreviation
			AND TCA.CUSTOMER_NO = Customer.CustomerNumber
WHERE
	ShipToNumber IS NULL