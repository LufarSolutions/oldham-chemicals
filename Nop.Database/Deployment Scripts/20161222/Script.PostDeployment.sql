﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

-- Change the paging options for the Manufacturer Product View
UPDATE Manufacturer
SET
	PageSize = 12
WHERE
	PageSize <> 12

UPDATE Manufacturer
SET
	PageSizeOptions = '12,24,36'
WHERE
	PageSizeOptions <> '12,24,36'

UPDATE Manufacturer
SET
	AllowCustomersToSelectPageSize = 1
WHERE
	AllowCustomersToSelectPageSize = 0