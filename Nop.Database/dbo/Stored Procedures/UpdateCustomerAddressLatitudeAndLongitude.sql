﻿CREATE PROCEDURE [dbo].[UpdateCustomerAddressLatitudeAndLongitude]
	@AddressId int,
	@Latitude decimal(10,7),
	@Longitude decimal(10,7)
AS
	
	UPDATE [Address]
	SET
		Latitude = @Latitude,
		Longitude = @Longitude
	WHERE
		Id = @AddressId
