﻿-- =============================================
-- Author:		Brandon Lewis
-- Description:	Returns a list of products for the provided manufacturer ID where the Full Description has not been entered
-- History:		(BLEWIS)	12/18/2016	Created
-- =============================================
CREATE PROCEDURE [dbo].[GetProductsWithoutFullDescriptionByManufacturer]
	@ManufacturerId int
AS

	SELECT
		*
	FROM Product
		INNER JOIN Product_Manufacturer_Mapping AS PMM ON PMM.ProductId = Product.Id
		INNER JOIN Manufacturer ON PMM.ManufacturerId = Manufacturer.Id
	WHERE
		Product.FullDescription IS NULL
		AND Product.Published = 1
		AND PMM.ManufacturerId = @ManufacturerId
	ORDER BY
		Product.Name

RETURN 0
