﻿CREATE PROCEDURE [dbo].[UpdateGroupedProductSku]

AS

	UPDATE Product
	SET
		Sku = SUBSTRING((SELECT ', ' + Sp.Sku AS [text()] FROM Product AS SP WHERE SP.ParentGroupedProductId = Product.Id AND Published = 1 ORDER BY SP.Sku FOR XML PATH ('')), 3, 1000)
	WHERE
		ProductTypeId = 10
		AND (Sku IS NULL OR LEN(Sku) = 0)