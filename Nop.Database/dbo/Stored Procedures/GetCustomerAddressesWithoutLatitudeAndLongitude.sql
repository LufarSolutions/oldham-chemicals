﻿CREATE PROCEDURE [dbo].[GetCustomerAddressesWithoutLatitudeAndLongitude]
AS

	SELECT TOP 50
		[Address].Id,
		[Address].Address1,
		[Address].Address2,
		[Address].City,
		StateProvince.Abbreviation,
		[Address].ZipPostalCode
	FROM CustomerAddresses
		INNER JOIN [Address] ON [Address].Id = CustomerAddresses.Address_Id
		INNER JOIN StateProvince ON StateProvince.Id = [Address].StateProvinceId
	WHERE
		([Address].Latitude IS NULL
		OR [Address].Longitude IS NULL)
		AND ShipToNumber IS NOT NULL
	ORDER BY
		CreatedOnUtc DESC
		
