﻿CREATE TABLE [dbo].[ContractPrice] (
    [Id]         INT             IDENTITY (1, 1) NOT NULL,
	[DateCreated] DATETIME NOT NULL DEFAULT getutcdate(),
	[DateUpdated] DATETIME, 
    [ProductId]  INT             NOT NULL,
    [StoreId]    INT             NOT NULL,
    [CustomerId] INT             NULL,
    [Quantity]   INT             NOT NULL,
    [Price]      DECIMAL (18, 4) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ContractPrice_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [ContractPrice_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([Id]) ON DELETE CASCADE
);

