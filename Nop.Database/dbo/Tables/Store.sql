﻿CREATE TABLE [dbo].[Store] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [Name]               NVARCHAR (400)  NOT NULL,
    [Url]                NVARCHAR (400)  NOT NULL,
    [SslEnabled]         BIT             NOT NULL,
    [SecureUrl]          NVARCHAR (400)  NULL,
    [Hosts]              NVARCHAR (1000) NULL,
    [DefaultLanguageId]  INT             NOT NULL,
    [DisplayOrder]       INT             NOT NULL,
    [CompanyName]        NVARCHAR (1000) NULL,
    [CompanyAddress]     NVARCHAR (1000) NULL,
    [CompanyFaxNumber]   NVARCHAR (1000) NULL,
    [CompanyPhoneNumber] NVARCHAR (1000) NULL,
    [CompanyVat]         NVARCHAR (1000) NULL,
    [Latitude]           DECIMAL (10, 7) NULL,
    [Longitude]          DECIMAL (10, 7) NULL,
    [StoreEmail]         NVARCHAR (1000) NULL,
    [StoreHours]         NVARCHAR (1000) NULL,
    [BillingAddressId] INT NULL, 
    CONSTRAINT [PK__Store__3214EC07B64037EC] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_Store_BillingAddress] FOREIGN KEY ([BillingAddressId]) REFERENCES [Address]([Id])
);

