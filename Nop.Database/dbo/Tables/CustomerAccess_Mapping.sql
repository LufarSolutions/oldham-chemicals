﻿CREATE TABLE [dbo].[CustomerAccess_Mapping]
(
	[Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [CustomerId] INT NOT NULL, 
    [CustomerRoleId] INT NOT NULL, 
    [AccessibleCustomerId] INT NOT NULL, 
    [DateCreated] DATETIME NOT NULL DEFAULT getutcdate(), 
    CONSTRAINT [FK_Customer_CustomerRoleAccess_Mapping_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [Customer](Id),
    CONSTRAINT [FK_Customer_CustomerRoleAccess_Mapping_CustomerRole] FOREIGN KEY ([CustomerRoleId]) REFERENCES [CustomerRole](Id),
    CONSTRAINT [FK_Customer_CustomerRoleAccess_Mapping_AccessibleCustomer] FOREIGN KEY ([AccessibleCustomerId]) REFERENCES [Customer](Id)
)
