﻿CREATE TABLE [dbo].[CustomerProductFavorites]
(
    [Id]			INT IDENTITY (1, 1) NOT NULL,
	[DateCreated]	DATETIME NOT NULL DEFAULT getutcdate(),
	[DateUpdated]	DATETIME, 
    [ProductId]		INT NOT NULL,
    [StoreId]		INT NOT NULL,
    [CustomerId]	INT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [CustomerProductFavorites_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [CustomerProductFavorites_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([Id]) ON DELETE CASCADE
)
