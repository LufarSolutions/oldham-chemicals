﻿CREATE FUNCTION [dbo].[ConvertUnitOfMeasureToAbbreviation] 
(
	@UnitOfMeasure nvarchar(25)
)
RETURNS nvarchar(2)
AS
BEGIN
	DECLARE @UOM nvarchar(2)

	SET @UOM = CASE 
		WHEN @UnitOfMeasure = 'BAG'THEN 'BG'
		WHEN @UnitOfMeasure = 'CASE' THEN 'CS'
		WHEN @UnitOfMeasure = 'CTN' THEN 'CT'
		WHEN @UnitOfMeasure = 'EACH' THEN 'EA'
		ELSE 'EA'
	END

	RETURN @UOM

END
GO
