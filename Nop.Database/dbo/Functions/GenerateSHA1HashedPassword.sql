﻿CREATE FUNCTION [dbo].[GenerateSHA1HashedPassword]
(
	@Password varchar(25),
	@Salt varchar(25)
)
RETURNS varchar(42)
AS
BEGIN
	
	-- Salt the password
	DECLARE @SaltedPassword varchar(50) = @Password + @Salt
	
	-- Convert to a byte array
	DECLARE @PasswordAsByteArray varbinary(max) = CONVERT(varbinary(max), @SaltedPassword)

	-- Hash the password
	DECLARE @HasedPassword varchar(42) = CONVERT(VARCHAR(42), HASHBYTES('SHA1', @PasswordAsByteArray), 2)

	RETURN @HasedPassword

END
