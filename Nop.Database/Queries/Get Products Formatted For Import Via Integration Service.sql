﻿SELECT
	LTRIM(RTRIM(ITEM_CODE)) + '|',
	LTRIM(RTRIM(VENDOR)) + '|',
	LTRIM(RTRIM(ITEM_DESC)) + '|',
	LTRIM(RTRIM(UOM)) + '|',
	'1' + '|',
	CAST(UNIT_PRICE_1 AS nvarchar(10)) + '|',
	CAST(UNIT_PRICE_2 AS nvarchar(10)) + '|',
	CAST(UNIT_PRICE_3 AS nvarchar(10)) + '|',
	CAST(UNIT_PRICE_4 AS nvarchar(10)) + '|',
	LTRIM(RTRIM(PROD_TYPE)) + '|',
	COALESCE(LTRIM(RTRIM(PROD_DESC)), '') + '|',
	COALESCE(LTRIM(RTRIM(MFGNUM)), '') + '|',
	COALESCE(LTRIM(RTRIM(DISCONTINUED)), '')
FROM Item
WHERE
	ITEM_DESC LIKE '%temprid fx%'
	OR ITEM_DESC LIKE '%gentrol complete%'
	OR ITEM_DESC LIKE '%zenprox xtend%'

