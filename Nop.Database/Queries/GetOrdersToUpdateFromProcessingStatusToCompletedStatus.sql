﻿SELECT
	DISTINCT [Order].*
FROM [Order]
WHERE
	ERPOrderId IS NOT NULL
	AND OrderStatusId = 20
	AND (SELECT COUNT(*) FROM ThoroughbredReplication.dbo.Invoice AS I 
			INNER JOIN ThoroughbredReplication.dbo.InvoiceItem II ON II.INV_NO = I.INV_NO
		WHERE 
			RTRIM(LTRIM(I.ORDER_NO)) = [Order].ERPOrderId
			AND II.SHIP_TODAY <> 0) > 0
	AND Deleted = 0
ORDER BY
	CreatedOnUtc DESC