﻿USE [ThoroughbredReplication]
GO

DECLARE @OrderId int = 14035
DECLARE @InvoiceDate datetime = getdate()
DECLARE @ShippingAmount decimal = 55

INSERT INTO [dbo].[Invoice]
           ([INV_NO]
           ,[CUST_NO]
           ,[INV_DATE]
           ,[ORDER_NO]
           ,[TERM_DESC]
           ,[DUE_DATE]
           ,[WAREHOUSE]
           ,[CUST_PO_NUM]
           ,[INVOICE_AMT]
           ,[INVOICE_BALANCE]
           ,[SALES_TAX_AMT]
           ,[FREIGHT_AMT]
           ,[SHIP_TO_CODE]
           ,[SHIP_VIA]
           ,[ORDER_DATE]
           ,[SHIP_DATE]
           ,[SHIP_TO_NAME]
           ,[SHIP_TO_ADDR1]
           ,[SHIP_TO_ADDR2]
           ,[SHIP_TO_CITY]
           ,[SHIP_TO_STATE]
           ,[SHIP_TO_ZIP]
           ,[TRACKING_NUMBER]
           ,[DATE_TIME_ADDED])
(SELECT
	[Order].Id,
	CustomerNumber,
	@InvoiceDate,
	ERPOrderId,
	'NET 30 DAYS',
	DATEADD(dd, 30, @InvoiceDate),
	'100',
	CAST([Order].CustomValuesXml as xml).value('(/DictionarySerializer/item/value)[1]', 'varchar(255)'),
	OrderTotal + @ShippingAmount + (OrderTotal * .0975),
	OrderTotal + @ShippingAmount + (OrderTotal * .0975),
	(OrderTotal * .0975),
	@ShippingAmount,
	COALESCE([Address].ShipToNumber, '0001'),
	'FEDEX',
	[Order].DateQueuedForERP,
	@InvoiceDate,
	[Address].FirstName + ' ' + [Address].LastName,
	[Address].Address1,
	[Address].Address2,
	[Address].City,
	StateProvince.Abbreviation,
	[Address].ZipPostalCode,
	'1234567890',
	getdate()
FROM oldhamchem_com.dbo.[Order]
	INNER JOIN oldhamchem_com.dbo.Customer ON [Order].CustomerId = Customer.Id
	INNER JOIN oldhamchem_com.dbo.[Address] ON [Address].Id = [Order].ShippingAddressId
	INNER JOIN oldhamchem_com.dbo.StateProvince ON [Address].StateProvinceId = StateProvince.Id
WHERE
	[Order].Id = @OrderId)

INSERT INTO InvoiceItem(
	[INV_NO]
    ,[LINE_NO]
    ,[ITEM_CODE]
    ,[LINE_TYPE]
    ,[LINE_MESSAGE]
    ,[TAXABLE]
    ,[SHIP_TODAY]
    ,[UNIT_PRICE]
    ,[EXT_PRICE]
    ,[DATE_TIME_ADDED])
(SELECT
	@OrderId,
	'00' + CAST(ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS nvarchar(2)),
	Sku,
	'I',
	CASE WHEN LEN(P.[Name]) > 23 THEN SUBSTRING(P.[Name], 0, 23) ELSE P.[Name] END,
	'Y',
	oldhamchem_com.dbo.[OrderItem].Quantity,
	oldhamchem_com.dbo.[OrderItem].UnitPriceExclTax,
	oldhamchem_com.dbo.[OrderItem].PriceExclTax,
	GETUTCDATE()
FROM oldhamchem_com.dbo.[OrderItem]
	INNER JOIN oldhamchem_com.dbo.Product P on P.Id = oldhamchem_com.dbo.[OrderItem].ProductId
WHERE
	oldhamchem_com.dbo.[OrderItem].OrderId = @OrderId)
	
GO