﻿SELECT
	DISTINCT [Order].*
FROM [Order]
	INNER JOIN Customer on Customer.Id = [Order].CustomerId
	INNER JOIN Customer_CustomerRole_Mapping AS CCRM ON CCRM.Customer_Id = Customer.Id
WHERE
	ERPOrderId IS NOT NULL
	AND OrderStatusId = 20
	AND StoreId = 1
	AND CCRM.CustomerRole_Id = 12
	AND (SELECT COUNT(*) FROM OrderNote WHERE OrderNote.OrderId = [Order].Id AND OrderNote.Note = 'Punchout Order Confirmation') = 0
	AND [Order].Deleted = 0	
ORDER BY
	[Order].CreatedOnUtc DESC