﻿
DECLARE @CustomerNumber nvarchar(20) = '7374121'

DECLARE @CustomerId int

SELECT @CustomerId = Id FROM Customer WHERE CustomerNumber = @CustomerNumber

DELETE FROM ContractPrice WHERE CustomerId = @CustomerId

INSERT INTO ContractPrice(
	DateCreated,
	DateUpdated,
	ProductId,
	StoreId,
	CustomerId,
	Quantity,
	Price)
(SELECT
	GETUTCDATE(),
	NULL,
	Product.Id,
	1,
	@CustomerId,
	1,
	ContractPrice.CONTRACT_PRICE
FROM [40.84.189.183].[ThoroughbredReplication].dbo.ContractPrice
	INNER JOIN Product ON Product.Sku = ContractPrice.ITEM_CODE
WHERE
	ContractPrice.CUSTOMER_NO = @CustomerNumber)