--UPDATE
--	Product
SELECT
	*
FROM Product
WHERE
	IsPrivate = 0
	AND (Name LIKE '% pvt%'
	OR Name LIKE '% pvlbl%'
	OR Name LIKE '% adams%'
	OR Name LIKE '% adp%'
	OR Name LIKE '% allstate%'
	OR Name LIKE '% all state%'
	OR Name LIKE '% amco%'
	OR Name LIKE '% american%'
	OR Name LIKE '% anderson%'
	OR Name LIKE '% arrow%'
	OR Name LIKE '% barnes%'
	OR Name LIKE '% bellmead%'
	OR Name LIKE '% benchmark%'
	OR Name LIKE '% black diamond%'
	OR Name LIKE '% blk dmd%'
	OR Name LIKE '% bugman%'
	OR Name LIKE '% bugmaster%'
	OR Name LIKE '% bug master%'
	OR Name LIKE '% bugstopper%'
	OR Name LIKE '% clark%'
	OR Name LIKE '% cook%'
	OR Name LIKE '% curry%'
	OR Name LIKE '% dayton%'
	OR Name LIKE '% diamond%'
	OR Name LIKE '% dugas%'
	OR Name LIKE '% e-town%'
	OR Name LIKE '% eagle%'
	OR Name LIKE '% esco%'
	OR Name LIKE '% four season%'
	OR Name LIKE '% fisher%'
	OR Name LIKE '% gregory%'
	OR Name LIKE '% gulf coast%'
	OR Name LIKE '% havard%'
	OR Name LIKE '% holper%'
	OR Name LIKE '% hooper%'
	OR Name LIKE '% hopper%'
	OR Name LIKE '% hulett%'
	OR Name LIKE '% home''s%'
	OR Name LIKE '% horne''s%'
	OR Name LIKE '% horn''s%'
	OR Name LIKE '% hydes%'
	OR Name LIKE '% J&J%'
	OR Name LIKE '% james%'
	OR Name LIKE '% jamison%'
	OR Name LIKE '% kirkland%'
	OR Name LIKE '% leitchfield%'
	OR Name LIKE '% mack%'
	OR Name LIKE '% mccarthy%'
	OR Name LIKE '% north ms%'
	OR Name LIKE '% okolona%'
	OR Name LIKE '% orkin%'
	OR Name LIKE '% pest inc%'
	OR Name LIKE '% pestinc%'
	OR Name LIKE '% pioneer%'
	OR Name LIKE '% red flag%'
	OR Name LIKE '% rid-a-pest%'
	OR Name LIKE '% rid-a-pst%'
	OR Name LIKE '% russell%'
	OR Name LIKE '% schendel%'
	OR Name LIKE '% scher%'
	OR Name LIKE '% scherzinger%'
	OR Name LIKE '% serfco%'
	OR Name LIKE '% servall%'
	OR Name LIKE '% sherrill%'
	OR Name LIKE '% state line%'
	OR Name LIKE '% steve%'
	OR Name LIKE '% superior%'
	OR Name LIKE '% team pest%'
	OR Name LIKE '% terminix%'
	OR Name LIKE '% thomas%'
	OR Name LIKE '% us pest%'
	OR Name LIKE '% wayne%')
