﻿SELECT
	DISTINCT [Order].*
FROM [Order]
WHERE
	ERPOrderId IS NOT NULL
	AND OrderStatusId = 10
	AND Deleted = 0
ORDER BY
	CreatedOnUtc DESC