using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Net;
using System.IO;
using System.Text;
using System.Web;

public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void SetLatitudeAndLongitude()
    {
        DataTable AddressDataTable = GetAddressesWithoutLatitudeAndLongitude();

        int AddressCount = 0;
        foreach (DataRow Address in AddressDataTable.Rows)
        {
            float Latitude = 0;
            float Longitude = 0;

            //Geocode Address via Google to get Lat/Long
            Nop.Database.GeocodeResponse GeocodeResponse = GeocodeAddress(Address);

            if (GeocodeResponse != null && GeocodeResponse.status == "OK")
            {
                Latitude = GeocodeResponse.results[0].geometry.location.lat;
                Longitude = GeocodeResponse.results[0].geometry.location.lng;
            }

            //Call UpdateAddress StoredProc
            try
            {
                int AddressId = int.Parse(Address["Id"].ToString());

                UpdateAddressLatitudeAndLongitude(AddressId, Latitude, Longitude);

                AddressCount++;
            }
            catch (Exception ex)
            {
                SqlContext.Pipe.Send(ex.Message);
            }

        }

    }

    private static DataTable GetAddressesWithoutLatitudeAndLongitude()
    {
        SqlConnection CurrentDbConnection = new SqlConnection("context connection=true");
        CurrentDbConnection.Open();

        SqlCommand GetAddressesCommand = new SqlCommand("GetCustomerAddressesWithoutLatitudeAndLongitude", CurrentDbConnection);
        GetAddressesCommand.CommandType = CommandType.StoredProcedure;

        SqlDataReader AddressDataReader = GetAddressesCommand.ExecuteReader();
        DataTable AddressDataTable = new DataTable();
        AddressDataTable.Load(AddressDataReader);

        AddressDataReader.Close();
        CurrentDbConnection.Close();

        return AddressDataTable;
    }

    private static void UpdateAddressLatitudeAndLongitude(int addressId, float latitude, float longitude)
    {
        SqlConnection CurrentDbConnection = new SqlConnection("context connection=true");
        CurrentDbConnection.Open();

        SqlCommand UpdateAddressCommand = new SqlCommand("UpdateCustomerAddressLatitudeAndLongitude", CurrentDbConnection);
        UpdateAddressCommand.CommandType = CommandType.StoredProcedure;

        SqlParameter AddressIdParam = new SqlParameter("@AddressId", addressId);
        SqlParameter LatitudeParam = new SqlParameter("@Latitude", latitude);
        SqlParameter LongitudeParam = new SqlParameter("@Longitude", longitude);

        UpdateAddressCommand.Parameters.Add(AddressIdParam);
        UpdateAddressCommand.Parameters.Add(LatitudeParam);
        UpdateAddressCommand.Parameters.Add(LongitudeParam);

        UpdateAddressCommand.ExecuteNonQuery();

        CurrentDbConnection.Close();
    }

    private static Nop.Database.GeocodeResponse GeocodeAddress(DataRow address)
    {
        try
        {
            string RequestUrl = "https://maps.googleapis.com/maps/api/geocode/json";
            string KeyParam = "AIzaSyAixiIDX0wQe55aJ7SOtKTnHonUS_pMq_I";
            string AddressParam = address["Address1"].ToString();

            if (address["Address2"] != DBNull.Value)
            {
                AddressParam += " " + address["Address2"].ToString();
            }

            AddressParam += " " + address["City"].ToString() + " " + address["Abbreviation"].ToString() + " " + address["ZipPostalCode"].ToString();

            AddressParam = AddressParam.Replace(" ", "+");

            SqlContext.Pipe.Send(AddressParam);

            string RequestParams = String.Format("?address={0}&key={1}", AddressParam, KeyParam);
            string RequestUrlWithParams = RequestUrl + RequestParams;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(RequestUrlWithParams);

            request.Method = "GET";
            request.ContentLength = 0;
            request.Credentials = CredentialCache.DefaultCredentials;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream receiveStream = response.GetResponseStream();
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            string JsonResponse = readStream.ReadToEnd();
            response.Close();
            readStream.Close();

            return Newtonsoft.Json.JsonConvert.DeserializeObject<Nop.Database.GeocodeResponse>(JsonResponse);
        }
        catch 
        {
            return null;
        }
    }
}
